﻿using Hangfire;
using Hangfire.Common;

using Hangfire.Redis;
using Microsoft.Owin;
using Owin;
using System;

[assembly: OwinStartupAttribute(typeof(ISE.WebhooksReceiver.Startup))]
namespace ISE.WebhooksReceiver
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
          
            GlobalConfiguration.Configuration.UseRedisStorage("localhost:6379", new RedisStorageOptions()
            {
                Db = 2,
                Prefix = "ise:"
            });



            app.UseHangfireDashboard();
            //app.UseHangfireServer(new BackgroundJobServerOptions
            //{
            //    WorkerCount = 5,
                

            //});


        }
    }
}
