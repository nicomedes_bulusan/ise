﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ISE.WebhooksReceiver
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
           

            config.MapHttpAttributeRoutes();

            // Load Web API controllers 
      
            config.InitializeReceiveGenericJsonWebHooks();
         

            config.EnsureInitialized();

            var webhookRoutes = config.Routes;
            ; // Set breakpoint here and inspect the variable above, you should see the custom incoming webhook route listened.

        }
    }
}
