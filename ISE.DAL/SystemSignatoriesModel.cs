﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public class SystemSignatoriesModel
    {
        public int ID { get; set; }
        public string BusinessLocationCode { get; set; }
        public int AgentID { get; set; }
        public string AgentName { get; set; }
        public string Designation { get; set; }
        public int ImmediateHeadID { get; set; }
        public string FormType { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateLastUpdated { get; set; }
        public string ImmediateHeadName { get; set; }
        public string ImmediateHeadDesignation { get; set; }
        public Boolean IsActive { get; set; }


        public SystemSignatoriesModel GetDefaultPreparer(string busLocCode, string formType)
        {
            using (var db = new isebookingEntities())
            {

                var result = (from a in db.systemsignatories
                              where a.BusinessLocationCode == busLocCode
                              && a.FormType == formType && a.IsActive == true
                              select new SystemSignatoriesModel
                              {
                                  AgentID = a.AgentID,
                                  AgentName = a.agent.FirstName + " " + a.agent.LastName,
                                  BusinessLocationCode = a.BusinessLocationCode,
                                  DateCreated = a.DateCreated,
                                  DateLastUpdated = a.DateLastUpdated,
                                  Designation = a.Designation,
                                  FormType = a.FormType,
                                  ID = a.ID,
                                  ImmediateHeadDesignation = a.ImmediateHeadDesignation,
                                  ImmediateHeadID = a.ImmediateHeadID,
                                  ImmediateHeadName = a.agent1.FirstName + " " + a.agent1.LastName,
                                  IsActive = a.IsActive

                              }).FirstOrDefault();

                return result;

            }
        }

        public SystemSignatoriesModel GetHighestSignatory(string busLocCode, string formType)
        {
            using (var db = new isebookingEntities())
            {

                var result = (from a in db.systemsignatories
                              where a.BusinessLocationCode == busLocCode
                              && a.FormType == formType && a.IsActive == true
                              select new SystemSignatoriesModel
                              {
                                  AgentID = a.AgentID,
                                  AgentName = a.agent.FirstName + " " + a.agent.LastName,
                                  BusinessLocationCode = a.BusinessLocationCode,
                                  DateCreated = a.DateCreated,
                                  DateLastUpdated = a.DateLastUpdated,
                                  Designation = a.Designation,
                                  FormType = a.FormType,
                                  ID = a.ID,
                                  ImmediateHeadDesignation = a.ImmediateHeadDesignation,
                                  ImmediateHeadID = a.ImmediateHeadID,
                                  ImmediateHeadName = a.agent1.FirstName + " " + a.agent1.LastName,
                                  IsActive = a.IsActive

                              }).FirstOrDefault();

                return result;

            }
        }

        public SystemSignatoriesModel Get(int agentID)
        {
            using (var db = new isebookingEntities())
            {

                var result = (from a in db.systemsignatories
                              where a.AgentID == agentID
                              select new SystemSignatoriesModel
                              {
                                  AgentID = a.AgentID,
                                  AgentName = a.agent.FirstName + " " + a.agent.LastName,
                                  BusinessLocationCode = a.BusinessLocationCode,
                                  DateCreated = a.DateCreated,
                                  DateLastUpdated = a.DateLastUpdated,
                                  Designation = a.Designation,
                                  FormType = a.FormType,
                                  ID = a.ID,
                                  ImmediateHeadDesignation = a.ImmediateHeadDesignation,
                                  ImmediateHeadID = a.ImmediateHeadID,
                                  ImmediateHeadName = a.agent1.FirstName + " " + a.agent1.LastName,
                                  IsActive = a.IsActive

                              }).FirstOrDefault();

                return result;

            }
        }

        public SystemSignatoriesModel CreateOrUpdate(SystemSignatoriesModel sig, AgentsModel agent)
        {
            isebookingEntities db = new isebookingEntities();
            Boolean isNewEntry = false;


            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var newSignatory = db.systemsignatories.Where(x => x.ID == sig.ID).FirstOrDefault();

                    if (newSignatory == null)
                    {
                        isNewEntry = true;
                        newSignatory = new systemsignatory();
                    }


                    newSignatory.AgentID = sig.AgentID;
                    newSignatory.BusinessLocationCode = sig.BusinessLocationCode;
                    newSignatory.DateCreated = DateTime.Now;
                    newSignatory.DateLastUpdated = DateTime.Now;
                    newSignatory.Designation = sig.Designation;
                    newSignatory.FormType = sig.FormType;
                    newSignatory.ImmediateHeadID = sig.ImmediateHeadID;
                    newSignatory.IsActive = sig.IsActive;
                    newSignatory.ImmediateHeadDesignation = sig.ImmediateHeadDesignation;

                    
                    if (isNewEntry)
                    {
                        db.systemsignatories.Add(newSignatory);
                    }

                    db.SaveChanges();
                    transaction.Commit();

                    sig.ID = newSignatory.ID;
                    return sig;

                }
                catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                {
                    transaction.Rollback();

                    Exception ex = dbU.GetBaseException();

                    throw new Exception(ex.Message);
                }
                catch (DbEntityValidationException dbEx)
                {
                    transaction.Rollback();
                    string errorMessages = null;
                    foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                    {
                        string entityName = validationResult.Entry.Entity.GetType().Name;
                        foreach (DbValidationError error in validationResult.ValidationErrors)
                        {
                            errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                        }
                    }

                    throw new Exception(errorMessages);
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }
        }

        public List<SystemSignatoriesModel> AllSignatories()
        {
            using (var db = new isebookingEntities())
            {


                var result = (from a in db.systemsignatories
                              select new SystemSignatoriesModel
                              {
                                  AgentID = a.AgentID,
                                  AgentName = a.agent.FirstName + " " + a.agent.LastName,
                                  BusinessLocationCode = a.BusinessLocationCode,
                                  DateCreated = a.DateCreated,
                                  DateLastUpdated = a.DateLastUpdated,
                                  Designation = a.Designation,
                                  FormType = a.FormType,
                                  ID = a.ID,
                                  ImmediateHeadDesignation = a.ImmediateHeadDesignation,
                                  ImmediateHeadID = a.ImmediateHeadID,
                                  ImmediateHeadName = a.agent1.FirstName + " " + a.agent1.LastName,
                                  IsActive = a.IsActive
                              }).ToList();

                return result;

            }
        }


        public SystemSignatoriesModel GetSOASignatories(int bookingID)
        {
            using (var db = new isebookingEntities())
            {

                var result = (from a in db.statementofaccounts
                              where a.BookingID == bookingID

                              select new SystemSignatoriesModel
                              {
                                  AgentID = 0,
                                  AgentName = a.IssuedBy,
                                  DateCreated = a.DateOfIssue,
                                  DateLastUpdated = DateTime.Now,
                                  Designation = a.IssuerDesignation,
                                  FormType = "SOA",
                                  ID = a.ID,
                                  ImmediateHeadDesignation = a.AuthorizerDesignation,
                                  ImmediateHeadID = 0,
                                  ImmediateHeadName = a.AuthorizedBy,
                                  IsActive = true

                              }).FirstOrDefault();

                return result;

            }
        }

        public SystemSignatoriesModel GetSVSignatories(int bookingID)
        {
            using (var db = new isebookingEntities())
            {

                var result = (from a in db.servicevouchers
                              where a.BookingID == bookingID

                              select new SystemSignatoriesModel
                              {
                                  AgentID = 0,
                                  AgentName = a.IssuedBy,
                                  DateCreated = a.DateOfIssue,
                                  DateLastUpdated = DateTime.Now,
                                  Designation = a.IssuerDesignation,
                                  FormType = "SOA",
                                  ID = a.ID,
                                  ImmediateHeadDesignation = a.AuthorizerDesignation,
                                  ImmediateHeadID = 0,
                                  ImmediateHeadName = a.AuthorizedBy,
                                  IsActive = true

                              }).FirstOrDefault();

                return result;

            }
        }

        public void AddSVSignatories(SystemSignatoriesModel sig, int bookingID)
        {

            using (var db = new isebookingEntities())
            {

                var newSVSignatory = new servicevoucher();
                newSVSignatory.AuthorizedBy = sig.ImmediateHeadName;
                newSVSignatory.AuthorizerDesignation = sig.ImmediateHeadDesignation;
                newSVSignatory.BookingID = bookingID;
                newSVSignatory.DateOfIssue = DateTime.Now;
                newSVSignatory.IssuedBy = sig.AgentName;
                newSVSignatory.IssuerDesignation = sig.Designation;
                db.servicevouchers.Add(newSVSignatory);
                db.SaveChanges();

            }
        }

        public void AddSOASignatories(SystemSignatoriesModel sig, int bookingID)
        {

            using (var db = new isebookingEntities())
            {

                var newSOASignatory = new statementofaccount();
                newSOASignatory.AuthorizedBy = sig.ImmediateHeadName;
                newSOASignatory.AuthorizerDesignation = sig.ImmediateHeadDesignation;
                newSOASignatory.BookingID = bookingID;
                newSOASignatory.DateOfIssue = DateTime.Now;
                newSOASignatory.IssuedBy = sig.AgentName;
                newSOASignatory.IssuerDesignation = sig.Designation;
                db.statementofaccounts.Add(newSOASignatory);
                db.SaveChanges();

            }
        }

    }
}
