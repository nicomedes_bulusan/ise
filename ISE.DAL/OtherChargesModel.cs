﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public class OtherChargesModel
    {
       public int ID { get; set; }
        public string Description { get; set; }
        public float Amount { get; set; }
        public int IsActive { get; set; }


        public List<OtherChargesModel> GetOtherCharges()
        {
            isebookingEntities db = new isebookingEntities();


            var result = (from a in db.othercharges
                          where a.IsActive==1
                          select new OtherChargesModel
                          {
                              ID = a.ID,
                              Amount = (float)a.Amount,
                              Description = a.Description,
                              IsActive = a.IsActive
                          }).ToList<OtherChargesModel>();

            return result;

        }



        public OtherChargesModel CreateOtherCharges(OtherChargesModel charge)
        {
            isebookingEntities db = new isebookingEntities();

            var newCharge = new othercharge();
            newCharge.Amount = charge.Amount;
            newCharge.DateCreated = DateTime.Now;
            newCharge.DateLastUpdated = DateTime.Now;
            newCharge.Description = charge.Description;
            newCharge.IsActive = 1;

            db.othercharges.Add(newCharge);
            db.SaveChanges();

            charge.ID = newCharge.ID;

            return charge;


        }


        public void DeleteCharge(OtherChargesModel charge)
        {
            isebookingEntities db = new isebookingEntities();
            var delCharge = db.othercharges.Where(x => x.ID == charge.ID).FirstOrDefault();

            if (delCharge == null) throw new Exception("Unknown charge!");

            //db.othercharges.Remove(delCharge);
            delCharge.IsActive = 0;
            db.SaveChanges();


        }


        public void UpdateCharge(OtherChargesModel charge)
        {
            isebookingEntities db = new isebookingEntities();

            var updateCharge = db.othercharges.Where(x => x.ID == charge.ID).FirstOrDefault();

            if (updateCharge == null) throw new Exception("Unknown charge!");


            updateCharge.Amount = charge.Amount;
            updateCharge.Description = charge.Description;
            updateCharge.IsActive = 1;
            db.SaveChanges();
        }
    }
}
