﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public class BookingSummaryModel: AgentsModel
    {
        public int Bookings { get; set; }
        public int Confirmed { get; set; }
        public int Finalized { get; set; }
        public int Cancelled { get; set; }
    }
}
