﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WooCommerce;

namespace ISE.DAL
{
    public class MobileIntegration
    {

        public void GetWalkIns()
        {
            using (var db = new isemobileEntities())
            {
                var data = db.books.Where(x => x.status == 1 && !x.cv_number.Contains("BOR") && !x.cv_number.Contains("MNL")).ToList();

                foreach (var tmp in data)
                {

                    tmp.status = 2;
                    tmp.no_adults = 1;
                    db.SaveChanges();

                    Hangfire.BackgroundJob.Enqueue(() => CreateWalkInBooking(tmp));
                }
            }
        }

        public void GetExcessBooking()
        {
            using (var db = new isemobileEntities())
            {
                
                string sql = " select * from book WHERE ((actual_a+0) >(book_a+0) OR (actual_k+0) >(book_k+0) OR" +
 " (actual_foc+0) >(book_foc+0) OR (actual_inf+0) >(book_inf+0) OR (actual_tg+0) >(book_tg+0) OR (actual_e+0) >(book_e+0) ) and status =1";

                var data = db.Database.SqlQuery<book>(sql).ToList<book>();

                foreach (var tmp in data)
                {

                    tmp.status = 2;
                    tmp.no_adults = 2;
                    var data2 = db.books.Where(x => x.ID == tmp.ID).FirstOrDefault();
                    data2.status = 2;
                    db.SaveChanges();

                    Hangfire.BackgroundJob.Enqueue(() => CreateExcessBooking(tmp));
                }

                List<string> pickups = data.GroupBy(x => x.location).ToList().Select(x=>x.Key).ToList<string>();
               // Hangfire.BackgroundJob.Enqueue(() => CreateManifestSchedule(pickups));
            }
        }
        public void CreateManifestForAddtionalBooking() {
           

             

                
                CreateManifestSchedule();
                //Hangfire.BackgroundJob.Enqueue(() => CreateManifestSchedule(pickups));
           
        }

        public void CreateManifestSchedule()
        {
            var db = new isebookingEntities();
            var location = db.destinations.Where(x => x.orderBy > 0).OrderBy(x => x.orderBy).ToList();
            DateTime _now = DateTime.Now.AddDays(2);
            
            foreach (var loc in location)
            {
                string pickupName = loc.Description.Split(' ')[0];
                ManifestoSchedules MS = new ManifestoSchedules();

                List<ManifestoDataset> MD = MS.GetArivingManifestoAdditional(_now, pickupName, 0);
                foreach (var details in MD) {
                    string mainLoc = details.Location.Split(' ')[0];
                    if (loc.orderBy == 1 && loc.Description.Contains(mainLoc))
                    {
                        MS.AddExcessManifestoDetails("", mainLoc, details.BookingID, Convert.ToInt16(details.Coordinator), 1);
                    }
                    if (loc.orderBy > 1) {
                        MS.AddExcessManifestoDetails("", pickupName, details.BookingID, Convert.ToInt16(details.Coordinator), 1);
                    }
                   //int order = db.destinations.Where(x => x.Description.Contains(mainLoc)).FirstOrDefault().ID;
                    //order = order + 1; // Next location
                   //string loc = db.destinations.Where(x => x.ID==order).FirstOrDefault().Description.Split(' ')[0];
                   
                    
                }

            }
        }

        public void CreateWalkInBooking(book refBook)
        {

            using (var db = new isemobileEntities())
            {
                Bookings bookRepo = new Bookings();
                Integration iWoo = new Integration();
                int adults = 0;
                int childrens = 0;
                int infants = 0;
                int escorts = 0;
                int tourguides = 0;
                int foc = 0;

                string hotel = refBook.resort_hotel ?? "";
                hotel = hotel.Trim().ToUpper();

                List<BookingOtherChargesIndicatorModel> otherChargesIndicators = new List<BookingOtherChargesIndicatorModel>();
                otherChargesIndicators.Add(new BookingOtherChargesIndicatorModel { Amount = 0, ChargeDescription = "", ChargingID = 1, IsChecked = true, BookingID = 0 });
                otherChargesIndicators.Add(new BookingOtherChargesIndicatorModel { Amount = 0, ChargeDescription = "", ChargingID = 3, IsChecked = true, BookingID = 0 });

                List<string> ocs = new List<string>();

                BookingHeaderModel newBook = new BookingHeaderModel();
                List<BookingDetailsModel> details = new List<BookingDetailsModel>();
                newBook.Class = ClassRate.REGULAR;
                newBook.Class2 = (int)ClassRate.REGULAR;
                newBook.ContactNumber = " ";
                newBook.ContactPerson = refBook.account_name;




                newBook.FlightDate1 = refBook.flight_date;

                if (refBook.type == 1)
                {
                    newBook.FlightNo1 = refBook.actual_arrival;

                }
                else
                {

                    newBook.FlightNo2 = refBook.actual_arrival;
                }


                newBook.PickUpArrival = iWoo.GetDestinations(refBook.location.Trim());
                newBook.DropOffArrival = iWoo.GetDestinations(refBook.resort_hotel.Trim());
                newBook.DropOffLocation1 = iWoo.GetHotel(hotel);


                newBook.GroupName = refBook.account_name;

                if (int.TryParse(refBook.actual_inf, out infants))
                {
                    newBook.Infants = infants;
                }
              
             

                if (int.TryParse(refBook.actual_a, out adults))
                {
                    newBook.Adults = adults;
                }
                if (int.TryParse(refBook.actual_e, out escorts))
                {
                    newBook.Escorts = escorts;
                }
                if (int.TryParse(refBook.actual_foc, out foc))
                {
                    newBook.FOC = foc;
                }
                if (int.TryParse(refBook.actual_tg, out tourguides))
                {
                    newBook.TourGuides = tourguides;
                }


                if(refBook.no_adults == 1) { 
                    newBook.Infants = parseInt(refBook.actual_inf);
                    newBook.Adults = parseInt(refBook.actual_a);
                    newBook.Escorts = parseInt(refBook.actual_e);
                    newBook.FOC = parseInt(refBook.actual_foc);
                    newBook.TourGuides = parseInt(refBook.actual_tg);
                    adults = newBook.Adults;
                }

                if (refBook.no_adults == 2)
                {
                    newBook.Infants = parseInt(refBook.actual_inf);
                    newBook.Adults = parseInt(refBook.actual_a);
                    newBook.Escorts = parseInt(refBook.actual_e);
                    newBook.FOC = parseInt(refBook.actual_foc);
                    newBook.TourGuides = parseInt(refBook.actual_tg);

                    int book_a = parseInt(refBook.book_a);
                    int book_inf = parseInt(refBook.book_inf);
                    int book_e = parseInt(refBook.book_e);
                    int book_foc = parseInt(refBook.book_foc);
                    int book_tg = parseInt(refBook.book_tg);

                    Boolean chkStat = true;
                    if (newBook.Infants > book_inf)
                    {
                        newBook.Infants = newBook.Infants - book_inf;
                        chkStat = false;
                    }
                    if (newBook.Adults > book_a)
                    {
                        newBook.Adults = newBook.Adults - book_a;
                        chkStat = false;
                    }
                    if (newBook.Escorts > book_e)
                    {
                        newBook.Escorts = newBook.Escorts - book_e;
                        chkStat = false;
                    }
                    if (newBook.FOC > book_foc)
                    {
                        newBook.FOC = newBook.FOC - book_foc;
                        chkStat = false;
                    }
                    if (newBook.TourGuides > book_tg)
                    {
                        newBook.TourGuides = newBook.TourGuides - book_foc;
                        chkStat = false;
                    }
                    if (chkStat)
                    {
                        return;
                    }
                    adults = newBook.Adults;
                }
                newBook.ChildrensBelowSix = 0;
                newBook.Senior = 0;


                newBook.Remarks = "WALK-IN BOOKING: " + Environment.NewLine + (refBook.remarks ?? "");
                newBook.Status = BookingStatus.SAVED;

                newBook.DateCreated = DateTime.Now.ToString();
                newBook.PaymentTypeID = PaymentType.BUY_AND_BOOK;

                newBook.CreatedBy = refBook.user.Value;
                newBook.Routing = 1;

                int people = adults  +childrens;

                double amount = 0;

                Double.TryParse(refBook.amount, out amount);

                BookingDetailsModel detail = new BookingDetailsModel();
                detail.Amount = amount/people;
                detail.Boat = 0;
                detail.BookingID = 0;
                detail.Description = String.Format("TRANSFER FROM {0} TO {1} ", refBook.location, refBook.resort_hotel);
                detail.Quantity = people;
                detail.SourceID = 1;

                details.Add(detail);


                //OTHER CHARGES SELECTED
                if (adults > 0)
                {
                    ocs.Add("withTFInAdult");
                    ocs.Add("withEnvFeeAdult");

                    if (newBook.Routing == 2)
                    {
                        ocs.Add("withTFOutAdult");
                    }
                }

                //OTHER CHARGES SELECTED
                if (childrens > 0)
                {
                    ocs.Add("withTFInChildren");
                    ocs.Add("withEnvFeeChildren");

                    if (newBook.Routing == 2)
                    {
                        ocs.Add("withTFOutChildren");
                    }

                }

                newBook.Total = (float)details.Sum(x => (x.Amount + x.Boat));
                newBook.Adults = adults;
                newBook.Childrens = childrens;
                newBook.Infants = infants;
                newBook.ChildrensBelowSix = 0;

                bookRepo.Header = newBook;
                bookRepo.Details = details;
                bookRepo.OtherCharges = otherChargesIndicators;
                bookRepo.OCs = ocs.ToArray();

                AgentsModel agent = new AgentsModel();
                agent = agent.FindAgentByID(refBook.user.Value);
                if (agent == null)
                {
                     agent = new AgentsModel();
                    agent = agent.FindAgentByID(1);
                }
               
                
                var book = bookRepo.CreateBooking(bookRepo, agent);
                List<ManifestoModel> mfList = new List<ManifestoModel>();
                ManifestoModel mf = new ManifestoModel();
              
                int mType = 1;
                if (book.Header.PickUpArrival == null)
                {
                    mType = 2;
                }

                mf.BookingID = book.Header.ID;
                mf.DateCreated = DateTime.Now;
                mf.DateLastUpdated = DateTime.Now;
                mf.Pax = book.Header.Adults + book.Header.Infants + book.Header.Childrens;
                mf.ResortHotel = 1;
                mf.Date = Convert.ToDateTime(book.Header.DateCreated);
              
                mf.Remarks = book.Header.Remarks;
                mf.Pickup = (mType == 1) ? book.Header.PickUpArrival.Value : book.Header.PickUpDeparture.Value;
                mf.DropOff = (mType == 1) ? book.Header.DropOffArrival.Value : book.Header.DropOffDeparture.Value;
                mf.Adults = book.Header.Adults;
                mf.Kids = book.Header.Childrens;
                mf.Infants = book.Header.Infants;
                mf.Type = 1;
                mf.FlightNo = (mType == 1) ? book.Header.FlightNo1 : book.Header.FlightNo2;
                mf.TourGuides = book.Header.TourGuides;
                mf.Escorts = book.Header.Escorts;
                mf.FOC = book.Header.FOC;
                mf.Guests = "Walk-In\n";
                mfList.Add(mf);
                bookRepo.Manifesto = mfList;
                bookRepo.SaveManifesto(bookRepo, agent);
                bookRepo.Finalized(book.Header.ID, agent);



                /*
                string parentID = Hangfire.BackgroundJob.Enqueue(() => bookRepo.Confirm(book.Header.ID, agent));

             

                //ELEVATE ROLE
                agent.Type = 2;

               var child = Hangfire.BackgroundJob.ContinueWith(parentID, () => bookRepo.Finalized(book.Header.ID, agent));

                ManifestoSchedules manifest = new ManifestoSchedules();

                

                Hangfire.BackgroundJob.ContinueWith(child, () => manifest.AddManifestoDetails(refBook.unit_atd,refBook.location,book.Header.ID,refBook.user.Value,refBook.type));
                */
            }

        }
        public Int32 parseInt(string str) {

            string input = String.Empty;
            int result = 0;
            try
            {
                 result = Convert.ToInt32(Math.Round(Convert.ToDouble(str)));
               
            }
            catch (FormatException)
            {
                result = 0;
            }
            return result;
        }
        public void CreateExcessBooking(book refBook)
        {

            using (var db = new isemobileEntities())
            {
                Bookings bookRepo = new Bookings();
                Integration iWoo = new Integration();
                int adults = 0;
                int childrens = 0;
                int infants = 0;
                int escorts = 0;
                int tourguides = 0;
                int foc = 0;

                string hotel = refBook.resort_hotel ?? "";
                hotel = hotel.Trim().ToUpper();

                List<BookingOtherChargesIndicatorModel> otherChargesIndicators = new List<BookingOtherChargesIndicatorModel>();
                otherChargesIndicators.Add(new BookingOtherChargesIndicatorModel { Amount = 0, ChargeDescription = "", ChargingID = 1, IsChecked = true, BookingID = 0 });
                otherChargesIndicators.Add(new BookingOtherChargesIndicatorModel { Amount = 0, ChargeDescription = "", ChargingID = 3, IsChecked = true, BookingID = 0 });

                List<string> ocs = new List<string>();

                BookingHeaderModel newBook = new BookingHeaderModel();
                List<BookingDetailsModel> details = new List<BookingDetailsModel>();
                var db2 = new isebookingEntities();
                var refBooking = db2.bookings.Where(x => x.CVNumber == refBook.cv_number).FirstOrDefault();
                newBook.Class = ClassRate.REGULAR;
                newBook.Class2 = (int)ClassRate.REGULAR;
                newBook.ContactNumber = " ";
                newBook.ContactPerson = refBook.account_name;




                newBook.FlightDate1 = refBook.flight_date;

                if (refBook.type == 1)
                {
                    newBook.FlightNo1 = refBook.actual_arrival;

                }
                else
                {

                    newBook.FlightNo2 = refBook.actual_arrival;
                }


                //newBook.PickUpArrival = iWoo.GetDestinations(refBook.location.Trim());
                //newBook.DropOffArrival = iWoo.GetDestinations(refBook.resort_hotel.Trim());
                //newBook.DropOffLocation1 = iWoo.GetHotel(hotel);

                newBook.PickUpArrival = refBooking.PickUpArrival;
                newBook.DropOffArrival = refBooking.DropOffArrival;
                newBook.DropOffLocation = refBooking.DropOffLocation;
                newBook.FlightDate1 = refBooking.FlightDate1;
                newBook.FlightDate2 = refBooking.FlightDate2;
                newBook.ResortHotel1 = refBooking.resortsandhotel == null ? "" : refBooking.resortsandhotel.Name;
                newBook.ResortHotel2 = refBooking.resortsandhotel1 == null ? "" : refBooking.resortsandhotel1.Name;

                newBook.GroupName = refBook.account_name;


                newBook.Infants = parseInt(refBook.actual_inf);
                newBook.Childrens = parseInt(refBook.actual_k);
                newBook.Adults = parseInt(refBook.actual_a);
                newBook.Escorts = parseInt(refBook.actual_e);
                newBook.FOC = parseInt(refBook.actual_foc);
                newBook.TourGuides = parseInt(refBook.actual_tg);

                int book_a = parseInt(refBook.book_a);
                int book_k = parseInt(refBook.book_k);
                int book_inf = parseInt(refBook.book_inf);
                int book_e = parseInt(refBook.book_e);
                int book_foc = parseInt(refBook.book_foc);
                int book_tg = parseInt(refBook.book_tg);

                Boolean chkStat = true;
                if (newBook.Infants > book_inf)
                {
                    newBook.Infants = newBook.Infants - book_inf;
                    chkStat = false;
                }
                if (newBook.Childrens > book_k)
                {
                    newBook.Childrens = newBook.Childrens - book_k;
                    childrens = newBook.Childrens;
                    chkStat = false;
                }
                if (newBook.Adults > book_a)
                {
                    newBook.Adults = newBook.Adults - book_a;
                    chkStat = false;
                }
                if (newBook.Escorts > book_e)
                {
                    newBook.Escorts = newBook.Escorts - book_e;
                    chkStat = false;
                }
                if (newBook.FOC > book_foc)
                {
                    newBook.FOC = newBook.FOC - book_foc;
                    chkStat = false;
                }
                if (newBook.TourGuides > book_tg)
                {
                    newBook.TourGuides = newBook.TourGuides - book_foc;
                    chkStat = false;
                }
                if (chkStat)
                {
                    return;
                }
                 adults = newBook.Adults;
                
                /*
                if (int.TryParse(refBook.actual_inf, out infants))
                {
                    int bookInfants = 0;
                    int.TryParse(refBook.book_inf, out bookInfants);
                    infants -= bookInfants;
                    newBook.Infants = infants;
                }

                if (int.TryParse(refBook.actual_k, out childrens))
                {
                    int bookChildren = 0;
                    int.TryParse(refBook.book_k, out bookChildren);
                    childrens -= bookChildren;
                    newBook.Childrens = childrens;
                }

                if (int.TryParse(refBook.actual_a, out adults))
                {
                    int bookAdults = 0;
                    int.TryParse(refBook.book_a, out bookAdults);
                    adults -= bookAdults;
                    newBook.Adults = adults;
                }
                if (int.TryParse(refBook.actual_e, out escorts))
                {
                    int bookEscorts = 0;
                    int.TryParse(refBook.book_e, out bookEscorts);
                    escorts -= bookEscorts;
                    newBook.Escorts = escorts;
                }
                if (int.TryParse(refBook.actual_foc, out foc))
                {
                    int bookFOC = 0;
                    int.TryParse(refBook.book_foc, out bookFOC);
                    foc -= bookFOC;
                    newBook.FOC = foc;
                }
                if (int.TryParse(refBook.actual_tg, out tourguides))
                {
                    int bookTg = 0;
                    int.TryParse(refBook.book_tg, out bookTg);
                    tourguides -= bookTg;
                    newBook.TourGuides = tourguides;
                }
                */
                newBook.ChildrensBelowSix = 0;
                newBook.Senior = 0;


                newBook.Remarks = "EXECESS OF BOOKING : " + refBook.cv_number + Environment.NewLine + (refBook.remarks ?? "");
                newBook.Status = BookingStatus.FINALIZED;

                newBook.DateCreated = DateTime.Now.ToString();
                newBook.PaymentTypeID = PaymentType.BUY_AND_BOOK;

                newBook.CreatedBy = 0;
                newBook.Routing = 1;

                int people = adults + childrens;

                double amount = 0;

                Double.TryParse(refBook.sales_handle, out amount);

                BookingDetailsModel detail = new BookingDetailsModel();
                detail.Amount = amount;
                detail.Boat = 0;
                detail.BookingID = 0;
                detail.Description = String.Format("TRANSFER FROM {0} TO {1} ", refBook.location, refBook.resort_hotel);
                detail.Quantity = people;
                detail.SourceID = 1;

                details.Add(detail);


                //OTHER CHARGES SELECTED
                if (adults > 0)
                {
                    ocs.Add("withTFInAdult");
                    ocs.Add("withEnvFeeAdult");

                    if (newBook.Routing == 2)
                    {
                        ocs.Add("withTFOutAdult");
                    }
                }

                //OTHER CHARGES SELECTED
                if (childrens > 0)
                {
                    ocs.Add("withTFInChildren");
                    ocs.Add("withEnvFeeChildren");

                    if (newBook.Routing == 2)
                    {
                        ocs.Add("withTFOutChildren");
                    }

                }

                newBook.Total = (float)details.Sum(x => (x.Amount + x.Boat));
                newBook.Adults = adults;
                newBook.Childrens = childrens;
                newBook.Infants = infants;
                newBook.ChildrensBelowSix = 0;

                bookRepo.Header = newBook;
                bookRepo.Details = details;
                bookRepo.OtherCharges = otherChargesIndicators;
                bookRepo.OCs = ocs.ToArray();

                AgentsModel agent = new AgentsModel();
                agent = agent.FindAgentByCompany(refBook.agency); //agent.FindAgentByID(refBook.user.Value);
                if (agent == null)
                {
                    agent = new AgentsModel();
                    agent = agent.FindAgentByID(1);
                    agent.TravelAgencyID = agent.FindAgentByCompany(refBook.agency).TravelAgencyID;
                }
               
                var book = bookRepo.CreateBooking(bookRepo, agent);
                List<ManifestoModel> mfList = new List<ManifestoModel>();
                ManifestoModel mf = new ManifestoModel();

                int mType = 1;
                if (book.Header.PickUpArrival == null)
                {
                    mType = 2;
                }

                mf.BookingID = book.Header.ID;
                mf.DateCreated = DateTime.Now;
                mf.DateLastUpdated = DateTime.Now;
                mf.Pax = book.Header.Adults + book.Header.Infants + book.Header.Childrens;
                mf.ResortHotel = 1;
                mf.Date = Convert.ToDateTime(book.Header.DateCreated);

                mf.Remarks = book.Header.Remarks;
                mf.Pickup = (mType == 1) ? book.Header.PickUpArrival.Value : book.Header.PickUpDeparture.Value;
                mf.DropOff = (mType == 1) ? book.Header.DropOffArrival.Value : book.Header.DropOffDeparture.Value;
                mf.Adults = book.Header.Adults;
                mf.Kids = book.Header.Childrens;
                mf.Infants = book.Header.Infants;
                mf.Type = 1;
                mf.FlightNo = (mType == 1) ? book.Header.FlightNo1 : book.Header.FlightNo2;
                mf.TourGuides = book.Header.TourGuides;
                mf.Escorts = book.Header.Escorts;
                mf.FOC = book.Header.FOC;
                mf.Guests = "Walk-In\n";
                mfList.Add(mf);
                bookRepo.Manifesto = mfList;
                agent.Type = 2;
                bookRepo.SaveManifesto(bookRepo, agent);
                bookRepo.Finalized(book.Header.ID, agent);
                /*
                ManifestoSchedules MS = new ManifestoSchedules();

                MS.AddManifestoDetails()
                
                int _pickup = (mType == 1) ? book.Header.PickUpArrival.Value : book.Header.PickUpDeparture.Value;
                string nextPickup = "";
                if (mType == 1)
                {
                    nextPickup = MS.getDestination(_pickup + 1);
                    booking bk = MS.getBookingInfoByCV(refBook.cv_number);
                    var id = MS.GetArivingManifesto(mf.DateCreated, nextPickup, 1).Where(x=>x.BookingID == bk.ID);


                }
                
                string parentID = Hangfire.BackgroundJob.Enqueue(() => bookRepo.Confirm(book.Header.ID, agent));

             

                //ELEVATE ROLE
                agent.Type = 2;

               var child = Hangfire.BackgroundJob.ContinueWith(parentID, () => bookRepo.Finalized(book.Header.ID, agent));

                ManifestoSchedules manifest = new ManifestoSchedules();

                

                Hangfire.BackgroundJob.ContinueWith(child, () => manifest.AddManifestoDetails(refBook.unit_atd,refBook.location,book.Header.ID,refBook.user.Value,refBook.type));
                */
            }


        }

    }
}
