﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
   public class BookingLogsModel
    {
       public int ID { get; set; }
       public int BookingID { get; set; }
       public AgentsModel Agent { get; set; }
       public BookingStatus Status { get; set; }
       public DateTime DateCreated { get; set; }
    }
}
