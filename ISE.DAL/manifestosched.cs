//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISE.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class manifestosched
    {
        public manifestosched()
        {
            this.manifestoscheddetails = new HashSet<manifestoscheddetail>();
        }
    
        public int ID { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateLastUpadated { get; set; }
        public int CoordinatorID { get; set; }
        public int ManisfestoType { get; set; }
        public System.DateTime ManifestoDate { get; set; }
        public int VehicleID { get; set; }
        public string Driver { get; set; }
        public int Status { get; set; }
        public int CreatedBy { get; set; }
        public string Location { get; set; }
        public int Touchpoint { get; set; }
    
        public virtual agent agent { get; set; }
        public virtual fleet fleet { get; set; }
        public virtual ICollection<manifestoscheddetail> manifestoscheddetails { get; set; }
    }
}
