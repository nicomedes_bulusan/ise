﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public class Mailer
    {

        public  IRestResponse SendSimpleMessage(string recipients,string message,string subject)
        {

            string appKey = ConfigurationManager.AppSettings["MGAPIKey"];
            string appDomain = ConfigurationManager.AppSettings["MGDomain"];
            string appAccount = ConfigurationManager.AppSettings["MGFrom"];

            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                    new HttpBasicAuthenticator("api",appKey);
            RestRequest request = new RestRequest();
            request.AddParameter("domain",   appDomain, ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", appAccount);
            request.AddParameter("to", recipients);    
            request.AddParameter("subject", subject);
            request.AddParameter("html", message);
            request.Method = Method.POST;
            return client.Execute(request);
        }

    }
}
