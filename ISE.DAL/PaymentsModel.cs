﻿using AutoMapper;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public enum PaymentReferenceType
    {
        NONE = 0,
        OR = 1,
        PR = 2,
        AR = 3
    }

    public enum PaymentStatus
    {
        OPEN = 0,
        VERIFIED = 1,
        CANCELLED = 2
    }

    public class PaymentsModel
    {
        private IMapper mapper;

        public PaymentsModel()
        {
            var newConfig = new MapperConfiguration(cfg =>
              {
                  cfg.CreateMap<agent, AgentsModel>();
                  cfg.CreateMap<travelagency, TravelAgencyModel>();
                  cfg.CreateMap<modeofpayment, ModeOfPaymentsModel>();

                  cfg.CreateMap<payment, PaymentHeaderModel>()
                       .MaxDepth(1)
                      .ForMember(dest => dest.PreparedByAgent, conf => conf.MapFrom(o => o.agent.FirstName + " " + o.agent.LastName))
                      .ForMember(dest => dest.VerifiedByName, conf => conf.MapFrom(o => o.agent1.FirstName + " " + o.agent1.LastName))
                      .ForMember(dest => dest.TravelAgency, conf => conf.MapFrom(o => o.travelagency))
                      .ForMember(dest => dest.Status, conf => conf.MapFrom(o => o.Status))
                      .ForMember(dest => dest.ModeOfPayment, conf => conf.MapFrom(o => o.modeofpayment));


                  cfg.CreateMap<journalentry, PaymentHeaderAndDetailsModel>()
                     .MaxDepth(1)
                     .ForMember(dest => dest.ID, conf => conf.MapFrom(o => o.PaymentID))
                      .ForMember(dest => dest.PaymentNo, conf => conf.MapFrom(o => o.payment.PaymentNo))
                    .ForMember(dest => dest.PaymentDate, conf => conf.MapFrom(o => o.payment.PaymentDate))
                    .ForMember(dest => dest.PreparedByAgent, conf => conf.MapFrom(o => o.payment.agent.FirstName + " " + o.payment.agent.LastName))
                    .ForMember(dest => dest.VerifiedByName, conf => conf.MapFrom(o => o.payment.agent1.FirstName + " " + o.payment.agent1.LastName))
                    .ForMember(dest => dest.TravelAgency, conf => conf.MapFrom(o => o.payment.travelagency))
                    .ForMember(dest => dest.Status, conf => conf.MapFrom(o => o.payment.Status))
                    .ForMember(dest => dest.ModeOfPayment, conf => conf.MapFrom(o => o.payment.modeofpayment));




                  cfg.CreateMap<booking, BookingHeaderModel>()
                      .ForMember(dest => dest.Agency, conf => conf.MapFrom(o => o.agent))
                      .ForMember(dest => dest.Payments, opt => opt.ResolveUsing(src => src.journalentries.Where(x => x.payment.Status != 2).Sum(x => x.Payments))
                  );







              });

            mapper = newConfig.CreateMapper();
        }



        public PaymentHeaderModel Payment { get; set; }
        public List<JournalEntriesModel> DebitEntries { get; set; }
        public List<JournalEntriesModel> CreditEntries { get; set; }



        public PaymentsModel GetPayment(AgentsModel agent, int paymentID)
        {
            using (isebookingEntities db = new isebookingEntities())
            {


                var paymentHeader = (from a in db.payments
                                     where a.ID == paymentID
                                     select new PaymentHeaderModel
                                     {
                                         BusinessLocationCode = a.BusinessLocationCode,
                                         CheckDate = a.CheckDate,
                                         CheckNo = a.CheckNo,
                                         ID = a.ID,
                                         ModeOfPaymentID = a.ModeOfPaymentID,
                                         PaymentDate = a.PaymentDate,
                                         PaymentNo = a.PaymentNo,
                                         PreparedBy = a.PreparedBy,
                                         //  ReceiptNo = a.ReceiptNo,
                                         Remarks = a.Remarks,
                                         TravelAgencyID = a.TravelAgencyID,
                                         TravelAgency = new TravelAgencyModel
                                         {
                                             Name = a.travelagency.Name
                                         },
                                         Status = (PaymentStatus)a.Status,
                                         VerifiedBy = a.VerifiedBy
                                     }).FirstOrDefault();


                var debitEntries = (from a in db.journalentries
                                    where a.PaymentID == paymentID && a.Type == (int)SalesType.DEBIT
                                    select new JournalEntriesModel
                                    {
                                        AccountingCode = a.AccountingCode,
                                        AccountName = a.AccountName,
                                        Amount = a.Amount,
                                        BookingID = a.BookingID,
                                        CVNumber = a.CVNumber,
                                        ID = a.ID,
                                        PaymentID = a.PaymentID,
                                        Remarks = a.Remarks,
                                        Type = (SalesType)a.Type,
                                        ReceiptNo = a.ReceiptNo,
                                        ReferenceType = (PaymentReferenceType)a.ReferenceType

                                    }).ToList<JournalEntriesModel>();


                var creditEntries = (from a in db.journalentries
                                     where a.PaymentID == paymentID && a.Type == (int)SalesType.CREDIT
                                     select new JournalEntriesModel
                                     {
                                         AccountingCode = a.AccountingCode,
                                         AccountName = a.AccountName,
                                         Amount = a.Amount,
                                         BookingID = a.BookingID,
                                         CVNumber = a.CVNumber,
                                         ID = a.ID,
                                         PaymentID = a.PaymentID,
                                         Remarks = a.Remarks,
                                         Type = (SalesType)a.Type

                                     }).ToList<JournalEntriesModel>();

                PaymentsModel paymentRef = new PaymentsModel();
                paymentRef.Payment = paymentHeader;
                paymentRef.DebitEntries = debitEntries;
                paymentRef.CreditEntries = creditEntries;

                return paymentRef;
            }

        }

        public void SubmitPayment(AgentsModel agent, PaymentsModel payment)
        {

            using (isebookingEntities db = new isebookingEntities())
            {

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {



                        if (agent.Type != 3) throw new Exception("ERROR! You are not authorized!");

                        if (payment.Payment == null) throw new Exception("Invalid!");

                        if (payment.Payment.PaymentDate > DateTime.Now) throw new Exception("Invalid payment date!");

                        var cvGenerator = db.paymentcounters.OrderByDescending(x => x.Series).Select(x => x.Series + 1).FirstOrDefault();

                        if (cvGenerator == 0)
                        {

                            cvGenerator = 1;
                        }



                        if (payment.Payment.PaymentDate.Year <= 2015)
                        {
                            throw new Exception("Invalid payment date!");
                        }

                        var newPayment = new payment();
                        newPayment.BusinessLocationCode = payment.Payment.BusinessLocationCode;
                        newPayment.CheckDate = payment.Payment.CheckDate;
                        newPayment.CheckNo = payment.Payment.CheckNo;
                        newPayment.ModeOfPaymentID = payment.Payment.ModeOfPaymentID;
                        newPayment.PaymentDate = payment.Payment.PaymentDate;
                        newPayment.PaymentNo = "";
                        newPayment.PreparedBy = agent.ID;
                        newPayment.Status = (int)PaymentStatus.OPEN;

                        //  newPayment.ReceiptNo = payment.Payment.ReceiptNo;
                        newPayment.Remarks = payment.Payment.Remarks;
                        newPayment.TravelAgencyID = payment.Payment.TravelAgencyID;
                        newPayment.VerifiedBy = null;
                        newPayment.PaymentNo = DateTime.Now.Year.ToString().Substring(2, 2) + "-" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "-" + cvGenerator.ToString().PadLeft(10, '0');

                        db.payments.Add(newPayment);
                        db.SaveChanges();




                        if (payment.CreditEntries == null) throw new Exception("Please add Credit entries!");

                        if (payment.DebitEntries == null) throw new Exception("Please add Debit entries!");


                        double creditTotal = Math.Round(payment.CreditEntries.Sum(x => x.Amount),2);
                        double debitTotal = Math.Round(payment.DebitEntries.Sum(x => x.Amount),2);

                        if (creditTotal != debitTotal)
                        {
                            throw new Exception("Ooopps! Debit and credit entries should be equal!");
                        }

                        foreach (var credit in (payment.CreditEntries.Where(x => x.AccountingCode == "1011000").ToList() ?? new List<JournalEntriesModel>()))
                        {

                            if (string.IsNullOrEmpty(credit.CVNumber)) throw new Exception("ERROR! CV number is invalid!");
                            var book = db.bookings.Where(x => x.ID == credit.BookingID).FirstOrDefault();
                            // if (book.IsPaid) throw new Exception("ERROR! Booking with CV number " + credit.CVNumber + " is already paid");

                            double dec = 0;
                            var totalPaymentsByBooking = GetTotalPayments(db, book.ID);

                            var newCredit = new journalentry();
                            newCredit.AccountingCode = credit.AccountingCode;
                            newCredit.AccountName = credit.AccountName;
                            newCredit.Amount = credit.Amount;
                            newCredit.BookingID = credit.BookingID;
                            newCredit.CVNumber = credit.CVNumber;
                            newCredit.PaymentID = newPayment.ID;
                            newCredit.Remarks = credit.Remarks ?? "";
                            newCredit.Type = (int)SalesType.CREDIT;

                            if (!(book.CVNumber == credit.CVNumber && credit.BookingID == book.ID))
                            {
                                throw new Exception(String.Format("Record does not match! BookingID: {0} BookingCVNumber: {1} ", book.ID.ToString(), book.CVNumber.ToString()));
                            }

                            dec = (double)(totalPaymentsByBooking + credit.Amount);
                            double excess = dec - book.Total ?? 0;
                            string paymentsFormatted = excess.ToString("C", new CultureInfo("en-PH"));
                            if (dec > book.Total) throw new Exception(String.Format("ERROR! Overpayment amounting to {0} on booking with CV# {1}", paymentsFormatted, book.CVNumber));

                            if (book.PaymentTypeID == (int)PaymentType.FOR_BILLING)
                            {
                                var billingPeriodID = book.accountreceivables.Where(x => x.BillingPeriodID != null).FirstOrDefault();
                                var arRef = billingPeriodID.booking.accountreceivables.Where(x => x.BookingID == book.ID && x.BillingPeriodID != null).FirstOrDefault();

                                if (billingPeriodID == null) throw new Exception("Unable to find billing period!");

                            }

                            book.IsPaid = dec == 0;
                            newCredit.Payments = credit.Amount;


                            db.journalentries.Add(newCredit);
                            db.SaveChanges();



                        }

                        //ADVANCES ENTRY
                        foreach (var credit in (payment.CreditEntries.Where(x => x.AccountingCode != "1011000").ToList() ?? new List<JournalEntriesModel>()))
                        {




                            var newCredit = new journalentry();
                            newCredit.AccountingCode = credit.AccountingCode;
                            newCredit.AccountName = credit.AccountName;
                            newCredit.Amount = credit.Amount;
                            newCredit.BookingID = credit.BookingID;
                            newCredit.CVNumber = credit.CVNumber;
                            newCredit.PaymentID = newPayment.ID;
                            newCredit.Remarks = credit.Remarks ?? "";
                            newCredit.Type = (int)SalesType.CREDIT;
                            db.journalentries.Add(newCredit);

                            db.SaveChanges();

                        }




                        var debitEntries = payment.DebitEntries.Where(x => x.AccountingCode != null).ToList();

                        var groupReferences = (from a in debitEntries
                                               group a by new { ReferenceType = a.ReferenceType, ReferenceNo = a.ReceiptNo } into pg
                                               select new
                                               {
                                                   ReferenceType = pg.Key.ReferenceType,
                                                   ReferenceNo = pg.Key.ReferenceNo
                                               }).ToList();



                        foreach (var debit in debitEntries)
                        {

                            var acctTerm = db.accountingterms.Where(x => x.Code == debit.AccountingCode).FirstOrDefault();
                            if (acctTerm == null) throw new Exception("ERROR! Invalid Account Name " + debit.AccountingCode);


                            var newDebit = new journalentry();
                            newDebit.AccountingCode = debit.AccountingCode;
                            newDebit.AccountName = acctTerm.Description;
                            newDebit.Amount = debit.Amount;
                            newDebit.BookingID = null;
                            // newDebit.BookingID = debit.BookingID;
                            // newDebit.CVNumber = debit.CVNumber;
                            newDebit.PaymentID = newPayment.ID;
                            newDebit.Remarks = debit.Remarks;
                            newDebit.Type = (int)SalesType.DEBIT;

                            if (String.IsNullOrEmpty(debit.ReceiptNo)) throw new Exception("Reference # is required!");
                            if (debit.ReferenceType == PaymentReferenceType.NONE) throw new Exception("Reference type is required for debit entry.");
                            newDebit.ReceiptNo = debit.ReceiptNo;
                            newDebit.ReferenceType = (int)debit.ReferenceType;
                            db.journalentries.Add(newDebit);

                            db.SaveChanges();

                        }

                        foreach (var grp in groupReferences)
                        {
                            var newJournalRef = new journalentriesreference();
                            newJournalRef.LocationCode = payment.Payment.BusinessLocationCode;
                            newJournalRef.JournalID = newPayment.ID;
                            newJournalRef.ReferenceNo = grp.ReferenceNo;
                            newJournalRef.ReferenceType = (int)grp.ReferenceType;
                            newJournalRef.DateCreated = DateTime.Now;

                            db.journalentriesreferences.Add(newJournalRef);
                            db.SaveChanges();
                        }

                        //NEW CREDIT ENTRY FOR CASH ADVANCE
                        //if (excessEntry > 0)
                        //{
                        //    var newDebit = new journalentry();
                        //    newDebit.AccountingCode = "100001";
                        //    newDebit.AccountName = "ADVANCE";
                        //    newDebit.Amount = excessEntry;
                        //    newDebit.BookingID = null;
                        //    newDebit.PaymentID = newPayment.ID;
                        //    newDebit.Remarks = "";
                        //    newDebit.Type = (int)SalesType.CREDIT;
                        //    newDebit.ReferenceType = (int)PaymentReferenceType.NONE;
                        //    db.journalentries.Add(newDebit);

                        //    db.SaveChanges();

                        //}

                        var payCt = new paymentcounter();
                        payCt.DateCreated = DateTime.Now;
                        payCt.Series = cvGenerator;
                        db.paymentcounters.Add(payCt);

                        db.SaveChanges();

                        transaction.Commit();
                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();

                        Exception ex = dbU.GetBaseException();

                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();
                        string errorMessages = null;
                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }
                        throw new Exception(errorMessages);
                    }
                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }
            }

        }

        public void VerifyPayment(AgentsModel agent, PaymentsModel payment)
        {

            using (isebookingEntities db = new isebookingEntities())
            {

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {



                        if (agent.Type != 3) throw new Exception("ERROR! You are not authorized!");

                        var paymentRef = db.payments.Where(x => x.ID == payment.Payment.ID).FirstOrDefault();

                        if (paymentRef == null) throw new Exception("Invalid payment reference!");


                        var travelAgency = db.travelagencies.Where(x => x.ID == paymentRef.TravelAgencyID).FirstOrDefault();


                        //  if (paymentRef.PreparedBy == agent.ID) throw new Exception("ERROR! You are not allowed to verify this payment! ");

                        paymentRef.VerifiedBy = agent.ID;
                        paymentRef.Status = (int)PaymentStatus.VERIFIED;

                        var creditEntries = paymentRef.journalentries.Where(x => x.Type == (int)SalesType.CREDIT && x.BookingID != null && x.AccountingCode == "1011000").DistinctBy(x => x.BookingID).ToList();
                        var debitEntries = paymentRef.journalentries.Where(x => x.Type == (int)SalesType.DEBIT).ToList();


                        var excessEntry = paymentRef.journalentries.Where(x => x.Type == (int)SalesType.CREDIT && x.BookingID == null).ToList().Sum(x => x.Amount);
                        var cashAdvances = paymentRef.journalentries.Where(x => x.Type == (int)SalesType.DEBIT && x.AccountingCode == "100001").ToList().Sum(x => x.Amount);

                        var totalPayments = (double)debitEntries.Sum(x => x.Amount);

                        if (excessEntry > 0)
                        {

                            travelAgency.CashDeposits += (double)(excessEntry);
                        }

                        if (cashAdvances > 0)
                        {
                            travelAgency.CashDeposits -= (double)(cashAdvances);
                        }


                        foreach (var entry in creditEntries)
                        {
                            double dec = 0;
                            var totalPaymentsByBooking = GetTotalPayments(db, entry.booking.ID);



                            dec = (double)(totalPaymentsByBooking + entry.Payments);
                            double excess = (dec - (entry.booking.Total ?? 0));
                            string paymentsFormatted = excess.ToString("C", new CultureInfo("en-PH"));

                            if (dec > entry.booking.Total) throw new Exception(String.Format("ERROR! Overpayment amounting to {0} on booking with CV# {1}", paymentsFormatted, entry.booking.CVNumber));



                            if (entry.booking.PaymentTypeID == (int)PaymentType.FOR_BILLING)
                            {
                                var billingPeriodID = entry.booking.accountreceivables.Where(x => x.BillingPeriodID != null).FirstOrDefault();

                                var arRef = billingPeriodID.booking.accountreceivables.Where(x => x.BookingID == entry.BookingID && x.BillingPeriodID != null).FirstOrDefault();

                                if (billingPeriodID == null) throw new Exception("Unable to find billing period!");

                                var bill = db.billingperiods.Where(x => x.ID == billingPeriodID.BillingPeriodID).FirstOrDefault();
                                bill.Status = (int)BillingStatementStatus.CONFIRMED_PAYMENT;
                                bill.Payment += entry.Amount;
                                bill.LastUpdate = DateTime.Now;
                                arRef.booking.IsPaid = dec == 0;
                                arRef.Balance = (arRef.Balance ?? 0) - entry.Amount;
                                entry.Payments = entry.Amount;
                                db.SaveChanges();
                            }
                        }

                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();

                        Exception ex = dbU.GetBaseException();

                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();
                        string errorMessages = null;
                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }
                        throw new Exception(errorMessages);
                    }
                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }
            }
        }

        public PaymentPagedModel GetAllPayments(AgentsModel agent, string search = null, int startIndex = 1, int pageSize = 10, string sortBy = "ID DESC")
        {
            using (isebookingEntities db = new isebookingEntities())
            {


                if (agent.Type != 3) throw new Exception("ERROR! Unauthorized access!");





                var resultset = db.journalentries.OrderByDescending(x => x.PaymentID).AsQueryable();

                string receiptNo = "";

                if (search != null)
                {

                    if (search.ToUpper().StartsWith("OR"))
                    {
                        receiptNo = search.Replace("OR", "");
                        resultset = resultset.Where(a => a.ReceiptNo.Contains(receiptNo) && a.ReferenceType == (int)PaymentReferenceType.OR);


                    }
                    else if (search.ToUpper().StartsWith("PR"))
                    {
                        receiptNo = search.Replace("PR", "");
                        resultset = resultset.Where(a => a.ReceiptNo.Contains(receiptNo) && a.ReferenceType == (int)PaymentReferenceType.PR);


                    }
                    else if (search.ToUpper().StartsWith("AR"))
                    {
                        receiptNo = search.Replace("AR", "");
                        resultset = resultset.Where(a => a.ReceiptNo.Contains(receiptNo) && a.ReferenceType == (int)PaymentReferenceType.AR);


                    }
                    else
                    {
                        resultset = resultset.Where(a => a.payment.PaymentNo.Contains(search)
                                    || a.CVNumber.Contains(search)
                                    || a.payment.travelagency.Name.Contains(search)

                                    );

                    }





                }


                var totalRowCount = resultset.Count();
                var recordTemp = resultset.Skip((startIndex - 1) * pageSize).Take(pageSize).ToList();


                PaymentPagedModel pagedRecords = new PaymentPagedModel();
                pagedRecords.PageNumber = startIndex;
                pagedRecords.TotalRecords = totalRowCount;

                pagedRecords.Records = mapper.Map<List<journalentry>, List<PaymentHeaderAndDetailsModel>>(recordTemp);

                var totalPages = (int)Math.Ceiling((decimal)pagedRecords.TotalRecords / (decimal)pageSize);
                var currentPage = startIndex == 0 ? (int)startIndex : 1;
                var startPage = currentPage - 5;
                var endPage = currentPage + 4;
                if (startPage <= 0)
                {
                    endPage -= (startPage - 1);
                    startPage = 1;
                }

                if (startIndex >= totalPages)
                {
                    endPage = totalPages;
                    if (endPage > 10)
                    {
                        startPage = endPage - 9;
                    }
                }
                pagedRecords.CurrentPage = startIndex;
                pagedRecords.StartPage = startPage;
                pagedRecords.EndPage = endPage;
                pagedRecords.TotalPages = totalPages;
                pagedRecords.PageSize = pageSize;
                pagedRecords.CurrentRecordCount = pagedRecords.Records.Count();

                return pagedRecords;
            }


        }

        public Boolean CheckReceiptIfUsed(PaymentReferenceType referenceType, string receiptNo, string businessLocation)
        {

            using (var db = new isebookingEntities())
            {
                var result = db.journalentriesreferences.Where(x => x.ReferenceType == (int)referenceType && x.LocationCode == businessLocation && x.ReferenceNo == receiptNo).FirstOrDefault();

                return result == null ? false : true;
            }

        }


        public void UpdateChanges(AgentsModel agent, PaymentsModel payment)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {



                        if (agent.Type != 3) throw new Exception("ERROR! You are not authorized!");

                        if (payment.Payment == null) throw new Exception("Invalid!");

                        if (payment.Payment.PaymentDate > DateTime.Now) throw new Exception("Invalid payment date!");

                        var newPayment = db.payments.Where(x => x.ID == payment.Payment.ID).FirstOrDefault();
                        if (newPayment == null) throw new Exception("Payment ID " + payment.Payment.ID + " does not exist.");

                        if (newPayment.Status != (int)PaymentStatus.OPEN) throw new Exception("Payment is already locked. Updating this record is no longer possible.");


                        if (payment.Payment.PaymentDate.Year <= 2015)
                        {
                            throw new Exception("Invalid payment date!");
                        }

                        newPayment.BusinessLocationCode = payment.Payment.BusinessLocationCode;
                        newPayment.CheckDate = payment.Payment.CheckDate;
                        newPayment.CheckNo = payment.Payment.CheckNo;
                        newPayment.ModeOfPaymentID = payment.Payment.ModeOfPaymentID;
                        newPayment.PaymentDate = payment.Payment.PaymentDate;

                        newPayment.Status = (int)PaymentStatus.OPEN;

                        newPayment.Remarks = payment.Payment.Remarks;
                        newPayment.TravelAgencyID = payment.Payment.TravelAgencyID;
                        newPayment.VerifiedBy = payment.Payment.VerifiedBy;


                        db.SaveChanges();





                        var journalEntries = db.journalentries.Where(x => x.PaymentID == newPayment.ID).ToList();

                        var journalIDs = journalEntries.Select(x => x.PaymentID).Distinct().ToList();

                        var journalEntryReferences = db.journalentriesreferences.Where(x => journalIDs.Contains(x.JournalID)).ToList();


                        var bookingIDs = journalEntries.Where(x => x.BookingID != null).Select(x => (int)x.BookingID).ToList() ?? new List<int>();


                        foreach (var bookID in bookingIDs)
                        {
                            var bookToUpdate = db.bookings.Where(x => x.ID == bookID).FirstOrDefault();
                            if (bookToUpdate != null)
                            {
                                bookToUpdate.IsPaid = false;
                                db.SaveChanges();
                            }
                        }

                        db.journalentriesreferences.RemoveRange(journalEntryReferences);
                        db.journalentries.RemoveRange(journalEntries);
                        db.SaveChanges();

                        if (payment.CreditEntries == null) throw new Exception("Please add Credit entries!");

                        if (payment.DebitEntries == null) throw new Exception("Please add Debit entries!");


                        double creditTotal = Math.Round(payment.CreditEntries.Sum(x => x.Amount),2);
                        double debitTotal = Math.Round(payment.DebitEntries.Sum(x => x.Amount),2);



                        if (creditTotal != debitTotal)
                        {
                            throw new Exception("Ooopps! Debit and credit entries should be equal!");
                        }

                        var totalPayments = debitTotal;
                        var remainingBalance = (double)totalPayments;

                        foreach (var credit in (payment.CreditEntries.Where(x => x.AccountingCode == "1011000").ToList() ?? new List<JournalEntriesModel>()))
                        {

                            if (string.IsNullOrEmpty(credit.CVNumber)) throw new Exception("ERROR! CV number is invalid!");
                            var book = db.bookings.Where(x => x.ID == credit.BookingID).FirstOrDefault();
                            // if (book.IsPaid) throw new Exception("ERROR! Booking with CV number " + credit.CVNumber + " is already paid");

                            var newCredit = new journalentry();
                            newCredit.AccountingCode = credit.AccountingCode;
                            newCredit.AccountName = credit.AccountName;
                            newCredit.Amount = credit.Amount;
                            newCredit.BookingID = credit.BookingID;
                            newCredit.CVNumber = credit.CVNumber;
                            newCredit.PaymentID = newPayment.ID;
                            newCredit.Remarks = credit.Remarks ?? "";
                            newCredit.Type = (int)SalesType.CREDIT;


                            if (!(book.CVNumber == credit.CVNumber && credit.BookingID == book.ID))
                            {
                                throw new Exception(String.Format("Record does not match! BookingID: {0} BookingCVNumber: {1} ", book.ID.ToString(), book.CVNumber.ToString()));
                            }



                            double dec = 0;
                            var totalPaymentsByBooking = GetTotalPayments(db, book.ID);

                            dec = (double)(totalPaymentsByBooking + credit.Amount);
                            double excess = (dec - book.Total ?? 0);
                            string paymentsFormatted = excess.ToString("C", new CultureInfo("en-PH"));

                            if (dec > book.Total) throw new Exception(String.Format("ERROR! Overpayment amounting to {0} on booking with CV# {1}", paymentsFormatted, book.CVNumber));

                            if (book.PaymentTypeID == (int)PaymentType.FOR_BILLING)
                            {

                                var billingPeriodID = book.accountreceivables.Where(x => x.BillingPeriodID != null).FirstOrDefault();
                                var arRef = billingPeriodID.booking.accountreceivables.Where(x => x.BookingID == book.ID && x.BillingPeriodID != null).FirstOrDefault();

                                if (billingPeriodID == null) throw new Exception("Unable to find billing period!");
                                var bill = db.billingperiods.Where(x => x.ID == billingPeriodID.BillingPeriodID).FirstOrDefault();

                            }

                            book.IsPaid = dec == 0;
                            newCredit.Payments = credit.Amount;

                            db.journalentries.Add(newCredit);
                            db.SaveChanges();

                        }

                        //ADVANCES ENTRY
                        foreach (var credit in (payment.CreditEntries.Where(x => x.AccountingCode != "1011000").ToList() ?? new List<JournalEntriesModel>()))
                        {




                            var newCredit = new journalentry();
                            newCredit.AccountingCode = credit.AccountingCode;
                            newCredit.AccountName = credit.AccountName;
                            newCredit.Amount = credit.Amount;
                            newCredit.BookingID = credit.BookingID;
                            newCredit.CVNumber = credit.CVNumber;
                            newCredit.PaymentID = newPayment.ID;

                            newCredit.Remarks = credit.Remarks ?? "";
                            newCredit.Type = (int)SalesType.CREDIT;
                            db.journalentries.Add(newCredit);

                            db.SaveChanges();

                        }


                        var debitEntries = payment.DebitEntries.Where(x => x.AccountingCode != null).ToList();

                        var groupReferences = (from a in debitEntries
                                               group a by new { ReferenceType = a.ReferenceType, ReferenceNo = a.ReceiptNo } into pg
                                               select new
                                               {
                                                   ReferenceType = pg.Key.ReferenceType,
                                                   ReferenceNo = pg.Key.ReferenceNo
                                               }).ToList();



                        foreach (var debit in debitEntries)
                        {

                            var acctTerm = db.accountingterms.Where(x => x.Code == debit.AccountingCode).FirstOrDefault();
                            if (acctTerm == null) throw new Exception("ERROR! Invalid Account Name " + debit.AccountingCode);


                            var newDebit = new journalentry();
                            newDebit.AccountingCode = debit.AccountingCode;
                            newDebit.AccountName = acctTerm.Description;
                            newDebit.Amount = debit.Amount;
                            newDebit.BookingID = null;
                            // newDebit.BookingID = debit.BookingID;
                            // newDebit.CVNumber = debit.CVNumber;
                            newDebit.PaymentID = newPayment.ID;
                            newDebit.Remarks = debit.Remarks;
                            newDebit.Type = (int)SalesType.DEBIT;

                            if (String.IsNullOrEmpty(debit.ReceiptNo)) throw new Exception("Reference # is required!");
                            if (debit.ReferenceType == PaymentReferenceType.NONE) throw new Exception("Reference type is required for debit entry.");
                            newDebit.ReceiptNo = debit.ReceiptNo;
                            newDebit.ReferenceType = (int)debit.ReferenceType;
                            db.journalentries.Add(newDebit);

                            db.SaveChanges();

                        }

                        foreach (var grp in groupReferences)
                        {
                            var newJournalRef = new journalentriesreference();
                            newJournalRef.LocationCode = payment.Payment.BusinessLocationCode;
                            newJournalRef.JournalID = newPayment.ID;
                            newJournalRef.ReferenceNo = grp.ReferenceNo;
                            newJournalRef.ReferenceType = (int)grp.ReferenceType;
                            newJournalRef.DateCreated = DateTime.Now;

                            db.journalentriesreferences.Add(newJournalRef);
                            db.SaveChanges();
                        }


                        //NEW DEBIT ENTRY FOR CASH ADVANCE
                        //if (excessEntry > 0)
                        //{
                        //    var newDebit = new journalentry();
                        //    newDebit.AccountingCode = "100001";
                        //    newDebit.AccountName = "CASH ADVANCE";
                        //    newDebit.Amount = excessEntry * -1;
                        //    newDebit.BookingID = null;
                        //    newDebit.PaymentID = newPayment.ID;
                        //    newDebit.Remarks = "";
                        //    newDebit.Type = (int)SalesType.CREDIT;
                        //    newDebit.ReferenceType = (int)PaymentReferenceType.NONE;
                        //    db.journalentries.Add(newDebit);

                        //    db.SaveChanges();

                        //}

                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();

                        Exception ex = dbU.GetBaseException();

                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();
                        string errorMessages = null;
                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }
                        throw new Exception(errorMessages);
                    }
                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }
            }
        }

        public void CancelPayment(AgentsModel agent, int paymentID)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {



                        if (agent.Type != 3) throw new Exception("ERROR! You are not authorized!");

                        var paymentRef = db.payments.Where(x => x.ID == paymentID).FirstOrDefault();
                        if (paymentRef == null) throw new Exception("ERROR! Payment ID " + paymentID + " does not exist!");

                        if (paymentRef.Status == (int)PaymentStatus.VERIFIED) throw new Exception("ERROR! Cancellation of payments is no longer possible!");

                        paymentRef.Status = (int)PaymentStatus.CANCELLED;

                        var journalIDs = paymentRef.journalentries.Select(x => x.PaymentID).Distinct().ToList();

                        var debitEntries = paymentRef.journalentries.Where(x => x.BookingID != null && x.Type == (int)SalesType.CREDIT).Select(x => x.BookingID).ToList();

                        foreach (var bookingID in debitEntries)
                        {

                            //var book = db.bookings.Where(x => x.ID == bookingID && x.PaymentTypeID != (int)PaymentType.FOR_BILLING).FirstOrDefault();
                            var book = db.bookings.Where(x => x.ID == bookingID).FirstOrDefault();

                            if (book != null)
                            {
                                book.IsPaid = false;
                                db.SaveChanges();
                            }

                        }

                        var journalEntryReferences = db.journalentriesreferences.Where(x => journalIDs.Contains(x.JournalID)).ToList();

                        db.journalentriesreferences.RemoveRange(journalEntryReferences);

                        db.SaveChanges();

                        transaction.Commit();
                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();

                        Exception ex = dbU.GetBaseException();

                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();
                        string errorMessages = null;
                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }
                        throw new Exception(errorMessages);
                    }
                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }

            }
        }
        
        private double GetTotalPayments(isebookingEntities db, int bookingID)
        {

            return (from a in db.payments
                    join b in db.journalentries on a.ID equals b.PaymentID
                    where b.BookingID == bookingID
                    && a.Status != (int)PaymentStatus.CANCELLED
                    && b.Payments > 0
                    && b.Type == (int)SalesType.CREDIT
                    select b.Payments).ToList().Sum();
        }


        public BookingsPaged GetUnpaidBookings(string cvNumber, string groupName, int travelAgencyID, string search = "", int startIndex = 1, int pageSize = 1000, string sortBy = "ID DESC")
        {
            using (isebookingEntities db = new isebookingEntities())
            {


                var allUnpaidBookings = db.Database.SqlQuery<int>("CALL getAllUnpaidBookings").ToList<int>();


                var resultset = db.bookings.Where(a => a.Status == (int)BookingStatus.FINALIZED && (a.PaymentTypeID != (int)PaymentType.FOR_BILLING)
                              && allUnpaidBookings.Contains(a.ID)
                              && a.agent.TravelAgencyID == travelAgencyID).OrderByDescending(x => x.ID).AsQueryable();

                if (!String.IsNullOrEmpty(cvNumber))
                {
                    resultset = resultset.Where(a => a.CVNumber.Contains(cvNumber));
                }

                if (!String.IsNullOrEmpty(groupName))
                {
                    resultset = resultset.Where(a => a.GroupName.Contains(groupName));
                }


                var totalRowCount = resultset.Count();
                var recordTemp = resultset.Skip((startIndex - 1) * pageSize).Take(pageSize).ToList();


                BookingsPaged pagedRecords = new BookingsPaged();
                pagedRecords.PageNumber = startIndex;
                pagedRecords.TotalRecords = totalRowCount;

                pagedRecords.Records = mapper.Map<List<booking>, List<BookingHeaderModel>>(recordTemp);

                var totalPages = (int)Math.Ceiling((decimal)pagedRecords.TotalRecords / (decimal)pageSize);
                var currentPage = startIndex == 0 ? (int)startIndex : 1;
                var startPage = currentPage - 5;
                var endPage = currentPage + 4;
                if (startPage <= 0)
                {
                    endPage -= (startPage - 1);
                    startPage = 1;
                }

                if (startIndex >= totalPages)
                {
                    endPage = totalPages;
                    if (endPage > 10)
                    {
                        startPage = endPage - 9;
                    }
                }
                pagedRecords.CurrentPage = startIndex;
                pagedRecords.StartPage = startPage;
                pagedRecords.EndPage = endPage;
                pagedRecords.TotalPages = totalPages;
                pagedRecords.PageSize = pageSize;
                pagedRecords.CurrentRecordCount = pagedRecords.Records.Count();

                return pagedRecords;
            }
        }


        public void ReversePayments(PaymentsModel payment)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        var paymentRef = db.payments.Where(x => x.ID == payment.Payment.ID).FirstOrDefault();

                        if (paymentRef == null) throw new Exception("Invalid payment reference!");


                        var travelAgency = db.travelagencies.Where(x => x.ID == paymentRef.TravelAgencyID).FirstOrDefault();

                        paymentRef.Status = (int)PaymentStatus.OPEN;


                        var creditEntries = paymentRef.journalentries.Where(x => x.Type == (int)SalesType.CREDIT && x.BookingID != null && x.AccountingCode == "1011000").DistinctBy(x => x.BookingID).ToList();
                        var debitEntries = paymentRef.journalentries.Where(x => x.Type == (int)SalesType.DEBIT).ToList();



                        var totalPayments = (double)debitEntries.Sum(x => x.Amount);




                        foreach (var entry in creditEntries)
                        {


                            if (entry.booking.PaymentTypeID == (int)PaymentType.FOR_BILLING)
                            {
                                var billingPeriodID = entry.booking.accountreceivables.Where(x => x.BillingPeriodID != null).FirstOrDefault();

                                var arRef = billingPeriodID.booking.accountreceivables.Where(x => x.BookingID == entry.BookingID && x.BillingPeriodID != null).FirstOrDefault();

                                if (billingPeriodID == null) throw new Exception("Unable to find billing period!");

                                var bill = db.billingperiods.Where(x => x.ID == billingPeriodID.BillingPeriodID).FirstOrDefault();
                                bill.Status = (int)BillingStatementStatus.OPEN;
                                bill.IsOpen = true;
                                bill.Payment -= entry.Amount;
                                bill.LastUpdate = DateTime.Now;
                                arRef.booking.IsPaid = false;
                                arRef.Balance -= entry.Amount;
                                entry.Payments = entry.Amount;
                                db.SaveChanges();
                            }
                        }

                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();

                        Exception ex = dbU.GetBaseException();

                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();
                        string errorMessages = null;
                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }
                        throw new Exception(errorMessages);
                    }
                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }
            }
        }

        public void ReverseAllVerifiedPayments(AgentsModel agent)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                var verifiedPayments = db.payments.Where(x => x.Status == (int)PaymentStatus.VERIFIED).Select(x => x.ID).ToList();

                foreach (var pay in verifiedPayments)
                {
                    var paymentData = GetPayment(agent, pay);
                    Hangfire.BackgroundJob.Enqueue(() => ReversePayments(paymentData));
                }
            }
        }

    }

    public class PaymentPagedModel
    {
        public List<PaymentHeaderAndDetailsModel> Records { get; set; }
        public long TotalRecords { get; set; }
        public long PageNumber { get; set; }
        public int PageSize { get; set; }
        public int StartPage { get; set; }
        public int EndPage { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int CurrentRecordCount { get; set; }
    }


}
