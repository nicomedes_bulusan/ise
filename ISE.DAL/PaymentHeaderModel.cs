﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public class PaymentHeaderModel
    {
        public int ID { get; set; }
        public string PaymentNo { get; set; }
        public DateTime PaymentDate { get; set; }
        public string BusinessLocationCode { get; set; }
        public int TravelAgencyID { get; set; }
        public TravelAgencyModel TravelAgency { get; set; }
        public int ModeOfPaymentID { get; set; }
        public ModeOfPaymentsModel ModeOfPayment { get; set; }
       // public string ReceiptNo { get; set; }
        public string CheckNo { get; set; }
        public DateTime? CheckDate { get; set; }
        public int PreparedBy { get; set; }
        public string PreparedByAgent { get; set; }
        public int? VerifiedBy { get; set; }
        public string VerifiedByName { get; set; } 
        public string Remarks { get; set; }
        public PaymentStatus Status { get; set; }
        public string StatusString { get
            {
                switch (Status)
                {
                    case PaymentStatus.OPEN:
                        return "OPEN";
                    case PaymentStatus.VERIFIED:
                        return "VERIFIED";
                    case PaymentStatus.CANCELLED:
                        return "CANCELLED";
                    default:
                        return "";
                }
            }
        
        }
   
    
    }

    public class PaymentHeaderAndDetailsModel
    {
        
        public int ID { get; set; }
        public string PaymentNo { get; set; }
        public DateTime PaymentDate { get; set; }
        public string BusinessLocationCode { get; set; }
        public int TravelAgencyID { get; set; }
        public TravelAgencyModel TravelAgency { get; set; }
        public int ModeOfPaymentID { get; set; }
        public ModeOfPaymentsModel ModeOfPayment { get; set; }
        public string ReceiptNo { get; set; }
        public int ReferenceType { get; set; }
        public string CheckNo { get; set; }
        public DateTime? CheckDate { get; set; }
        public int PreparedBy { get; set; }
        public string PreparedByAgent { get; set; }
        public int? VerifiedBy { get; set; }
        public string VerifiedByName { get; set; }
        public string Remarks { get; set; }
        public PaymentStatus Status { get; set; }
        public string StatusString
        {
            get
            {
                switch (Status)
                {
                    case PaymentStatus.OPEN:
                        return "OPEN";
                    case PaymentStatus.VERIFIED:
                        return "VERIFIED";
                    case PaymentStatus.CANCELLED:
                        return "CANCELLED";
                    default:
                        return "";
                }
            }

        }
        public string AccountingCode { get; set; }
        public string AccountName { get; set; }
        public string CVNumber { get; set; }
        public string FormattedReceiptNo
        {
            get
            {

                if (ReferenceType != 0)
                {
                    switch (ReferenceType)
                    {
                        case 1:
                            return "OR" + ReceiptNo;
                           
                        case 2:
                            return "PR" + ReceiptNo;
                        case 3:
                            return "AR" + ReceiptNo;
                           
                    }
                }
                return "";
            }
        }
    }
}
