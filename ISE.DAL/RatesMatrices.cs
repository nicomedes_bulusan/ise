﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public class RatesMatrices
    {
        public int ID { get; set; }
        public int PickUp { get; set; }
        public string PickUpLocation { get; set; }
        public int DropOff { get; set; }
        public string DropOffLocation { get; set; }
        public Double Amount { get; set; }
        public Double Boat { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastUpdated { get; set; }
        public Boolean Status { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public int Type { get; set; }
        public ClassRate Class { get; set; }




        public RatesMatrices GetRate(int pickUp, int dropOFf, int type, AgentsModel agent, DateTime date,ClassRate classRate=ClassRate.REGULAR)
        {
            using (isebookingEntities db = new isebookingEntities())
            {


                if (agent.Type == 1)
                {

                    return (from a in db.ratematrixagents
                                  where a.PickUp == pickUp && a.DropOff == dropOFf && a.Type == type
                                  && a.TravelAgencyID == agent.TravelAgencyID && date >= a.FromDate && date <= a.ToDate
                                  && a.Status == true
                                  && a.Class == (int)classRate
                                  select new RatesMatrices
                                  {
                                      Amount = a.Amount,
                                      Boat = a.Boat,
                                      DateCreated = a.DateCreated,
                                      DateLastUpdated = a.DateLastUpdated,
                                      DropOff = a.DropOff,
                                      DropOffLocation = a.destination1.Description,
                                      ID = a.ID,
                                      PickUp = a.PickUp,
                                      PickUpLocation = a.destination.Description,
                                      Status = a.Status,
                                      Type = a.Type,
                                      FromDate = a.FromDate,
                                      ToDate = a.ToDate,
                                      Class = (ClassRate)a.Class
                                  }).FirstOrDefault();

                    

                }
                else
                {
                    return (from a in db.ratematrices
                                  where a.PickUp == pickUp && a.DropOff == dropOFf && a.Type == type
                                  && date >= a.FromDate && date <= a.ToDate
                                  && a.Status == true
                                     && a.Class == (int)classRate
                                  select new RatesMatrices
                                  {
                                      Amount = a.Amount,
                                      Boat = a.Boat,
                                      DateCreated = a.DateCreated,
                                      DateLastUpdated = a.DateLastUpdated,
                                      DropOff = a.DropOff,
                                      DropOffLocation = a.destination1.Description,
                                      ID = a.ID,
                                      PickUp = a.PickUp,
                                      PickUpLocation = a.destination.Description,
                                      Status = a.Status,
                                      Type = a.Type,
                                      Class = (ClassRate)a.Class
                                  }).FirstOrDefault();

                    
                }

            }


        }


        public List<RatesMatrices> GetMatrices(AgentsModel agent)
        {
            using (isebookingEntities db = new isebookingEntities())
            {


                if (agent.Type == 1)
                {
                   return (from a in db.ratematrixagents
                                  where a.Status == true &&
                                  a.TravelAgencyID == agent.TravelAgencyID
                                  select new RatesMatrices
                                  {
                                      Amount = a.Amount,
                                      Boat = a.Boat,
                                      DateCreated = a.DateCreated,
                                      DateLastUpdated = a.DateLastUpdated,
                                      DropOff = a.DropOff,
                                      DropOffLocation = a.destination1.Description,
                                      ID = a.ID,
                                      PickUp = a.PickUp,
                                      PickUpLocation = a.destination.Description,
                                      Status = a.Status,
                                      Type = a.Type,
                                      FromDate = a.FromDate,
                                      ToDate = a.ToDate,
                                      Class = (ClassRate)a.Class
                                  }).ToList<RatesMatrices>(); ;

                  
                }
                else
                {
                    return (from a in db.ratematrices
                                  where a.Status == true
                                  select new RatesMatrices
                                  {
                                      Amount = a.Amount,
                                      Boat = a.Boat,
                                      DateCreated = a.DateCreated,
                                      DateLastUpdated = a.DateLastUpdated,
                                      DropOff = a.DropOff,
                                      DropOffLocation = a.destination1.Description,
                                      ID = a.ID,
                                      PickUp = a.PickUp,
                                      PickUpLocation = a.destination.Description,
                                      Status = a.Status,
                                      Type = a.Type,
                                      FromDate = a.FromDate,
                                      ToDate = a.ToDate,
                                      Class = (ClassRate)a.Class
                                  }).ToList<RatesMatrices>(); ;

                }
            }
        }


        public RatesMatrices CreateMatrix(RatesMatrices rate, AgentsModel agent)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (agent.Type == 2)
                        {
                            var checkOverlapping = db.ratematrices.Where(x => x.Status == true
                                      && x.PickUp == rate.PickUp && x.DropOff == rate.DropOff
                                      && x.Type == rate.Type
                                      && x.Class == (int)rate.Class
                                        && x.FromDate <= rate.FromDate && x.ToDate >= rate.ToDate).ToList();

                            if ((checkOverlapping != null && checkOverlapping.Count > 0)) throw new Exception("Overlapping scheduled rate detected!");

                            var newRate = new ratematrix();
                            newRate.Amount = rate.Amount;
                            newRate.Boat = rate.Boat;
                            newRate.DateCreated = DateTime.Now;
                            newRate.DateLastUpdated = DateTime.Now;
                            newRate.PickUp = rate.PickUp;
                            newRate.Status = true;
                            newRate.Type = rate.Type;
                            newRate.DropOff = rate.DropOff;
                            newRate.FromDate = rate.FromDate;
                            newRate.ToDate = rate.ToDate;
                            newRate.Class = (int)rate.Class;
                            db.ratematrices.Add(newRate);
                            db.SaveChanges();
                            transaction.Commit();
                            rate.ID = newRate.ID;

                            return rate;
                        }
                        else
                        {

                            var checkOverlapping = db.ratematrixagents.Where(x => x.Status == true
                           && x.PickUp == rate.PickUp && x.DropOff == rate.DropOff
                           && x.TravelAgencyID == agent.TravelAgencyID
                            && x.Type == rate.Type
                             && x.Class == (int)rate.Class
                             && x.FromDate <= rate.FromDate && x.ToDate >= rate.ToDate).ToList();

                            if ((checkOverlapping != null && checkOverlapping.Count > 0)) throw new Exception("Overlapping scheduled rate detected!");


                            var newRate = new ratematrixagent();
                            newRate.Amount = rate.Amount;
                            newRate.Boat = rate.Boat;
                            newRate.DateCreated = DateTime.Now;
                            newRate.DateLastUpdated = DateTime.Now;
                            newRate.PickUp = rate.PickUp;
                            newRate.Status = true;
                            newRate.Type = rate.Type;
                            newRate.DropOff = rate.DropOff;
                            newRate.TravelAgencyID = agent.TravelAgencyID;
                            newRate.FromDate = rate.FromDate;
                            newRate.ToDate = rate.ToDate;
                            newRate.Class = (int)rate.Class;
                            db.ratematrixagents.Add(newRate);
                            db.SaveChanges();
                            transaction.Commit();
                            rate.ID = newRate.ID;

                            return rate;

                        }
                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();

                        Exception ex = dbU.GetBaseException();

                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();

                        string errorMessages = null;

                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }

                        throw new Exception(errorMessages);

                    }

                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }


            }



        }



        public void DeleteMatrix(RatesMatrices rate, AgentsModel agent)
        {
            using (isebookingEntities db = new isebookingEntities())
            {


                if (agent.Type == 1)
                {
                    var delRate = db.ratematrixagents.Where(x => x.ID == rate.ID && x.TravelAgencyID == agent.TravelAgencyID).FirstOrDefault();

                    if (delRate == null) throw new Exception("Unknown rate matrix!");

                    delRate.Status = false;
                    db.SaveChanges();
                }
                else
                {
                    var delRate = db.ratematrices.Where(x => x.ID == rate.ID).FirstOrDefault();

                    if (delRate == null) throw new Exception("Unknown rate matrix!");

                    delRate.Status = false;
                    db.SaveChanges();
                }

            }

            // db.ratematrices.Remove(delRate);
        }



        public void Updatematrix(RatesMatrices rate, AgentsModel agent)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        if (agent.Type == 1)
                        {
                            var checkOverlapping = db.ratematrixagents.Where(x => x.Status == true
                     && x.PickUp == rate.PickUp && x.DropOff == rate.DropOff
                     && x.TravelAgencyID == agent.TravelAgencyID
                      && x.Type == rate.Type
                       && x.Class == (int)rate.Class
                       && x.ID != rate.ID
                       && x.FromDate <= rate.FromDate && x.ToDate >= rate.ToDate).ToList();

                            if ((checkOverlapping != null && checkOverlapping.Count > 0)) throw new Exception("Overlapping scheduled rate detected!");


                            var updateRate = db.ratematrixagents.Where(x => x.ID == rate.ID && x.TravelAgencyID == agent.TravelAgencyID).FirstOrDefault();

                            if (updateRate == null) throw new Exception("Unknown rate matrix");

                            updateRate.Amount = rate.Amount;
                            updateRate.Boat = rate.Boat;
                            updateRate.DateLastUpdated = DateTime.Now;
                            updateRate.DropOff = rate.DropOff;
                            updateRate.PickUp = rate.PickUp;
                            updateRate.Status = true;
                            updateRate.Type = rate.Type;
                            updateRate.FromDate = rate.FromDate;
                            updateRate.ToDate = rate.ToDate;
                            updateRate.Class = (int)rate.Class;
                            db.SaveChanges();
                            transaction.Commit();
                        }
                        else
                        {

                            var checkOverlapping = db.ratematrices.Where(x => x.Status == true
                             && x.PickUp == rate.PickUp && x.DropOff == rate.DropOff
                              && x.Type == rate.Type
                               && x.Class == (int)rate.Class
                                  && x.ID != rate.ID
                               && x.FromDate <= rate.FromDate && x.ToDate >= rate.ToDate).ToList();

                            if ((checkOverlapping != null && checkOverlapping.Count > 0)) throw new Exception("Overlapping scheduled rate detected!");

                            var updateRate = db.ratematrices.Where(x => x.ID == rate.ID).FirstOrDefault();

                            if (updateRate == null) throw new Exception("Unknown rate matrix");

                            updateRate.Amount = rate.Amount;
                            updateRate.Boat = rate.Boat;
                            updateRate.DateLastUpdated = DateTime.Now;
                            updateRate.DropOff = rate.DropOff;
                            updateRate.PickUp = rate.PickUp;
                            updateRate.Status = true;
                            updateRate.Type = rate.Type;
                            updateRate.FromDate = rate.FromDate;
                            updateRate.ToDate = rate.ToDate;
                            updateRate.Class = (int)rate.Class;
                            db.SaveChanges();
                            transaction.Commit();
                        }
                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();

                        Exception ex = dbU.GetBaseException();

                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();

                        string errorMessages = null;

                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }

                        throw new Exception(errorMessages);

                    }

                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }

            }
        }

    }
}
