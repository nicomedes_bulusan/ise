﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{

    public class ManifestoVehicleAllocation
    {
        public string Location { get; set; }
        public int Adults { get; set; }
        public int Kids { get; set; }
        public int VehicleCategoryID { get; set; }
        public string VehicleCategory { get; set; }
        public int ActualAdults { get; set; }
        public int ActualKids { get; set; }
        public int VehicleCapacity { get; set; }
        public int VehicleID { get; set; }
        public string Vehicle { get; set; }
        public string Type { get; set; }
        public int ActualPax
        {
            get
            {
                return this.ActualAdults + this.ActualKids;
            }
        }
    }

    public class ManifestoDataset
    {

        public string Driver { get; set; }
        public string Vehicle { get; set; }
        public int VehicleID { get; set; }
        public string Coordinator { get; set; }
        public string ManifestoType
        {
            get
            {
                if (this.Type == 1)
                {
                    return "ARRIVAL";

                }
                else
                {
                    return "DEPARTURE";
                }
            }
        }
        public string FleetCategory { get; set; }
        public int FleetCategoryID { get; set; }

        public int ID { get; set; }
        public int BookingID { get; set; }
        public string GroupName { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNumber { get; set; }
        public string CVNumber { get; set; }
        public int Adults { get; set; }
        public int Childrens { get; set; }
        public int ChildrensBelowSix { get; set; }
        public int Infants { get; set; }
        public int Senior { get; set; }
        public int TourGuides { get; set; }
        public int Escorts { get; set; }
        public int FOC { get; set; }
        public int Kids { get; set; }
        public int Routing { get; set; }
        public Nullable<int> PickUpArrival { get; set; }
        public Nullable<int> DropOffArrival { get; set; }
        public Nullable<int> DropOffLocation { get; set; }
        public Nullable<int> PickUpDeparture { get; set; }
        public Nullable<int> DropOffDeparture { get; set; }
        public string FlightNo1 { get; set; }
        public string FlightNo2 { get; set; }
        public Nullable<DateTime> FlightDate1 { get; set; }
        public Nullable<DateTime> FlightDate2 { get; set; }
        public Double Total { get; set; }
        public int CreatedBy { get; set; }

        public Nullable<int> DropOffLocation1 { get; set; }
        public Nullable<int> DropOffLocation2 { get; set; }
        public int Status { get; set; }
        public int PaymentTypeID { get; set; }
        public Nullable<DateTime> CVDate { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastUpdated { get; set; }
        public int Pax { get; set; }
        public int ResortHotel { get; set; }
        public string ResortHotelDescription { get; set; }
        public DateTime? Date { get; set; }
        public int Type { get; set; }
        public string FlightNo { get; set; }
        public string Remarks { get; set; }
        public string Guests { get; set; }
        public int Pickup { get; set; }
        public int DropOff { get; set; }

        public string Agent { get; set; }
        public string AgencyName { get; set; }

        public string Location { get; set; }
        public string TouchPoint { get; set; }
        public TimeSpan Time
        {
            get
            {

                return new TimeSpan(this.Date.Value.Hour, this.Date.Value.Minute, this.Date.Value.Second);

            }
        }


        public int ActualAdults { get; set; }
        public int ActualInfants { get; set; }
        public int ActualTourGuides { get; set; }
        public int ActualEscorts { get; set; }
        public int ActualFOC { get; set; }
        public int ActualKids { get; set; }

        public Boolean RequiresAuthCode
        {
            get
            {

                return (Adults < ActualAdults || Infants < ActualInfants || Kids < ActualKids || FOC < ActualFOC ||  Escorts < ActualEscorts || TourGuides < ActualTourGuides);

            }
        }

        public string AuthCode { get; set; }


        public DateTime? ActualDate { get; set; }
        public TimeSpan? ActualTime { get; set; }

        public ManifestoDataset()
        {
            ActualAdults = 0;
            ActualInfants = 0;
            ActualTourGuides = 0;
            ActualEscorts = 0;
            ActualFOC = 0;
            ActualKids = 0;
            ActualInfants = 0;
        }
    }
}
