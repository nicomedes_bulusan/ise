﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoreLinq;

namespace ISE.DAL
{
    public class AgentsModel
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastUpdated { get; set; }

        public Double CreditLimit { get; set; }
        public double RemainingCredit { get; set; }
        public double AccountBalance { get; set; }
        public int GracePeriod { get; set; }

        public double MonthlyInterestRate { get; set; }
        public double YearToDateAmount { get; set; }
        public double MonthToDateAmount { get; set; }
        public Boolean IsActive { get; set; }
        public int Type { get; set; }
        public PaymentType PaymentTypeID { get; set; }
        public string Company { get; set; }

        public string Address { get; set; }
        public int MarketTypeID { get; set; }
        public string Location { get; set; }
        public string MarketType
        {
            get
            {
                using (isebookingEntities db = new isebookingEntities())
                {

                    return db.markettypes.Where(x => x.ID == this.MarketTypeID).Select(x => x.Description).FirstOrDefault();
                }

            }
        }

        public string Fullname
        {
            get
            {
                return (this.FirstName + " " + this.LastName);
            }
        }
        public int TravelAgencyID { get; set; }
        public string Password { get; set; }

        public string RoleName { get
            {
                switch (this.Type)
                {
                    case 1:
                        return "AGENT";
                    case 2:
                        return "BACKOFFICE";
                    case 3: 
                            return "ACCOUNTING";
                    case 4:
                        return "MANIFESTO";
                    case 5:
                        return "COORDINATOR";
                    default:
                        return "";

                }
            } }

        private bool IsValidEmail(string emailaddress = "")
        {
            try
            {
                System.Net.Mail.MailAddress m = new System.Net.Mail.MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public double CheckBalance(int travelAgencyID)
        {
            using (var db = new isebookingEntities())
            {
               return db.travelagencies.Where(x => x.ID == travelAgencyID).Select(x => x.CashDeposits).FirstOrDefault();          
                
            }
        }

        public AgentsModel FindAgent(string userName, string passsword,Boolean isCoordinator=false)
        {
            using (isebookingEntities db = new isebookingEntities())
            {
                string hashPassword = passsword;
                //if (!isCoordinator)
                //{
              
                //    hashPassword = BCrypt.Net.BCrypt.HashPassword(passsword);
                //}
               

                var result = (from a in db.agents
                              where a.IsActive == true && a.Email == userName
                              select new AgentsModel
                              {
                                  ContactNumber = a.ContactNumber,
                                  CreditLimit = a.CreditLimit,
                                  DateCreated = a.DateCreated,
                                  DateLastUpdated = a.DateLastUpdated,
                                  Email = a.Email,
                                  FirstName = a.FirstName,
                                  ID = a.ID,
                                  IsActive = a.IsActive,
                                  LastName = a.LastName,
                                  MonthToDateAmount = a.MonthToDateAmount,
                                  RemainingCredit = a.RemainingCredit,
                                  YearToDateAmount = a.YearToDateAmount,
                                  Type = a.Type,
                                  AccountBalance = a.AccountBalance,
                                  Address = a.Address,
                                  Company = a.travelagency.Name,
                                  GracePeriod = a.GracePeriod,
                                  MonthlyInterestRate = a.MonthlyInterestRate,
                                  PaymentTypeID = (PaymentType)a.PaymentTypeID,
                                  MarketTypeID = a.MarketTypeID,
                                  Location = a.Location,
                                  TravelAgencyID = a.TravelAgencyID
                              }).FirstOrDefault();



                if (result == null) throw new Exception("Invalid username/password!");

                var agentData = db.agents.Where(x => x.ID == result.ID).FirstOrDefault();

               

                if (!BCrypt.Net.BCrypt.Verify(passsword, agentData.Password)) throw new Exception("Invalid username/password!");


                return result;
            }
        }


        public AgentsModel FindAgentByID(int agentID)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                return (from a in db.agents
                        where a.ID == agentID
                        select new AgentsModel
                        {
                            ContactNumber = a.ContactNumber,
                            CreditLimit = a.CreditLimit,
                            DateCreated = a.DateCreated,
                            DateLastUpdated = a.DateLastUpdated,
                            Email = a.Email,
                            FirstName = a.FirstName,
                            ID = a.ID,
                            IsActive = a.IsActive,
                            LastName = a.LastName,
                            MonthToDateAmount = a.MonthToDateAmount,
                            RemainingCredit = a.RemainingCredit,
                            YearToDateAmount = a.YearToDateAmount,
                            Type = a.Type,
                            AccountBalance = a.AccountBalance,
                            Address = a.Address,
                            Company = a.travelagency.Name,
                            GracePeriod = a.GracePeriod,
                            MonthlyInterestRate = a.MonthlyInterestRate,
                            PaymentTypeID = (PaymentType)a.PaymentTypeID,
                            MarketTypeID = a.MarketTypeID,
                            Location = a.Location,
                            TravelAgencyID = a.TravelAgencyID
                        }).FirstOrDefault();
            }
           

        }
        public AgentsModel FindAgentByCompany(string Cname)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                return (from a in db.agents
                        where a.Company == Cname
                        select new AgentsModel
                        {
                            ContactNumber = a.ContactNumber,
                            CreditLimit = a.CreditLimit,
                            DateCreated = a.DateCreated,
                            DateLastUpdated = a.DateLastUpdated,
                            Email = a.Email,
                            FirstName = a.FirstName,
                            ID = a.ID,
                            IsActive = a.IsActive,
                            LastName = a.LastName,
                            MonthToDateAmount = a.MonthToDateAmount,
                            RemainingCredit = a.RemainingCredit,
                            YearToDateAmount = a.YearToDateAmount,
                            Type = a.Type,
                            AccountBalance = a.AccountBalance,
                            Address = a.Address,
                            Company = a.travelagency.Name,
                            GracePeriod = a.GracePeriod,
                            MonthlyInterestRate = a.MonthlyInterestRate,
                            PaymentTypeID = (PaymentType)a.PaymentTypeID,
                            MarketTypeID = a.MarketTypeID,
                            Location = a.Location,
                            TravelAgencyID = a.TravelAgencyID
                        }).FirstOrDefault();
            }


        }

        public AgentsModel FindAgentByEmail(string email)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                return (from a in db.agents
                        where a.Email == email && a.IsActive == true
                        select new AgentsModel
                        {
                            ContactNumber = a.ContactNumber,
                            CreditLimit = a.CreditLimit,
                            DateCreated = a.DateCreated,
                            DateLastUpdated = a.DateLastUpdated,
                            Email = a.Email,
                            FirstName = a.FirstName,
                            ID = a.ID,
                            IsActive = a.IsActive,
                            LastName = a.LastName,
                            MonthToDateAmount = a.MonthToDateAmount,
                            RemainingCredit = a.RemainingCredit,
                            YearToDateAmount = a.YearToDateAmount,
                            Type = a.Type,
                            AccountBalance = a.AccountBalance,
                            Address = a.Address,
                            Company = a.travelagency.Name,
                            GracePeriod = a.GracePeriod,
                            MonthlyInterestRate = a.MonthlyInterestRate,
                            PaymentTypeID = (PaymentType)a.PaymentTypeID,
                            MarketTypeID = a.MarketTypeID,
                            Location = a.Location,
                            TravelAgencyID = a.TravelAgencyID
                        }).FirstOrDefault();

            }
        }

        public List<AgentsModel> GetAgents(Boolean activeOnly = true, int agentType = 0, Boolean unique = false)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                // LINQ TO SQL SORTING BUG
                var propertyInfo = typeof(AgentsModel).GetProperty("Company");
                OrderByDirection sortDirec = OrderByDirection.Ascending;

                var result = (from a in db.agents

                              where (activeOnly == true ? a.IsActive == true : true) &&
                              (agentType == 0 ? true : a.Type == agentType)

                              select new AgentsModel
                              {
                                  ContactNumber = a.ContactNumber,
                                  CreditLimit = a.CreditLimit,
                                  DateCreated = a.DateCreated,
                                  DateLastUpdated = a.DateLastUpdated,
                                  Email = a.Email,
                                  FirstName = a.FirstName,
                                  ID = a.ID,
                                  IsActive = a.IsActive,
                                  LastName = a.LastName,
                                  MonthToDateAmount = a.MonthToDateAmount,
                                  RemainingCredit = a.RemainingCredit,
                                  Type = a.Type,
                                  YearToDateAmount = a.YearToDateAmount,
                                  AccountBalance = a.AccountBalance,
                                  Company = a.travelagency.Name,
                                  GracePeriod = a.GracePeriod,
                                  MonthlyInterestRate = a.MonthlyInterestRate,
                                  PaymentTypeID = (PaymentType)a.PaymentTypeID,
                                  Address = a.Address,
                                  MarketTypeID = a.MarketTypeID,
                                  Location = a.Location,
                                  TravelAgencyID = a.TravelAgencyID

                              }).OrderBy(x => propertyInfo.GetValue(x, null), sortDirec).ToList<AgentsModel>();


                if (unique)
                {
                    result = result.DistinctBy(x => x.TravelAgencyID).ToList();
                }
                return result;
            }
        }



        public AgentsModel Create(AgentsModel agent, string newPassword)
        {
            using (isebookingEntities db = new isebookingEntities())
            {
                Mailer mailer = new Mailer();

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        if (!IsValidEmail(agent.Email))
                        {
                            throw new Exception("Invalid email address: " + agent.Email);
                        }


                        var newtravelAgency = db.travelagencies.Where(x => x.ID == agent.TravelAgencyID).FirstOrDefault();

                        if (newtravelAgency == null)
                        {
                            newtravelAgency = new travelagency();
                            newtravelAgency.DateCreated = DateTime.Now;
                            newtravelAgency.DateLastUpdated = DateTime.Now;
                            newtravelAgency.Name = agent.Company;
                            newtravelAgency.IsActive = true;
                            db.travelagencies.Add(newtravelAgency);
                            db.SaveChanges();
                        }





                        var newAgent = new agent();
                        newAgent.ContactNumber = agent.ContactNumber;
                        newAgent.CreditLimit = agent.CreditLimit;
                        newAgent.DateCreated = DateTime.Now;
                        newAgent.DateLastUpdated = DateTime.Now;
                        newAgent.Email = agent.Email;
                        newAgent.FirstName = agent.FirstName;
                        // newAgent.ID = agent.ID;
                        newAgent.IsActive = agent.IsActive;
                        newAgent.LastName = agent.LastName;
                        newAgent.MiddleName = Getsha256(newPassword);
                        newAgent.MonthToDateAmount = agent.MonthToDateAmount;
                        newAgent.Password = BCrypt.Net.BCrypt.HashPassword(newPassword);
                        newAgent.RemainingCredit = agent.CreditLimit;
                        newAgent.Type = agent.Type;
                        newAgent.YearToDateAmount = 0;
                        newAgent.PaymentTypeID = (int)agent.PaymentTypeID;
                        newAgent.Company = newtravelAgency.Name;
                        newAgent.Address = agent.Address;
                        newAgent.MarketTypeID = agent.MarketTypeID;
                        newAgent.Location = agent.Location;
                        newAgent.TravelAgencyID = newtravelAgency.ID;
                        db.agents.Add(newAgent);


                        db.SaveChanges();
                        agent.ID = newAgent.ID;
                        agent.RemainingCredit = agent.CreditLimit;


                        transaction.Commit();



                        var mailResult = mailer.SendSimpleMessage(newAgent.Email, "Welcome to ISE!", "Welcome to ISE!");

                        return agent;

                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();

                        Exception ex = dbU.GetBaseException();

                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {


                        transaction.Rollback();

                        string errorMessages = null;

                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }

                        throw new Exception(errorMessages);

                    }

                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }
            }

        }


        public AgentsModel Update(AgentsModel agent, string oldPassword, string newPassword)
        {
            using (isebookingEntities db = new isebookingEntities())
            {


                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        var updateAgent = db.agents.Where(x => x.ID == agent.ID).FirstOrDefault();

                        if (updateAgent == null) throw new Exception("Agent does not exist!");


                        // if (updateAgent.Password != oldPassword) throw new Exception("Password does not match!");


                        if (!IsValidEmail(agent.Email))
                        {
                            throw new Exception("Invalid email address: " + agent.Email);
                        }


                         if (!BCrypt.Net.BCrypt.Verify(oldPassword, updateAgent.Password)) throw new Exception("Invalid username/password!");


                        var newtravelAgency = db.travelagencies.Where(x => x.ID == agent.TravelAgencyID).FirstOrDefault();

                        if (newtravelAgency == null)
                        {
                            newtravelAgency = new travelagency();
                            newtravelAgency.DateCreated = DateTime.Now;
                            newtravelAgency.DateLastUpdated = DateTime.Now;
                            newtravelAgency.Name = agent.Company;
                            newtravelAgency.IsActive = true;
                            db.travelagencies.Add(newtravelAgency);
                            db.SaveChanges();
                        }

                        updateAgent.ContactNumber = agent.ContactNumber;

                        updateAgent.DateLastUpdated = DateTime.Now;
                        updateAgent.Email = agent.Email;
                        updateAgent.FirstName = agent.FirstName;
                        updateAgent.IsActive = agent.IsActive;
                        updateAgent.LastName = agent.LastName ?? "";
                        updateAgent.MiddleName = Getsha256(newPassword);
                         updateAgent.Password = BCrypt.Net.BCrypt.HashPassword(newPassword);
                        updateAgent.RemainingCredit = updateAgent.RemainingCredit + (agent.CreditLimit - updateAgent.CreditLimit);
                        updateAgent.CreditLimit = agent.CreditLimit;
                        updateAgent.Type = agent.Type;
                        updateAgent.PaymentTypeID = (int)agent.PaymentTypeID;
                        updateAgent.Company = newtravelAgency.Name;
                        updateAgent.Address = agent.Address;
                        updateAgent.MarketTypeID = agent.MarketTypeID;
                        updateAgent.Location = agent.Location;
                        updateAgent.TravelAgencyID = newtravelAgency.ID;
                        db.SaveChanges();

                        transaction.Commit();
                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();

                        Exception ex = dbU.GetBaseException();

                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {


                        transaction.Rollback();

                        string errorMessages = null;

                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }

                        throw new Exception(errorMessages);

                    }

                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }


                return agent;
            }

        }

        public List<AgentsModel> GetAccountingPersons()
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                // LINQ TO SQL SORTING BUG
                var propertyInfo = typeof(AgentsModel).GetProperty("Company");
                OrderByDirection sortDirec = OrderByDirection.Ascending;

               return (from a in db.agents
                              where a.IsActive == true
                              && a.Type == 3
                              select new AgentsModel
                              {
                                  ContactNumber = a.ContactNumber,
                                  CreditLimit = a.CreditLimit,
                                  DateCreated = a.DateCreated,
                                  DateLastUpdated = a.DateLastUpdated,
                                  Email = a.Email,
                                  FirstName = a.FirstName,
                                  ID = a.ID,
                                  IsActive = a.IsActive,
                                  LastName = a.LastName,
                                  MonthToDateAmount = a.MonthToDateAmount,
                                  RemainingCredit = a.RemainingCredit,
                                  Type = a.Type,
                                  YearToDateAmount = a.YearToDateAmount,
                                  AccountBalance = a.AccountBalance,
                                  Company = a.travelagency.Name,
                                  GracePeriod = a.GracePeriod,
                                  MonthlyInterestRate = a.MonthlyInterestRate,
                                  PaymentTypeID = (PaymentType)a.PaymentTypeID,
                                  Address = a.Address,
                                  MarketTypeID = a.MarketTypeID,
                                  Location = a.Location,
                                  TravelAgencyID = a.TravelAgencyID

                              }).OrderBy(x => propertyInfo.GetValue(x, null), sortDirec).ToList<AgentsModel>();



               
            }
        }


        public bool Deactivate(AgentsModel agent)
        {
            using (isebookingEntities db = new isebookingEntities())
            {


                var updateAgent = db.agents.Where(x => x.ID == agent.ID).FirstOrDefault();

                if (updateAgent == null) throw new Exception("Agent does not exist!");


                updateAgent.IsActive = false;
                db.SaveChanges();
                return true;
            }
        }


        public List<TravelAgencyModel> GetTravelAgencies()
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                return (from a in db.travelagencies
                        select new TravelAgencyModel
                        {
                            ID = a.ID,
                            DateCreated = a.DateCreated,
                            DateLastUpdated = a.DateLastUpdated,
                            IsActive = a.IsActive,
                            Name = a.Name

                        }).ToList<TravelAgencyModel>();

            }
        }

        public List<AgentsModel> GetCoordinators(DateTime lastSyncDate,Boolean includeCredentials=false)
        {
            using (var db = new isebookingEntities())
            {
                var propertyInfo = typeof(AgentsModel).GetProperty("Company");
                OrderByDirection sortDirec = OrderByDirection.Ascending;

                var result = (from a in db.agents
                              where 
                             
                              a.DateLastUpdated >= lastSyncDate
                              && a.Type == 5
                              select new AgentsModel
                              {
                                  ContactNumber = a.ContactNumber,
                                  CreditLimit = a.CreditLimit,
                                  DateCreated = a.DateCreated,
                                  DateLastUpdated = a.DateLastUpdated,
                                  Email = a.Email,
                                  FirstName = a.FirstName,
                                  ID = a.ID,
                                  IsActive = a.IsActive,
                                  LastName = a.LastName,
                                  MonthToDateAmount = a.MonthToDateAmount,
                                  RemainingCredit = a.RemainingCredit,
                                  Type = a.Type,
                                  YearToDateAmount = a.YearToDateAmount,
                                  AccountBalance = a.AccountBalance,
                                  Company = a.travelagency.Name,
                                  GracePeriod = a.GracePeriod,
                                  MonthlyInterestRate = a.MonthlyInterestRate,
                                  PaymentTypeID = (PaymentType)a.PaymentTypeID,
                                  Address = a.Address,
                                  MarketTypeID = a.MarketTypeID,
                                  Location = a.Location,
                                  TravelAgencyID = a.TravelAgencyID,
                                  Password = includeCredentials==false?"":a.Password

                              }).OrderBy(x => propertyInfo.GetValue(x, null), sortDirec).ToList<AgentsModel>();


                
                return result;
            }
        }


        public List<AuthCodes> GetAuthCodes(int travelAgencyID)
        {
            using(var db = new isebookingEntities())
            {
                var result = (from a in db.authorizationcodes
                              where a.TravelAgencyID == travelAgencyID && a.Status==0
                              select new AuthCodes
                              {
                                  Code=a.AuthCode,
                                  DateCreated =a.DateCreated,
                                  TravelAgencyID=0,
                                  Status=a.Status

                              }).ToList();

                return result;
            }
        }


        private string Getsha256(string randomString)
        {
            var crypt = new System.Security.Cryptography.SHA256Managed();
            var hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }

        public void SyncUsersToMobile()
        {
            using(var db = new isebookingEntities())
            {
                int affectedRows = db.Database.SqlQuery<int>("insert into isemobile.`user`(ID,`username`,`password`,`complete_name`,`status`,`agency_id`) (SELECT a.ID, a.Email, a.MiddleName, CONCAT(a.FirstName, ' ', a.LastName), a.IsActive,a.TravelAgencyID FROM agents a WHERE Type = 5) ON DUPLICATE KEY UPDATE isemobile.`user`.`status` = a.IsActive; SELECT ROW_COUNT()").FirstOrDefault();

                int auths = db.Database.SqlQuery<int>("INSERT INTO isemobile.auth_code(user_id,auth_code) (SELECT auth.TravelAgencyID,auth.AuthCode FROM authorizationcodes auth WHERE auth.`Status`=0) ON DUPLICATE KEY UPDATE auth_code=auth.AuthCode; SELECT ROW_COUNT();").FirstOrDefault();

                int vehicles = db.Database.SqlQuery<int>("INSERT INTO isemobile.vehicle(name) (SELECT f.VIDNumber FROM fleet f ) ON DUPLICATE KEY UPDATE name=f.VIDNumber; SELECT ROW_COUNT();").FirstOrDefault();
            }
        }
    }

   public class AuthCodes
    {
        public string Code { get; set; }
        public DateTime DateCreated { get; set; }
        public int TravelAgencyID { get; set; }
        public int Status { get; set; }
    }

}
