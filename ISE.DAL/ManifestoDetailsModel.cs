﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
   public class ManifestoDetailsModel
    {
        public int ID { get; set; }
        public int ManifestoID { get; set; }
        public string GuestName { get; set; }
    }
}
