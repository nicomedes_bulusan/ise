//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISE.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class paymentlog
    {
        public int ID { get; set; }
        public int BillingPeriodID { get; set; }
        public System.DateTime PaymentDate { get; set; }
        public int PostedBy { get; set; }
        public string Remarks { get; set; }
        public double Amount { get; set; }
    
        public virtual agent agent { get; set; }
        public virtual billingperiod billingperiod { get; set; }
    }
}
