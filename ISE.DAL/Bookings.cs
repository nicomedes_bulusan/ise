﻿using HashidsNet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MoreLinq;
using System.Data.Entity;

namespace ISE.DAL
{
    public enum ManifestoType
    {
        ARRIVAL = 1,
        DEPARTURE = 2
    }

    public enum ManifestoStatus
    {
        UNKNOWN = 0,
        OPEN = 1,
        POSTED = 2,
    }

    public enum ClassRate
    {
        REGULAR = 1,
        SPECIAL = 2,
        UPHILL = 3
    }

    public enum SalesSource
    {
        TRANSFER = 1,
        ENVIRONMENT_FEE = 2,
        BLTMPC = 3,
        TERMINAL_FEE = 4,
        NIGHT_NAVIGATION = 5,
        OTHERS = 6
    }

    public enum AgentType
    {
        AGENT = 1,
        BACKOFFICE = 2,
        ACCOUNTING = 3,
        MANIFESTO = 4,
        COORDINATOR = 5
    }

    public class Bookings
    {
        public BookingHeaderModel Header { get; set; }
        public List<BookingDetailsModel> Details { get; set; }

        public List<BookingAttachmentsModel> Attachments { get; set; }

        public List<BookingOtherChargesIndicatorModel> OtherCharges { get; set; }

        public List<ManifestoModel> Manifesto { get; set; }

        private IMapper mapper;

        public Bookings()
        {
            var newConfig = new MapperConfiguration(cfg =>
              {
                  cfg.CreateMap<agent, AgentsModel>();
                  cfg.CreateMap<manifestodetail, ManifestoDetailsModel>();
                  cfg.CreateMap<bookinglog, BookingLogsModel>()
                      .ForMember(dest => dest.Agent, conf => conf.MapFrom(o => o.agent));
                  cfg.CreateMap<booking, BookingHeaderModel>()
                      .ForMember(dest => dest.ResortHotel1, conf => conf.MapFrom(o => o.resortsandhotel.Name))
                       .ForMember(dest => dest.ResortHotel2, conf => conf.MapFrom(o => o.resortsandhotel1.Name))
                      .ForMember(dest => dest.Agency, conf => conf.MapFrom(o => o.agent))
                      .ForMember(dest => dest.Logs, conf => conf.MapFrom(o => o.bookinglogs));
                  cfg.CreateMap<manifesto, ManifestoModel>()
                      .ForMember(dest => dest.Details, conf => conf.MapFrom(o => o.manifestodetails))
                      .ForMember(dest => dest.BookingHeader, conf => conf.MapFrom(o => o.booking));



              });

            mapper = newConfig.CreateMapper();

        }
        public string[] OCs { get; set; }

        public Bookings CreateBooking(Bookings book, AgentsModel agent)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        booking newBook = new booking();
                        if (book.Header.ID > 0)
                            newBook = (from a in db.bookings where a.ID == book.Header.ID select a).FirstOrDefault();




                        newBook.Adults = book.Header.Adults;

                        newBook.Childrens = book.Header.Childrens;
                        newBook.ChildrensBelowSix = book.Header.ChildrensBelowSix;
                        newBook.Senior = book.Header.Senior;
                        newBook.ContactNumber = book.Header.ContactNumber == null ? "" : book.Header.ContactNumber;
                        newBook.ContactPerson = book.Header.ContactPerson ?? " ";


                        int people = book.Header.FOC + book.Header.Adults + book.Header.Senior + book.Header.Childrens + book.Header.ChildrensBelowSix;

                        if (people <= 0) throw new Exception("Please indicate number of pax for this booking!");

                        newBook.CreatedBy = book.Header.CreatedBy;
                        if (book.Header.CreatedBy == 0)
                        {
                            newBook.CreatedBy = agent.ID;
                            newBook.TravelAgencyID = agent.TravelAgencyID;
                        }
                        else
                        {
                            var agentef = db.agents.Where(x => x.ID == book.Header.CreatedBy).FirstOrDefault();
                            if (agentef != null)
                            {
                                newBook.CreatedBy = agentef.ID;
                                newBook.TravelAgencyID = agentef.TravelAgencyID;
                            }
                        }

                        newBook.DropOffDeparture = book.Header.DropOffDeparture;
                        newBook.DropOffArrival = book.Header.DropOffArrival;
                        newBook.DropOffLocation = book.Header.DropOffLocation;
                        newBook.Escorts = book.Header.Escorts;
                        newBook.FlightDate1 = book.Header.FlightDate1;
                        newBook.FlightDate2 = book.Header.FlightDate2;
                        newBook.FlightNo1 = book.Header.FlightNo1;
                        newBook.FlightNo2 = book.Header.FlightNo2;
                        newBook.FOC = book.Header.FOC;
                        newBook.GroupName = book.Header.GroupName;
                        newBook.Infants = book.Header.Infants;
                        newBook.PickUpArrival = book.Header.PickUpArrival;
                        newBook.PickUpDeparture = book.Header.PickUpDeparture;
                        newBook.Remarks = book.Header.Remarks == null ? " " : book.Header.Remarks;
                        newBook.Routing = book.Header.Routing;
                        newBook.Status = (int)book.Header.Status; //(int)BookingStatus.SAVED;
                        newBook.Total = (float)book.Details.Sum(x => (x.Amount + x.Boat) * x.Quantity);
                        newBook.TourGuides = book.Header.TourGuides;

                        newBook.DateLastUpdated = DateTime.Now;
                        newBook.DropOffLocation1 = book.Header.DropOffLocation1;
                        newBook.DropOffLocation2 = book.Header.DropOffLocation2;
                        newBook.PaymentTypeID = (int)book.Header.PaymentTypeID;
                        newBook.Class = (int)book.Header.Class;
                        //newBook.TravelAgencyID = book.Header.TravelAgencyID;
                        newBook.Class2 = book.Header.Class2;
                        newBook.OnlineBookingOrderID = book.Header.OnlineBookingOrderID;

                        if ((newBook.Status == (int)BookingStatus.SAVED || newBook.Status == (int)BookingStatus.SUBMITTED) && newBook.Creator == null)
                        {
                            try
                            {
                                newBook.Creator = agent.ID;
                            }
                            catch {
                                newBook.Creator = 1;
                            }
                            newBook.CreationDate = DateTime.Now;
                        }


                        if (book.Header.ID == 0)
                        {
                            newBook.DateCreated = DateTime.Now;
                            db.bookings.Add(newBook);
                        }


                        db.SaveChanges();


                        if (newBook.Status == (int)BookingStatus.OVERRIDE)
                        {
                            newBook.Status = (int)BookingStatus.FINALIZED;
                            OverrideBooking(db, newBook, agent);
                        }


                        var log = new bookinglog();
                        log.AgentID = agent.ID;
                        log.BookingID = newBook.ID;
                        log.DateCreated = DateTime.Now;
                        log.Status = (int)book.Header.Status;
                        db.bookinglogs.Add(log);
                        db.SaveChanges();


                        if (book.Header.ID > 0)
                        {
                            var bd = db.bookingdetails.Where(x => x.BookingID == book.Header.ID).ToList<bookingdetail>();
                            db.bookingdetails.RemoveRange(bd);
                            var ba = db.bookingattachments.Where(x => x.BookingID == book.Header.ID).ToList<bookingattachment>();
                            db.bookingattachments.RemoveRange(ba);
                            var bo = db.bookingotherchargesindicators.Where(x => x.BookingID == book.Header.ID).ToList<bookingotherchargesindicator>();
                            db.bookingotherchargesindicators.RemoveRange(bo);
                            var bos = db.bookingotherchargesselecteds.Where(x => x.BookingID == book.Header.ID).ToList<bookingotherchargesselected>();
                            db.bookingotherchargesselecteds.RemoveRange(bos);
                            db.SaveChanges();
                        }
                        foreach (var detail in book.Details.Where(x => x.Quantity > 0))
                        {
                            bookingdetail bookDetails = new bookingdetail();
                            bookDetails.Amount = detail.Amount;
                            bookDetails.BookingID = newBook.ID;
                            bookDetails.Description = detail.Description;
                            bookDetails.Quantity = detail.Quantity;
                            bookDetails.Boat = detail.Boat;
                            bookDetails.SourceID = detail.SourceID;
                            db.bookingdetails.Add(bookDetails);
                        }

                        if (book.Attachments != null)
                        {
                            foreach (var attachment in book.Attachments)
                            {
                                bookingattachment bookAttachment = new bookingattachment();
                                bookAttachment.BookingID = newBook.ID;
                                bookAttachment.Filename = attachment.Filename;
                                bookAttachment.DateCreated = DateTime.Now;
                                bookAttachment.MimeType = attachment.MimeType;
                                bookAttachment.OriginalFilename = attachment.OriginalFilename;
                                db.bookingattachments.Add(bookAttachment);

                            }
                        }

                        if (book.OtherCharges != null)
                        {
                            foreach (var otherCharges in book.OtherCharges)
                            {
                                bookingotherchargesindicator chargeIndicator = new bookingotherchargesindicator();

                                if (otherCharges.IsChecked)
                                {
                                    chargeIndicator.BookingID = newBook.ID;
                                    chargeIndicator.ChargeID = otherCharges.ChargingID;
                                    db.bookingotherchargesindicators.Add(chargeIndicator);
                                }


                            }

                        }
                        List<bookingotherchargesselected> bocssList = new List<bookingotherchargesselected>();
                        if (book.OCs != null)
                        {
                            foreach (var oc in book.OCs)
                            {
                                bookingotherchargesselected bocss = new bookingotherchargesselected();
                                bocss.BookingID = newBook.ID;
                                bocss.OCID = oc;
                                bocssList.Add(bocss);
                            }

                            db.bookingotherchargesselecteds.AddRange(bocssList);
                        }



                        db.SaveChanges();
                        transaction.Commit();

                        book.Header.ID = newBook.ID;

                        return book;
                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();

                        Exception ex = dbU.GetBaseException();

                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {


                        transaction.Rollback();

                        string errorMessages = null;

                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }

                        throw new Exception(errorMessages);

                    }

                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }
            }
        }



        private bool OverrideBooking(isebookingEntities db, booking book, AgentsModel agent)
        {

            if (book == null) throw new Exception("Booking does not exist!");

            if (agent.Type != 3 && (book.Status == (int)BookingStatus.FINALIZED)) throw new Exception("ERROR! You are not authorized to OVERRIDE this booking!");


            db.SaveChanges();

            var acctAR = book.accountreceivables.Where(x => x.OriginalAmount >= 0).OrderByDescending(x => x.ID).FirstOrDefault();

            if (acctAR != null)
            {

                if (acctAR.BillingPeriodID != null) throw new Exception("ERROR! This booking has been billed! Please see billing statement # " + acctAR.BillingPeriodID.ToString());

                var reverseAR = new accountreceivable();
                reverseAR.AgentID = acctAR.AgentID;
                reverseAR.Balance = acctAR.Balance * -1;
                reverseAR.OriginalAmount = acctAR.OriginalAmount * -1;
                reverseAR.TransactionDate = DateTime.Now;
                reverseAR.DueDate = acctAR.DueDate;
                reverseAR.BookingID = acctAR.BookingID;
                reverseAR.BillingPeriodID = null;

                db.accountreceivables.Add(reverseAR);
                db.SaveChanges();
            }


            //CHECK IF THERE IS AN OPEN/VERIFIED PAYMENT
            var paymentData = db.journalentries.Where(x => x.BookingID == book.ID && (x.payment.Status != (int)PaymentStatus.CANCELLED)).ToList();


            if (paymentData != null)
            {
                if (paymentData.Count() > 0)
                {
                    string paymentNos = String.Join(",", paymentData.Select(x => x.payment.PaymentNo).ToList().ToArray());

                    throw new Exception("ERROR! There is an exisiting payment for this booking! Please cancel the payment No: " + paymentNos + " from Collections Entry before overriding this booking.");
                }
            }

            var salesData = book.sales.Where(x => x.Total >= 0).FirstOrDefault();

            if (salesData != null)
            {
                var reverseSales = new sale();
                reverseSales.AgentID = salesData.AgentID;
                reverseSales.BookingID = salesData.BookingID;
                reverseSales.TransactionDate = DateTime.Now;
                reverseSales.Type = salesData.Type;
                reverseSales.SalesTax = salesData.SalesTax * -1;
                reverseSales.Total = salesData.Total * -1;
                reverseSales.Comment = "Transaction OVERRIDE by  " + agent.Fullname;
                reverseSales.ReferenceNumber = salesData.ReferenceNumber;

                db.sales.Add(reverseSales);
                db.SaveChanges();
            }

            var log = new bookinglog();
            log.AgentID = agent.ID;
            log.BookingID = book.ID;
            log.DateCreated = DateTime.Now;
            log.Status = book.Status;
            db.bookinglogs.Add(log);
            db.SaveChanges();



            int paymentType = book.PaymentTypeID;
            var chargeTo = db.agents.Where(x => x.ID == book.CreatedBy).FirstOrDefault();
            if (paymentType == (int)PaymentType.FOR_BILLING)
            {
                if (chargeTo == null) throw new Exception("Unable to determine the agency to be charge for this booking!");
                var newAR = new accountreceivable();
                // newAR.AgentID = (int)book.CreatedBy;
                newAR.BookingID = book.ID;
                newAR.TransactionDate = DateTime.Now;
                newAR.AgentID = chargeTo.ID;
                newAR.DueDate = DateTime.Now.AddDays(chargeTo.GracePeriod);
                newAR.Balance = (double)book.Total;
                newAR.OriginalAmount = (double)book.Total;
                newAR.BillingPeriodID = (acctAR != null) ? acctAR.BillingPeriodID : null;

                db.accountreceivables.Add(newAR);
                db.SaveChanges();
            }




            var newSales = new sale();
            newSales.AgentID = agent.ID;
            newSales.BookingID = book.ID;
            newSales.TransactionDate = DateTime.Now;
            newSales.Type = paymentType == 1 ? (int)SalesType.CREDIT : (int)SalesType.DEBIT;
            newSales.Total = (double)book.Total;
            db.sales.Add(newSales);


            return true;
        }

        public IEnumerable<BookingHeaderModel> GetBookings(AgentsModel agent)
        {
            using (isebookingEntities db = new isebookingEntities())
            {
                var result = db.bookings.Where(a => (agent.Type == 2 ? true : a.CreatedBy == agent.ID)).ToList();

                return mapper.Map<List<booking>, List<BookingHeaderModel>>(result);
            }
        }

        public BookingSummaryModel GetBookingSummary(AgentsModel agent)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                var numberOfBookings = db.bookings.Where(x => x.CreatedBy == agent.ID).Count();
                double total = (from a in db.bookings
                                join b in db.bookingdetails.GroupBy(x => x.BookingID).Select(x => new { BookingID = x.Key, SumAmtBook = x.Sum(w => w.Amount + w.Boat) }) on a.ID equals b.BookingID
                                where a.CreatedBy == agent.ID
                                select new { b.SumAmtBook, a.CreatedBy }
                               ).GroupBy(x => x.CreatedBy).Select(x => new { amt = x.Sum(y => y.SumAmtBook) }).FirstOrDefault().amt;



                var numberOfConfirmedBookings = db.bookings.Where(x => x.CreatedBy == agent.ID && x.Status == (int)BookingStatus.CONFIRMED).Count();

                var numberOfFinalizedBookings = db.bookings.Where(x => x.CreatedBy == agent.ID && x.Status == (int)BookingStatus.FINALIZED).Count();

                var numberOfCancelledBookings = db.bookings.Where(x => x.CreatedBy == agent.ID && x.Status == (int)BookingStatus.CANCELLED).Count();

                var bookingSummary = (from a in db.agents
                                      where a.ID == agent.ID
                                      select new BookingSummaryModel
                                      {
                                          Bookings = numberOfBookings,
                                          Cancelled = numberOfCancelledBookings,
                                          Confirmed = numberOfConfirmedBookings,
                                          ContactNumber = a.ContactNumber,
                                          CreditLimit = a.CreditLimit,
                                          DateCreated = a.DateCreated,
                                          DateLastUpdated = a.DateLastUpdated,
                                          Email = a.Email,
                                          Finalized = numberOfFinalizedBookings,
                                          FirstName = a.FirstName,
                                          ID = a.ID,
                                          IsActive = a.IsActive,
                                          LastName = a.LastName,
                                          MonthToDateAmount = a.MonthToDateAmount,
                                          RemainingCredit = a.RemainingCredit - total,
                                          YearToDateAmount = a.RemainingCredit,
                                          AccountBalance = a.AccountBalance
                                      }).FirstOrDefault();

                return bookingSummary;

            }

        }

        public Bookings GetBooking(int bookingID)
        {



            using (isebookingEntities db = new isebookingEntities())
            {
                Bookings book = new Bookings();

                var bookRef = db.bookings.Where(x => x.ID == bookingID).FirstOrDefault();

                if (bookRef == null) throw new Exception("Booking #" + bookingID + " does not exists!");

                var bookingHeader = mapper.Map<booking, BookingHeaderModel>(bookRef);



                var bookingDetails = (from a in db.bookingdetails
                                      where a.BookingID == bookingID
                                      select new BookingDetailsModel
                                      {
                                          Amount = a.Amount,
                                          BookingID = a.BookingID,
                                          Description = a.Description,
                                          ID = a.ID,
                                          Quantity = a.Quantity,
                                          Boat = a.Boat
                                      }).ToList<BookingDetailsModel>();


                var bookingAttachments = (from a in db.bookingattachments
                                          where a.BookingID == bookingID
                                          select new BookingAttachmentsModel
                                          {
                                              BookingID = a.BookingID,
                                              DateCreated = a.DateCreated,
                                              Filename = a.Filename,
                                              ID = a.ID,
                                              MimeType = a.MimeType,
                                              OriginalFilename = a.OriginalFilename
                                          }).ToList<BookingAttachmentsModel>();

                var bookingOtherCharges = (from a in db.othercharges
                                           join b in db.bookingotherchargesindicators on new { ID = a.ID, BookingID = bookingID } equals new { ID = b.ChargeID, BookingID = b.BookingID } into charge
                                           from bb in charge.DefaultIfEmpty()
                                               //where bb.BookingID == bookingID
                                           select new BookingOtherChargesIndicatorModel
                                           {
                                               Amount = bb == null ? 0 : a.Amount,
                                               BookingID = bb == null ? 0 : bb.BookingID,
                                               ChargeDescription = a.Description,
                                               ChargingID = bb == null ? 0 : bb.ChargeID,
                                               ID = bb == null ? 0 : bb.ID,
                                               IsChecked = bb == null ? false : true
                                           }).ToList<BookingOtherChargesIndicatorModel>();

                var bookingOtherChargesSelected = (from a in db.bookingotherchargesselecteds
                                                   where a.BookingID == bookingID
                                                   select a.OCID
                                                  ).ToArray<string>();
               


                var manifestoRef = db.manifestos.Where(x => x.BookingID == bookingID).ToList();
                var manifestoList = new List<ManifestoModel>();
                if (manifestoRef != null)
                {
                    manifestoList = mapper.Map<List<manifesto>, List<ManifestoModel>>(manifestoRef);
                }




                foreach (var man in manifestoList)
                {
                    man.Guests = string.Join(Environment.NewLine, man.Details.Select(x => x.GuestName).ToArray());
                }
                book.Manifesto = manifestoList;
                book.Attachments = bookingAttachments;
                book.Details = bookingDetails;
                book.Header = bookingHeader;
                book.OtherCharges = bookingOtherCharges;
                book.OCs = bookingOtherChargesSelected;
               

                return book;
            }


        }

        public Bookings SaveManifesto(Bookings book, AgentsModel agent)
        {
            using (isebookingEntities db = new isebookingEntities())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        booking newBook = (from a in db.bookings where a.ID == book.Header.ID select a).FirstOrDefault();

                        if (newBook == null) throw new Exception("Booking does not exist!");


                        if (agent.Type == 1) throw new Exception("You are not authorized to confirm this booking!");


                        if (book.Attachments != null)
                        {
                            foreach (var attachment in book.Attachments)
                            {
                                bookingattachment bookAttachment = new bookingattachment();
                                bookAttachment.BookingID = newBook.ID;
                                bookAttachment.Filename = attachment.Filename;
                                bookAttachment.DateCreated = DateTime.Now;
                                bookAttachment.MimeType = attachment.MimeType;
                                bookAttachment.OriginalFilename = attachment.OriginalFilename;
                                db.bookingattachments.Add(bookAttachment);

                            }
                        }

                        if (book.Manifesto == null || book.Manifesto.Count == 0) throw new Exception("Please add manifesto!");


                        db.manifestos.RemoveRange(newBook.manifestos);
                        db.SaveChanges();

                        foreach (var manifestoRef in book.Manifesto)
                        {
                            if (manifestoRef.Guests == null) manifestoRef.Guests = "";

                            var manifestoDetails = manifestoRef.Guests.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);

                            if (manifestoDetails == null || manifestoDetails.Length == 0) throw new Exception("Please add manifesto guests!");
                            var manifestoNew = new manifesto();

                            if (manifestoRef.ResortHotel == 0) throw new Exception("Please select resort/hotel!");

                            if (manifestoRef.Pickup == 0) throw new Exception("Please select pickup location!");

                            if (manifestoRef.DropOff == 0) throw new Exception("Please select dropoff location!");

                            manifestoNew.Date = manifestoRef.Date;
                            manifestoNew.DateLastUpdated = DateTime.Now;
                            manifestoNew.Type = manifestoRef.Type;
                            //manifestoNew.Pax = manifestoRef.Adults + manifestoRef.Kids + manifestoRef.Infants;
                            manifestoNew.Remarks = manifestoRef.Remarks;
                            manifestoNew.Pickup = manifestoRef.Pickup;
                            manifestoNew.DropOff = manifestoRef.DropOff;
                            manifestoNew.ResortHotel = manifestoRef.ResortHotel;
                            manifestoNew.Adults = manifestoRef.Adults;
                            manifestoNew.Kids = manifestoRef.Kids;
                            manifestoNew.Infants = manifestoRef.Infants;
                            manifestoNew.FlightNo = manifestoRef.FlightNo;
                            manifestoNew.Pax = manifestoRef.Adults + manifestoRef.Kids + manifestoRef.Infants + manifestoRef.Escorts + manifestoRef.TourGuides + manifestoRef.FOC;
                            manifestoNew.Escorts = manifestoRef.Escorts;
                            manifestoNew.TourGuides = manifestoRef.TourGuides;
                            manifestoNew.FOC = manifestoRef.FOC;
                            manifestoNew.BookingID = newBook.ID;
                            manifestoNew.DateCreated = DateTime.Now;

                            if (manifestoNew.Pax == 0) continue;


                            db.manifestos.Add(manifestoNew);
                            db.SaveChanges();


                            var details = db.manifestodetails.Where(x => x.ManifestoID == manifestoNew.ID).ToList();
                            if (details != null)
                            {
                                db.manifestodetails.RemoveRange(details);
                                db.SaveChanges();
                            }

                            foreach (var guest in manifestoDetails)
                            {
                                var newGuest = new manifestodetail();
                                newGuest.GuestName = guest;
                                newGuest.ManifestoID = manifestoNew.ID;
                                db.manifestodetails.Add(newGuest);
                                db.SaveChanges();
                            }
                        }




                        db.SaveChanges();
                        transaction.Commit();

                        return book;
                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();

                        Exception ex = dbU.GetBaseException();

                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();
                        string errorMessages = null;
                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }

                        throw new Exception(errorMessages);

                    }

                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }
            }
        }

        public bool Confirm(int bookingID, AgentsModel agent)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var book = db.bookings.Where(x => x.ID == bookingID).FirstOrDefault();

                        if (book == null) throw new Exception("Booking does not exist!");
                        if (agent.Type == 1) throw new Exception("You are not authorized to confirm this booking!");
                        book.Status = (int)BookingStatus.CONFIRMED;
                        if (book.ConfirmedBy == null)
                        {
                            book.ConfirmedBy = agent.ID;
                            book.ConfirmationDate = DateTime.Now;
                        }
                        db.SaveChanges();



                        var log = new bookinglog();
                        log.AgentID = agent.ID;
                        log.BookingID = book.ID;
                        log.DateCreated = DateTime.Now;
                        log.Status = book.Status;
                        db.bookinglogs.Add(log);
                        db.SaveChanges();



                        var manifestos = db.manifestos.Where(x => x.BookingID == bookingID).ToList();

                        if (manifestos.Count() > 0)
                        {
                            db.manifestos.RemoveRange(manifestos);
                            db.SaveChanges();
                        }


                        if (book.DropOffArrival != null && book.PickUpArrival != null && book.DropOffLocation1 != null)
                        {
                            var newManifesto = new manifesto();

                            newManifesto.Adults = book.Adults + book.Senior;
                            newManifesto.BookingID = book.ID;
                            newManifesto.Date = (DateTime)book.FlightDate1;
                            newManifesto.Pickup = (int)book.PickUpArrival;
                            newManifesto.DropOff = (int)book.DropOffArrival;
                            newManifesto.ResortHotel = (int)book.DropOffLocation1;
                            newManifesto.Kids = book.Childrens + book.ChildrensBelowSix;
                            newManifesto.Infants = book.Infants;
                            newManifesto.Type = 1;

                            newManifesto.DateCreated = DateTime.Now;
                            newManifesto.DateLastUpdated = DateTime.Now;
                            newManifesto.FlightNo = book.FlightNo1;
                            newManifesto.Pax = newManifesto.Adults + newManifesto.Kids + newManifesto.Infants + newManifesto.Escorts + newManifesto.TourGuides + newManifesto.FOC;
                            newManifesto.Remarks = book.Remarks;
                            newManifesto.Escorts = book.Escorts;
                            newManifesto.TourGuides = book.TourGuides;
                            newManifesto.FOC = book.FOC;
                            db.manifestos.Add(newManifesto);
                            db.SaveChanges();

                        }


                        if (book.DropOffDeparture != null && book.PickUpDeparture != null && book.DropOffLocation2 != null)
                        {
                            var newManifesto = new manifesto();
                            newManifesto.Adults = book.Adults + book.Senior;
                            newManifesto.BookingID = book.ID;
                            newManifesto.Date = (DateTime)book.FlightDate2;
                            newManifesto.Pickup = (int)book.PickUpDeparture;
                            newManifesto.DropOff = (int)book.DropOffDeparture;
                            newManifesto.ResortHotel = (int)book.DropOffLocation2;
                            newManifesto.Kids = book.Childrens + book.ChildrensBelowSix;
                            newManifesto.Infants = book.Infants;
                            newManifesto.Type = 2;
                            newManifesto.DateCreated = DateTime.Now;
                            newManifesto.DateLastUpdated = DateTime.Now;
                            newManifesto.FlightNo = book.FlightNo2;
                            newManifesto.Pax = newManifesto.Adults + newManifesto.Kids + newManifesto.Infants + newManifesto.Escorts + newManifesto.TourGuides + newManifesto.FOC;
                            newManifesto.Remarks = book.Remarks;

                            newManifesto.Escorts = book.Escorts;
                            newManifesto.TourGuides = book.TourGuides;
                            newManifesto.FOC = book.FOC;

                            db.manifestos.Add(newManifesto);
                            db.SaveChanges();

                        }

                        transaction.Commit();
                        return true;
                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();
                        Exception ex = dbU.GetBaseException();
                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();
                        string errorMessages = null;
                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }
                        throw new Exception(errorMessages);
                    }
                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }
            }
        }

        public bool Finalized(int bookingID, AgentsModel agent)
        {

            using (isebookingEntities db = new isebookingEntities())
            {
                var hasher = new Hashids("!S3_BOOK1ng", 8, "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        var currentYear = (short)DateTime.Now.Year;

                        var cvGenerator = db.cvcounters.Where(x => x.Year == currentYear).OrderByDescending(x => x.Series).Select(x => x.Series + 1).FirstOrDefault();

                        if (cvGenerator == 0)
                        {

                            cvGenerator = 1;
                        }



                        var book = db.bookings.Where(x => x.ID == bookingID).FirstOrDefault();
                        var chargeTo = db.agents.Where(x => x.ID == book.CreatedBy).FirstOrDefault();

                        if (book == null) throw new Exception("Booking does not exist!");
                        if (book.manifestos == null || book.manifestos.Count == 0) throw new Exception("Please add manifesto!");

                        if (agent.Type == 1) throw new Exception("You are not authorized to confirm this booking!");


                        book.Status = (int)BookingStatus.FINALIZED;
                        book.CVDate = DateTime.Now;
                        book.CVNumber = agent.Location + " - " + DateTime.Now.Year.ToString() + cvGenerator.ToString().PadLeft(7, '0');

                        if (book.FinalizedBy == null)
                        {
                            book.FinalizedBy = agent.ID;
                            book.FinalizedDate = DateTime.Now;
                        }

                        var bookAgent = db.agents.Where(x => x.ID == book.CreatedBy).FirstOrDefault();

                        bookAgent.RemainingCredit = bookAgent.RemainingCredit - (double)book.Total;
                        bookAgent.AccountBalance += (double)book.Total;

                        db.SaveChanges();

                        SystemSignatoriesModel signatoryRepo = new SystemSignatoriesModel();
                        int preparerID = book.bookinglogs.Where(x => x.Status == (int)BookingStatus.FINALIZED).Select(x => x.AgentID).FirstOrDefault();
                        SystemSignatoriesModel preparer = signatoryRepo.Get(preparerID);
                        SystemSignatoriesModel verifier = signatoryRepo.GetHighestSignatory(book.agent.Location, "SOA");
                        SystemSignatoriesModel checker = signatoryRepo.GetHighestSignatory(book.agent.Location, "SV");

                        if (preparer == null)
                        {
                            preparer = verifier;
                        }


                        var newSOASignatories = new statementofaccount();
                        newSOASignatories.AuthorizedBy = verifier.ImmediateHeadName;
                        newSOASignatories.AuthorizerDesignation = verifier.ImmediateHeadDesignation;
                        newSOASignatories.BookingID = book.ID;
                        newSOASignatories.DateOfIssue = DateTime.Now;
                        newSOASignatories.IssuedBy = preparer.AgentName;
                        newSOASignatories.IssuerDesignation = preparer.Designation;
                        db.statementofaccounts.Add(newSOASignatories);
                        db.SaveChanges();

                        var newSVSignatories = new servicevoucher();
                        newSVSignatories.AuthorizedBy = checker.ImmediateHeadName;
                        newSVSignatories.AuthorizerDesignation = checker.ImmediateHeadDesignation;
                        newSVSignatories.BookingID = book.ID;
                        newSVSignatories.DateOfIssue = DateTime.Now;
                        newSVSignatories.IssuedBy = preparer.AgentName;
                        newSVSignatories.IssuerDesignation = preparer.Designation;
                        db.servicevouchers.Add(newSVSignatories);
                        db.SaveChanges();


                        var newCvNumber = new cvcounter();
                        newCvNumber.DateCreated = DateTime.Now;
                        newCvNumber.Series = cvGenerator;
                        newCvNumber.Year = currentYear;
                        db.cvcounters.Add(newCvNumber);
                        db.SaveChanges();

                        var log = new bookinglog();
                        log.AgentID = agent.ID;
                        log.BookingID = book.ID;
                        log.DateCreated = DateTime.Now;
                        log.Status = book.Status;
                        db.bookinglogs.Add(log);
                        db.SaveChanges();

                        int paymentType = book.PaymentTypeID;

                        if (paymentType == (int)PaymentType.FOR_BILLING)
                        {
                            if (chargeTo == null) throw new Exception("Unable to determine the agency to be charge for this booking!");
                            var newAR = new accountreceivable();
                            // newAR.AgentID = (int)book.CreatedBy;
                            newAR.BookingID = book.ID;
                            newAR.TransactionDate = DateTime.Now;
                            newAR.AgentID = chargeTo.ID;
                            newAR.DueDate = DateTime.Now.AddDays(chargeTo.GracePeriod);
                            newAR.Balance = (double)book.Total;
                            newAR.OriginalAmount = (double)book.Total;

                            db.accountreceivables.Add(newAR);
                            db.SaveChanges();
                        }




                        var newSales = new sale();
                        newSales.AgentID = agent.ID;
                        newSales.BookingID = book.ID;
                        newSales.TransactionDate = DateTime.Now;
                        newSales.Type = paymentType == 1 ? (int)SalesType.CREDIT : (int)SalesType.DEBIT;
                        newSales.Total = (double)book.Total;
                        db.sales.Add(newSales);

                        db.SaveChanges();
                        transaction.Commit();
                        return true;
                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();
                        Exception ex = dbU.GetBaseException();
                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();
                        string errorMessages = null;
                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }
                        throw new Exception(errorMessages);
                    }
                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }
            }
        }




        public bool Cancel(int bookingID, AgentsModel agent)
        {

            using (isebookingEntities db = new isebookingEntities())
            {
                var hasher = new Hashids("!S3_BOOK1ng", 8, "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var book = db.bookings.Where(x => x.ID == bookingID).FirstOrDefault();


                        if (book == null) throw new Exception("Booking does not exist!");

                        if (book.CreatedBy == agent.ID && agent.Type == 1 && (book.Status == (int)BookingStatus.CONFIRMED || book.Status == (int)BookingStatus.FINALIZED)) throw new Exception("ERROR! You are not authorized to cancel this booking!");


                        book.Status = (int)BookingStatus.CANCELLED;


                        db.SaveChanges();

                        var acctAR = book.accountreceivables.Where(x => x.OriginalAmount >= 0).OrderByDescending(x => x.ID).FirstOrDefault();

                        if (acctAR != null)
                        {
                            var reverseAR = new accountreceivable();
                            reverseAR.AgentID = acctAR.AgentID;
                            reverseAR.Balance = acctAR.Balance * -1;
                            reverseAR.OriginalAmount = acctAR.OriginalAmount * -1;
                            reverseAR.TransactionDate = DateTime.Now;
                            reverseAR.DueDate = acctAR.DueDate;
                            reverseAR.BookingID = acctAR.BookingID;
                            reverseAR.BillingPeriodID = null;

                            db.accountreceivables.Add(reverseAR);
                            db.SaveChanges();
                        }


                        var salesData = book.sales.Where(x => x.Total >= 0).FirstOrDefault();

                        if (salesData != null)
                        {
                            var reverseSales = new sale();
                            reverseSales.AgentID = salesData.AgentID;
                            reverseSales.BookingID = salesData.BookingID;
                            reverseSales.TransactionDate = DateTime.Now;
                            reverseSales.Type = salesData.Type;
                            reverseSales.SalesTax = salesData.SalesTax * -1;
                            reverseSales.Total = salesData.Total * -1;
                            reverseSales.Comment = "Transaction VOID by  " + agent.Fullname;
                            reverseSales.ReferenceNumber = salesData.ReferenceNumber;

                            db.sales.Add(reverseSales);
                            db.SaveChanges();
                        }

                        var log = new bookinglog();
                        log.AgentID = agent.ID;
                        log.BookingID = book.ID;
                        log.DateCreated = DateTime.Now;
                        log.Status = book.Status;
                        db.bookinglogs.Add(log);
                        db.SaveChanges();

                        transaction.Commit();

                        return true;

                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();
                        Exception ex = dbU.GetBaseException();
                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();
                        string errorMessages = null;
                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }
                        throw new Exception(errorMessages);
                    }
                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }
            }
        }


        //public Bookings Rebook(int bookingID, AgentsModel agent)
        //{
        //    isebookingEntities db = new isebookingEntities();
        //    using (var transaction = db.Database.BeginTransaction())
        //    {
        //        try
        //        {
        //            var book = db.bookings.Where(x => x.ID == bookingID).FirstOrDefault();

        //            if (book == null) throw new Exception("Booking does not exist!");
        //            if (agent.Type == 3 && (book.Status == (int)BookingStatus.CONFIRMED || book.Status == (int)BookingStatus.FINALIZED)) throw new Exception("ERROR! You are not authorized to cancel this booking!");


        //            book.Status = (int)BookingStatus.CANCELLED;


        //            db.SaveChanges();

        //            var acctAR = book.accountreceivables.Where(x => x.OriginalAmount >= 0).OrderByDescending(x => x.ID).FirstOrDefault();

        //            if (acctAR != null)
        //            {
        //                var reverseAR = new accountreceivable();
        //                reverseAR.AgentID = acctAR.AgentID;
        //                reverseAR.Balance = acctAR.Balance * -1;
        //                reverseAR.OriginalAmount = acctAR.OriginalAmount * -1;
        //                reverseAR.TransactionDate = DateTime.Now;
        //                reverseAR.DueDate = acctAR.DueDate;
        //                reverseAR.BookingID = acctAR.BookingID;
        //                reverseAR.BillingPeriodID = null;

        //                db.accountreceivables.Add(reverseAR);
        //                db.SaveChanges();
        //            }


        //            var salesData = book.sales.Where(x => x.Total >= 0).FirstOrDefault();

        //            if (salesData != null)
        //            {
        //                var reverseSales = new sale();
        //                reverseSales.AgentID = salesData.AgentID;
        //                reverseSales.BookingID = salesData.BookingID;
        //                reverseSales.TransactionDate = DateTime.Now;
        //                reverseSales.Type = salesData.Type;
        //                reverseSales.SalesTax = salesData.SalesTax * -1;
        //                reverseSales.Total = salesData.Total * -1;
        //                reverseSales.Comment = "Transaction VOID by  " + agent.Fullname + " FOR REBOOKING";
        //                reverseSales.ReferenceNumber = salesData.ReferenceNumber;

        //                db.sales.Add(reverseSales);
        //                db.SaveChanges();
        //            }

        //            var log = new bookinglog();
        //            log.AgentID = agent.ID;
        //            log.BookingID = book.ID;
        //            log.DateCreated = DateTime.Now;
        //            log.Status = book.Status;
        //            db.bookinglogs.Add(log);
        //            db.SaveChanges();


        //            var referenceBooking = GetBooking(book.ID);



        //            booking newBook = new booking();

        //            newBook.Adults = referenceBooking.Header.Adults;

        //            newBook.Childrens = referenceBooking.Header.Childrens;
        //            newBook.ChildrensBelowSix = referenceBooking.Header.ChildrensBelowSix;
        //            newBook.Senior = referenceBooking.Header.Senior;
        //            newBook.ContactNumber = referenceBooking.Header.ContactNumber == null ? "" : referenceBooking.Header.ContactNumber;
        //            newBook.ContactPerson = referenceBooking.Header.ContactPerson ?? " ";
        //            newBook.CreatedBy = agent.ID;
        //            newBook.CVNumber = "";
        //            newBook.DropOffDeparture = referenceBooking.Header.DropOffDeparture;
        //            newBook.DropOffArrival = referenceBooking.Header.DropOffArrival;
        //            newBook.DropOffLocation = referenceBooking.Header.DropOffLocation;
        //            newBook.Escorts = referenceBooking.Header.Escorts;
        //            newBook.FlightDate1 = referenceBooking.Header.FlightDate1;
        //            newBook.FlightDate2 = referenceBooking.Header.FlightDate2;
        //            newBook.FlightNo1 = referenceBooking.Header.FlightNo1;
        //            newBook.FlightNo2 = referenceBooking.Header.FlightNo2;
        //            newBook.FOC = referenceBooking.Header.FOC;
        //            newBook.GroupName = referenceBooking.Header.GroupName;
        //            newBook.Infants = referenceBooking.Header.Infants;
        //            newBook.PickUpArrival = referenceBooking.Header.PickUpArrival;
        //            newBook.PickUpDeparture = referenceBooking.Header.PickUpDeparture;
        //            newBook.Remarks = referenceBooking.Header.Remarks == null ? " " : referenceBooking.Header.Remarks;
        //            newBook.Routing = referenceBooking.Header.Routing;
        //            newBook.Status = (int)referenceBooking.Header.Status; //(int)BookingStatus.SAVED;
        //            newBook.Total = (float)referenceBooking.Details.Sum(x => (x.Amount + x.Boat) * x.Quantity);
        //            newBook.TourGuides = referenceBooking.Header.TourGuides;
        //            newBook.DateCreated = DateTime.Now;
        //            newBook.DateLastUpdated = DateTime.Now;
        //            newBook.DropOffLocation1 = referenceBooking.Header.DropOffLocation1;
        //            newBook.DropOffLocation2 = referenceBooking.Header.DropOffLocation2;
        //            newBook.PaymentTypeID = (int)referenceBooking.Header.PaymentTypeID;
        //            newBook.Class = (int)referenceBooking.Header.Class;
        //            newBook.TravelAgencyID = agent.TravelAgencyID;


        //            db.bookings.Add(newBook);

        //            db.SaveChanges();



        //            var newBooklog = new bookinglog();
        //            log.AgentID = agent.ID;
        //            log.BookingID = newBook.ID;
        //            log.DateCreated = DateTime.Now;
        //            log.Status = (int)referenceBooking.Header.Status;
        //            db.bookinglogs.Add(log);
        //            db.SaveChanges();



        //            foreach (var detail in referenceBooking.Details.Where(x => x.Quantity > 0))
        //            {
        //                bookingdetail bookDetails = new bookingdetail();
        //                bookDetails.Amount = detail.Amount;
        //                bookDetails.BookingID = newBook.ID;
        //                bookDetails.Description = detail.Description;
        //                bookDetails.Quantity = detail.Quantity;
        //                bookDetails.Boat = detail.Boat;
        //                bookDetails.SourceID = detail.SourceID;
        //                db.bookingdetails.Add(bookDetails);
        //            }

        //            if (referenceBooking.Attachments != null)
        //            {
        //                foreach (var attachment in referenceBooking.Attachments)
        //                {
        //                    bookingattachment bookAttachment = new bookingattachment();
        //                    bookAttachment.BookingID = newBook.ID;
        //                    bookAttachment.Filename = attachment.Filename;
        //                    bookAttachment.DateCreated = DateTime.Now;
        //                    bookAttachment.MimeType = attachment.MimeType;
        //                    bookAttachment.OriginalFilename = attachment.OriginalFilename;
        //                    db.bookingattachments.Add(bookAttachment);

        //                }
        //            }

        //            if (referenceBooking.OtherCharges != null)
        //            {
        //                foreach (var otherCharges in referenceBooking.OtherCharges)
        //                {
        //                    bookingotherchargesindicator chargeIndicator = new bookingotherchargesindicator();

        //                    if (otherCharges.IsChecked)
        //                    {
        //                        chargeIndicator.BookingID = newBook.ID;
        //                        chargeIndicator.ChargeID = otherCharges.ChargingID;
        //                        db.bookingotherchargesindicators.Add(chargeIndicator);
        //                    }


        //                }

        //            }
        //            List<bookingotherchargesselected> bocssList = new List<bookingotherchargesselected>();
        //            if (referenceBooking.OCs != null)
        //            {
        //                foreach (var oc in referenceBooking.OCs)
        //                {
        //                    bookingotherchargesselected bocss = new bookingotherchargesselected();
        //                    bocss.BookingID = newBook.ID;
        //                    bocss.OCID = oc;
        //                    bocssList.Add(bocss);
        //                }

        //                db.bookingotherchargesselecteds.AddRange(bocssList);
        //            }



        //            db.SaveChanges();
        //            transaction.Commit();

        //            referenceBooking.Header.ID = newBook.ID;



        //            transaction.Commit();


        //            return referenceBooking;

        //        }
        //        catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
        //        {
        //            transaction.Rollback();
        //            Exception ex = dbU.GetBaseException();
        //            throw new Exception(ex.Message);
        //        }
        //        catch (DbEntityValidationException dbEx)
        //        {
        //            transaction.Rollback();
        //            string errorMessages = null;
        //            foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
        //            {
        //                string entityName = validationResult.Entry.Entity.GetType().Name;
        //                foreach (DbValidationError error in validationResult.ValidationErrors)
        //                {
        //                    errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
        //                }
        //            }
        //            throw new Exception(errorMessages);
        //        }
        //        finally
        //        {
        //            db.Database.Connection.Close();

        //        }
        //    }
        //}


        public Bookings GetBookingByCVNumber(string cvNumber)
        {

            // var formatted = cvNumber.Split('-');
            // if (formatted.Count() <= 1)
            // {
            //      throw new Exception("Invalid CVNumber!");
            // }

            // var hasher = new Hashids("!S3_BOOK1ng", 8, "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
            // var bookingID = hasher.Decode(formatted[1].ToString());
            isebookingEntities db = new isebookingEntities();
            var bookingID = db.bookings.Where(x => x.CVNumber == cvNumber).Select(x => x.ID);

            return this.GetBooking(bookingID.FirstOrDefault());

        }


    }


    public class OCModel
    {
        public string[] OC { get; set; }
    }
}
