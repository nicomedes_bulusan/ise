﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
   public  class ModeOfPaymentsModel
    {
       public int ID { get; set; }
       public string Description { get; set; }


       public List<ModeOfPaymentsModel> GetAll()
       {
           using (var db = new isebookingEntities())
           {

               return (from a in db.modeofpayments
                       select new ModeOfPaymentsModel
                       {
                           Description = a.Description,
                           ID = a.ID
                       }).ToList<ModeOfPaymentsModel>();

           }
       }

    }
}
