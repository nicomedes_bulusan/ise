﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoreLinq;
using System.Data.Entity;

namespace ISE.DAL
{

    public enum BillingStatementStatus
    {
        OPEN = 0,
        RECEIVED_BY_AGENCY = 1,
        UNCONFIRMED_PAYMENT = 2,
        CONFIRMED_PAYMENT = 3,
        PARTIALLY_PAID = 4,
        FULLY_PAID = 5,
        CLOSED = 6,
        CANCELLED = 7
    }

    public class AccountReceivablesModel
    {
        public int ID { get; set; }
        public int AgentID { get; set; }
        public int BookingID { get; set; }
        public DateTime TransactionDate { get; set; }
        public DateTime DueDate { get; set; }
        public double OriginalAmount { get; set; }
        public double? Balance { get; set; }
        public string Description { get; set; }
        public string CVNumber { get; set; }
        public string PaxName { get; set; }
        public int Adults { get; set; }
        public int Kids { get; set; }
        public int FOC { get; set; }
        public int TourGuides { get; set; }
        public int Escorts { get; set; }
        public int Pax { get; set; }
        public string ArrivalFlight { get; set; }
        public string DepartureFlight { get; set; }
        public DateTime? DateIn { get; set; }
        public DateTime? DateOut { get; set; }
        public Nullable<int> BillingPeriodID { get; set; }
        public string Agent { get; set; }
        public int TravelAgencyID { get; set; }
        public string TravelAgencyName { get; set; }

        public int Rank { get; set; }


        string path = System.AppDomain.CurrentDomain.BaseDirectory + "\\Reports\\Statements\\";


        public List<AccountReceivablesModel> GetAccountReceivables()
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                var result = (from a in db.accountreceivables
                              join b in db.bookings on a.BookingID equals b.ID
                              where a.BillingPeriodID == null && a.OriginalAmount > 0
                              && b.PaymentTypeID == (int)PaymentType.FOR_BILLING
                              orderby a.ID descending
                              select new AccountReceivablesModel
                              {
                                  AgentID = a.AgentID,
                                  Balance = a.Balance,
                                  BookingID = a.BookingID,
                                  CVNumber = a.booking.CVNumber,
                                  Description = a.booking.GroupName,
                                  DueDate = a.DueDate,
                                  ID = a.ID,
                                  OriginalAmount = a.OriginalAmount,
                                  Pax = 0,
                                  TransactionDate = a.TransactionDate,
                                  Agent = a.agent.Company,
                                  BillingPeriodID = a.BillingPeriodID,
                                  PaxName = a.booking.GroupName,
                                  Adults = a.booking.Adults,
                                  ArrivalFlight = a.booking.FlightNo1,
                                  DateIn = a.booking.FlightDate1,
                                  DateOut = a.booking.FlightDate2,
                                  DepartureFlight = a.booking.FlightNo2,
                                  Escorts = a.booking.Escorts,
                                  FOC = a.booking.FOC,
                                  Kids = a.booking.Childrens + a.booking.ChildrensBelowSix,
                                  TourGuides = a.booking.TourGuides


                              }).ToList().DistinctBy(x => x.BookingID).OrderBy(x => x.TransactionDate).ToList();

                return result;
            }

        }


        public AccountsReceivablePaged GetAccountsReceivablePaged(DateTime fromDate, DateTime toDate, string search = "", List<int> travelAgencies = null, int startIndex = 0, int pageSize = 1000)
        {


            using (isebookingEntities db = new isebookingEntities())
            {

                var result = (from a in db.accountreceivables
                              join b in db.bookings on a.BookingID equals b.ID
                              where a.BillingPeriodID == null && a.OriginalAmount > 0
                              && b.PaymentTypeID == (int)PaymentType.FOR_BILLING


                              &&
                              (
                              (DbFunctions.TruncateTime(a.booking.FlightDate1) >= fromDate && DbFunctions.TruncateTime(a.booking.FlightDate1) <= toDate)
                                     ||
                               (DbFunctions.TruncateTime(a.booking.FlightDate2) >= fromDate && DbFunctions.TruncateTime(a.booking.FlightDate2) <= toDate)
                              )
                              orderby a.ID descending
                              select new AccountReceivablesModel
                             {
                                 AgentID = a.AgentID,
                                 Balance = a.Balance,
                                 BookingID = a.BookingID,
                                 CVNumber = a.booking.CVNumber,
                                 Description = a.booking.GroupName,
                                 DueDate = a.DueDate,
                                 ID = a.ID,
                                 OriginalAmount = a.OriginalAmount,
                                 Pax = 0,
                                 TransactionDate = a.TransactionDate,
                                 Agent = a.agent.Company,
                                 BillingPeriodID = a.BillingPeriodID,
                                 PaxName = a.booking.GroupName,
                                 Adults = a.booking.Adults,
                                 ArrivalFlight = a.booking.FlightNo1,
                                 DateIn = a.booking.FlightDate1,
                                 DateOut = a.booking.FlightDate2,
                                 DepartureFlight = a.booking.FlightNo2,
                                 Escorts = a.booking.Escorts,
                                 FOC = a.booking.FOC,
                                 Kids = a.booking.Childrens + a.booking.ChildrensBelowSix,
                                 TourGuides = a.booking.TourGuides,
                                 TravelAgencyID = a.agent.TravelAgencyID,
                                 TravelAgencyName = a.agent.travelagency.Name


                             }
                            ).DistinctBy(x => x.BookingID).OrderBy(x => x.TransactionDate).AsQueryable();

                if (!String.IsNullOrEmpty(search))
                {
                    result = result.Where(x => x.CVNumber.Contains(search));
                }

                if (travelAgencies != null)
                {
                    result = result.Where(x => travelAgencies.Contains(x.TravelAgencyID));
                }

                var totalRowCount = result.Count();

                // var resultset = result.OrderBy(x => x.BookingID).Take(pageSize).ToList();


                var resultset = result.Select((x, index) => new AccountReceivablesModel
                {
                    AgentID = x.AgentID,
                    Balance = x.Balance,
                    BookingID = x.BookingID,
                    CVNumber = x.CVNumber,
                    Description = x.Description,
                    DueDate = x.DueDate,
                    ID = x.ID,
                    OriginalAmount = x.OriginalAmount,
                    Pax = 0,
                    TransactionDate = x.TransactionDate,
                    Agent = x.Agent,
                    BillingPeriodID = x.BillingPeriodID,
                    PaxName = x.PaxName,
                    Adults = x.Adults,
                    ArrivalFlight = x.ArrivalFlight,
                    DateIn = x.DateIn,
                    DateOut = x.DateOut,
                    DepartureFlight = x.DepartureFlight,
                    Escorts = x.Escorts,
                    FOC = x.FOC,
                    Kids = x.Kids,
                    TourGuides = x.TourGuides,
                    TravelAgencyID = x.TravelAgencyID,
                    TravelAgencyName = x.TravelAgencyName,
                    Rank = index

                }).Where(x => x.Rank >= (startIndex - 1) * pageSize).Take(pageSize);


                AccountsReceivablePaged pagedRecords = new AccountsReceivablePaged();
                pagedRecords.PageNumber = startIndex;
                pagedRecords.TotalRecords = totalRowCount;

                pagedRecords.Records = resultset.ToList();

                var totalPages = (int)Math.Ceiling((decimal)pagedRecords.TotalRecords / (decimal)pageSize);
                var currentPage = startIndex == 0 ? (int)startIndex : 1;
                var startPage = currentPage - 5;
                var endPage = currentPage + 4;
                if (startPage <= 0)
                {
                    endPage -= (startPage - 1);
                    startPage = 1;
                }

                if (startIndex >= totalPages)
                {
                    endPage = totalPages;
                    if (endPage > 10)
                    {
                        startPage = endPage - 9;
                    }
                }
                pagedRecords.CurrentPage = startIndex;
                pagedRecords.StartPage = startPage;
                pagedRecords.EndPage = endPage;
                pagedRecords.TotalPages = totalPages;
                pagedRecords.PageSize = pageSize;
                pagedRecords.CurrentRecordCount = pagedRecords.Records.Count();

                return pagedRecords;
            }
        }

        public void GenerateMissingStatements(AgentsModel currentUser)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                var bps = (from a in db.billingperiods
                           join b in db.billingstatements on a.ID equals b.BillingPeriodID into c
                           from cc in c.DefaultIfEmpty()
                           where a.Status == (int)BillingStatementStatus.OPEN
                           && cc == null
                           select new
                           {
                               BillingPeriodID = a.ID,
                               BatchID = a.BatchID
                           }).ToList();

                foreach (var bp in bps)
                {
                    Hangfire.BackgroundJob.Enqueue(() => GeneratePDF(currentUser, bp.BillingPeriodID, bp.BatchID));
                }
            }

        }

        public void GenerateStatement(List<int> bookingIDs, AgentsModel currentUser)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var acctRcvables = db.accountreceivables.Where(x => bookingIDs.Contains(x.BookingID) && x.OriginalAmount > 0).OrderByDescending(x => x.TransactionDate).DistinctBy(x => x.BookingID).ToList();

                        var groupItems = acctRcvables.GroupBy(x => x.agent.TravelAgencyID).Select(g => new
                        {
                            TravelAgencyID = g.Key,
                            AccountReceivables = g.Select(i => i)

                        }).ToList();

                        var newBatch = new batch();
                        newBatch.DateCreated = DateTime.Now;
                        newBatch.DateLastUpdated = DateTime.Now;
                        newBatch.Processed = 0;
                        newBatch.Total = groupItems.Count();
                        db.batches.Add(newBatch);

                        foreach (var acctAR in groupItems)
                        {
                            double receivableAmount = acctAR.AccountReceivables.Sum(x => x.OriginalAmount);
                            double ewt = (receivableAmount * 0.02);
                            double netAmount = receivableAmount - ewt;

                            var startDateF1 = (DateTime?)acctAR.AccountReceivables.Where(x => x.booking.FlightDate1 != null).Min(x => x.booking.FlightDate1);
                            var startDateF2 = (DateTime?)acctAR.AccountReceivables.Where(x => x.booking.FlightDate2 != null).Min(x => x.booking.FlightDate2);


                            var endDateF1 = (DateTime?)acctAR.AccountReceivables.Where(x => x.booking.FlightDate1 != null).Max(x => x.booking.FlightDate1);
                            var endDateF2 = (DateTime?)acctAR.AccountReceivables.Where(x => x.booking.FlightDate2 != null).Max(x => x.booking.FlightDate2);





                            var newBillingPeriod = new billingperiod();
                            newBillingPeriod.AgentID = acctAR.AccountReceivables.Max(x => x.AgentID);

                            if (startDateF1 == null)
                            {
                                newBillingPeriod.StartingDate = (DateTime)startDateF2;
                            }
                            else if (startDateF2 == null)
                            {
                                newBillingPeriod.StartingDate = (DateTime)startDateF1;
                            }
                            else
                            {
                                newBillingPeriod.StartingDate = startDateF1 < startDateF2 ? (DateTime)startDateF1 : (DateTime)startDateF2;
                            }


                            if (endDateF1 == null)
                            {
                                newBillingPeriod.EndingDate = (DateTime)endDateF2;
                            }
                            else if (endDateF2 == null)
                            {
                                newBillingPeriod.EndingDate = (DateTime)endDateF1;
                            }
                            else
                            {
                                newBillingPeriod.EndingDate = endDateF1 > endDateF2 ? (DateTime)endDateF1 : (DateTime)endDateF2;
                            }



                            newBillingPeriod.IsOpen = true;

                            newBillingPeriod.BatchID = newBatch.ID;

                            newBillingPeriod.GrossTotal = receivableAmount;
                            newBillingPeriod.EWT = ewt;
                            newBillingPeriod.NetTotal = netAmount;

                            db.billingperiods.Add(newBillingPeriod);
                            db.SaveChanges();


                            foreach (var ar in acctAR.AccountReceivables)
                            {
                                ar.BillingPeriodID = newBillingPeriod.ID;
                                db.SaveChanges();
                            }

                        }
                        transaction.Commit();



                        Hangfire.BackgroundJob.Enqueue(() => ProcessBatch(currentUser, newBatch.ID));

                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();

                        Exception ex = dbU.GetBaseException();

                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();
                        string errorMessages = null;
                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }

                        throw new Exception(errorMessages);

                    }

                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }
            }
        }


        //public void GetBatchJobs()
        //{
        //    isebookingEntities db = new isebookingEntities();

        //    var openBactches = db.batches.Where(x => x.Total > x.Processed).ToList();

        //    foreach (var period in openBactches.SelectMany(x => x.billingperiods))
        //    {
        //        Hangfire.BackgroundJob.Enqueue(() => GeneratePDF(period.ID, period.BatchID));
        //        //System.Threading.Thread.Sleep(1000);
        //    }

        //}


        public void ProcessBatch(AgentsModel currentUser, int batchID)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                var openBactches = db.batches.Where(x => x.ID == batchID).ToList();

                foreach (var period in openBactches.SelectMany(x => x.billingperiods))
                {
                    Hangfire.BackgroundJob.Enqueue(() => GeneratePDF(currentUser, period.ID, period.BatchID));

                }
            }

        }

        public void RegeneratePDF(AgentsModel currentUser, int billingPeriodID, int batchID)
        {
            if (currentUser.Type != 3) throw new Exception("You are not authorized to rgenerate this billing statement!");


            AccountReceivablesModel acctARRepo = new AccountReceivablesModel();
            using (isebookingEntities db = new isebookingEntities())
            {

                var filename = db.billingstatements.Where(x => x.BillingPeriodID == billingPeriodID).FirstOrDefault();
                long epochTicks = new DateTime(1970, 1, 1).Ticks;
                long unixTime = ((DateTime.UtcNow.Ticks - epochTicks) / TimeSpan.TicksPerSecond);
                string timestamp = unixTime.ToString();


                string fullPath = path;
                string pdfFile = "";
                string excelFile = "";

                if (filename != null)
                {
                    // fileExist = true;
                    fullPath += filename;
                    excelFile = fullPath.Replace(".pdf", ".xls");
                    pdfFile = fullPath.Replace(".xls", ".pdf");
                }

                if (File.Exists(excelFile))
                {
                    File.Move(excelFile, excelFile + "." + timestamp);
                }

                if (File.Exists(pdfFile))
                {
                    File.Move(pdfFile, pdfFile + "." + timestamp);
                }


                AgentsModel agentRepo = new AgentsModel();

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        var acctAR = (from a in db.accountreceivables
                                      where a.BillingPeriodID == billingPeriodID
                                      orderby a.TransactionDate ascending
                                      select new AccountReceivablesModel
                                      {
                                          AgentID = a.AgentID,
                                          Balance = a.Balance,
                                          BookingID = a.BookingID,
                                          CVNumber = a.booking.CVNumber,
                                          Description = a.booking.GroupName,
                                          DueDate = a.DueDate,
                                          ID = a.ID,
                                          OriginalAmount = a.OriginalAmount,
                                          Pax = 0,
                                          TransactionDate = a.TransactionDate,
                                          Agent = a.agent.Company,
                                          BillingPeriodID = a.BillingPeriodID,

                                          PaxName = a.booking.GroupName,
                                          Adults = a.booking.Adults + a.booking.Senior,
                                          ArrivalFlight = a.booking.FlightNo1,
                                          DateIn = a.booking.FlightDate1,
                                          DateOut = a.booking.FlightDate2,
                                          DepartureFlight = a.booking.FlightNo2,
                                          Escorts = a.booking.Escorts,
                                          FOC = a.booking.FOC,
                                          Kids = a.booking.Childrens + a.booking.ChildrensBelowSix,
                                          TourGuides = a.booking.TourGuides

                                      }).ToList<AccountReceivablesModel>();

                        if (acctAR == null) throw new Exception("Billing period is already closed!");


                        double receivableAmount = acctAR.Sum(x => x.OriginalAmount);
                        double ewt = (receivableAmount * 0.02);
                        double netAmount = receivableAmount - ewt;

                        var startDateF1 = (DateTime?)acctAR.Where(x => x.DateIn != null).Min(x => x.DateIn);
                        var startDateF2 = (DateTime?)acctAR.Where(x => x.DateOut != null).Min(x => x.DateOut);


                        var endDateF1 = (DateTime?)acctAR.Where(x => x.DateIn != null).Max(x => x.DateIn);
                        var endDateF2 = (DateTime?)acctAR.Where(x => x.DateOut != null).Max(x => x.DateOut);


                        List<int> closedStatus = new List<int>{(int)BillingStatementStatus.CLOSED,
                        (int)BillingStatementStatus.CONFIRMED_PAYMENT,
                        (int)BillingStatementStatus.FULLY_PAID,
                        (int)BillingStatementStatus.PARTIALLY_PAID,
                        (int)BillingStatementStatus.CANCELLED};


                        var newBillingPeriod = db.billingperiods.Where(x => x.ID == billingPeriodID).FirstOrDefault();

                        if (newBillingPeriod == null) throw new Exception("Billing Period does not exist!");

                        if (closedStatus.Contains(newBillingPeriod.Status)) throw new Exception("This billing statement is already closed!");


                        newBillingPeriod.LastUpdate = DateTime.Now;

                        // newBillingPeriod.AgentID = acctAR.Max(x => x.AgentID);

                        if (startDateF1 == null)
                        {
                            newBillingPeriod.StartingDate = (DateTime)startDateF2;
                        }
                        else if (startDateF2 == null)
                        {
                            newBillingPeriod.StartingDate = (DateTime)startDateF1;
                        }
                        else
                        {
                            newBillingPeriod.StartingDate = startDateF1 < startDateF2 ? (DateTime)startDateF1 : (DateTime)startDateF2;
                        }


                        if (endDateF1 == null)
                        {
                            newBillingPeriod.EndingDate = (DateTime)endDateF2;
                        }
                        else if (endDateF2 == null)
                        {
                            newBillingPeriod.EndingDate = (DateTime)endDateF1;
                        }
                        else
                        {
                            newBillingPeriod.EndingDate = endDateF1 > endDateF2 ? (DateTime)endDateF1 : (DateTime)endDateF2;
                        }

                        newBillingPeriod.IsOpen = true;
                        newBillingPeriod.BatchID = batchID;
                        newBillingPeriod.GrossTotal = receivableAmount;
                        newBillingPeriod.EWT = ewt;
                        newBillingPeriod.NetTotal = netAmount;
                        db.SaveChanges();





                        var chargeTo = agentRepo.FindAgentByID(acctAR.FirstOrDefault().AgentID);

                        //PDF Generation Here
                        var lr = new LocalReport
                        {
                            ReportPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\\Reports\\Billing Statement.rdlc",
                            //ReportEmbeddedResource ="ISE.Reports.Billing Statement.rdlc",
                            EnableExternalImages = true,
                            EnableHyperlinks = true

                        };


                        var startDate = startDateF1 < startDateF2 ? (DateTime)startDateF1 : (DateTime)startDateF2;

                        var endDate = endDateF1 > endDateF2 ? (DateTime)endDateF1 : (DateTime)endDateF2;


                        if (startDateF1 == null)
                        {
                            startDate = (DateTime)startDateF2;
                        }
                        else if (startDateF2 == null)
                        {
                            startDate = (DateTime)startDateF1;
                        }
                        else
                        {
                            startDate = startDateF1 < startDateF2 ? (DateTime)startDateF1 : (DateTime)startDateF2;
                        }


                        if (endDateF1 == null)
                        {
                            endDate = (DateTime)endDateF2;
                        }
                        else if (endDateF2 == null)
                        {
                            endDate = (DateTime)endDateF1;
                        }
                        else
                        {
                            endDate = endDateF1 > endDateF2 ? (DateTime)endDateF1 : (DateTime)endDateF2;
                        }



                        ReportParameter p1 = new ReportParameter("PreparedBy", currentUser.Fullname);
                        ReportParameter p2 = new ReportParameter("StartDate", startDate.ToShortDateString());
                        ReportParameter p3 = new ReportParameter("EndDate", endDate.ToShortDateString());

                        lr.SetParameters(new ReportParameter[] { p1, p2, p3 });

                        lr.DataSources.Add(new ReportDataSource("Agent", new List<AgentsModel>() { chargeTo }));
                        lr.DataSources.Add(new ReportDataSource("AccountReceivables", acctAR));

                        Warning[] warnings;

                        string mimeType, encoding, extension;
                        string[] streams;
                        var renderedBytes = lr.Render
                            (
                                "PDF",
                                @"<DeviceInfo><OutputFormat>PDF</OutputFormat><HumanReadablePDF>False</HumanReadablePDF></DeviceInfo>",
                                out mimeType,
                                out encoding,
                                out extension,
                                out streams,
                                out warnings
                            );

                        string filename1 = CleanFileNames(chargeTo.Company ?? chargeTo.Fullname) + " Batch - " + batchID + ".pdf";
                        using (FileStream fs = new FileStream(path + filename1, FileMode.Create))
                        {
                            fs.Write(renderedBytes, 0, renderedBytes.Length);
                        }


                        //GENERATE EXCEL
                        var lrExcel = new LocalReport
                        {
                            ReportPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\\Reports\\Billing Statement Detailed.rdlc",
                            //ReportEmbeddedResource ="ISE.Reports.Billing Statement Detailed.rdlc",
                            EnableExternalImages = true,
                            EnableHyperlinks = true

                        };


                        lrExcel.DataSources.Add(new ReportDataSource("Agent", new List<AgentsModel>() { chargeTo }));
                        lrExcel.DataSources.Add(new ReportDataSource("AccountReceivables", acctAR));

                        lrExcel.SetParameters(new ReportParameter[] { p2, p3 });

                        Warning[] warningsExcel;

                        string mimeTypeExcel, encodingExcel, extensionExcel;
                        string[] streamsExcel;
                        var renderedBytesExcel = lrExcel.Render
                            (
                                "EXCEL",
                                @"<DeviceInfo><OutputFormat>PDF</OutputFormat><HumanReadablePDF>False</HumanReadablePDF></DeviceInfo>",
                                out mimeTypeExcel,
                                out encodingExcel,
                                out extensionExcel,
                                out streamsExcel,
                                out warningsExcel
                            );

                        string filenameExcel = CleanFileNames(chargeTo.Company ?? chargeTo.Fullname) + " Batch - " + batchID + ".xls";
                        using (FileStream fs1 = new FileStream(path + filenameExcel, FileMode.Create))
                        {
                            fs1.Write(renderedBytesExcel, 0, renderedBytesExcel.Length);
                        }

                        //Log billing statement



                        var bill = db.billingstatements.Where(x => x.BillingPeriodID == billingPeriodID && x.Filename.Contains(".pdf")).FirstOrDefault();

                        bill.DateLastUpdated = DateTime.Now;
                        bill.LastUpdatedBy = currentUser.ID;


                        db.SaveChanges();


                        //var billExcel = db.billingstatements.Where(x => x.BillingPeriodID == billingPeriodID && x.Filename.Contains(".xls")).FirstOrDefault();
                        //billExcel.LastUpdatedBy = currentUser.ID;
                        //billExcel.DateLastUpdated = DateTime.Now;

                        //db.SaveChanges();



                        transaction.Commit();


                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();
                        Exception ex = dbU.GetBaseException();
                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();
                        string errorMessages = null;
                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }
                        throw new Exception(errorMessages);
                    }
                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }
            }

        }

        public void GeneratePDF(AgentsModel currentUser, int billingPeriodID, int batchID)
        {
            using (isebookingEntities db = new isebookingEntities())
            {
                AgentsModel agentRepo = new AgentsModel();

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        var acctAR = (from a in db.accountreceivables
                                      where a.BillingPeriodID == billingPeriodID
                                      orderby a.TransactionDate ascending
                                      select new AccountReceivablesModel
                                      {
                                          AgentID = a.AgentID,
                                          Balance = a.Balance,
                                          BookingID = a.BookingID,
                                          CVNumber = a.booking.CVNumber,
                                          Description = a.booking.GroupName,
                                          DueDate = a.DueDate,
                                          ID = a.ID,
                                          OriginalAmount = a.OriginalAmount,
                                          Pax = 0,
                                          TransactionDate = a.TransactionDate,
                                          Agent = a.agent.Company,
                                          BillingPeriodID = a.BillingPeriodID,

                                          PaxName = a.booking.GroupName,
                                          Adults = a.booking.Adults + a.booking.Senior,
                                          ArrivalFlight = a.booking.FlightNo1,
                                          DateIn = a.booking.FlightDate1,
                                          DateOut = a.booking.FlightDate2,
                                          DepartureFlight = a.booking.FlightNo2,
                                          Escorts = a.booking.Escorts,
                                          FOC = a.booking.FOC,
                                          Kids = a.booking.Childrens + a.booking.ChildrensBelowSix,
                                          TourGuides = a.booking.TourGuides

                                      }).ToList<AccountReceivablesModel>();

                        if (acctAR == null) throw new Exception("Billing period is already closed!");


                        var chargeTo = agentRepo.FindAgentByID(acctAR.FirstOrDefault().AgentID);

                        //PDF Generation Here
                        var lr = new LocalReport
                        {
                            ReportPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\\Reports\\Billing Statement.rdlc",
                            //ReportEmbeddedResource ="ISE.Reports.Billing Statement.rdlc",
                            EnableExternalImages = true,
                            EnableHyperlinks = true

                        };

                        DateTime startDate;
                        DateTime endDate;


                        var startDateF1 = (DateTime?)acctAR.Where(x => x.DateIn != null).Min(x => x.DateIn);
                        var startDateF2 = (DateTime?)acctAR.Where(x => x.DateOut != null).Min(x => x.DateOut);


                        //var startDate = startDateF1 < startDateF2 ? (DateTime)startDateF1 : (DateTime)startDateF2;



                        var endDateF1 = (DateTime?)acctAR.Where(x => x.DateIn != null).Max(x => x.DateIn);
                        var endDateF2 = (DateTime?)acctAR.Where(x => x.DateOut != null).Max(x => x.DateOut);
                        // var endDate = endDateF1 > endDateF2 ? (DateTime)endDateF1 : (DateTime)endDateF2;


                        if (startDateF1 == null)
                        {
                            startDate = (DateTime)startDateF2;
                        }
                        else if (startDateF2 == null)
                        {
                            startDate = (DateTime)startDateF1;
                        }
                        else
                        {
                            startDate = startDateF1 < startDateF2 ? (DateTime)startDateF1 : (DateTime)startDateF2;
                        }


                        if (endDateF1 == null)
                        {
                            endDate = (DateTime)endDateF2;
                        }
                        else if (endDateF2 == null)
                        {
                            endDate = (DateTime)endDateF1;
                        }
                        else
                        {
                            endDate = endDateF1 > endDateF2 ? (DateTime)endDateF1 : (DateTime)endDateF2;
                        }



                        ReportParameter p1 = new ReportParameter("PreparedBy", currentUser.Fullname);
                        ReportParameter p2 = new ReportParameter("StartDate", startDate.ToShortDateString());
                        ReportParameter p3 = new ReportParameter("EndDate", endDate.ToShortDateString());

                        lr.SetParameters(new ReportParameter[] { p1, p2, p3 });

                        lr.DataSources.Add(new ReportDataSource("Agent", new List<AgentsModel>() { chargeTo }));
                        lr.DataSources.Add(new ReportDataSource("AccountReceivables", acctAR));

                        Warning[] warnings;

                        string mimeType, encoding, extension;
                        string[] streams;
                        var renderedBytes = lr.Render
                            (
                                "PDF",
                                @"<DeviceInfo><OutputFormat>PDF</OutputFormat><HumanReadablePDF>False</HumanReadablePDF></DeviceInfo>",
                                out mimeType,
                                out encoding,
                                out extension,
                                out streams,
                                out warnings
                            );

                        string filename = CleanFileNames(chargeTo.Company ?? chargeTo.Fullname) + " Batch - " + batchID + ".pdf";
                        using (FileStream fs = new FileStream(path + filename, FileMode.Create))
                        {
                            fs.Write(renderedBytes, 0, renderedBytes.Length);
                        }


                        //GENERATE EXCEL
                        var lrExcel = new LocalReport
                        {
                            ReportPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\\Reports\\Billing Statement Detailed.rdlc",
                            //ReportEmbeddedResource ="ISE.Reports.Billing Statement Detailed.rdlc",
                            EnableExternalImages = true,
                            EnableHyperlinks = true

                        };


                        lrExcel.DataSources.Add(new ReportDataSource("Agent", new List<AgentsModel>() { chargeTo }));
                        lrExcel.DataSources.Add(new ReportDataSource("AccountReceivables", acctAR));

                        lrExcel.SetParameters(new ReportParameter[] { p2, p3 });

                        Warning[] warningsExcel;

                        string mimeTypeExcel, encodingExcel, extensionExcel;
                        string[] streamsExcel;
                        var renderedBytesExcel = lrExcel.Render
                            (
                                "EXCEL",
                                @"<DeviceInfo><OutputFormat>PDF</OutputFormat><HumanReadablePDF>False</HumanReadablePDF></DeviceInfo>",
                                out mimeTypeExcel,
                                out encodingExcel,
                                out extensionExcel,
                                out streamsExcel,
                                out warningsExcel
                            );

                        string filenameExcel = CleanFileNames(chargeTo.Company ?? chargeTo.Fullname) + " Batch - " + batchID + ".xls";
                        using (FileStream fs1 = new FileStream(path + filenameExcel, FileMode.Create))
                        {
                            fs1.Write(renderedBytesExcel, 0, renderedBytesExcel.Length);
                        }

                        //Log billing statement



                        var bill = new billingstatement();
                        bill.AgentID = chargeTo.ID;
                        bill.BacthID = batchID;
                        bill.DateCreated = DateTime.Now;
                        bill.BillingPeriodID = billingPeriodID;
                        bill.DateSent = null;
                        bill.Filename = filename;
                        bill.IsSent = false;
                        bill.DateLastUpdated = DateTime.Now;

                        db.billingstatements.Add(bill);
                        db.SaveChanges();


                        var billExcel = new billingstatement();
                        billExcel.AgentID = chargeTo.ID;
                        billExcel.BacthID = batchID;
                        billExcel.DateCreated = DateTime.Now;
                        billExcel.DateSent = null;
                        // bill.BillingPeriodID = billingPeriodID;
                        billExcel.Filename = filenameExcel;
                        billExcel.IsSent = false;
                        billExcel.DateLastUpdated = DateTime.Now;
                        db.billingstatements.Add(billExcel);
                        db.SaveChanges();

                        //Update Batch and AR Here
                        // Race condition when using EF
                        db.Database.ExecuteSqlCommand("CALL UpdateBatch(" + batchID + ")");

                        transaction.Commit();


                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();
                        Exception ex = dbU.GetBaseException();
                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();
                        string errorMessages = null;
                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }
                        throw new Exception(errorMessages);
                    }
                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }



            }

        }

        public IEnumerable<string> GetFilesByBatch(int batchID)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                var files = db.billingstatements.Where(x => x.BacthID == batchID).Select(x => path + x.Filename).AsEnumerable();

                return files;
            }
        }


        // public void TagBillingStatement(AgentsModel agent,int billingPeriodID)

        public List<CollectiblesModel> GetCollectibles(AgentsModel agent, bool includeCancelled = false)
        {
            if (agent.Type == 1) throw new Exception("You are not authorized to view this records!");

            using (isebookingEntities db = new isebookingEntities())
            {



                var result = (from bp in db.billingperiods
                              join bs in db.billingstatements on bp.ID equals bs.BillingPeriodID
                              where bp.IsOpen == true &&
                              (includeCancelled ? true : bp.Status != (int)BillingStatementStatus.CANCELLED)

                              select new CollectiblesModel
                              {
                                  BatchID = bp.BatchID,
                                  ID = bp.ID,
                                  AgentID = bp.AgentID,
                                  TravelAgency = bp.agent.travelagency.Name,
                                  StartDate = bp.StartingDate,
                                  EndDate = bp.EndingDate,
                                  GenerationDate = bs.DateCreated,
                                  EWT = bp.EWT,
                                  GrossTotal = bp.GrossTotal,
                                  NetTotal = bp.NetTotal,
                                  Balance = bp.accountreceivables.Sum(x => x.Balance) ?? 0,
                                  Payment = bp.Payment,
                                  Status = (BillingStatementStatus)bp.Status,
                                  CVNumbers = bp.accountreceivables.Where(x => x.OriginalAmount >= 0).OrderByDescending(x => x.TransactionDate).Select(x => x.booking.CVNumber).ToList<string>()

                              }).ToList<CollectiblesModel>();



                return result;
            }
        }


        public void CancelBillingStatement(AgentsModel agent, int billingPeriodID)
        {
            using (isebookingEntities db = new isebookingEntities())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        if (agent.Type != 3) throw new Exception("You are not authorized to cancel this billing statement!");


                        var billingStatement = db.billingstatements.Where(x => x.BillingPeriodID == billingPeriodID).FirstOrDefault();
                        if (billingStatement == null) throw new Exception("Billing statement does not exist!");


                        var billingPeriod = db.billingperiods.Where(x => x.ID == billingPeriodID).FirstOrDefault();
                        if (billingPeriod == null) throw new Exception("Billing statement does not exist!");

                        if (!(billingPeriod.Status == (int)BillingStatementStatus.RECEIVED_BY_AGENCY || billingPeriod.Status == (int)BillingStatementStatus.OPEN)) throw new Exception("Unable to cancel Billing Statement.");


                        var acctARs = billingPeriod.accountreceivables.ToList();

                        foreach (var ar in acctARs)
                        {
                            ar.BillingPeriodID = null;
                            db.SaveChanges();
                        }


                        billingPeriod.Status = (int)BillingStatementStatus.CANCELLED;
                        db.SaveChanges();

                        transaction.Commit();

                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();
                        Exception ex = dbU.GetBaseException();
                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();
                        string errorMessages = null;
                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }
                        throw new Exception(errorMessages);
                    }
                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }
            }
        }

        public List<CollectiblesModel> GetUnconfirmedCollectibles(AgentsModel agent)
        {
            if (agent.Type == 1) throw new Exception("You are not authorized to view this records!");

            using (isebookingEntities db = new isebookingEntities())
            {

                var result = (from bp in db.billingperiods
                              join bs in db.billingstatements on bp.ID equals bs.BillingPeriodID
                              where bp.IsOpen == true
                             && bs.DateReceived == null
                              select new CollectiblesModel
                              {
                                  BatchID = bp.BatchID,
                                  ID = bp.ID,
                                  AgentID = bp.AgentID,
                                  TravelAgency = bp.agent.travelagency.Name,
                                  StartDate = bp.StartingDate,
                                  EndDate = bp.EndingDate,
                                  EWT = bp.EWT,
                                  GrossTotal = bp.GrossTotal,
                                  NetTotal = bp.NetTotal,
                                  Balance = bp.accountreceivables.Sum(x => x.Balance) ?? 0,
                                  DateReceived = bs.DateReceived
                              }).ToList<CollectiblesModel>();

                return result;
            }
        }

        public void TagAsReceived(AgentsModel agent, int billingPeriodID, DateTime dateReceived)
        {
            using (isebookingEntities db = new isebookingEntities())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        if (agent.Type != 3) throw new Exception("You are not authorized to tag this billing statement!");


                        var billingStatement = db.billingstatements.Where(x => x.BillingPeriodID == billingPeriodID).FirstOrDefault();
                        if (billingStatement == null) throw new Exception("Billing statement does not exist!");

                        if (billingStatement.DateReceived != null) throw new Exception("Billing statement already tagged as received!");



                        if (dateReceived > DateTime.Now) throw new Exception("Future date is not valid!");

                        billingStatement.DateReceived = dateReceived;
                        billingStatement.DateLastUpdated = DateTime.Now;
                        billingStatement.LastUpdatedBy = agent.ID;


                        var billingPeriod = db.billingperiods.Where(x => x.ID == billingPeriodID).FirstOrDefault();

                        if (billingPeriod != null)
                        {
                            billingPeriod.Status = (int)BillingStatementStatus.RECEIVED_BY_AGENCY;
                        }

                        db.SaveChanges();

                        transaction.Commit();

                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();
                        Exception ex = dbU.GetBaseException();
                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();
                        string errorMessages = null;
                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }
                        throw new Exception(errorMessages);
                    }
                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }
            }
        }

        public void PostPayment(AgentsModel agent, int billingPeriodID, double amount, string remarks)
        {
            using (isebookingEntities db = new isebookingEntities())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {

                        if (agent.Type != 3) throw new Exception("You are not authorized to post this payment!");

                        var newPayment = new paymentlog();
                        newPayment.PostedBy = agent.ID;
                        newPayment.BillingPeriodID = billingPeriodID;
                        newPayment.Amount = amount;
                        newPayment.Remarks = remarks ?? " ";
                        newPayment.PaymentDate = DateTime.Now;
                        db.paymentlogs.Add(newPayment);
                        db.SaveChanges();


                        double ewt = amount * 0.02;
                        double totalPayment = amount + ewt;

                        var acctARs = db.accountreceivables.Where(x => x.Balance > 0 && x.BillingPeriodID == billingPeriodID).ToList();

                        if (acctARs == null) throw new Exception("This billing period is either invalid or closed!");


                        foreach (var ar in acctARs)
                        {


                            totalPayment = totalPayment - (double)ar.Balance;

                            if (totalPayment >= 0)
                            {
                                ar.Balance = 0;
                            }
                            else
                            {
                                ar.Balance = totalPayment * -1;
                            }

                            db.SaveChanges();
                        }




                        transaction.Commit();

                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();
                        Exception ex = dbU.GetBaseException();
                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();
                        string errorMessages = null;
                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }
                        throw new Exception(errorMessages);
                    }
                    finally
                    {
                        db.Database.Connection.Close();

                    }
                }
            }
        }

        private string CleanFileNames(string filename)
        {
            return Path.GetInvalidFileNameChars().Aggregate(filename, (current, c) => current.Replace(c.ToString(), string.Empty));
        }
        public object GetBatches()
        {
            using (isebookingEntities db = new isebookingEntities())
            {
                var result = (from a in db.batches
                              select new
                              {
                                  ID = a.ID,
                                  DateCreated = a.DateCreated,
                                  DateLastUpdated = a.DateLastUpdated,
                                  Processed = a.Processed,
                                  Total = a.Total
                              }).ToList();

                return result;
            }
        }

        public string GetBillingStatement(int billingPeriodID, int agentID, AgentsModel agent)
        {

            if (agent == null) throw new Exception("Only logged on users can view this document!");

            if (agent.Type == 1) throw new Exception("You are not authorized to view this records!");

            using (isebookingEntities db = new isebookingEntities())
            {

                var result = db.billingstatements.Where(x => x.BillingPeriodID == billingPeriodID && x.AgentID == agentID).FirstOrDefault();

                if (result == null) throw new Exception("Statement does not exists!");

                return result.Filename;
            }
        }

    }


    public class AccountsReceivablePaged
    {
        public List<AccountReceivablesModel> Records { get; set; }
        public long TotalRecords { get; set; }
        public long PageNumber { get; set; }
        public int PageSize { get; set; }
        public int StartPage { get; set; }
        public int EndPage { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int CurrentRecordCount { get; set; }

    }
}
