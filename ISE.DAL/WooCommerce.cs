﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

using ISE.DAL;
using System.Configuration;
using RestSharp;
using Newtonsoft.Json.Linq;
using Hangfire;

namespace WooCommerce
{
    public class Root
    {
        [JsonProperty("0")]
        public ProductMeta ProductMeta { get; set; }

        [JsonProperty("transfer")]
        public Transfer[] Transfer { get; set; }
    }


    public class ProductMeta
    {
        [JsonProperty("meta")]
        public PostMeta Meta { get; set; }
    }

    public class PostMeta
    {
        [JsonProperty("attribute_transfers_pa_transport_type")]
        public string AttributeTransfersPaTransportType { get; set; }

        [JsonProperty("attribute_transfers_pa_destination_to")]
        public string AttributeTransfersPaDestinationTo { get; set; }

        [JsonProperty("attribute_transfers_pa_destination_from")]
        public string AttributeTransfersPaDestinationFrom { get; set; }

        [JsonProperty("_virtual")]
        public string Virtual { get; set; }

        [JsonProperty("_stock_status")]
        public string StockStatus { get; set; }

        [JsonProperty("_sold_individually")]
        public string SoldIndividually { get; set; }

        [JsonProperty("_downloadable")]
        public string Downloadable { get; set; }
    }

    public class ListOrders
    {
        [JsonProperty("orders")]
        public Order[] Orders { get; set; }
    }

    public class Order
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("order_number")]
        public long OrderNumber { get; set; }

        [JsonProperty("order_key")]
        public string OrderKey { get; set; }

        [JsonProperty("created_at")]
        public System.DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public System.DateTimeOffset UpdatedAt { get; set; }

        [JsonProperty("completed_at")]
        public System.DateTimeOffset CompletedAt { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("subtotal")]
        public string Subtotal { get; set; }

        [JsonProperty("total_line_items_quantity")]
        public long TotalLineItemsQuantity { get; set; }

        [JsonProperty("total_tax")]
        public string TotalTax { get; set; }

        [JsonProperty("total_shipping")]
        public string TotalShipping { get; set; }

        [JsonProperty("cart_tax")]
        public string CartTax { get; set; }

        [JsonProperty("shipping_tax")]
        public string ShippingTax { get; set; }

        [JsonProperty("total_discount")]
        public string TotalDiscount { get; set; }

        [JsonProperty("shipping_methods")]
        public string ShippingMethods { get; set; }

        [JsonProperty("payment_details")]
        public PaymentDetails PaymentDetails { get; set; }

        [JsonProperty("billing_address")]
        public BillingAddress BillingAddress { get; set; }

        [JsonProperty("shipping_address")]
        public BillingAddress ShippingAddress { get; set; }

        [JsonProperty("note")]
        public string Note { get; set; }

        [JsonProperty("customer_ip")]
        public string CustomerIp { get; set; }

        //[JsonProperty("customer_user_agent")]
        //public string CustomerUserAgent { get; set; }

        [JsonProperty("customer_id")]
        public long CustomerId { get; set; }

        [JsonProperty("view_order_url")]
        public string ViewOrderUrl { get; set; }

        [JsonProperty("line_items")]
        public List<LineItem> LineItems { get; set; }

        //[JsonProperty("shipping_lines")]
        //public object[] ShippingLines { get; set; }

        //[JsonProperty("tax_lines")]
        //public object[] TaxLines { get; set; }

        //[JsonProperty("fee_lines")]
        //public object[] FeeLines { get; set; }

        //[JsonProperty("coupon_lines")]
        //public object[] CouponLines { get; set; }

        [JsonProperty("is_vat_exempt")]
        public bool IsVatExempt { get; set; }

        [JsonProperty("customer")]
        public Customer Customer { get; set; }

        [JsonProperty("order_meta")]
        public OrderMeta OrderMeta { get; set; }
    }

    public class BillingAddress
    {
        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("company")]
        public string Company { get; set; }

        [JsonProperty("address_1")]
        public string Address1 { get; set; }

        [JsonProperty("address_2")]
        public string Address2 { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("postcode")]
        public string Postcode { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }
    }

    public class Customer
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("billing_address")]
        public BillingAddress BillingAddress { get; set; }

        [JsonProperty("shipping_address")]
        public BillingAddress ShippingAddress { get; set; }
    }

    public class LineItem
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("subtotal")]
        public string Subtotal { get; set; }

        [JsonProperty("subtotal_tax")]
        public string SubtotalTax { get; set; }

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("total_tax")]
        public string TotalTax { get; set; }

        [JsonProperty("price")]
        public string Price { get; set; }

        [JsonProperty("quantity")]
        public long Quantity { get; set; }

        [JsonProperty("tax_class")]
        public object TaxClass { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("product_id")]
        public string ProductId { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("meta")]
        public Meta[] Meta { get; set; }
    }

    public class Meta
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public class OrderMeta
    {
        [JsonProperty("flight_arrival")]
        public string FlightArrival { get; set; }

        [JsonProperty("flight_departure")]
        public string FlightDeparture { get; set; }

        [JsonProperty("billing_hotel")]
        public string BillingHotel { get; set; }

        [JsonProperty("wpml_language")]
        public string WpmlLanguage { get; set; }

        [JsonProperty("Payer PayPal address")]
        public string PayerPayPalAddress { get; set; }

        [JsonProperty("Payer first name")]
        public string PayerFirstName { get; set; }

        [JsonProperty("Payer last name")]
        public string PayerLastName { get; set; }

        [JsonProperty("Payment type")]
        public string PaymentType { get; set; }

        [JsonProperty("PayPal Transaction Fee")]
        public string PayPalTransactionFee { get; set; }

        [JsonProperty("flight_arrival_datetime")]
        public DateTime? FlightDate1 { get; set; }

        [JsonProperty("flight_departure_datetime")]
        public DateTime? FlightDate2 { get; set; }
    }

    public class PaymentDetails
    {
        [JsonProperty("method_id")]
        public string MethodId { get; set; }

        [JsonProperty("method_title")]
        public string MethodTitle { get; set; }

        [JsonProperty("paid")]
        public bool Paid { get; set; }
    }


    public class Transfer
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("availability_id")]
        public string AvailabilityId { get; set; }

        [JsonProperty("booking_datetime")]
        public System.DateTimeOffset BookingDatetime { get; set; }

        [JsonProperty("people_count")]
        public string PeopleCount { get; set; }

        [JsonProperty("is_private")]
        public string IsPrivate { get; set; }

        [JsonProperty("total_price")]
        public string TotalPrice { get; set; }

        [JsonProperty("user_id")]
        public string UserId { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("town")]
        public string Town { get; set; }

        [JsonProperty("zip")]
        public string Zip { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("woo_order_id")]
        public string WooOrderId { get; set; }

        [JsonProperty("cart_key")]
        public string CartKey { get; set; }

        [JsonProperty("woo_status")]
        public string WooStatus { get; set; }

        [JsonProperty("created")]
        public System.DateTimeOffset Created { get; set; }
    }

    public class Integration
    {

        AgentsModel agent = new AgentsModel();
        string appKey = "";
        string appSecret = "";
        string appDomain = "";

        public Integration()
        {

            appKey = ConfigurationManager.AppSettings["APIKey"];
            appSecret = ConfigurationManager.AppSettings["APISecret"];
            appDomain = ConfigurationManager.AppSettings["OnlineBookingHost"];
            var agentConfig = ConfigurationManager.AppSettings["OnlineBookingUserID"].ToString() ?? "0";

            agent = agent.FindAgentByID(Convert.ToInt32(agentConfig));

            if (agent == null) throw new Exception("WooCommerce Agent not found!");
            agent.Type = 2;

        }


        [SkipWhenPreviousJobIsRunningAttribute]
        public void SyncCompletedOrders()
        {

            synclog lastSync = new synclog();

            using (isebookingEntities db = new isebookingEntities())
            {

                lastSync = db.synclogs.FirstOrDefault();

                if (lastSync == null)
                {
                    lastSync = new synclog();
                    lastSync.DateCreated = DateTime.Now;
                    lastSync.DateLastUpdated = DateTime.Now;
                    lastSync.CompletedDate = new DateTime(2017, 7, 1);
                    db.synclogs.Add(lastSync);
                    db.SaveChanges();
                }




            }


            RestClient client = new RestClient();

            client.BaseUrl = new Uri(appDomain + "/" + "wc-api/v3/orders");

            RestRequest request = new RestRequest();
            request.AddParameter("consumer_key", appKey);
            request.AddParameter("consumer_secret", appSecret);
            //CUSTOM FIELDS
            request.AddParameter("filter[meta]", "true");
            request.AddParameter("status", "completed");
            request.AddParameter("filter[orderby]", "completed_at");
            request.AddParameter("filter[order]", "asc");
            request.AddParameter("filter[limit]", 25);
            request.AddParameter("filter[created_at_min]", lastSync.CompletedDate.ToString("s") + "Z");
            //  request.AddParameter("filter[offset]", 10 + lastPage);

            request.Method = Method.GET;


            IRestResponse response = client.Execute(request);

            isebookingEntities db2 = new isebookingEntities();
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                //var data = Newtonsoft.Json.JsonConvert.DeserializeObject<ListOrders>(response.Content);
                JObject jo = JObject.Parse(response.Content);

                var data = jo.SelectToken("orders", false).ToObject<Order[]>();


                if (data.Length == 0) return;

                var latestDate = data.Max(x => x.CompletedAt).AddMinutes(-5);
                UpdateLastPage(latestDate.LocalDateTime);

                foreach (var myorder in data)
                {



                    // CreateBooking(myorder)
                    Hangfire.BackgroundJob.Enqueue(() => CreateBooking(myorder));
                }


            }
            else
            {
                throw new Exception((string)response.Content);
            }

        }

        public void CreateOrderFromWebHook(long ordernumber)
        {
            RestClient client = new RestClient();
            Order myorder = new Order();

            client.BaseUrl = new Uri(appDomain);

            RestRequest request = new RestRequest("wc-api/v3/orders/{id}");

            request.AddUrlSegment("id", ordernumber.ToString());
            request.AddParameter("consumer_key", appKey);
            request.AddParameter("consumer_secret", appSecret);
            //CUSTOM FIELDS
            request.AddParameter("filter[meta]", "true");


            //  request.AddParameter("filter[offset]", 10 + lastPage);

            request.Method = Method.GET;


            IRestResponse response = client.Execute(request);

            
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                JObject jo = JObject.Parse(response.Content);
                myorder = jo.SelectToken("order", false).ToObject<Order>();

                // CreateBooking(myorder)
                Hangfire.BackgroundJob.Enqueue(() => CreateBooking(myorder));

            }
            else if (response.StatusCode == 0)
            {
                throw response.ErrorException;
            }
            else
            {
                throw new Exception((string)response.Content);
            }
        }

        public void CreateBooking(Order myorder)
        {

            try
            {


                DeleteEntryIfMissing(myorder.OrderNumber);



                Bookings bookRepo = new Bookings();
                int adults = 0;
                int childrens = 0;
                int infants = 0;
                int rtt = 0;

                int people = 0;

                string hotel = myorder.OrderMeta.BillingHotel ?? "";
                hotel = hotel.Trim().ToUpper();

                List<BookingOtherChargesIndicatorModel> otherChargesIndicators = new List<BookingOtherChargesIndicatorModel>();
                otherChargesIndicators.Add(new BookingOtherChargesIndicatorModel { Amount = 0, ChargeDescription = "", ChargingID = 1, IsChecked = true, BookingID = 0 });
                otherChargesIndicators.Add(new BookingOtherChargesIndicatorModel { Amount = 0, ChargeDescription = "", ChargingID = 3, IsChecked = true, BookingID = 0 });

                List<string> ocs = new List<string>();

                BookingHeaderModel newBook = new BookingHeaderModel();
                List<BookingDetailsModel> details = new List<BookingDetailsModel>();
                newBook.Class = ClassRate.REGULAR;
                newBook.Class2 = (int)ClassRate.REGULAR;
                newBook.ContactNumber = myorder.BillingAddress.Phone;
                newBook.ContactPerson = myorder.BillingAddress.FirstName + " " + myorder.BillingAddress.LastName ?? " ";
                newBook.DropOffLocation1 = GetHotel(hotel);


                newBook.Escorts = 0;

                //PARSE DATETIME FROM ORDERMETA.FLIGHT
                newBook.FlightDate1 = myorder.OrderMeta.FlightDate1;


                newBook.FlightNo1 = myorder.OrderMeta.FlightArrival;
                newBook.FlightNo2 = myorder.OrderMeta.FlightDeparture;
                newBook.FOC = 0;
                newBook.GroupName = myorder.BillingAddress.FirstName + " " + myorder.BillingAddress.LastName ?? " ";
                newBook.Infants = infants;
                newBook.Childrens = childrens;
                newBook.Adults = adults;
                newBook.ChildrensBelowSix = 0;
                newBook.Senior = 0;
                newBook.OnlineBookingOrderID = myorder.OrderNumber.ToString();


                newBook.Remarks = "ONLINE BOOKING#: " + myorder.OrderNumber.ToString() + Environment.NewLine + myorder.Note;

                newBook.Status = BookingStatus.SAVED;

                newBook.TourGuides = 0;

                newBook.DateCreated = DateTime.Now.ToString();

                newBook.PaymentTypeID = PaymentType.BUY_AND_BOOK;




                newBook.CreatedBy = 0;
                newBook.Routing = 1;

                foreach (var prod in myorder.LineItems.Where(x => !x.Name.Contains("Transfers Product")).OrderBy(x => x.Id).ToList())
                {
                    BookingDetailsModel detail = new BookingDetailsModel();
                    detail.Amount = Convert.ToDouble(prod.Price);
                    detail.Boat = 0;
                    detail.BookingID = 0;
                    detail.Description = prod.Name;
                    detail.Quantity = Convert.ToInt32(prod.Quantity);
                    detail.SourceID = GetSourceID(prod.Name);

                    details.Add(detail);

                    people = Convert.ToInt32(prod.Quantity);

                }

                var transferProducts = myorder.LineItems.Where(x => x.Name.Contains("Transfers Product")).OrderBy(x => x.Id).ToList();



                foreach (var prod in transferProducts)
                {
                    var root = GetProductVariation(Convert.ToInt64(prod.ProductId), myorder.OrderNumber);


                    var postMeta = root.ProductMeta.Meta;

                    var transfers = root.Transfer;

                    var bookingDate = (from a in root.Transfer

                                       group a by a.WooOrderId into pg
                                       select new
                                       {
                                           Arrival = pg.Min(x => x.BookingDatetime.LocalDateTime),
                                           Departure = pg.Max(x => x.BookingDatetime.LocalDateTime)
                                       }).FirstOrDefault();

                    if (bookingDate == null) throw new Exception("Transfer booking not found!");

                    string transfers_pa_booking_id = prod.Meta.Where(x => x.Key == "transfers_pa_booking_id").Select(x => x.Value).FirstOrDefault() ?? "";

                    people = Convert.ToInt32((transfers.Where(x => x.Id == transfers_pa_booking_id).Select(x => x.PeopleCount).FirstOrDefault() ?? "0"));

                    if (postMeta.AttributeTransfersPaTransportType.ToUpper().Contains("ADULT"))
                    {
                        adults = people;
                    }
                    if (postMeta.AttributeTransfersPaTransportType.ToUpper().Contains("CHILD"))
                    {
                        childrens = people;
                    }
                    if (postMeta.AttributeTransfersPaTransportType.ToUpper().Contains("INFANT"))
                    {
                        infants = people;
                    }



                    BookingDetailsModel detail = new BookingDetailsModel();
                    detail.Amount = Convert.ToDouble(prod.Price) / people;
                    detail.Boat = 0;
                    detail.BookingID = 0;
                    detail.Description = postMeta.AttributeTransfersPaTransportType + " " + postMeta.AttributeTransfersPaDestinationFrom + " - " + postMeta.AttributeTransfersPaDestinationTo;
                    detail.Quantity = people;
                    detail.SourceID = 1;

                    details.Add(detail);

                    if (rtt == 0)
                    {
                        newBook.PickUpArrival = GetDestinations(postMeta.AttributeTransfersPaDestinationFrom.Trim());
                        newBook.DropOffArrival = GetDestinations(postMeta.AttributeTransfersPaDestinationTo.Trim());
                        newBook.FlightDate1 = bookingDate.Arrival;
                        if (bookingDate.Arrival == bookingDate.Departure)
                        {

                            newBook.Routing = 1;
                        }
                        else
                        {


                            newBook.DropOffDeparture = GetDestinations(postMeta.AttributeTransfersPaDestinationFrom.Trim());
                            newBook.PickUpDeparture = GetDestinations(postMeta.AttributeTransfersPaDestinationTo.Trim());
                            newBook.FlightDate2 = bookingDate.Departure;
                            newBook.DropOffLocation2 = newBook.DropOffLocation1;
                            newBook.Routing = 2;
                        }
                    }



                    rtt += 1;
                }

                //OTHER CHARGES SELECTED
                if (adults > 0)
                {
                    ocs.Add("withTFInAdult");
                    ocs.Add("withEnvFeeAdult");

                    if (newBook.Routing == 2)
                    {
                        ocs.Add("withTFOutAdult");
                    }
                }

                //OTHER CHARGES SELECTED
                if (childrens > 0)
                {
                    ocs.Add("withTFInChildren");
                    ocs.Add("withEnvFeeChildren");

                    if (newBook.Routing == 2)
                    {
                        ocs.Add("withTFOutChildren");
                    }

                }


                if (newBook.Routing == 2)
                {
                    otherChargesIndicators.Add(new BookingOtherChargesIndicatorModel { Amount = 0, ChargeDescription = "", ChargingID = 2, IsChecked = true, BookingID = 0 });
                }


                newBook.Total = (float)details.Sum(x => (x.Amount + x.Boat) * x.Quantity);
                newBook.Adults = adults;
                newBook.Childrens = childrens;
                newBook.Infants = infants;
                newBook.ChildrensBelowSix = 0;

                bookRepo.Header = newBook;
                bookRepo.Details = details;
                bookRepo.OtherCharges = otherChargesIndicators;
                bookRepo.OCs = ocs.ToArray();


                ValidateOnlineBooking(myorder.OrderNumber);

                var book = bookRepo.CreateBooking(bookRepo, agent);

            }
            catch (Exception)
            {
                Hangfire.BackgroundJob.Enqueue(() => DeleteEntryIfMissing(myorder.OrderNumber));
                
            }











        }



        public int GetHotel(string name)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                var result = db.resortsandhotels.Where(x => x.Name.Contains(name)).FirstOrDefault();

                if (result == null)
                {
                    result = new resortsandhotel();
                    result.DateCreated = DateTime.Now;
                    result.DateLastUpdated = DateTime.Now;
                    result.IsActive = true;
                    result.Name = name;
                    db.resortsandhotels.Add(result);
                    db.SaveChanges();
                }

                return result.ID;
            }
        }


        public void LogError(long orderID, string errorMessage)
        {
            using (var db = new isebookingEntities())
            {
                var syncError = new syncerror();
                syncError.DateCreated = DateTime.Now;
                syncError.OrderID = (int)orderID;
                syncError.Remarks = errorMessage;
                db.syncerrors.Add(syncError);
                db.SaveChanges();
            }

        }


        private void ValidateOnlineBooking(long orderid)
        {
            string errorMessage = "";

            using (var db = new isebookingEntities())
            {


                try
                {

                    var onlineBooking = new onlinebooking();
                    onlineBooking.DateCreated = DateTime.Now;
                    onlineBooking.OnlineBookingID = (int)orderid;

                    db.onlinebookings.Add(onlineBooking);
                    db.SaveChanges();


                }
                catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                {
                    Exception ex = dbU.GetBaseException();
                    errorMessage = ex.Message;
                    throw new Exception(ex.Message);

                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;

                    throw;
                }
                finally
                {
                    if (!String.IsNullOrEmpty(errorMessage))
                    {
                        Hangfire.BackgroundJob.Enqueue(() => LogError(orderid, errorMessage));
                    }


                    db.Database.Connection.Close();
                }



            }
        }

        public void DeleteEntryIfMissing(long orderid)
        {
            using (var db = new isebookingEntities())
            {
                try
                {

                    var book = db.bookings.Where(x => x.OnlineBookingOrderID == orderid.ToString()).FirstOrDefault();

                    if (book == null)
                    {
                        var onlineBooking = db.onlinebookings.Where(x => x.OnlineBookingID == orderid).FirstOrDefault();
                        db.onlinebookings.Remove(onlineBooking);
                        db.SaveChanges();
                    }

                }

                catch (Exception)
                {

                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }

        }

        public int GetDestinations(string description)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                var result = db.destinations.Where(x => x.Description.Contains(description)).FirstOrDefault();

                if (result == null)
                {
                    //OTHERS
                    return 7;
                }
                else
                {
                    return result.ID;
                }
            }

        }

        private int GetSourceID(string description)
        {

            if (description.ToUpper().Contains("TRANSFER"))
            {
                return 1;
            }
            else if (description.ToUpper().Contains("ENVIRONMENT"))
            {
                return 2;
            }
            else if (description.ToUpper().Contains("ENVIRONMENT"))
            {
                return 2;
            }
            else if (description.ToUpper().Contains("TERMINAL"))
            {
                return 4;
            }
            else
            {
                return 6;
            }


        }

        private void UpdateLastPage(DateTime latest)
        {


            using (isebookingEntities db = new isebookingEntities())
            {

                var lastSync = db.synclogs.OrderByDescending(x => x.ID).FirstOrDefault();

                lastSync.CompletedDate = latest;

                db.SaveChanges();



            }



        }

        private Root GetProductVariation(long productID, long orderid)
        {


            RestClient client = new RestClient();

            client.BaseUrl = new Uri(appDomain);

            RestRequest request = new RestRequest("wp-json/b2b/{id}");
            request.AddUrlSegment("id", productID.ToString());
            request.AddParameter("consumer_key", appKey);
            request.AddParameter("consumer_secret", appSecret);
            request.AddParameter("orderid", orderid);

            // request.AddParameter("id", appDomain, ParameterType.UrlSegment);
            // request.Resource = "{id}";

            request.Method = Method.GET;


            IRestResponse response = client.Execute(request);




            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var data = Newtonsoft.Json.JsonConvert.DeserializeObject<Root>(response.Content);
                //JObject jo = JObject.Parse(response.Content);

                //var data = jo.ToObject<PostMeta[]>();
                //return data.FirstOrDefault() ?? new PostMeta();

                if (data == null)
                {
                    return new Root();
                }
                else
                {
                    // return data[0].Meta ?? new PostMeta();
                    return data ?? new Root();
                }

            }
            else
            {
                throw new Exception((string)response.Content);
            }

        }



    }

}
