﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public class DestinationsModel
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime DateLastUpdated { get; set; }

        public Boolean IsActive { get; set; }

        public List<DestinationsModel> GetDestinations()
        {
            using (isebookingEntities db = new isebookingEntities()) {

                return (from a in db.destinations
                       where a.IsActive == true
                       select new DestinationsModel
                       {
                           DateCreated = a.DateCeated,
                           DateLastUpdated = a.DateLastUpdated,
                           Description = a.Description,
                           ID = a.ID,
                           Code = a.Code,
                           IsActive = a.IsActive
                       }).ToList<DestinationsModel>();


            }

        }

    }
}
