﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public class BookingAttachmentsModel
    {
        public int ID { get; set; }
        public string Filename { get; set; }
        public string MimeType { get; set; }
        public DateTime DateCreated { get; set; }
        public int BookingID { get; set; }
        public string OriginalFilename { get; set; }
    }
}
