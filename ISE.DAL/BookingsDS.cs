﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public class BookingsDS
    {
        public int ID { get; set; }
        public string GroupName { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNumber { get; set; }
        public string Remarks { get; set; }
        public string CVNumber { get; set; }
        public int Adults { get; set; }
        public int Childrens { get; set; }
        public int Infants { get; set; }
        public int TourGuides { get; set; }
        public int Escorts { get; set; }
        public int FOC { get; set; }
        public int Routing { get; set; }
        public int PickUpArrival { get; set; }
        public int DropOffArrival { get; set; }
        public string DropOffLocation { get; set; }
        public Nullable<int> PickUpDeparture { get; set; }
        public Nullable<int> DropOffDeparture { get; set; }
        public string FlightNo1 { get; set; }
        public string FlightNo2 { get; set; }
        public Nullable<System.DateTime> FlightDate1 { get; set; }
        public Nullable<System.DateTime> FlightDate2 { get; set; }
        public decimal Total { get; set; }
        public int CreatedBy { get; set; }
        public int Status { get; set; }
        public System.DateTime DateCreated { get; set; }
        public string DateLastUpdated { get; set; }
        public int ChildrensBelowSix { get; set; }
        public string DropOffLocation1 { get; set; }
        public string DropOffLocation2 { get; set; }
        public string AgentName { get; set; }
        public string FromDestination { get; set; }
        public string ToDestination { get; set; }
        public ClassRate Class { get; set; }
    }
}
