﻿using HashidsNet;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public class SyncState
    {
        public string CVNumber { get; set; }
        public int Status { get; set; }
        public string ErrorMessage { get; set; }
        public SyncState()
        {
            CVNumber = "";
            Status = 0;
            ErrorMessage = "";
        }
    }

    public class ManifestoModel
    {
        public int ID { get; set; }
        public int BookingID { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateLastUpdated { get; set; }
        public int Pax { get; set; }
        public int ResortHotel { get; set; }
        public System.DateTime Date { get; set; }

        public string Remarks { get; set; }
        public int Pickup { get; set; }
        public int DropOff { get; set; }
        public int Adults { get; set; }
        public int Kids { get; set; }
        public int Infants { get; set; }
        public int Type { get; set; }
        public string FlightNo { get; set; }
        public string ResortHotelDescription { get; set; }
        public int TourGuides { get; set; }
        public int Escorts { get; set; }
        public int FOC { get; set; }


        public List<ManifestoDetailsModel> Details { get; set; }

        public string Guests { get; set; }

        public string LocationArrival { get; set; }
        public string LocationDeparture { get; set; }

        public BookingHeaderModel BookingHeader { get; set; }

    }


    public class ManifestoSchedules
    {
        public ManifestoSchedModel Header { get; set; }
        public List<ManifestoDataset> Details { get; set; }

        public ManifestoSchedules()
        {
            Header = new ManifestoSchedModel();
            Details = new List<ManifestoDataset>();


        }

        public Boolean VerifyAuthCode(string code, int travelAgencyID)
        {
            using (var db = new isebookingEntities())
            {
                var result = db.authorizationcodes.Where(x => x.TravelAgencyID == travelAgencyID && x.AuthCode == code && x.Status == 1).FirstOrDefault();
                if (result == null) throw new Exception("Authorization denied!");

                return true;
            }
        }

        public void RevokeAuthCode(string code)
        {
            using (var db = new isebookingEntities())
            {
                var result = db.authorizationcodes.Where(x => x.AuthCode == code).FirstOrDefault();

                result.Status = 0;
                db.SaveChanges();
            }
        }

        public void GenerateAuthCodes()
        {
            using (var db = new isebookingEntities())
            {
                 var result = db.travelagencies.Select(x => x.ID).ToList();

                foreach (var id in result)
                {
                    Hangfire.BackgroundJob.Enqueue(() => GenerateAuthCodeByAgency(id));
                }
              
                //var coordinators = db.agents.Where(x => x.Type == (int)AgentType.COORDINATOR).ToList();

                //foreach (var x in coordinators)
                //{
                //    Hangfire.BackgroundJob.Enqueue(() => GenerateAuthCodeByAgent(x.ID));
                //}

            }
        }

        public void GenerateAuthCodeByAgency(int id)
        {
            using (var db = new isebookingEntities())
            {

                for (int i = 0; i < 5; i++)
                {

                    using (var transaction = db.Database.BeginTransaction())
                    {
                        var hasher = new Hashids("!S3_BOOK1ng", 6, "ABCDEFGHIJKLMNPQRSTUVWXYZ1234567890");
                        var guid = Guid.NewGuid();

                        authorizationcode authCode = new authorizationcode();
                        authCode.DateCreated = DateTime.Now;
                        authCode.DateLastUpdated = DateTime.Now;
                        authCode.AuthCode = guid.ToString();
                        authCode.Status = 1;
                        authCode.TravelAgencyID = id;
                        db.authorizationcodes.Add(authCode);
                        db.SaveChanges();
                        authCode.AuthCode = hasher.Encode(authCode.ID);
                        db.SaveChanges();

                        transaction.Commit();

                    }

                }

            }
        }


        public void GenerateAuthCodeByAgent(int id)
        {
            using (var db = new isebookingEntities())
            {

                for (int i = 0; i < 15; i++)
                {

                    using (var transaction = db.Database.BeginTransaction())
                    {
                        var hasher = new Hashids("!S3_BOOK1ng", 8, "ABCDEFGHIJKLMNPQRSTUVWXYZ1234567890");
                        var guid = Guid.NewGuid();

                        agentauthcode authCode = new agentauthcode();
                        authCode.DateCreated = DateTime.Now;
                        authCode.DateUpdated = DateTime.Now;
                        authCode.Code = guid.ToString();
                        authCode.Status = 0;
                        authCode.AgentID = id;
                        db.agentauthcodes.Add(authCode);
                        db.SaveChanges();
                        authCode.Code = hasher.Encode(authCode.ID);
                        db.SaveChanges();

                        transaction.Commit();

                    }

                }

            }
        }



        public List<ManifestoDataset> GetArivingManifesto(DateTime date,string touchPointLoc,int tpMarker=0)
        {
            touchPointLoc = touchPointLoc.Split(' ')[0];
            using (var db = new isebookingEntities())
            {
                var tp =  db.destinations;

                int tpLocOrderID = tp.Where(x => x.Description.Contains(touchPointLoc) && x.IsActive == true).FirstOrDefault().orderBy.Value;
                int[] tpIDs = tp.Where(x => x.orderBy <= tpLocOrderID).Select(x => x.ID).ToArray<int>();       
               

                var existing = (from a in db.manifestoscheds
                                join b in db.manifestoscheddetails on a.ID equals b.ManifestoSchedID
                                where a.ManisfestoType == 1 && a.Location.Contains(touchPointLoc) && a.Touchpoint == tpMarker && a.ManifestoDate.Day == date.Day && a.ManifestoDate.Month == date.Month && a.ManifestoDate.Year == date.Year
                                select b.CVNumber).ToList();
                Reporting reports = new Reporting();

                var data = reports.GetManifestoArrivals(date);

                return data.Where(x => !existing.Contains(x.CVNumber) && tpIDs.Contains(x.Pickup)).ToList();

            }


        }

        public List<ManifestoDataset> GetArivingManifestoAdditional(DateTime date, string touchPointLoc, int tpMarker = 0)
        {
            touchPointLoc = touchPointLoc.Split(' ')[0];
            using (var db = new isebookingEntities())
            {
                var tp = db.destinations;

                int tpLocOrderID = tp.Where(x => x.Description.Contains(touchPointLoc) && x.IsActive == true).FirstOrDefault().orderBy.Value;
                int[] tpIDs = tp.Where(x => x.orderBy <= tpLocOrderID).Select(x => x.ID).ToArray<int>();
               // int tpLocOrderID = tp.Where(x => x.Description.Contains(touchPointLoc) && x.IsActive == true).FirstOrDefault().ID;
               // int[] tpIDs = tp.Where(x => x.orderBy <= tpLocOrderID).Select(x => x.ID).ToArray<int>();


                var existing = (from a in db.manifestoscheds
                                join b in db.manifestoscheddetails on a.ID equals b.ManifestoSchedID
                                where a.ManisfestoType == 1 && a.Location.Contains(touchPointLoc) && a.Touchpoint == tpMarker && a.ManifestoDate.Day == date.Day && a.ManifestoDate.Month == date.Month && a.ManifestoDate.Year == date.Year
                                select b.CVNumber).ToList();
                Reporting reports = new Reporting();

                var data = reports.GetManifestoArrivals(date);

                // return data.Where(x => !existing.Contains(x.CVNumber) && tpLocOrderID == x.Pickup).ToList();
                return data.Where(x => !existing.Contains(x.CVNumber) && tpIDs.Contains(x.Pickup)).ToList();

            }


        }

        public List<ManifestoDataset> GetDepartingManifesto(DateTime date,string touchPointLoc,int tpMarker=0)
        {

            using (var db = new isebookingEntities())
            {
                var tp = db.destinations;

                int tpLocOrderID = tp.Where(x => x.Description.Contains(touchPointLoc) && x.IsActive == true).FirstOrDefault().orderBy.Value;
                int[] tpIDs = tp.Where(x => x.orderBy >= tpLocOrderID).Select(x => x.ID).ToArray<int>();
                var existing = (from a in db.manifestoscheds
                                join b in db.manifestoscheddetails on a.ID equals b.ManifestoSchedID
                                where a.ManisfestoType == 2 && a.Location==touchPointLoc && a.Touchpoint==tpMarker
                                select b.CVNumber).ToList();
                Reporting reports = new Reporting();

                var data = reports.GetManifestoDepartures(date);

                return data.Where(x => !existing.Contains(x.CVNumber) && tpIDs.Contains(x.Pickup)).ToList();

            }

        }


        public List<ManifestoSchedModel> GetSchedules(ManifestoStatus status)
        {
            using (var db = new isebookingEntities())
            {
                var desti = db.destinations.ToList();
                var manifescheddetails = db.manifestoscheddetails.GroupBy(x => x.ManifestoSchedID).Select(x => new { id = x.Key, pax=  x.Sum(a => a.Adult + a.Kid + a.FOC + a.Escort + a.TourGuide) });
                return (from a in db.manifestoscheds
                        join b in db.fleets on a.VehicleID equals b.ID
                        join c in db.agents on a.CoordinatorID equals c.ID
                        join d in manifescheddetails on a.ID equals d.id
                        where
                       (status == ManifestoStatus.UNKNOWN ? true : (a.Status == (int)status))
                        select new ManifestoSchedModel
                        {
                            ID = a.ID,
                            CoordinatorID = a.CoordinatorID,
                            Coordinator = c.FirstName + " " + c.LastName,
                            DateCreated = a.DateCreated,
                            DateLastUpadated = a.DateLastUpadated,
                            Driver = a.Driver,
                            ManifestoDate = a.ManifestoDate,
                            ManifestoType = a.ManisfestoType,
                            Status = (ManifestoStatus)a.Status,
                            VehicleID = a.VehicleID,
                            Vehicle = b.VIDNumber + " " + b.Type,
                            Location = a.Location,
                            TouchPoint = a.Touchpoint,
                            TotalPax = d.pax
                           
                        }).OrderBy(x=>x.ManifestoDate).ThenBy(x=>x.DateCreated).ToList();

            }
        }

        public List<ManifestoDataset> GetSchedulesByCoordinator(AgentsModel agent, DateTime fromDate)
        {
            Reporting reports = new Reporting();
            var data = reports.GetManifestoArrivals(fromDate);
            var data2 = reports.GetManifestoDepartures(fromDate);

            return data.Union(data2).ToList();

        }

        public async Task<Boolean> CreateOrUpdateManifestoSchedule(ManifestoSchedules myManifesto, AgentsModel agent)
        {


            using (var db = new isebookingEntities())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (myManifesto.Details.Count == 0) throw new Exception("Please add details for this manifesto!");


                        Boolean isNewEntry = false;

                        manifestosched newSched = db.manifestoscheds.Where(x => x.ID == myManifesto.Header.ID).FirstOrDefault();

                        if (newSched == null)
                        {
                            isNewEntry = true;
                            newSched = new manifestosched();
                            newSched.DateCreated = DateTime.Now;
                            newSched.CreatedBy = agent.ID;
                        }

                        newSched.CoordinatorID = myManifesto.Header.CoordinatorID;


                        newSched.DateLastUpadated = DateTime.Now;
                        newSched.Driver = myManifesto.Header.Driver;
                        newSched.ManifestoDate = myManifesto.Header.ManifestoDate;
                        newSched.ManisfestoType = myManifesto.Header.ManifestoType;
                        newSched.Status = (int)ManifestoStatus.OPEN;
                        newSched.VehicleID = myManifesto.Header.VehicleID;
                        newSched.Location = myManifesto.Header.Location;
                        newSched.Touchpoint = myManifesto.Header.TouchPoint;

                        if (isNewEntry)
                        {
                            db.manifestoscheds.Add(newSched);
                        }


                        db.SaveChanges();

                        var details = db.manifestoscheddetails.Where(x => x.ManifestoSchedID == newSched.ID && isNewEntry == false).ToList();

                        db.manifestoscheddetails.RemoveRange(details);

                        foreach (var detail in myManifesto.Details)
                        {
                            manifestoscheddetail schedDetail = new manifestoscheddetail();
                            schedDetail.AccountName = detail.GroupName;
                            schedDetail.CVNumber = detail.CVNumber;
                            schedDetail.Adult = detail.Adults;

                            schedDetail.Escort = detail.Escorts;
                            schedDetail.FOC = detail.FOC;
                            schedDetail.Infant = detail.Infants;
                            schedDetail.Kid = detail.Kids;
                            schedDetail.Location = detail.Location;
                            schedDetail.TourGuide = detail.TourGuides;


                            schedDetail.ActualAdult = detail.ActualAdults;
                            schedDetail.ActualEscort = detail.ActualEscorts;
                            schedDetail.FOC = detail.ActualFOC;
                            schedDetail.ActualInfant = detail.ActualInfants;
                            schedDetail.ActualKid = detail.ActualKids;
                            schedDetail.ActualTourGuide = detail.ActualTourGuides;

                            schedDetail.ActualDate = detail.ActualDate;
                            schedDetail.ActualTime = detail.ActualTime;
                            schedDetail.BookingID = detail.BookingID;
                            

                            schedDetail.ManifestoSchedID = newSched.ID;
                            db.manifestoscheddetails.Add(schedDetail);

                        }

                        db.SaveChanges();
                        transaction.Commit();
                        // schedID = newSched.ID;
                        //status = 1;
                        return true;
                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();
                        Exception ex = dbU.GetBaseException();
                        // errMsg = ex.Message;
                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();
                        string errorMessages = null;
                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }
                        // errMsg = errorMessages;
                        throw new Exception(errorMessages);
                    }
                    finally
                    {
                        db.Database.Connection.Close();

                    }


                    //   return new SyncState { ErrorMessage = errMsg, ScheduleID = schedID, Status = status };

                }
            }
        }

        public void AddManifestoDetails(string unit_atd,string location,int bookID,int coordinatorID,int type) {

            using(var db = new isebookingEntities())
            {
                Bookings bookRepo = new Bookings();
                var book = bookRepo.GetBooking(bookID).Header;

                var fleetUnit = db.fleets.Where(x => x.VIDNumber == unit_atd).FirstOrDefault();
                if (fleetUnit == null) throw new Exception("Fleet not found!");

                var flightDate = book.FlightDate1.Value.Date;

                var manifestSched = db.manifestoscheds.Where(x => x.CoordinatorID == coordinatorID 
                && x.VehicleID == fleetUnit.ID && x.ManifestoDate == flightDate && x.ManisfestoType ==type).FirstOrDefault();

                if (manifestSched != null)
                {

                    var newDetails = new manifestoscheddetail();
                    newDetails.ManifestoSchedID = manifestSched.ID;
                    newDetails.Location = location;
                    newDetails.CVNumber = book.CVNumber;
                    newDetails.AccountName = book.GroupName;
                    newDetails.Adult = book.Adults;
                    newDetails.Kid = book.Childrens;
                    newDetails.FOC = book.FOC;
                    newDetails.Infant = book.Infants;
                    newDetails.TourGuide = book.TourGuides;
                    newDetails.Escort = book.Escorts;

                    newDetails.ActualAdult = book.Adults;
                    newDetails.ActualFOC = book.FOC;
                    newDetails.ActualKid = book.Childrens;
                    newDetails.ActualInfant = book.Infants;
                    newDetails.ActualTourGuide = book.TourGuides;
                    newDetails.ActualEscort = book.Escorts;
                    newDetails.BookingID = book.ID;
                    newDetails.ActualTime = null;
                    newDetails.ActualDate = null;

                    db.manifestoscheddetails.Add(newDetails);
                    db.SaveChanges();
                }

            }

        }

        public void AddExcessManifestoDetails(string unit_atd, string location, int bookID, int coordinatorID, int type)
        {

            using (var db = new isebookingEntities())
            {
                Bookings bookRepo = new Bookings();
                var book = bookRepo.GetBooking(bookID).Header;

               // var fleetUnit = db.fleets.Where(x => x.VIDNumber == unit_atd).FirstOrDefault();
               // if (fleetUnit == null) throw new Exception("Fleet not found!");

                var flightDate = book.FlightDate1.Value.Date;

                var manifestSched = db.manifestoscheds.Where(x => x.Location.Contains(location)
                && x.ManifestoDate == flightDate && x.ManisfestoType == type).FirstOrDefault();

                if (manifestSched != null)
                {

                    var newDetails = new manifestoscheddetail();
                    newDetails.ManifestoSchedID = manifestSched.ID;
                    newDetails.Location = location;
                    newDetails.CVNumber = book.CVNumber;
                    newDetails.AccountName = book.GroupName;
                    newDetails.Adult = book.Adults;
                    newDetails.Kid = book.Childrens;
                    newDetails.FOC = book.FOC;
                    newDetails.Infant = book.Infants;
                    newDetails.TourGuide = book.TourGuides;
                    newDetails.Escort = book.Escorts;

                    newDetails.ActualAdult = book.Adults;
                    newDetails.ActualFOC = book.FOC;
                    newDetails.ActualKid = book.Childrens;
                    newDetails.ActualInfant = book.Infants;
                    newDetails.ActualTourGuide = book.TourGuides;
                    newDetails.ActualEscort = book.Escorts;
                    newDetails.BookingID = book.ID;
                    newDetails.ActualTime = null;
                    newDetails.ActualDate = null;

                    db.manifestoscheddetails.Add(newDetails);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        string err = ex.Message;
                    }
                }

            }

        }


        public string getDestination(int id)
        {
            string destination = "";
            using (var db = new isebookingEntities())
            {

                try
                {
                    destination = db.destinations.Where(x => x.ID == id).FirstOrDefault().Description;
                }
                catch (Exception ex)
                {
                    destination = "";
                }

                return destination;

            }
        }
        public booking getBookingInfoByCV(string CV)
        {
            booking book = null;
            using (var db = new isebookingEntities())
            {

                try
                {
                    book = db.bookings.Where(x => x.CVNumber == CV).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    book = null;
                }

                return book;

            }
        }

        public ManifestoSchedules GetSchedule(int id)
        {
            using (var db = new isebookingEntities())
            {
                ManifestoSchedules schedule = new ManifestoSchedules();
                schedule.Header = (from a in db.manifestoscheds
                                   where a.ID == id
                                   select new ManifestoSchedModel
                                   {
                                       Coordinator = a.agent.FirstName + " " + a.agent.LastName,
                                       CoordinatorID = a.CoordinatorID,
                                       DateCreated = a.DateCreated,
                                       DateLastUpadated = a.DateLastUpadated,
                                       Driver = a.Driver,
                                       ID = a.ID,
                                       ManifestoDate = a.ManifestoDate,
                                       ManifestoType = a.ManisfestoType,
                                       Status = (ManifestoStatus)a.Status,
                                       VehicleID = a.VehicleID,
                                       Location = a.Location,
                                       TouchPoint=a.Touchpoint
                                   }).FirstOrDefault();

                schedule.Details = (from a in db.manifestoscheddetails
                                    where a.ManifestoSchedID == id
                                    join b in db.manifestos on new { BookingID = a.BookingID, ManifestType = a.manifestosched.ManisfestoType } equals new { BookingID = b.BookingID, ManifestType = b.Type } into bb
                                    from bb1 in bb.DefaultIfEmpty()
                                    select new ManifestoDataset
                                    {

                                        ActualAdults = a.ActualAdult,
                                        ActualDate = a.ActualDate,
                                        ActualEscorts = a.ActualEscort,
                                        ActualFOC = a.ActualFOC,
                                        ActualInfants = a.ActualInfant,
                                        ActualKids = a.ActualKid,
                                        ActualTime = a.ActualTime,
                                        ActualTourGuides = a.ActualTourGuide,
                                        Adults = a.Adult,
                                        AgencyName = a.booking.agent.travelagency.Name,
                                        Agent = a.booking.bookinglogs.Where(x => x.Status == (int)BookingStatus.FINALIZED).Select(x => x.agent.FirstName + " " + x.agent.LastName).FirstOrDefault(),
                                        AuthCode = "",
                                        BookingID = a.BookingID,
                                        Childrens = 0,
                                        ChildrensBelowSix = 0,
                                        ContactNumber = a.booking.ContactNumber,
                                        ContactPerson = a.booking.ContactPerson,
                                        CreatedBy = a.manifestosched.CreatedBy,
                                        CVDate = a.booking.CVDate,
                                        CVNumber = a.CVNumber,
                                        Date = bb1 != null ? bb1.Date : a.booking.FlightDate1,
                                        DateCreated = a.manifestosched.DateCreated,
                                        DateLastUpdated = a.manifestosched.DateLastUpadated,
                                        DropOff = bb1 != null ? bb1.DropOff : 0,
                                        DropOffArrival = a.booking.DropOffArrival,
                                        DropOffDeparture = a.booking.DropOffDeparture,
                                        DropOffLocation = a.booking.DropOffLocation,
                                        DropOffLocation1 = a.booking.DropOffLocation1,
                                        DropOffLocation2 = a.booking.DropOffLocation2,
                                        Escorts = a.Escort,

                                        FlightDate1 = a.booking.FlightDate1,
                                        FlightDate2 = a.booking.FlightDate2,
                                        FlightNo = bb1 != null ? bb1.FlightNo : "",
                                        FlightNo1 = a.booking.FlightNo1,
                                        FlightNo2 = a.booking.FlightNo2,
                                        FOC = a.FOC,
                                        GroupName = a.AccountName,
                                        Guests = a.AccountName,
                                        ID = a.ID,
                                        Infants = a.Infant,
                                        Kids = a.Kid,
                                        Location = a.Location,
                                        PaymentTypeID = a.booking.PaymentTypeID,
                                        Pax = bb1 != null ? bb1.Pax : 0,
                                        Pickup = bb1 != null ? bb1.Pickup : 0,
                                        PickUpArrival = a.booking.PickUpArrival,
                                        PickUpDeparture = a.booking.PickUpDeparture,
                                        Remarks = a.booking.Remarks,
                                        ResortHotel = bb1 != null ? bb1.ResortHotel : 0,
                                        ResortHotelDescription = bb1 != null ? bb1.resortsandhotel.Name : "",
                                        Routing = a.booking.Routing,
                                        Senior = a.booking.Senior,
                                        Status = a.booking.Status,

                                        Total = 0,
                                        TourGuides = a.TourGuide,
                                        Type = a.manifestosched.ManisfestoType

                                    }).ToList();
                return schedule;
            }
        }
 
        private async Task<SyncState> SyncDetails(ManifestoDataset data, AgentsModel agent)
        {

            SyncState state = new SyncState();

            using (var db = new isebookingEntities())
            {

                try
                {

                    var manifest = db.manifestoscheddetails.Where(x => x.BookingID == data.BookingID && x.manifestosched.ManisfestoType == data.Type).FirstOrDefault();

                    if (manifest == null) throw new Exception("Manifesto does not exist!");



                    manifest.ActualTime = data.ActualTime;
                    manifest.ActualDate = data.ActualDate;
                    manifest.ActualAdult = data.ActualAdults;
                    manifest.ActualEscort = data.ActualEscorts;
                    manifest.ActualFOC = data.ActualFOC;
                    manifest.ActualInfant = data.ActualInfants;
                    manifest.ActualKid = data.ActualKids;
                    manifest.ActualTourGuide = data.ActualTourGuides;

                    db.SaveChanges();

                    state.CVNumber = data.CVNumber;
                    state.Status = 1;

                }
                catch (Exception ex)
                {
                    state.Status = 0;
                    state.ErrorMessage = ex.Message;
                    state.CVNumber = data.CVNumber;
                }

                return state;
            }

        }

        public async Task<List<SyncState>> SyncManifesto(List<ManifestoDataset> schedules, AgentsModel agent)
        {
            List<SyncState> syncStatus = new List<SyncState>();
            List<Task<SyncState>> myjobs = new List<Task<SyncState>>();

            foreach (var sched in schedules)
            {
                Task<SyncState> job = SyncDetails(sched, agent);
                myjobs.Add(job);
            }


            var myactions = await Task.WhenAll(myjobs);


            foreach (var action in myactions)
            {
                syncStatus.Add(new SyncState { ErrorMessage = action.ErrorMessage, CVNumber = action.CVNumber, Status = action.Status });
            }
            return syncStatus;
        }

        public string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }



    }


    public class ManifestoSchedModel
    {
        public int ID { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateLastUpadated { get; set; }
        public int CoordinatorID { get; set; }
        public int ManifestoType { get; set; }
        public System.DateTime ManifestoDate { get; set; }
        public int VehicleID { get; set; }
        public string Driver { get; set; }
        public ManifestoStatus Status { get; set; }

        public string Coordinator { get; set; }
        public string Vehicle { get; set; }
        public string ManifestoDateString
        {
            get { return ManifestoDate.ToString("MMM d yyyy"); }
        }
        public string StatusString
        {
            get
            {
                return Status.Description();
            }
        }

        public string Location { get; set; }
        public int TouchPoint { get; set; }

        public int TotalPax { get; set; }

        public int orderBy { get; set; }

    }

    public class ManifestoSchedDetail
    {
        public int ID { get; set; }
        public int ManifestoSchedID { get; set; }
        public string Location { get; set; }
        public string CVNumber { get; set; }
        public string AccountName { get; set; }
        public int Adult { get; set; }
        public int Kid { get; set; }
        public int FOC { get; set; }
        public int Infant { get; set; }
        public int TourGuide { get; set; }
        public int Escort { get; set; }

        public int TotalPax
        {
            get
            {
                return Adult + Kid + FOC + Infant;
            }
        }

        public int ActualAdult { get; set; }
        public int ActualKid { get; set; }
        public int ActualFOC { get; set; }
        public int ActualInfant { get; set; }
        public int ActualTourGuide { get; set; }
        public int ActualEscort { get; set; }
        public int ActualTotalPax
        {
            get
            {
                return ActualAdult + ActualKid + ActualFOC + ActualInfant;
            }
        }

        public string Agency { get; set; }
        public string SalesHandle { get; set; }
        public DateTime? ActualDate { get; set; }
        public TimeSpan? ActualTime { get; set; }

        public DateTime? BookingDate { get; set; }
        public TimeSpan BookingTime
        {
            get
            {
                return new TimeSpan(BookingDate.Value.Hour, BookingDate.Value.Minute, BookingDate.Value.Second);

            }
        }
        public string ResortHotel { get; set; }

    }

}
