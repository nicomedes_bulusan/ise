﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public enum SalesType
    {
        DEBIT = 1,
        CREDIT = 2
    }
    public class SalesModel
    {
        public int ID { get; set; }
        public int BookingID { get; set; }
        public System.DateTime TransactionDate { get; set; }
        public Nullable<int> AgentID { get; set; }
        public SalesType Type { get; set; }
        public double Total { get; set; }
        public double SalesTax { get; set; }
        public string Comment { get; set; }
        public string ReferenceNumber { get; set; }


        public string CVNumber { get; set; }
        public string PaxName { get; set; }
        public int Adults { get; set; }
        public int Kids { get; set; }
        public int Infants { get; set; }
        public int FOC { get; set; }
        public int TourGuides { get; set; }
        public int Escorts { get; set; }
        public int Pax { get; set; }
        public string ArrivalFlight { get; set; }
        public string DepartureFlight { get; set; }
        public DateTime? DateIn { get; set; }
        public DateTime? DateOut { get; set; }
        public int TravelAgencyID { get; set; }
        public string TravelAgency { get; set; }
        public string AgentName { get; set; }





    }


}
