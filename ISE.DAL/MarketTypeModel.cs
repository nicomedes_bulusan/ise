﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public class MarketTypeModel
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastUpdated { get; set; }

        public List<MarketTypeModel> GetList()
        {
            isebookingEntities db = new isebookingEntities();


            var result = (from a in db.markettypes
                          select new MarketTypeModel
                          {
                              DateCreated = a.DateCreated,
                              DateLastUpdated = a.DateLastUpdated,
                              Description = a.Description,
                              ID = a.ID
                          }).ToList<MarketTypeModel>();
            return result;
        }



        public MarketTypeModel CreateOrUpdate(MarketTypeModel marketType)
        {
            isebookingEntities db = new isebookingEntities();

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    markettype market = new markettype();

                    if (marketType.ID != 0)
                    {
                        market = db.markettypes.Where(x => x.ID == marketType.ID).FirstOrDefault();

                        if (market == null) throw new Exception("Market Type does not exist!");
                    }

                    if (market.ID == 0)
                    {
                        market.DateCreated = DateTime.Now;
                    }


                    market.Description = marketType.Description;
                    market.DateLastUpdated = DateTime.Now;

                    marketType.ID = market.ID;

                    return marketType;

                }
                catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                {
                    transaction.Rollback();

                    Exception ex = dbU.GetBaseException();

                    throw new Exception(ex.Message);
                }
                catch (DbEntityValidationException dbEx)
                {


                    transaction.Rollback();

                    string errorMessages = null;

                    foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                    {
                        string entityName = validationResult.Entry.Entity.GetType().Name;
                        foreach (DbValidationError error in validationResult.ValidationErrors)
                        {
                            errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                        }
                    }

                    throw new Exception(errorMessages);

                }

                finally
                {
                    db.Database.Connection.Close();

                }
            }
        }

    }
}
