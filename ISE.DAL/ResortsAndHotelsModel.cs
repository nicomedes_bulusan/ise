﻿using MoreLinq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public class ResortsAndHotelsModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateLastUpdated { get; set; }
        public bool IsActive { get; set; }



        public List<ResortsAndHotelsModel> GetResortsAndHotels(bool activeOnly = true)
        {
            isebookingEntities db = new isebookingEntities();

            // LINQ TO SQL SORTING BUG
            var propertyInfo = typeof(ResortsAndHotelsModel).GetProperty("Name");
            OrderByDirection sortDirec = OrderByDirection.Ascending;

            var result = (from a in db.resortsandhotels
                          where activeOnly == true ? a.IsActive == true : true
                          select new ResortsAndHotelsModel
                          {
                              ID = a.ID,
                              DateCreated = a.DateCreated,
                              DateLastUpdated = a.DateLastUpdated,
                              IsActive = a.IsActive,
                              Name = a.Name

                          }).OrderBy(x => propertyInfo.GetValue(x, null), sortDirec).ToList<ResortsAndHotelsModel>();
            return result;
        }



        public ResortsAndHotelsModel Create(ResortsAndHotelsModel rhRef)
        {
            isebookingEntities db = new isebookingEntities();


            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {



                    var rh = new resortsandhotel();
                    rh.Name = rhRef.Name;
                    rh.DateCreated = DateTime.Now;
                    rh.DateLastUpdated = DateTime.Now;
                    rh.IsActive = rhRef.IsActive;

                    db.resortsandhotels.Add(rh);

                    rhRef.ID = rh.ID;
                    db.SaveChanges();

                    transaction.Commit();


                    return rhRef;

                }
                catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                {
                    transaction.Rollback();

                    Exception ex = dbU.GetBaseException();

                    throw new Exception(ex.Message);
                }
                catch (DbEntityValidationException dbEx)
                {


                    transaction.Rollback();

                    string errorMessages = null;

                    foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                    {
                        string entityName = validationResult.Entry.Entity.GetType().Name;
                        foreach (DbValidationError error in validationResult.ValidationErrors)
                        {
                            errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                        }
                    }

                    throw new Exception(errorMessages);

                }

                finally
                {
                    db.Database.Connection.Close();

                }
            }


        }


        public ResortsAndHotelsModel Get(int id=0)
        {
            using (var db = new isebookingEntities())
            {

                var result = (from a in db.resortsandhotels
                              where a.ID==id
                              select new ResortsAndHotelsModel
                              {
                                  DateCreated = a.DateCreated,
                                  DateLastUpdated = a.DateLastUpdated,
                                  ID = a.ID,
                                  IsActive = a.IsActive,
                                  Name = a.Name
                              }).FirstOrDefault();
                return result;
            }
        }

        public ResortsAndHotelsModel Update(ResortsAndHotelsModel rhRef)
        {
            isebookingEntities db = new isebookingEntities();


            var updateRh = db.resortsandhotels.Where(x => x.ID == rhRef.ID).FirstOrDefault();

            if (updateRh == null) throw new Exception("Resort or hotel does not exist!");
            updateRh.Name = rhRef.Name;
            updateRh.IsActive = rhRef.IsActive;
            updateRh.DateLastUpdated = DateTime.Now;


            db.SaveChanges();



            return rhRef;


        }



    }
}
