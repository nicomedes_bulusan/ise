//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISE.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class destination
    {
        public destination()
        {
            this.bookings = new HashSet<booking>();
            this.bookings1 = new HashSet<booking>();
            this.bookings2 = new HashSet<booking>();
            this.bookings3 = new HashSet<booking>();
            this.manifestos = new HashSet<manifesto>();
            this.manifestos1 = new HashSet<manifesto>();
            this.ratematrices = new HashSet<ratematrix>();
            this.ratematrices1 = new HashSet<ratematrix>();
            this.ratematrixagents = new HashSet<ratematrixagent>();
            this.ratematrixagents1 = new HashSet<ratematrixagent>();
        }
    
        public int ID { get; set; }
        public string Description { get; set; }
        public System.DateTime DateCeated { get; set; }
        public System.DateTime DateLastUpdated { get; set; }
        public string Code { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> orderBy { get; set; }
    
        public virtual ICollection<booking> bookings { get; set; }
        public virtual ICollection<booking> bookings1 { get; set; }
        public virtual ICollection<booking> bookings2 { get; set; }
        public virtual ICollection<booking> bookings3 { get; set; }
        public virtual ICollection<manifesto> manifestos { get; set; }
        public virtual ICollection<manifesto> manifestos1 { get; set; }
        public virtual ICollection<ratematrix> ratematrices { get; set; }
        public virtual ICollection<ratematrix> ratematrices1 { get; set; }
        public virtual ICollection<ratematrixagent> ratematrixagents { get; set; }
        public virtual ICollection<ratematrixagent> ratematrixagents1 { get; set; }
    }
}
