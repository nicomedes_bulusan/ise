//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISE.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class bookingattachment
    {
        public int ID { get; set; }
        public string Filename { get; set; }
        public string MimeType { get; set; }
        public System.DateTime DateCreated { get; set; }
        public int BookingID { get; set; }
        public string OriginalFilename { get; set; }
    
        public virtual booking booking { get; set; }
    }
}
