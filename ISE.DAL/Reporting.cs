﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoreLinq;

namespace ISE.DAL
{
    public class Reporting
    {
        private IMapper mapper;

        public Reporting()
        {
            var newConfig = new MapperConfiguration(cfg =>
           {
               cfg.CreateMap<agent, AgentsModel>();
               cfg.CreateMap<manifestodetail, ManifestoDetailsModel>();


               cfg.CreateMap<bookinglog, BookingLogsModel>()
                    .ForMember(dest => dest.Agent, conf => conf.MapFrom(o => o.agent));
               cfg.CreateMap<booking, BookingHeaderModel>()
                   .ForMember(dest => dest.Agency, conf => conf.MapFrom(o => o.agent))
                    .ForMember(dest => dest.Logs, conf => conf.MapFrom(o => o.bookinglogs));

               cfg.CreateMap<manifesto, ManifestoModel>()
                   .ForMember(dest => dest.Details, conf => conf.MapFrom(o => o.manifestodetails))
                   .ForMember(dest => dest.BookingHeader, conf => conf.MapFrom(o => o.booking))
                   .ForMember(dest => dest.ResortHotelDescription, conf => conf.MapFrom(o => o.resortsandhotel.Name))
                   .ForMember(dest => dest.LocationArrival, conf => conf.MapFrom(o => o.destination.Description))
                    .ForMember(dest => dest.LocationDeparture, conf => conf.MapFrom(o => o.destination1.Description)
                   );



           });

            mapper = newConfig.CreateMapper();
        }



        public List<BookingsDS> GetBookingsByAgentDateStatus()
        {
            using (isebookingEntities db = new isebookingEntities())
            {
                return db.Database.SqlQuery<BookingsDS>("select books.*,CONCAT_WS(' ',agent.FirstName,agent.LastName) as AgentName,dest.Description as FromDestination,dest1.Description as ToDestination from bookings books INNER JOIN agents agent on books.CreatedBy = agent.ID INNER JOIN destinations dest on books.PickUpArrival = dest.ID INNER JOIN destinations dest1 on books.DropOffArrival =dest1.ID").ToList<BookingsDS>();
            }
        }


        public List<ManifestoDataset> GetManifestoArrivals(DateTime referenceDate)
        {


            using (isebookingEntities db = new isebookingEntities())
            {
                var result = db.manifestos.Where(x => x.Date.Year == referenceDate.Year
                    && x.Date.Month == referenceDate.Month
                    && x.Date.Day == referenceDate.Day
                    && x.Type == 1
                    && (x.booking.Status == (int)BookingStatus.FINALIZED || x.booking.Status == (int)BookingStatus.SAVED)).ToList();

                var mappedResult = mapper.Map<List<manifesto>, List<ManifestoModel>>(result);

                return (from a in mappedResult
                        select new ManifestoDataset
                        {
                            Adults = a.Adults,
                            Date = a.Date,
                            Kids = a.Kids,
                            //  ChildrensBelowSix=a.BookingHeader.ChildrensBelowSix,
                            ContactNumber = a.BookingHeader.ContactNumber,
                            ContactPerson = a.BookingHeader.ContactPerson,
                            CreatedBy = a.BookingHeader.CreatedBy,
                            CVDate = a.BookingHeader.CVDate,
                            CVNumber = a.BookingHeader.CVNumber,

                            DateCreated = a.DateCreated,
                            DateLastUpdated = a.DateLastUpdated,
                            DropOffArrival = a.BookingHeader.DropOffArrival,
                            DropOffDeparture = a.BookingHeader.DropOffDeparture,
                            DropOffLocation = a.BookingHeader.DropOffLocation,
                            DropOffLocation1 = a.BookingHeader.DropOffLocation1,
                            DropOffLocation2 = a.BookingHeader.DropOffLocation2,
                            Escorts = a.Escorts,
                            FlightDate1 = a.BookingHeader.FlightDate1,
                            FlightDate2 = a.BookingHeader.FlightDate2,
                            FlightNo = a.FlightNo,
                            ResortHotelDescription = a.ResortHotelDescription,
                            FOC = a.FOC,
                            Type = 1,
                            GroupName = a.BookingHeader.GroupName,
                            Guests = string.Join(Environment.NewLine, a.Details.Select(x => x.GuestName).ToArray()),
                            ID = a.ID,
                            Infants = a.Infants,
                            Pax = a.Pax + a.FOC + a.Escorts + a.TourGuides - a.Infants  ,
                            PaymentTypeID = (int)a.BookingHeader.PaymentTypeID,
                            PickUpArrival = a.BookingHeader.PickUpArrival,
                            PickUpDeparture = a.BookingHeader.PickUpDeparture,
                            Remarks = a.Remarks,
                            ResortHotel = a.ResortHotel,
                            Routing = a.BookingHeader.Routing,
                            // Senior=a.BookingHeader.Senior,
                            Status = (int)a.BookingHeader.Status,
                            Total = a.BookingHeader.Total,
                            TourGuides = a.TourGuides,
                            Pickup = a.Pickup,
                            BookingID = a.BookingID,

                            DropOff = a.DropOff,
                            // Agent = a.BookingHeader.Agency.FirstName + " " + a.BookingHeader.Agency.LastName,
                            Agent = a.BookingHeader.Logs.Where(x => x.Status == BookingStatus.FINALIZED).Select(x => x.Agent.FirstName + " " + x.Agent.LastName).FirstOrDefault(),
                            AgencyName = a.BookingHeader.Agency.Company,
                            Location = a.LocationArrival,

                            //  LocationDeparture = a.LocationDeparture

                        }).OrderBy(x => x.Location).ThenBy(x=>x.FlightDate1).ToList<ManifestoDataset>();

            }


        }

        public List<ManifestoDataset> GetManifestoDepartures(DateTime referenceDate)
        {

            using (isebookingEntities db = new isebookingEntities())
            {
                var result = db.manifestos.Where(x => x.Date.Year == referenceDate.Year
                    && x.Date.Month == referenceDate.Month
                    && x.Date.Day == referenceDate.Day
                    && x.Type == 2
                    && x.booking.Status == (int)BookingStatus.FINALIZED).ToList();

                var mappedResult = mapper.Map<List<manifesto>, List<ManifestoModel>>(result);

                return (from a in mappedResult
                        
                        select new ManifestoDataset
                        {
                            Adults = a.Adults,
                            Date = a.Date,
                            Kids = a.Kids,

                            //  ChildrensBelowSix=a.BookingHeader.ChildrensBelowSix,
                            ContactNumber = a.BookingHeader.ContactNumber,
                            ContactPerson = a.BookingHeader.ContactPerson,
                            CreatedBy = a.BookingHeader.CreatedBy,
                            CVDate = a.BookingHeader.CVDate,
                            CVNumber = a.BookingHeader.CVNumber,
                            DateCreated = a.DateCreated,
                            DateLastUpdated = a.DateLastUpdated,

                            DropOffArrival = a.BookingHeader.DropOffArrival,
                            DropOffDeparture = a.BookingHeader.DropOffDeparture,
                            DropOffLocation = a.BookingHeader.DropOffLocation,
                            DropOffLocation1 = a.BookingHeader.DropOffLocation1,
                            DropOffLocation2 = a.BookingHeader.DropOffLocation2,
                            Escorts = a.Escorts,
                            FlightDate1 = a.BookingHeader.FlightDate1,
                            FlightDate2 = a.BookingHeader.FlightDate2,
                            FlightNo = a.FlightNo,
                            ResortHotelDescription = a.ResortHotelDescription,
                            FOC = a.FOC,
                            GroupName = a.BookingHeader.GroupName,
                            Guests = string.Join(Environment.NewLine, a.Details.Select(x => x.GuestName).ToArray()),
                            ID = a.ID,
                            Infants = a.Infants,
                            Pax = a.Pax + a.FOC + a.Escorts + a.TourGuides - a.Infants,
                            PaymentTypeID = (int)a.BookingHeader.PaymentTypeID,
                            PickUpArrival = a.BookingHeader.PickUpArrival,
                            PickUpDeparture = a.BookingHeader.PickUpDeparture,
                            Remarks = a.Remarks,
                            ResortHotel = a.ResortHotel,
                            Routing = a.BookingHeader.Routing,
                            // Senior=a.BookingHeader.Senior,
                            Status = (int)a.BookingHeader.Status,
                            Total = a.BookingHeader.Total,
                            Type = 2,
                            TourGuides = a.TourGuides,
                            Pickup = a.Pickup,
                            DropOff = a.DropOff,
                            BookingID = a.BookingID,
                            Agent = a.BookingHeader.Logs.Where(x => x.Status == BookingStatus.FINALIZED).Select(x => x.Agent.FirstName + " " + x.Agent.LastName).FirstOrDefault(),
                            AgencyName = a.BookingHeader.Agency.Company,
                            // LocationArrival = a.LocationArrival,
                            Location = a.LocationDeparture

                        }).OrderBy(x => x.Location).ThenBy(x => x.FlightDate2).ToList<ManifestoDataset>();

            }




        }

        public List<ManifestoDataset> GetManifestoSummarry(DateTime referenceDate)
        {
            using (var db = new isebookingEntities())
            {
                var arriving = (from aa in db.manifestoscheds
                                join a in db.manifestoscheddetails on aa.ID equals a.ManifestoSchedID
                                join veh in db.fleets on aa.VehicleID equals veh.ID
                                join cat in db.fleetcategories on veh.CategoryID equals cat.ID
                                join b in db.manifestos on new { BookingID = a.BookingID, ManifestType = a.manifestosched.ManisfestoType } equals new { BookingID = b.BookingID, ManifestType = b.Type } into bb
                                from bb1 in bb.DefaultIfEmpty()
                                where  DbFunctions.TruncateTime(aa.ManifestoDate) == DbFunctions.TruncateTime(referenceDate)
                                
                                select new ManifestoDataset
                                {

                                    Vehicle = veh.VIDNumber,
                                    Coordinator = aa.agent.FirstName + " " + aa.agent.LastName,
                                    Driver = aa.Driver,
                                    FleetCategory=cat.Description,
                                    FleetCategoryID=cat.ID,
                                    VehicleID = veh.ID,

                                    ActualAdults = a.ActualAdult,
                                    ActualDate = a.ActualDate,
                                    ActualEscorts = a.ActualEscort,
                                    ActualFOC = a.ActualFOC,
                                    ActualInfants = a.ActualInfant,
                                    ActualKids = a.ActualKid,
                                    ActualTime = a.ActualTime,
                                    ActualTourGuides = a.ActualTourGuide,
                                    Adults = a.Adult,
                                    AgencyName = a.booking.agent.travelagency.Name,
                                    Agent = a.booking.bookinglogs.Where(x => x.Status == (int)BookingStatus.FINALIZED).Select(x => x.agent.FirstName + " " + x.agent.LastName).FirstOrDefault(),
                                    AuthCode = "",
                                    BookingID = a.BookingID,
                                    Childrens = 0,
                                    ChildrensBelowSix = 0,
                                    ContactNumber = a.booking.ContactNumber,
                                    ContactPerson = a.booking.ContactPerson,
                                    CreatedBy = a.manifestosched.CreatedBy,
                                    CVDate = a.booking.CVDate,
                                    CVNumber = a.CVNumber,
                                    Date = bb1 != null ? bb1.Date : a.booking.FlightDate1,
                                    DateCreated = a.manifestosched.DateCreated,
                                    DateLastUpdated = a.manifestosched.DateLastUpadated,
                                    DropOff = bb1 != null ? bb1.DropOff : 0,
                                    DropOffArrival = a.booking.DropOffArrival,
                                    DropOffDeparture = a.booking.DropOffDeparture,
                                    DropOffLocation = a.booking.DropOffLocation,
                                    DropOffLocation1 = a.booking.DropOffLocation1,
                                    DropOffLocation2 = a.booking.DropOffLocation2,
                                    Escorts = a.Escort,

                                    FlightDate1 = a.booking.FlightDate1,
                                    FlightDate2 = a.booking.FlightDate2,
                                    FlightNo = bb1 != null ? bb1.FlightNo : "",
                                    FlightNo1 = a.booking.FlightNo1,
                                    FlightNo2 = a.booking.FlightNo2,
                                    FOC = a.FOC,
                                    GroupName = a.AccountName,
                                    Guests = a.AccountName,
                                    ID = a.ID,
                                    Infants = a.Infant,
                                    Kids = a.Kid,
                                    Location = a.Location,
                                    PaymentTypeID = a.booking.PaymentTypeID,
                                    Pax = bb1 != null ? bb1.Pax : 0,
                                    Pickup = bb1 != null ? bb1.Pickup : 0,
                                    PickUpArrival = a.booking.PickUpArrival,
                                    PickUpDeparture = a.booking.PickUpDeparture,
                                    Remarks = a.booking.Remarks,
                                    ResortHotel = bb1 != null ? bb1.ResortHotel : 0,
                                    ResortHotelDescription = bb1 != null ? bb1.resortsandhotel.Name : "",
                                    Routing = a.booking.Routing,
                                    Senior = a.booking.Senior,
                                    Status = a.booking.Status,

                                    Total = 0,
                                    TourGuides = a.TourGuide,
                                    Type = a.manifestosched.ManisfestoType,
                                    TouchPoint=a.manifestosched.Location


                                }).ToList().OrderBy(x => x.ManifestoType).ThenBy(x => x.Location).ThenBy(x => x.Date);
        
                return arriving.ToList();
            }
        }


        public List<ManifestoVehicleAllocation> GetManifestoVehicleAllocation(List<ManifestoDataset> manifest)
        {

            FleetModel fleetRepo = new FleetModel();
            DestinationsModel destRepo = new DestinationsModel();

            var destinations = destRepo.GetDestinations();

            var vehicles = fleetRepo.GetFleet();

            var result = (from a in manifest

                          group a by new { Location =a.Location,ManifestoType=a.ManifestoType} into grp
                          select new ManifestoVehicleAllocation
                          {
                              ActualAdults = grp.Sum(x => x.ActualAdults),
                              ActualKids = grp.Sum(x => x.ActualKids),
                              Adults = grp.Sum(x => x.Adults),
                              Kids = grp.Sum(x => x.Kids),
                              Location = grp.Key.Location,
                              Type = grp.Key.ManifestoType,
                              VehicleID = grp.Select(x => x.VehicleID).FirstOrDefault(),
                              Vehicle = "",
                              VehicleCapacity = 0,
                              VehicleCategory = "",
                              VehicleCategoryID = 0,

                          }
                              ).ToList();

            var resultWithVehicles = (from a in result
                                      join b in vehicles on a.VehicleID equals b.ID
                                      select new ManifestoVehicleAllocation
                                      {
                                          ActualAdults = a.ActualAdults,
                                          ActualKids = a.ActualKids,
                                          Adults = a.Adults,
                                          Kids = a.Kids,
                                          Location = a.Location,
                                          Type = a.Type,
                                          VehicleID = a.VehicleID,
                                          Vehicle = b.VIDNumber,
                                          VehicleCapacity = b.Capacity ?? 0,
                                          VehicleCategory = b.Category,
                                          VehicleCategoryID = b.CategoryID,
                                      }
                                             ).ToList();





            return resultWithVehicles;
        }

        public List<SalesModel> GetSales(DateTime fromDate, DateTime toDate, AgentsModel agent)
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                return db.Database.SqlQuery<SalesModel>("CALL rptGetSales(?fromDate,?toDate)",
                    new MySql.Data.MySqlClient.MySqlParameter("?fromDate", fromDate),
                    new MySql.Data.MySqlClient.MySqlParameter("?toDate", toDate)).ToList<SalesModel>();

            }

        }


        public List<SalesByMarketType> SalesByMarketTypeSummary(DateTime fromDate, DateTime toDate)
        {

            using (isebookingEntities db = new isebookingEntities())
            {

                var initialData = db.Database.SqlQuery<SalesByMarketType>("CALL rptGetSalesByMarketTypeSummary(?fromDate,?toDate)",
                   new MySql.Data.MySqlClient.MySqlParameter("?fromDate", fromDate),
                   new MySql.Data.MySqlClient.MySqlParameter("?toDate", toDate)).ToList<SalesByMarketType>();



                return (from a in initialData
                        group a by new { MarketTypeID = a.MarketTypeID, MarketType = a.MarketType } into pg

                        select new SalesByMarketType
                        {
                            MarketTypeID = pg.Key.MarketTypeID,
                            MarketType = pg.Key.MarketType,
                            Transfer = pg.Where(x => x.SourceID == (int)SalesSource.TRANSFER).Sum(x => x.Quantity * x.Amount),
                            BoatFee = pg.Where(x => x.SourceID == (int)SalesSource.TRANSFER).Sum(z => z.Quantity * z.Boat),
                            EnvironmentFee = pg.Where(x => x.SourceID == (int)SalesSource.ENVIRONMENT_FEE).Sum(z => z.Quantity * z.Amount),
                            TerminalFee = pg.Where(x => x.SourceID == (int)SalesSource.TERMINAL_FEE).Sum(z => z.Quantity * z.Amount),
                            Others = pg.Where(x => x.SourceID == (int)SalesSource.OTHERS).Sum(z => z.Quantity * z.Amount),
                            NightNavigation = pg.Where(x => x.SourceID == (int)SalesSource.NIGHT_NAVIGATION).Sum(z => z.Quantity * z.Amount)
                        }).OrderBy(x => x.MarketType).ToList();


            }

        }



        public List<SalesByMarketType> SalesByMarketTypeBreakdown(DateTime fromDate, DateTime toDate)
        {

            using (isebookingEntities db = new isebookingEntities())
            {
                var initialData = db.Database.SqlQuery<SalesByMarketType>("CALL rptGetSalesByMarketTypeBreakdown(?fromDate,?toDate)",
                   new MySql.Data.MySqlClient.MySqlParameter("?fromDate", fromDate),
                   new MySql.Data.MySqlClient.MySqlParameter("?toDate", toDate)).ToList<SalesByMarketType>();



                return (from a in initialData
                        group a by a.BookingID into pg

                        select new SalesByMarketType
                        {
                            BookingID = pg.Key,
                            CVNumber = pg.Select(x => x.CVNumber).FirstOrDefault(),
                            PaxName = pg.Select(x => x.PaxName).FirstOrDefault(),
                            TravelAgency = pg.Select(x => x.TravelAgency).FirstOrDefault(),
                            TravelAgencyID = pg.Select(x => x.TravelAgencyID).FirstOrDefault(),
                            MarketTypeID = pg.Select(x => x.MarketTypeID).FirstOrDefault(),
                            MarketType = pg.Select(x => x.MarketType).FirstOrDefault(),
                            Transfer = pg.Where(x => x.SourceID == (int)SalesSource.TRANSFER).Sum(x => x.Quantity * x.Amount),
                            BoatFee = pg.Where(x => x.SourceID == (int)SalesSource.TRANSFER).Sum(z => z.Quantity * z.Boat),
                            EnvironmentFee = pg.Where(x => x.SourceID == (int)SalesSource.ENVIRONMENT_FEE).Sum(z => z.Quantity * z.Amount),
                            TerminalFee = pg.Where(x => x.SourceID == (int)SalesSource.TERMINAL_FEE).Sum(z => z.Quantity * z.Amount),
                            Others = pg.Where(x => x.SourceID == (int)SalesSource.OTHERS).Sum(z => z.Quantity * z.Amount),
                            NightNavigation = pg.Where(x => x.SourceID == (int)SalesSource.NIGHT_NAVIGATION).Sum(z => z.Quantity * z.Amount)
                        }).OrderBy(x => x.MarketType).ToList();


            }

        }

        public List<SalesByMarketType> SalesByAgentByMarketType(DateTime fromDate, DateTime toDate)
        {

            using (isebookingEntities db = new isebookingEntities())
            {

                var initialData = db.Database.SqlQuery<SalesByMarketType>("CALL rptGetSalesByAgentByMarketType(?fromDate,?toDate)",
                   new MySql.Data.MySqlClient.MySqlParameter("?fromDate", fromDate),
                   new MySql.Data.MySqlClient.MySqlParameter("?toDate", toDate)).ToList<SalesByMarketType>();



                return (from a in initialData
                        group a by a.BookingID into pg

                        select new SalesByMarketType
                        {
                            BookingID = pg.Key,
                            CVNumber = pg.Select(x => x.CVNumber).FirstOrDefault(),
                            PaxName = pg.Select(x => x.PaxName).FirstOrDefault(),
                            TravelAgency = pg.Select(x => x.TravelAgency).FirstOrDefault(),
                            TravelAgencyID = pg.Select(x => x.TravelAgencyID).FirstOrDefault(),
                            AgentID = pg.Select(x => x.AgentID).FirstOrDefault(),
                            AgentName = pg.Select(x => x.AgentName).FirstOrDefault(),
                            MarketTypeID = pg.Select(x => x.MarketTypeID).FirstOrDefault(),
                            MarketType = pg.Select(x => x.MarketType).FirstOrDefault(),
                            Transfer = pg.Where(x => x.SourceID == (int)SalesSource.TRANSFER).Sum(x => x.Quantity * x.Amount),
                            BoatFee = pg.Where(x => x.SourceID == (int)SalesSource.TRANSFER).Sum(z => z.Quantity * z.Boat),
                            EnvironmentFee = pg.Where(x => x.SourceID == (int)SalesSource.ENVIRONMENT_FEE).Sum(z => z.Quantity * z.Amount),
                            TerminalFee = pg.Where(x => x.SourceID == (int)SalesSource.TERMINAL_FEE).Sum(z => z.Quantity * z.Amount),
                            Others = pg.Where(x => x.SourceID == (int)SalesSource.OTHERS).Sum(z => z.Quantity * z.Amount),
                            NightNavigation = pg.Where(x => x.SourceID == (int)SalesSource.NIGHT_NAVIGATION).Sum(z => z.Quantity * z.Amount)
                        }).OrderBy(x => x.MarketType).ToList();


            }

        }


        public List<SalesByAgentTop> GetTopTenSellers(DateTime fromDate, DateTime toDate)
        {

            using (isebookingEntities db = new isebookingEntities())
            {

                var initialData = db.Database.SqlQuery<SalesByAgentTop>("CALL rptGetSalesBySeller(?fromDate,?toDate)",
               new MySql.Data.MySqlClient.MySqlParameter("?fromDate", fromDate),
               new MySql.Data.MySqlClient.MySqlParameter("?toDate", toDate)).ToList<SalesByAgentTop>();


                return (from a in initialData
                        group a by new { AgentID = a.AgentID, AgentName = a.AgentName } into pg

                        select new SalesByAgentTop
                        {
                            AgentID = pg.Key.AgentID,
                            AgentName = pg.Key.AgentName,
                            Transfer = pg.Where(x => x.SourceID == (int)SalesSource.TRANSFER).Sum(z => z.Quantity * z.Amount),
                            BoatFee = pg.Where(x => x.SourceID == (int)SalesSource.TRANSFER && x.Boat > 0).Sum(z => z.Quantity * z.Boat),
                            EnvironmentFee = pg.Where(x => x.SourceID == (int)SalesSource.ENVIRONMENT_FEE).Sum(z => z.Quantity * z.Amount),
                            TerminalFee = pg.Where(x => x.SourceID == (int)SalesSource.TERMINAL_FEE).Sum(z => z.Quantity * z.Amount),
                            Others = pg.Where(x => x.SourceID == (int)SalesSource.OTHERS).Sum(z => z.Quantity * z.Amount),
                            NightNavigation = pg.Where(x => x.SourceID == (int)SalesSource.NIGHT_NAVIGATION).Sum(z => z.Quantity * z.Amount)
                        }).ToList().OrderByDescending(x => x.Gross).ToList();

            }

        }

        public List<SalesByAgentTop> GetTopTenTravelAgency(DateTime fromDate, DateTime toDate)
        {

            using (isebookingEntities db = new isebookingEntities())
            {

                var initialData = db.Database.SqlQuery<SalesByAgentTop>("CALL rptGetSalesByTravelAgency(?fromDate,?toDate)",
               new MySql.Data.MySqlClient.MySqlParameter("?fromDate", fromDate),
               new MySql.Data.MySqlClient.MySqlParameter("?toDate", toDate)).ToList<SalesByAgentTop>();



                return (from a in initialData
                        group a by new { AgentID = a.AgentID, AgentName = a.AgentName } into pg

                        select new SalesByAgentTop
                        {
                            AgentID = pg.Key.AgentID,
                            AgentName = pg.Key.AgentName,
                            Transfer = pg.Where(x => x.SourceID == (int)SalesSource.TRANSFER).Sum(z => z.Quantity * z.Amount),
                            BoatFee = pg.Where(x => x.SourceID == (int)SalesSource.TRANSFER).Sum(z => z.Quantity * z.Boat),
                            EnvironmentFee = pg.Where(x => x.SourceID == (int)SalesSource.ENVIRONMENT_FEE).Sum(z => z.Quantity * z.Amount),
                            TerminalFee = pg.Where(x => x.SourceID == (int)SalesSource.TERMINAL_FEE).Sum(z => z.Quantity * z.Amount),
                            Others = pg.Where(x => x.SourceID == (int)SalesSource.OTHERS).Sum(z => z.Quantity * z.Amount),
                            NightNavigation = pg.Where(x => x.SourceID == (int)SalesSource.NIGHT_NAVIGATION).Sum(z => z.Quantity * z.Amount)
                        }).ToList().OrderByDescending(x => x.Gross).ToList();


            }

        }


        public List<TrialBalance> GetTrialBalance(DateTime fromDate, DateTime toDate)
        {

            using (isebookingEntities db = new isebookingEntities())
            {

                return db.Database.SqlQuery<TrialBalance>("CALL rptTrialBalance(?fromDate,?toDate)",
               new MySql.Data.MySqlClient.MySqlParameter("?fromDate", fromDate),
               new MySql.Data.MySqlClient.MySqlParameter("?toDate", toDate)).ToList<TrialBalance>();

            }


        }


        public List<CashReceipts> GetCashReceiptsForPR(DateTime fromDate, DateTime toDate, string businessLocation = "MNL")
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                return db.Database.SqlQuery<CashReceipts>("CALL rptCashReceiptsPR(?fromDate,?toDate,?businessLoc)",
               new MySql.Data.MySqlClient.MySqlParameter("?fromDate", fromDate),
               new MySql.Data.MySqlClient.MySqlParameter("?toDate", toDate),
                new MySql.Data.MySqlClient.MySqlParameter("?businessLoc", businessLocation)).ToList<CashReceipts>();
            }

        }

        public List<CashReceipts> GetCashReceiptsForORandAR(DateTime fromDate, DateTime toDate, string businessLocation = "MNL")
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                return db.Database.SqlQuery<CashReceipts>("CALL rptCashReceiptsORAR(?fromDate,?toDate,?businessLoc)",
               new MySql.Data.MySqlClient.MySqlParameter("?fromDate", fromDate),
               new MySql.Data.MySqlClient.MySqlParameter("?toDate", toDate),
                new MySql.Data.MySqlClient.MySqlParameter("?businessLoc", businessLocation)).ToList<CashReceipts>();
            }


        }





        public List<SalesCommission> GetSalesCommission(DateTime fromDate, DateTime toDate, bool allowWithoutPayment = false)
        {
            using (isebookingEntities db = new isebookingEntities())
            {


                var initialData = new List<SalesCommission>();

                if (allowWithoutPayment)
                {
                    initialData = db.Database.SqlQuery<SalesCommission>("CALL rptSalesVsPayments(?fromDate,?toDate)",
                  new MySql.Data.MySqlClient.MySqlParameter("?fromDate", fromDate),
                  new MySql.Data.MySqlClient.MySqlParameter("?toDate", toDate)).ToList<SalesCommission>();

                }
                else
                {

                    initialData = db.Database.SqlQuery<SalesCommission>("CALL rptSalesCommission(?fromDate,?toDate)",
                   new MySql.Data.MySqlClient.MySqlParameter("?fromDate", fromDate),
                   new MySql.Data.MySqlClient.MySqlParameter("?toDate", toDate)).ToList<SalesCommission>();

                }

                return initialData.DistinctBy(x => new { RefNo = x.PaymentReferenceNo, CVNumber = x.CVNumber }).OrderBy(x => x.TransactionDate).ToList();
            }

        }

        public List<AccountsReceivableAgingSummary> GetAgingSummary()
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                var initialData = db.Database.SqlQuery<AccountsReceivableAging>("CALL rptAccountsReceivableAging").ToList<AccountsReceivableAging>();


                var groupItems = initialData.GroupBy(x => x.TravelAgencyID).Select(g => new AccountsReceivableAgingSummary
                {
                    TravelAgencyID = g.Key,
                    TravelAgency = g.Select(x => x.Name).FirstOrDefault(),
                    Current = g.Where(x => x.Age == 0).Sum(x => x.Balance),
                    Days30 = g.Where(x => x.Age == 30).Sum(x => x.Balance),
                    Days60 = g.Where(x => x.Age == 60).Sum(x => x.Balance),
                    Days90 = g.Where(x => x.Age == 90).Sum(x => x.Balance),
                    Days90Plus = g.Where(x => x.Age == 91).Sum(x => x.Balance)

                }).OrderBy(x => x.TravelAgency).ToList();


                return groupItems;
            }
        }


        public List<AccountsReceivableAging> GetAgingDetailed()
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                var initialData = db.Database.SqlQuery<AccountsReceivableAging>("CALL rptAccountsReceivableAging").ToList<AccountsReceivableAging>();



                return initialData.OrderBy(x => x.Name).ToList();
            }
        }

    }


    public class AccountsReceivableAgingSummary
    {

        public int TravelAgencyID { get; set; }
        public string TravelAgency { get; set; }
        public double Current { get; set; }
        public double Days30 { get; set; }
        public double Days60 { get; set; }
        public double Days90 { get; set; }
        public double Days90Plus { get; set; }

    }


    public class AccountsReceivableAging
    {
        public int TravelAgencyID { get; set; }
        public int BookingID { get; set; }
        public string CVNumber { get; set; }
        public string AccountName { get; set; }
        public string Name { get; set; }
        public DateTime TransactionDate { get; set; }
        public double Balance { get; set; }
        public double Age
        {
            get
            {
                var age = (DateTime.Now - TransactionDate).TotalDays;


                if (age < 1)
                {
                    Current = Balance;
                    return 0;
                }
                else if (age <= 30)
                {
                    Day30 = Balance;
                    return 30;
                }
                else if (age <= 60)
                {
                    Day60 = Balance;
                    return 60;
                }
                else if (age <= 90)
                {
                    Day90 = Balance;
                    return 90;
                }
                else
                {
                    Day90Plus = Balance;
                    return 91;
                }


            }
        }
        public double Current { get; set; }
        public double Day30 { get; set; }
        public double Day60 { get; set; }
        public double Day90 { get; set; }
        public double Day90Plus { get; set; }
    }

    public class SalesCommission
    {
        public DateTime TransactionDate { get; set; }
        public string CVNumber { get; set; }
        public string TravelAgency { get; set; }
        public string AccountName { get; set; }
        public double Sales { get; set; }
        public double? Collected { get; set; }
        public double? Balance
        {
            get
            {
                return Sales - (Collected ?? 0);
            }
        }
        public string PaymentReferenceNo { get; set; }
        public string ReceiptNo { get; set; }
        public DateTime? CollectionDate { get; set; }
        public string Agent { get; set; }
    }

    public class SalesByMarketType
    {
        public int BookingID { get; set; }
        public string CVNumber { get; set; }
        public string PaxName { get; set; }

        public int AgentID { get; set; }
        public string AgentName { get; set; }

        public int TravelAgencyID { get; set; }
        public string TravelAgency { get; set; }
        public int MarketTypeID { get; set; }
        public string MarketType { get; set; }
        public double? Transfer { get; set; }
        public double? BoatFee { get; set; }
        public double? EnvironmentFee { get; set; }
        public double? TerminalFee { get; set; }
        public double? Others { get; set; }
        public double? NightNavigation { get; set; }
        public double? CostOfService
        {
            get
            {
                return (BoatFee ?? 0) + (TerminalFee ?? 0) + (NightNavigation ?? 0) + (EnvironmentFee ?? 0);

            }

        }

        public double? Gross
        {
            get
            {
                return CostOfService + (Transfer ?? 0) + (Others ?? 0);

            }
        }

        public int? SourceID { get; set; }

        public double Amount { get; set; }
        public double Quantity { get; set; }
        public double Boat { get; set; }

    }

    public class SalesByAgentTop
    {
        public int AgentID { get; set; }
        public string AgentName { get; set; }
        public double? Transfer { get; set; }
        public double? BoatFee { get; set; }
        public double? EnvironmentFee { get; set; }
        public double? TerminalFee { get; set; }
        public double? Others { get; set; }
        public double? NightNavigation { get; set; }
        public double? CostOfService
        {
            get
            {
                return (BoatFee ?? 0) + (TerminalFee ?? 0) + (NightNavigation ?? 0) + (EnvironmentFee ?? 0);

            }

        }

        public double? Gross
        {
            get
            {
                return CostOfService + (Transfer ?? 0) + (Others ?? 0);

            }
        }

        public int? SourceID { get; set; }

        public double Amount { get; set; }
        public double Quantity { get; set; }
        public double Boat { get; set; }

    }

    public class CashReceipts
    {
        public int TravelAgencyID { get; set; }
        public string TravelAgency { get; set; }
        public DateTime PaymentDate { get; set; }
        public string ReceiptNo { get; set; }
        public int ReferenceType { get; set; }
        public string Remarks { get; set; }
        public string AccountCode { get; set; }
        public string AccountName { get; set; }
        public string Account
        {
            get
            {
                return AccountCode + " - " + AccountName;
            }
        }
        public double Amount { get; set; }
        public string DivisionCode { get; set; }
        public string FormattedReceiptNo
        {
            get
            {

                if (ReferenceType != 0)
                {
                    switch (ReferenceType)
                    {
                        case 1:
                            return "OR" + ReceiptNo;

                        case 2:
                            return "PR" + ReceiptNo;

                        case 3:
                            return "AR" + ReceiptNo;

                    }
                }
                return "";
            }
        }
    }

    public class TrialBalance
    {
        public DateTime PaymentDate { get; set; }
        public string PaymentNo { get; set; }
        public string TravelAgency { get; set; }
        public DateTime? CheckDate { get; set; }
        public string CheckNo { get; set; }
        public string Remarks { get; set; }
        public string CVNumber { get; set; }
        public string ReceiptNo { get; set; }
        public string AccountName { get; set; }
        public double Amount { get; set; }
        public int Type { get; set; }
    }

}
