﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public class CollectiblesModel
    {
        public int BatchID { get; set; }
        public int ID { get; set; }
        public int AgentID { get; set; }
        public string TravelAgency { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Double EWT { get; set; }
        public Double GrossTotal { get; set; }
        public Double NetTotal { get; set; }
        public Double? Balance { get; set; }
        public DateTime GenerationDate { get; set; }
        public DateTime? DateReceived { get; set; }
        public Double Payment { get; set; }
        public BillingStatementStatus Status { get; set; }
        public List<string> CVNumbers { get; set; }

    }
}
