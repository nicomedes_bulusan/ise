//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISE.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class othercharge
    {
        public othercharge()
        {
            this.bookingotherchargesindicators = new HashSet<bookingotherchargesindicator>();
        }
    
        public int ID { get; set; }
        public string Description { get; set; }
        public double Amount { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateLastUpdated { get; set; }
        public int IsActive { get; set; }
    
        public virtual ICollection<bookingotherchargesindicator> bookingotherchargesindicators { get; set; }
    }
}
