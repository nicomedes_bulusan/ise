﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public class JournalEntriesModel
    {


        public int ID { get; set; }
        public string AccountingCode { get; set; }
        public string AccountName { get; set; }
        public string CVNumber { get; set; }
        public int? BookingID { get; set; }
        public double Amount { get; set; }
        public string Remarks { get; set; }
        public int PaymentID { get; set; }
        public SalesType Type { get; set; }
        public string ReceiptNo { get; set; }
        public PaymentReferenceType ReferenceType { get; set; }
    }
}
