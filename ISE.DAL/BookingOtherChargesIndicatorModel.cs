﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public class BookingOtherChargesIndicatorModel
    {
        public int ID { get; set; }
        public int BookingID { get; set; }
        public int ChargingID { get; set; }

        public bool IsChecked { get; set; }

        public string ChargeDescription { get; set; }

        public Double Amount { get; set; }

        public List<BookingOtherChargesIndicatorModel> GetOtherChargesInitialIndicator()
        {
            isebookingEntities db = new isebookingEntities();

            var result = (from a in db.othercharges
                          where a.IsActive==1
                          select new BookingOtherChargesIndicatorModel
                          {
                              BookingID = 0,
                              ChargingID = a.ID,
                              ID = 0,
                              IsChecked = false,
                              ChargeDescription=a.Description,
                              Amount=a.Amount
                          }).ToList<BookingOtherChargesIndicatorModel>();

            return result;

        }

    }
}
