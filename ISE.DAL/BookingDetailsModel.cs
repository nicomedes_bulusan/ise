﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public class BookingDetailsModel
    {
        public int ID { get; set; }
        public int BookingID { get; set; }
        public string Description { get; set; }
        public Double Amount { get; set; }
        public int Quantity { get; set; }
        public Double Boat { get; set; }
        public int SourceID { get; set; }
    }
}
