﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ISE.DAL
{
    public class AccountingTermsModel
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastUpdated { get; set; }
        public Boolean IsActive { get; set; }

        
    
        public List<AccountingTermsModel> GetAll()
        {
             
           
           
            using (var db = new isebookingEntities())
            {

                return (from a in db.accountingterms
                        select new AccountingTermsModel
                        {
                            Code = a.Code,
                            DateCreated = a.DateCreated,
                            DateLastUpdated = a.DateLastUpdated,
                            Description = a.Description,
                            IsActive = a.IsActive
                        }).ToList<AccountingTermsModel>();

            }

        }

    }
}
