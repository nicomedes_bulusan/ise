﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
    public enum StatusFlag
    {
        NO_OPERATION = 0,
        ADDED = 1,
        MODIFIED = 2,
        DELETED = 3
    }

    public enum BookingStatus
    {

        SUBMITTED = 0,
        CONFIRMED = 1,
        FINALIZED = 2,
        CANCELLED = 3,
        SAVED = 9,
        OVERRIDE=10

    }
    public enum PaymentType
    {
        FOR_BILLING = 1,
        BUY_AND_BOOK = 2,
        ADVANCE_PAYMENT = 3
    }
    public class BookingHeaderModel
    {
        public int ID { get; set; }
        public string GroupName { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNumber { get; set; }
        public string Remarks { get; set; }
        public string CVNumber { get; set; }
        public int Adults { get; set; }
        public int Childrens { get; set; }
        public int ChildrensBelowSix { get; set; }
        public int Infants { get; set; }
        public int Senior { get; set; }
        public int TourGuides { get; set; }
        public int Escorts { get; set; }
        public int FOC { get; set; }
        public int Routing { get; set; }
        public Nullable<int> PickUpArrival { get; set; }
        public Nullable<int> DropOffArrival { get; set; }
        public Nullable<int> DropOffLocation { get; set; }
        public Nullable<int> PickUpDeparture { get; set; }
        public Nullable<int> DropOffDeparture { get; set; }
        public string FlightNo1 { get; set; }
        public string FlightNo2 { get; set; }
        public Nullable<DateTime> FlightDate1 { get; set; }
        public Nullable<DateTime> FlightDate2 { get; set; }
        public Double Total { get; set; }
        public int CreatedBy { get; set; }

        public string DateCreated { get; set; }
        public Nullable<int> DropOffLocation1 { get; set; }
        public Nullable<int> DropOffLocation2 { get; set; }

        public BookingStatus Status { get; set; }
        public PaymentType PaymentTypeID { get; set; }
        public Nullable<DateTime> CVDate { get; set; }


        public AgentsModel Agency { get; set; }

        public List<BookingLogsModel> Logs { get; set; }

        public ClassRate Class { get; set; }
        public string ResortHotel1 { get; set; }
        public string ResortHotel2 { get; set; }
        public int? Class2 { get; set; }
        public Double Payments { get; set; }
        public string OnlineBookingOrderID { get; set; }
        public int? Creator { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? ConfirmedBy { get; set; }
        public DateTime? ConfirmationDate { get; set; }
        public int? FinalizedBy { get; set; }
        public DateTime? FinalizedDate { get; set; }



    }
}
