﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoreLinq;
using System.Data.Entity;

namespace ISE.DAL
{
    public class BookingsPaged
    {
        public List<BookingHeaderModel> Records { get; set; }
        public long TotalRecords { get; set; }
        public long PageNumber { get; set; }
        public int PageSize { get; set; }
        public int StartPage { get; set; }
        public int EndPage { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int CurrentRecordCount { get; set; }

        private IMapper mapper;

        public BookingsPaged()
        {

            var newConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<agent, AgentsModel>();
                cfg.CreateMap<manifestodetail, ManifestoDetailsModel>();

                cfg.CreateMap<bookinglog, BookingLogsModel>()
                    .ForMember(dest => dest.Agent, conf => conf.MapFrom(o => o.agent));
                cfg.CreateMap<booking, BookingHeaderModel>()
                    .ForMember(dest => dest.Logs, conf => conf.MapFrom(o => o.bookinglogs))
                    .ForMember(dest => dest.Agency, conf => conf.MapFrom(o => o.agent));


                cfg.CreateMap<manifesto, ManifestoModel>()
                    .ForMember(dest => dest.Details, conf => conf.MapFrom(o => o.manifestodetails))
                    .ForMember(dest => dest.BookingHeader, conf => conf.MapFrom(o => o.booking));



            });

            mapper = newConfig.CreateMapper();

        }




        public BookingsPaged GetList(AgentsModel agent, DateTime fromDate, DateTime toDate, List<int> statuses = null, string search = null, int startIndex = 1, int pageSize = 10, string sortBy = "ID DESC")
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                if (statuses == null)
                {
                    statuses = new List<int> { 0, 1, 2, 3, 9 };
                }

                var propertyInfo = typeof(booking).GetProperty("ID");

                //OrderByDirection sortDirec = OrderByDirection.Ascending;
                string agentBusinessLoc = agent.Location;


                var resultset = db.bookings.OrderByDescending(x => x.ID).AsQueryable();

                if (agent.Type == 2)
                {
                   // resultset = resultset.Where(a => a.agent.businesslocation.Code == agentBusinessLoc);
                }
                if (agent.Type == 1)
                {
                    resultset = resultset.Where(a => a.TravelAgencyID == agent.TravelAgencyID);
                }

                if (search != null)
                {
                    resultset = resultset.Where(x => x.Remarks.ToString().Contains(search) || x.ID.ToString().Contains(search) || x.CVNumber.ToString().Contains(search) || x.GroupName.ToString().Contains(search));
                }

                resultset = resultset.Where(a => (DbFunctions.TruncateTime(a.FlightDate1) >= fromDate && DbFunctions.TruncateTime(a.FlightDate1) <= toDate)
                                     ||
                                      (DbFunctions.TruncateTime(a.FlightDate2) >= fromDate && DbFunctions.TruncateTime(a.FlightDate2) <= toDate)
                                    );
                resultset = resultset.Where(a => statuses.Contains(a.Status));

                var totalRowCount = resultset.Count();
                var recordTemp = resultset.Skip((startIndex - 1) * pageSize).Take(pageSize).ToList();


                BookingsPaged pagedRecords = new BookingsPaged();
                pagedRecords.PageNumber = startIndex;
                pagedRecords.TotalRecords = totalRowCount;

                pagedRecords.Records = mapper.Map<List<booking>, List<BookingHeaderModel>>(recordTemp);

                var totalPages = (int)Math.Ceiling((decimal)pagedRecords.TotalRecords / (decimal)pageSize);
                var currentPage = startIndex == 0 ? (int)startIndex : 1;
                var startPage = currentPage - 5;
                var endPage = currentPage + 4;
                if (startPage <= 0)
                {
                    endPage -= (startPage - 1);
                    startPage = 1;
                }

                if (startIndex >= totalPages)
                {
                    endPage = totalPages;
                    if (endPage > 10)
                    {
                        startPage = endPage - 9;
                    }
                }
                pagedRecords.CurrentPage = startIndex;
                pagedRecords.StartPage = startPage;
                pagedRecords.EndPage = endPage;
                pagedRecords.TotalPages = totalPages;
                pagedRecords.PageSize = pageSize;
                pagedRecords.CurrentRecordCount = pagedRecords.Records.Count();

                return pagedRecords;
            }


        }


        public Object GetUnpaidBillingStatements(int travelAgencyID, string search = null, int startIndex = 1, int pageSize = 10, string sortBy = "ID DESC")
        {
            using (isebookingEntities db = new isebookingEntities())
            {

               return (from a in db.bookings
                                 join b in db.accountreceivables on a.ID equals b.BookingID
                                 where (a.Status == (int)BookingStatus.FINALIZED) &&
                                 b.OriginalAmount > 0
                                 &&
               (a.PaymentTypeID == (int)PaymentType.FOR_BILLING)
               &&
               b.BillingPeriodID != null && b.Balance > 0
               &&
              a.IsPaid == false
              &&
              a.agent.TravelAgencyID == travelAgencyID
                                  &&
                                  (
                                 (search == null ? true : a.ID.ToString().Contains(search)) ||
                                 (search == null ? true : a.CVNumber.Contains(search)) ||
                                 (search == null ? true : a.GroupName.Contains(search)) ||
                                 (search == null ? true : a.ContactNumber.Contains(search))

                                  )
                                 group b by b.BillingPeriodID into pg
                                 select new BillingStatementModel
                                 {
                                     BillingID = pg.Key,
                                     Amount = pg.Sum(x => x.Balance),
                                     StatementDate = DateTime.Now,
                                     StartingDate = pg.Select(x => x.billingperiod.StartingDate).FirstOrDefault(),
                                     EndingDate = pg.Select(x => x.billingperiod.EndingDate).FirstOrDefault(),
                                     //CVNumbers=pg.Select(x=>x.booking.CVNumber).ToList<string>(),
                                     BookingCount = pg.Count()

                                 }


                         ).ToList();



               
            }
        }


        public BookingsPaged GetUpaidByBillingID(int billingID, string search = null, int startIndex = 1, int pageSize = 10, string sortBy = "ID DESC")
        {
            using (isebookingEntities db = new isebookingEntities())
            {

                var propertyInfo = typeof(booking).GetProperty("ID");

                OrderByDirection sortDirec = OrderByDirection.Ascending;




                var resultset = (from a in db.bookings
                                 join b in db.accountreceivables on a.ID equals b.BookingID
                                 where
               (a.Status == (int)BookingStatus.FINALIZED)
               &&
               (a.PaymentTypeID == (int)PaymentType.FOR_BILLING)
               &&

              a.IsPaid == false
              &&
              b.Balance > 0
              &&
              b.BillingPeriodID == billingID
                                  &&
                                  (
                                 (search == null ? true : a.ID.ToString().Contains(search)) ||
                                 (search == null ? true : a.CVNumber.Contains(search)) ||
                                 (search == null ? true : a.GroupName.Contains(search)) ||
                                 (search == null ? true : a.ContactNumber.Contains(search))

                                  )
                                 select a

                ).OrderBy(x => propertyInfo.GetValue(x, null), sortDirec);





                var totalRowCount = resultset.Count();
                var recordTemp = resultset.Skip((startIndex - 1) * pageSize).Take(pageSize).ToList();


                BookingsPaged pagedRecords = new BookingsPaged();
                pagedRecords.PageNumber = startIndex;
                pagedRecords.TotalRecords = totalRowCount;

                pagedRecords.Records = mapper.Map<List<booking>, List<BookingHeaderModel>>(recordTemp);

                var totalPages = (int)Math.Ceiling((decimal)pagedRecords.TotalRecords / (decimal)pageSize);
                var currentPage = startIndex == 0 ? (int)startIndex : 1;
                var startPage = currentPage - 5;
                var endPage = currentPage + 4;
                if (startPage <= 0)
                {
                    endPage -= (startPage - 1);
                    startPage = 1;
                }

                if (startIndex >= totalPages)
                {
                    endPage = totalPages;
                    if (endPage > 10)
                    {
                        startPage = endPage - 9;
                    }
                }
                pagedRecords.CurrentPage = startIndex;
                pagedRecords.StartPage = startPage;
                pagedRecords.EndPage = endPage;
                pagedRecords.TotalPages = totalPages;
                pagedRecords.PageSize = pageSize;
                pagedRecords.CurrentRecordCount = pagedRecords.Records.Count();

                return pagedRecords;
            }
        }
    }


    public class BillingStatementModel
    {
        public int? BillingID { get; set; }
        public Double? Amount { get; set; }
        public DateTime StatementDate { get; set; }
        public DateTime? StartingDate { get; set; }
        public DateTime? EndingDate { get; set; }
        public int? BookingCount { get; set; }
        public string Period
        {
            get
            {
                if (StartingDate == null || EndingDate == null) return "";

                return String.Format("{0:MMM d, yyyy} - {1:MMM d, yyyy}", StartingDate, EndingDate);

            }
        }
        public List<string> CVNumbers { get; set; }
    }
}
