﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.DAL
{
   public class FleetModel
    {
       public int ID { get; set; }
        public string VIDNumber { get; set; }
        public string PlateNo { get; set; }
        public string Type { get; set; }
        public Nullable<int> Capacity { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateLastUpdated { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<int> LastUpdateBy { get; set; }
        public string Description
        {
            get { return VIDNumber.ToString() + " " + Type; }
        }
        public int CategoryID { get; set; }
        public string Category { get; set; }

        public List<FleetModel> GetFleet()
        {
            using (var db = new isebookingEntities())
            {


                return (from a in db.fleets
                        select new FleetModel
                        {
                            ID=a.ID,
                            Capacity = a.Capacity,
                            CreatedBy = a.CreatedBy,
                            DateCreated = a.DateCreated,
                            DateLastUpdated = a.DateLastUpdated,
                            LastUpdateBy = a.LastUpdateBy,
                            PlateNo = a.PlateNo,
                            Type = a.Type,
                            VIDNumber = a.VIDNumber,
                            CategoryID=a.CategoryID,
                            Category=a.fleetcategory.Description
                        }).ToList();

            }
        }

        public Boolean CreateOrUpdate(FleetModel fleetRef, AgentsModel agent)
        {
            using (isebookingEntities db = new isebookingEntities())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try {

                        Boolean isNewEntry = false;
                        fleet newFleet = db.fleets.Where(x => x.ID == fleetRef.ID).FirstOrDefault();

                        if (newFleet == null)
                        {
                            isNewEntry = true;
                            newFleet = new fleet();
                            newFleet.CreatedBy = agent.ID;
                            newFleet.DateCreated = DateTime.Now;
                        }

                        newFleet.VIDNumber = fleetRef.VIDNumber;
                        newFleet.Capacity = fleetRef.Capacity;                   
                      
                        newFleet.DateLastUpdated = DateTime.Now;
                        newFleet.PlateNo = fleetRef.PlateNo;
                        newFleet.Type = fleetRef.Type;
                        newFleet.LastUpdateBy = agent.ID;
                        newFleet.CategoryID = fleetRef.CategoryID;

                        if (isNewEntry)
                        {
                            db.fleets.Add(newFleet);
                        }
                        db.SaveChanges();
                        transaction.Commit();
                        return true;
                    }
                    catch (System.Data.Entity.Infrastructure.DbUpdateException dbU)
                    {
                        transaction.Rollback();
                        Exception ex = dbU.GetBaseException();
                        throw new Exception(ex.Message);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        transaction.Rollback();
                        string errorMessages = null;
                        foreach (DbEntityValidationResult validationResult in dbEx.EntityValidationErrors)
                        {
                            string entityName = validationResult.Entry.Entity.GetType().Name;
                            foreach (DbValidationError error in validationResult.ValidationErrors)
                            {
                                errorMessages += (entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                            }
                        }

                        throw new Exception(errorMessages);
                    }
                    finally
                    {
                        db.Database.Connection.Close();

                    }

                }
                  
                
            }
        }

    }



}
