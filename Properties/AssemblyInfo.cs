﻿using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Barnhill Software")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("Copyright © Brad Barnhill 2007-present")]
[assembly: AssemblyDescription("Barcode Image Generation Library")]
[assembly: AssemblyFileVersion("1.0.0.22")]
[assembly: AssemblyProduct("BarcodeLib")]
[assembly: AssemblyTitle("BarcodeLib")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("1.0.0.22")]
[assembly: CompilationRelaxations(8)]
[assembly: ComVisible(false)]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.Default | DebuggableAttribute.DebuggingModes.DisableOptimizations | DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | DebuggableAttribute.DebuggingModes.EnableEditAndContinue)]
[assembly: Guid("6b4a01ba-c67e-443d-813f-24c0c0f39a02")]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows=true)]
