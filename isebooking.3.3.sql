INSERT INTO resortsandhotels (ID,Name,DateCreated,DateLastUpdated,IsActive) VALUES (1,'OTHERS',NOW(),NOW(),1); 
SET @last_id_in_table1 = LAST_INSERT_ID();
UPDATE bookings set DropOffLocation1 = @last_id_in_table1 where DropOffLocation1 is null;
UPDATE bookings set DropOffLocation2 = @last_id_in_table1 where  DropOffLocation2 is null;

ALTER TABLE `bookings`
MODIFY COLUMN `DropOffLocation1`  int(11) NULL DEFAULT 1 AFTER `DropOffLocation2`,
MODIFY COLUMN `DropOffLocation2`  int(11) NULL DEFAULT 1 AFTER `DropOffLocation1`;