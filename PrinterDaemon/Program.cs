﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace PrinterDaemon
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<HttpApiService>(sc =>
                {
                  
                   
                    sc.ConstructUsing(name => new HttpApiService(new Uri("http://localhost:1234")));
                   

                });


                x.EnableServiceRecovery(r =>
                {
                    
                    //you can have up to three of these
                    // r.RestartComputer(5, "message");
                    r.RestartService(0);
                    //the last one will act for all subsequent failures
                    r.RunProgram(7, "ping google.com");

                    //should this be true for crashed or non-zero exits

                    r.OnCrashOnly();
                    //number of days until the error count resets
                    r.SetResetPeriod(1);
                });


                x.RunAsNetworkService();
                x.StartAutomatically();
                x.SetDescription("Billing Statement Genrator Windows service");
                x.SetDisplayName("ISE Billing Generator");
                x.SetServiceName("ISE Billing Generator");


            });


        }
    }
}
