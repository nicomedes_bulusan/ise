﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ISE
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
           

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

          //  config.Routes.MapHttpRoute(
          //    name: "CustomApi",
          //    routeTemplate: "api/{controller}/{action}/{id}",
          //    defaults: new { id = RouteParameter.Optional }
          //);



            // Load Web API controllers 
           // config.InitializeReceiveWordPressWebHooks();
         
            //config.InitializeCustomWebHooks();
            //config.InitializeCustomWebHooksApis();
            config.InitializeReceiveCustomWebHooks();
            config.InitializeReceiveGenericJsonWebHooks();
         

            config.EnsureInitialized();

            var webhookRoutes = config.Routes;
            ; // Set breakpoint here and inspect the variable above, you should see the custom incoming webhook route listened.

        }
    }
}
