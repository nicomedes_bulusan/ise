﻿using ISE.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace ISE.Models
{
    public class AuthorizationGateway
    {
        public static AgentsModel GetAuthorizedInfo(LoginViewModel login,Boolean isCoordinator =false)
        {
            AgentsModel em = new AgentsModel();

            var agent = System.Web.HttpContext.Current;

            HttpContext.Current.Session.Timeout = 30;
            var session = HttpContext.Current.Session["UserInfo"];

            bool debugMode = false;

#if DEBUG
            debugMode = false;
#endif


         

            if (debugMode)
            {

                var emp = em.FindAgent("johndoe@gmail.com", "P@ssw0rd");
                    session = emp;

                    return emp;

                //  return em.FindAgent("juandelacruz@gmail.com", "P@ssw0rd");

            }
            else
            {

                if (session != null)
                {
                    
                    return (AgentsModel)session;
                }


                if (login == null)
                {
                    return null;
                }

                var emp = em.FindAgent(login.UserName, login.Password, isCoordinator);

                session = emp;
                return emp;
            }
        }

        public static string CalculateMD5Hash(string input)
        {

            MD5 md5 = System.Security.Cryptography.MD5.Create();

            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);

            byte[] hash = md5.ComputeHash(inputBytes);


            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {

                sb.Append(hash[i].ToString("X2"));

            }

            return sb.ToString();

        }



    }
}