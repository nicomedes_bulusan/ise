/// <autosync enabled="true" />
/// <reference path="../admin-lte/js/app.min.js" />
/// <reference path="angular.min.js" />
/// <reference path="angular-animate.min.js" />
/// <reference path="angular-aria.min.js" />
/// <reference path="angular-cookies.min.js" />
/// <reference path="angular-file-upload.min.js" />
/// <reference path="angular-loader.min.js" />
/// <reference path="angular-message-format.min.js" />
/// <reference path="angular-messages.min.js" />
/// <reference path="angular-mocks.js" />
/// <reference path="angular-parse-ext.min.js" />
/// <reference path="angular-resource.min.js" />
/// <reference path="angular-route.min.js" />
/// <reference path="angular-sanitize.min.js" />
/// <reference path="angular-scenario.js" />
/// <reference path="angular-touch.min.js" />
/// <reference path="angular-ui/ui-bootstrap.min.js" />
/// <reference path="angular-ui/ui-bootstrap-tpls.min.js" />
/// <reference path="angular-ui-router.min.js" />
/// <reference path="bootstrap.min.js" />
/// <reference path="custom/startup.js" />
/// <reference path="datetimepicker.js" />
/// <reference path="datetimepicker.templates.js" />
/// <reference path="ionic.bundle.min.js" />
/// <reference path="ionic.min.js" />
/// <reference path="ionic-angular.min.js" />
/// <reference path="jquery.blockui.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-2.1.3.js" />
/// <reference path="jquery-2.1.3.min.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="moment.min.js" />
/// <reference path="moment-with-locales.min.js" />
/// <reference path="npm.js" />
/// <reference path="respond.js" />
/// <reference path="toastr.min.js" />
/// <reference path="smalot-datetimepicker/bootstrap-datetimepicker.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.ar.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.bg.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.ca.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.cs.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.da.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.de.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.ee.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.el.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.es.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.fi.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.fr.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.he.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.hr.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.hu.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.id.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.is.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.it.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.ja.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.ko.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.lt.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.lv.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.ms.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.nb.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.nl.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.no.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.pl.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.pt-br.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.pt.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.ro.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.rs-latin.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.rs.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.ru.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.sk.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.sl.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.sv.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.sw.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.th.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.tr.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.ua.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.uk.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.zh-cn.js" />
/// <reference path="smalot-datetimepicker/locales/bootstrap-datetimepicker.zh-tw.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="angular-validate.min.js" />
/// <reference path="angular-ui-form-validation.js" />
/// <reference path="zebra_datepicker.js" />
/// <reference path="zebra_datepicker.src.js" />
/// <reference path="angular-validator.min.js" />
/// <reference path="angular-moment-picker.min.js" />
/// <reference path="dirpagination.js" />
/// <reference path="dirpagination.spec.js" />
/// <reference path="angularjs-dropdown-multiselect.min.js" />
/// <reference path="custom/generate_key.js" />
/// <reference path="custom/booking.js" />
/// <reference path="ng-tags-input.js" />
/// <reference path="daterangepicker.js" />
/// <reference path="isteven-multi-select.js" />
/// <reference path="mask.min.js" />
/// <reference path="angular-modal-service.min.js" />
/// <reference path="ui-grid.js" />
/// <reference path="angular-daterangepicker.js" />
/// <reference path="custom/sha256.js" />
