﻿



var desiredKeyLength=3;

var  maxKeyLength=5;

var  ignoredWords= ["THE", "A", "AN", "AS", "AND", "OF", "OR"];

var characterMap= {
    199: "C",
    231: "c",
    252: "u",
    251: "u",
    250: "u",
    249: "u",
    233: "e",
    234: "e",
    235: "e",
    232: "e",
    226: "a",
    228: "a",
    224: "a",
    229: "a",
    225: "a",
    239: "i",
    238: "i",
    236: "i",
    237: "i",
    196: "A",
    197: "A",
    201: "E",
    230: "ae",
    198: "Ae",
    244: "o",
    246: "o",
    242: "o",
    243: "o",
    220: "U",
    255: "Y",
    214: "O",
    241: "n",
    209: "N"
};
            
          function  getTotalLength(words) {
              return words.join("").length;
            }
        
          function removeIgnoredWords(word) {
              return $.grep(word, function (b) {
                  return $.inArray(b, ignoredWords) === -1
              })
          };

          function  createAcronym(b) {
                var a = "";
                $.each(b, function (c, d) {
                    a += d.charAt(0)
                });
                return a
            }
            
          function  getFirstSyllable(c) {
                var b = false;
                var a;
                for (a = 0; a < c.length; a++) {
                    if (isVowelOrY(c[a])) {
                        b = true
                    } else {
                        if (b) {
                            return c.substring(0, a + 1)
                        }
                    }
                }
                return c
            }
            
         function   isVowelOrY(a) {
                return a && a.length === 1 && a.search("[AEIOUY]") !== -1
            }
            
         function   generate(b) {
                b = $.trim(b);
                if (!b) {
                    return ""
                }

                var a = [];
                for (var d = 0, f = b.length; d < f; d++) {
                    var e = characterMap[b.charCodeAt(d)];
                    a.push(e ? e : b[d]);
                }

                b = a.join("");
                var h = [];
                $.each(b.split(/\s+/), function (j, k) {
                    if (k) {
                        k = k.replace(/[^a-zA-Z]/g, "");
                        k = k.toUpperCase();
                        k.length && h.push(k);
                    }
                });

                if (desiredKeyLength && 
                        getTotalLength(h) > desiredKeyLength) {
                    h = removeIgnoredWords(h);
                }

                var c;
                if (!h.length) {
                    c = ""
                } else {
                    if (h.length === 1) {
                        var g = h[0];
                        if (desiredKeyLength && g.length > desiredKeyLength) {
                            c = getFirstSyllable(g)
                        } else {
                            c = g
                        }
                    } else {
                        c = createAcronym(h)
                    }
                }

                if (maxKeyLength && c.length > maxKeyLength) {
                    c = c.substr(0, maxKeyLength)
                }

                return c
            }

        


 
