﻿

jQuery.validator.addMethod("valueNotEquals", function (value, element, arg) {
    return arg != jQuery(element).find('option:selected').text();
}, "This field is required.");

jQuery.validator.addMethod("requiredFlightDate1", function (value, element, arg) {

    var pickup1 = $('select[name="txtPickUpArrival"]').val().replace("number:", "");
    var dropOff1 = $('select[name="txtDropOffArrival"]').val().replace("number:", "");



    var pickUpLen = pickup1.length;
    var dropOffLen = dropOff1.length;

    if (pickup1 == "object:null" && dropOff1 == "object:null") {



        return true;
    }

    if ((pickUpLen > 0 || dropOffLen > 0)) {

        var date = new Date(value);
        var isDate = (date instanceof Date && !isNaN(date.valueOf()));



        return isDate;
    }

    return true;

}, "Please enter a valid date.");

jQuery.validator.addMethod("requiredLocation1", function (value, element, arg) {

    var pickup1 = $('select[name="txtPickUpArrival"]').val().replace("number:", "");
    var dropOff1 = $('select[name="txtDropOffArrival"]').val().replace("number:", "");
    var pickUpLen = pickup1.length;
    var dropOffLen = dropOff1.length;

    if (pickup1 == "object:null" && dropOff1 == "object:null") {

        return true;
    }


    if (value == "object:null") return false;

    return true;

}, "This field is required.");

jQuery.validator.addMethod("requiredLocation2", function (value, element, arg) {

    var pickup1 = $('select[name="txtPickUpDeparture"]').val().replace("number:", "");
    var dropOff1 = $('select[name="txtDropOffDeparture"]').val().replace("number:", "");

    var pickUpLen = pickup1.length;
    var dropOffLen = dropOff1.length;

    if (pickup1 == "object:null" && dropOff1 == "object:null") {

        return true;
    }



    if (value == "object:null") return false;



    return true;

}, "This field is required.");



jQuery.validator.addMethod("requiredClass2", function (value, element, arg) {

    var pickup1 = $('select[name="txtPickUpDeparture"]').val().replace("number:", "");
    var dropOff1 = $('select[name="txtDropOffDeparture"]').val().replace("number:", "");

    var pickUpLen = pickup1.length;
    var dropOffLen = dropOff1.length;

    if (pickup1 == "object:null" && dropOff1 == "object:null") {

        return true;
    }



    if (value == "object:null" || value =="") return false;



    return true;

}, "This field is required.");



jQuery.validator.addMethod("requiredFlightDate2", function (value, element, arg) {
    var pickup1 = $('select[name="txtPickUpDeparture"]').val().replace("number:", "");
    var dropOff1 = $('select[name="txtDropOffDeparture"]').val().replace("number:", "");

    var pickUpLen = pickup1.length;
    var dropOffLen = dropOff1.length;

    if (pickup1 == "object:null" && dropOff1 == "object:null") {

        return true;
    }

    if ((pickUpLen > 0 || dropOffLen > 0)) {


        var date = new Date(value);
        var isDate = (date instanceof Date && !isNaN(date.valueOf()));

        return isDate;
    }

    return true;
}, "Please enter a valid date.");

App.controller("bookingController", ['$scope', '$http', 'jsonFilter', 'FileUploader', '$timeout', '$filter', function ($scope, $http, jsonFilter, FileUploader, $timeout, $filter) {
    var logResult = function (str, data, status, headers, config) {
        console.log(data);
    };

    $scope.submissionDate = moment();
    $scope.Editing = false;
    $scope.EditAgent = false;

    $scope.myTravelAgency = {};
    $scope.activeAgents = [];

    $scope.selectedHotelResort1 = [];
    $scope.selectedHotelResort2 = [];

    //console.log($scope.submissionDate)

    $scope.bookingStatus = [
			  { ID: 0, Description: "SUBMITTED" },
			   { ID: 1, Description: "CONFIRMED" },
				{ ID: 2, Description: "FINALIZED" },
				 { ID: 3, Description: "CANCELLED" },
				  { ID: 9, Description: "SAVED" }
    ]


    $scope.manifestoType = [{ ID: 1, Description: "Arrival" }, { ID: 2, Description: "Departure" }]

    $scope.classRate = [{ ID: 1, Description: "REGULAR" }, { ID: 2, Description: "SPECIAL" }, { ID: 3, Description: "UPHILL" }];

    $scope.getAllResortsAndHotels = function () {

        $http.post("/ResortsAndHotels/GetResortsAndHotels").success(function (data, status, headers, config) {

            logResult("GET SUCCESS", data, status, headers, config);
            $scope.resortsAndHotels = data;
            $scope.resortsAndHotels.unshift({ ID: null, Name: "" });

        }).error(function (data, status, headers, config) {

            logResult("GET ERROR", data, status, headers, config);
        });
    };


    $scope.grandTotal = 0;

    $scope.computeGrandTotal = function (itenerary) {

        $scope.grandTotal += itenerary.Quantity * (itenerary.Amount + parseFloat(itenerary.Boat || 0));


    }


    $scope.getTravelAgency = function () {
        console.log($scope.myTravelAgency)

        if ($scope.myTravelAgency.length > 0) {
            $scope.bookingInfo.Header.CreatedBy = $scope.myTravelAgency[0].ID;
            $scope.bookingInfo.Header.PaymentTypeID = $scope.myTravelAgency[0].PaymentTypeID;

            $scope.bookingInfo.Header.Agency = $scope.myTravelAgency[0];

        } else {
            $scope.bookingInfo.Header.CreatedBy = 0;
            $scope.bookingInfo.Header.Agency = myID;
        }
        $scope.EditAgent = false;
    }


    $scope.parseNumber = function (val) {
        return parseFloat(val || 0);
    }

    $scope.bookingDetails = [];
    $scope.bookingManifesto = [];
    $scope.bookingInfo = {
        Header: {

            GroupName: "",
            ContactPerson: "",
            ContactNumber: "",
            Remarks: "",
            CVNumber: "",
            Adults: 0,
            Childrens: null,
            Infants: null,
            Senior: null,
            TourGuides: null,
            Escorts: null,
            FOC: null,
            Routing: 2,
            PickUpArrival: null,
            DropOffArrival: null,
            DropOffLocation1: null,
            DropOffLocation2: null,
            DropOffLocation: null,
            PickUpDeparture: null,
            DropOffDeparture: null,
            FlightNo1: null,
            FlightNo2: null,
            FlightDate1: null,
            FlightDate2: null,
            Total: null,
            Status: 1,
            Class: 1,
            Class2:null,
            CreatedBy: myID.ID,
            PaymentTypeID: myID.PaymentTypeID
        },
        Details: {},
        Attachments: null,
        //Detail: {[
        //    ID: 0,
        //    BookingID: 0,
        //    Description: "",
        //    Amount: 0,
        //    Quantity: 0
        //]},
        OCs: {},
        Manifesto: {}


    };

    $scope.destinations = [];
    $scope.otherCharges = null;
    $scope.bookingAttachments = null;
    $scope.OCs = [];


    // $scope.dropOffLocations = [{ ID: "RESORT", Description: "RESORT" }, {ID:"OTHERS",Description:"OTHERS"}];
    $scope.paymentTypes = [{ ID: 1, Description: "FOR BILLING" }, { ID: 2, Description: "BOOK AND BUY" }, { ID: 3, Description: "ADVANCE PAYMENT" }];

    //TOGGLE TF IN
    $scope.toggleTFIN = function (value) {
        if (value) {
            $scope.otherCharges[0].IsChecked = true;
        }

    }

    //TOGGLE TF OUT
    $scope.toggleTFOUT = function (value) {

        if (value) {
            $scope.otherCharges[1].IsChecked = true;
        }

    }


    //TOGGLE ENV FEE
    $scope.toggleEnvFee = function (value) {

        if (value) {
            $scope.otherCharges[2].IsChecked = true;
        }

    }


    //TOGGLE NN FEE
    $scope.toggleNNFee = function (value) {
        if (value) {
            $scope.otherCharges[3].IsChecked = true;
        }
    }


    //GET DESTINATIONS
    $scope.getDestinations = function () {
        $http.get("/Destinations/GetDestinations")
							 .success(function (data, status, headers, config) {

							     logResult("GET SUCCESS", data, status, headers, config);



							     $scope.destinations = data;

							     $scope.destinations.unshift({ ID: null, Description: "" });


							 }).error(function (data, status, headers, config) {
							     logResult("GET ERROR", data, status, headers, config);


							 });



        $scope.getOtherCharges();

    };


    //GET INITIAL CHARGES
    $scope.getOtherCharges = function () {
        $http.get("/Booking/GetInitialOtherCharges")
							 .success(function (data, status, headers, config) {

							     logResult("GET SUCCESS", data, status, headers, config);
							     $scope.otherCharges = data;
							     $scope.getBooking();

							 }).error(function (data, status, headers, config) {
							     logResult("GET ERROR", data, status, headers, config);


							 });

    };




    
    $scope.setCurrentTravelAgency = function () {
        $scope.EditAgent = true;

        $scope.currentTravelAgency = $filter('filter')($scope.activeAgents, { ID: $scope.bookingInfo.Header.CreatedBy }, true)[0];

        if ($scope.currentTravelAgency != null) {
            $scope.currentTravelAgency.ticked = true;
            $scope.$apply()
        }



    }



    //GET BOOKING
    $scope.getBooking = function () {




        if (bookingID == 0 || bookingID == null) {

            $scope.getAgents();
            return;

        } 


        $http.post("/Booking/GetBooking/" + bookingID)
						   .success(function (data, status, headers, config) {

						       logResult("GET SUCCESS", data, status, headers, config);

						       $scope.bookingInfo = data;
						       $scope.bookingDetails = data.Details;
						       // $scope.otherCharges = data.OtherCharges;
						       //alert(data.OtherCharges.length)
						       $scope.OCs = data.OCs;
						       $scope.bookingManifesto = data.Manifesto;

						       for (var j = 0; j < $scope.bookingManifesto.length; j++) {

						           // $scope.bookingManifesto[j].Date = moment($scope.bookingManifesto[j].Date, 'MM/DD/YYYY HH:mm');

						       }

						       if (data.Header.Status != 9) {
						           $scope.Editing = true;
						       }


						       $scope.getAgents();


						       //$scope.currentTravelAgency = $filter('filter')($scope.activeAgents, { ID: $scope.bookingInfo.Header.CreatedBy }, true)[0];

						       //if ($scope.currentTravelAgency != null) {
						       //    $scope.currentTravelAgency.ticked = true;
						       //}

						       //debugger;
						       // $scope.bookingInfo.Header.FlightDate1 = moment($scope.bookingInfo.Header.FlightDate1, 'MM/DD/YYYY HH:mm');
						       // $scope.bookingInfo.Header.FlightDate2 = moment($scope.bookingInfo.Header.FlightDate2, 'MM/DD/YYYY HH:mm');
						       // console.log(  $scope.bookingInfo.Manifesto);




						       for (var i = 0; i < data.OCs.length; i++) {
						           //alert(data.OCs[i].indexOf("withTe") +"=="+data.OCs[i])
						           if (data.OCs[i].indexOf("withTFIn") !== -1)
						               $scope.otherCharges[0].IsChecked = true;
						           if (data.OCs[i].indexOf("withTFOut") !== -1)
						               $scope.otherCharges[1].IsChecked = true;
						           if (data.OCs[i].indexOf("withEnvFee") !== -1)
						               $scope.otherCharges[2].IsChecked = true;
						           if (data.OCs[i].indexOf("withNNFee") !== -1)
						               $scope.otherCharges[3].IsChecked = true;

						           $scope[data.OCs[i]] = true;
						       }

						   }).error(function (data, status, headers, config) {
						       logResult("GET ERROR", data, status, headers, config);


						   });


    }
    /////////////////////////Create Booking/////////////////////////



    //UPLOAD FILES
    $scope.uploadFiles = function (detailID, index) {


        $('#myModal').modal('show');

    };



    //COMPUTE BOOKING
    $scope.computeBooking = function () {


        checkSession();


        var itenerariesTemp = $filter('filter')($scope.bookingDetails, { IsItenerary: true });
        $scope.bookingDetails = [];
        $scope.bookingDetails = itenerariesTemp;

        $scope.grandTotal = 0;
        var isValid = true;

        var numberOfPerson = parseFloat($scope.bookingInfo.Header.Senior || 0) + parseFloat($scope.bookingInfo.Header.Adults || 0) +
		   parseFloat($scope.bookingInfo.Header.Childrens || 0);

        var numberOfChildren = parseFloat($scope.bookingInfo.Header.Childrens || 0) + parseFloat($scope.bookingInfo.Header.ChildrensBelowSix || 0);

        // alert(numberOfChildren);


        if (!isValid) {
            return;
        }



        var numberOfPersonsTFIn = 0;
        var numberOfPersonsTFOut = 0;
        var numberOfPersonsEnvFee = 0;
        var numberOfPersonsNNFee = 0;
        var OC = [];
        var i = 0;


        //NN FEE COUNTS

        if ($scope.withNNFeeTourGuide) {
            numberOfPersonsNNFee += parseFloat($scope.bookingInfo.Header.TourGuides || 0);
            OC[i] = "withNNFeeTourGuide";
            i++;
        }
        if ($scope.withNNFeeEscort) {
            numberOfPersonsNNFee += parseFloat($scope.bookingInfo.Header.Escorts || 0);
            OC[i] = "withNNFeeEscort";
            i++;
        }
        if ($scope.withNNFeeFOC) {
            numberOfPersonsNNFee += parseFloat($scope.bookingInfo.Header.FOC || 0);
            OC[i] = "withNNFeeFOC";
            i++;
        }
        if ($scope.withNNFeeBelowSix) {
            numberOfPersonsNNFee += parseFloat($scope.bookingInfo.Header.ChildrensBelowSix || 0);
            OC[i] = "withNNFeeBelowSix";
            i++;
        }

        if ($scope.withNNFeeSenior) {
            numberOfPersonsNNFee += parseFloat($scope.bookingInfo.Header.Senior || 0);
            OC[i] = "withNNFeeSenior";
            i++;
        }
        if ($scope.withNNFeeAdult) {
            numberOfPersonsNNFee += parseFloat($scope.bookingInfo.Header.Adults || 0);
            OC[i] = "withNNFeeAdult";
            i++;
        }
        if ($scope.withNNFeeChildren) {
            numberOfPersonsNNFee += parseFloat($scope.bookingInfo.Header.Childrens || 0);
            OC[i] = "withNNFeeChildren";
            i++;
        }
        if ($scope.withNNFeeInfant) {
            numberOfPersonsNNFee += parseFloat($scope.bookingInfo.Header.Infants || 0);
            OC[i] = "withNNFeeInfant";
            i++;
        }



        if ($scope.withTFInTourGuide) {
            numberOfPersonsTFIn += parseFloat($scope.bookingInfo.Header.TourGuides || 0);
            OC[i] = "withTFInTourGuide";
            i++;
        }
        if ($scope.withTFInEscort) {
            numberOfPersonsTFIn += parseFloat($scope.bookingInfo.Header.Escorts || 0);
            OC[i] = "withTFInEscort";
            i++;
        }
        if ($scope.withTFInFOC) {
            numberOfPersonsTFIn += parseFloat($scope.bookingInfo.Header.FOC || 0);
            OC[i] = "withTFInFOC";
            i++;
        }
        if ($scope.withTFInBelowSix) {
            numberOfPersonsTFIn += parseFloat($scope.bookingInfo.Header.ChildrensBelowSix || 0);
            OC[i] = "withTFInBelowSix";
            i++;
        }

        if ($scope.withTFInSenior) {
            numberOfPersonsTFIn += parseFloat($scope.bookingInfo.Header.Senior || 0);
            OC[i] = "withTFInSenior";
            i++;
        }
        if ($scope.withTFInAdult) {
            numberOfPersonsTFIn += parseFloat($scope.bookingInfo.Header.Adults || 0);
            OC[i] = "withTFInAdult";
            i++;
        }
        if ($scope.withTFInChildren) {
            numberOfPersonsTFIn += parseFloat($scope.bookingInfo.Header.Childrens || 0);
            OC[i] = "withTFInChildren";
            i++;
        }
        if ($scope.withTFInInfant) {
            numberOfPersonsTFIn += parseFloat($scope.bookingInfo.Header.Infants || 0);
            OC[i] = "withTFInInfant";
            i++;
        }


        if ($scope.bookingInfo.Header.Routing == 2) {

            if ($scope.withTFOutTourGuide) {
                numberOfPersonsTFOut += parseFloat($scope.bookingInfo.Header.TourGuides || 0);
                OC[i] = "withTFOutTourGuide";
                i++;
            }
            if ($scope.withTFOutEscort) {
                numberOfPersonsTFOut += parseFloat($scope.bookingInfo.Header.Escorts || 0);
                OC[i] = "withTFOutEscort";
                i++;
            }
            if ($scope.withTFOutFOC) {
                numberOfPersonsTFOut += parseFloat($scope.bookingInfo.Header.FOC || 0);
                OC[i] = "withTFOutFOC";
                i++;
            }
            if ($scope.withTFOutBelowSix) {
                numberOfPersonsTFOut += parseFloat($scope.bookingInfo.Header.ChildrensBelowSix || 0);
                OC[i] = "withTFOutBelowSix";
                i++;
            }


            if ($scope.withTFOutSenior) {
                numberOfPersonsTFOut += parseFloat($scope.bookingInfo.Header.Senior || 0);
                OC[i] = "withTFOutSenior";
                i++;
            }
            if ($scope.withTFOutAdult) {
                numberOfPersonsTFOut += parseFloat($scope.bookingInfo.Header.Adults || 0);
                OC[i] = "withTFOutAdult";
                i++;
            }
            if ($scope.withTFOutChildren) {
                numberOfPersonsTFOut += parseFloat($scope.bookingInfo.Header.Childrens || 0);
                OC[i] = "withTFOutChildren";
                i++;
            }
            if ($scope.withTFOutInfant) {
                numberOfPersonsTFOut += parseFloat($scope.bookingInfo.Header.Infants || 0);
                OC[i] = "withTFOutInfant";
                i++;
            }


        }


        if ($scope.withEnvFeeTourGuide) {
            numberOfPersonsEnvFee += parseFloat($scope.bookingInfo.Header.TourGuides || 0);
            OC[i] = "withEnvFeeTourGuide";
            i++;
        }
        if ($scope.withEnvFeeEscort) {
            numberOfPersonsEnvFee += parseFloat($scope.bookingInfo.Header.Escorts || 0);
            OC[i] = "withEnvFeeEscort";
            i++;
        }
        if ($scope.withEnvFeeFOC) {
            numberOfPersonsEnvFee += parseFloat($scope.bookingInfo.Header.FOC || 0);
            OC[i] = "withEnvFeeFOC";
            i++;
        }
        if ($scope.withEnvFeeBelowSix) {
            numberOfPersonsEnvFee += parseFloat($scope.bookingInfo.Header.ChildrensBelowSix || 0);
            OC[i] = "withEnvFeeBelowSix";
            i++;
        }


        if ($scope.withEnvFeeSenior) {
            numberOfPersonsEnvFee += parseFloat($scope.bookingInfo.Header.Senior || 0);
            OC[i] = "withEnvFeeSenior";
            i++;
        }
        if ($scope.withEnvFeeAdult) {
            numberOfPersonsEnvFee += parseFloat($scope.bookingInfo.Header.Adults || 0);
            OC[i] = "withEnvFeeAdult";
            i++;
        }
        if ($scope.withEnvFeeChildren) {
            numberOfPersonsEnvFee += parseFloat($scope.bookingInfo.Header.Childrens || 0);
            OC[i] = "withEnvFeeChildren";
            i++;
        }
        if ($scope.withEnvFeeInfant) {
            numberOfPersonsEnvFee += parseFloat($scope.bookingInfo.Header.Infants || 0);
            OC[i] = "withEnvFeeInfant";
            i++;
        }




        var OCd = {
            //ID: 0,
            //BookingID: 0,
            OC: OC

        };


        $scope.OCs = OC;

        //COMPUTE OTHER CHARGES

        for (var i = 0; i < $scope.otherCharges.length; i++) {


            if ($scope.otherCharges[i].IsChecked) {

                var numberOfPersonFinal = 0;
                var sourceID = 6;

                if (i == 0) {
                    numberOfPersonFinal = numberOfPersonsTFIn;
                    sourceID = 4;
                } else if (i == 1) {
                    numberOfPersonFinal = numberOfPersonsTFOut;
                    sourceID = 4;
                } else if (i == 2) {
                    numberOfPersonFinal = numberOfPersonsEnvFee;
                    sourceID = 2;
                }
                else if (i == 3) {
                    numberOfPersonFinal = numberOfPersonsNNFee;
                    sourceID = 5;
                }
                else {
                    numberOfPersonFinal = numberOfPerson;
                }

                var chargeDetail = {
                    //ID: 0,
                    //BookingID: 0,
                    Description: $scope.otherCharges[i].ChargeDescription,
                    Amount: $scope.otherCharges[i].Amount,
                    Quantity: numberOfPersonFinal,
                    SourceID: sourceID,
                    Boat: 0
                };

                $scope.bookingDetails.push(chargeDetail);
            }
        }

        if ($scope.bookingInfo.Header.FOC > 0) {
            var chargeDetail = {
                //ID: 0,
                //BookingID: 0,
                Description: "Free-of-charge",
                Amount: 0,
                Boat: 0,
                SourceID:1,
                Quantity: $scope.bookingInfo.Header.FOC
            };

            $scope.bookingDetails.push(chargeDetail);
        }



        //SENIOR CITIZEN RATE
        if ($scope.bookingInfo.Header.Senior > 0) {
            var adultRate = $scope.getRates($scope.bookingInfo.Header.PickUpArrival,
				$scope.bookingInfo.Header.DropOffArrival, 1, $scope.bookingInfo.Header.Senior, $scope.submissionDate, $scope.bookingInfo.Header.Class);

            adultRate.success(function (response) {

                if (response.Result == "ERROR") {
                    toastr.error(response.Message, "");
                    isValid = false;
                    throw response.Message;
                    return;

                } else {
                    var chargeDetail = {
                        //ID: 0,
                        //BookingID: 0,
                        Description: response.Record.PickUpLocation + " - " + response.Record.DropOffLocation + "  (SENIOR CITIZEN" + "-" + $filter('filter')($scope.classRate, { ID: $scope.bookingInfo.Header.Class })[0].Description + ")",
                        Amount: response.Record.Amount * (1 - 0.20),
                        Quantity: $scope.bookingInfo.Header.Senior,
                        Boat: response.Record.Boat * (1 - 0.20),
                        SourceID:1
                    };

                    $scope.bookingDetails.push(chargeDetail);
                    isValid = true;
                }

            });



            if ($scope.bookingInfo.Header.Routing == 2) {
                var adultRate2 = $scope.getRates($scope.bookingInfo.Header.PickUpDeparture,
					$scope.bookingInfo.Header.DropOffDeparture, 1, $scope.bookingInfo.Header.Senior, $scope.submissionDate, $scope.bookingInfo.Header.Class2);


                adultRate2.success(function (response) {



                    if (response.Result == "ERROR") {
                        toastr.error(response.Message, "");
                        isValid = false;
                        throw response.Message;
                        return;
                    } else {
                        var chargeDetail = {
                            //ID: 0,
                            //BookingID: 0,
                            Description: response.Record.PickUpLocation + " - " + response.Record.DropOffLocation + "  (SENIOR CITIZEN" + "-" + $filter('filter')($scope.classRate, { ID: $scope.bookingInfo.Header.Class2 })[0].Description + ")",
                            Amount: response.Record.Amount * (1 - 0.20),
                            Quantity: $scope.bookingInfo.Header.Senior,
                            Boat: response.Record.Boat * (1 - 0.20),
                            SourceID: 1
                        };

                        $scope.bookingDetails.push(chargeDetail);
                        isValid = true;

                    }

                });


            }

        }



        if ($scope.bookingInfo.Header.Adults > 0) {
            var adultRate = $scope.getRates($scope.bookingInfo.Header.PickUpArrival,
				$scope.bookingInfo.Header.DropOffArrival, 1, $scope.bookingInfo.Header.Adults, $scope.submissionDate, $scope.bookingInfo.Header.Class);



            adultRate.success(function (response) {

                if (response.Result == "ERROR") {
                    toastr.error(response.Message, "");
                    isValid = false;
                    return false;
                } else {
                    var chargeDetail = {
                        //ID: 0,
                        //BookingID: 0,
                        Description: response.Record.PickUpLocation + " - " + response.Record.DropOffLocation + " (ADULT" + "-" + $filter('filter')($scope.classRate, { ID: $scope.bookingInfo.Header.Class })[0].Description + ")",
                        Amount: response.Record.Amount,
                        Quantity: $scope.bookingInfo.Header.Adults,
                        Boat: response.Record.Boat,
                        SourceID: 1
                    };

                    $scope.bookingDetails.push(chargeDetail);
                    isValid = true;
                    return true;
                }

            });



            if ($scope.bookingInfo.Header.Routing == 2) {
                var adultRate2 = $scope.getRates($scope.bookingInfo.Header.PickUpDeparture,
					$scope.bookingInfo.Header.DropOffDeparture, 1, $scope.bookingInfo.Header.Adults, $scope.submissionDate, $scope.bookingInfo.Header.Class2);


                adultRate2.success(function (response) {

                    if (response.Result == "ERROR") {
                        toastr.error(response.Message, "");
                        isValid = false;
                        return;
                    } else {
                        var chargeDetail = {
                            //ID: 0,
                            //BookingID: 0,
                            Description: response.Record.PickUpLocation + " - " + response.Record.DropOffLocation + " (ADULT" + "-" + $filter('filter')($scope.classRate, { ID: $scope.bookingInfo.Header.Class2 })[0].Description + ")",
                            Amount: response.Record.Amount,
                            Quantity: $scope.bookingInfo.Header.Adults,
                            Boat: response.Record.Boat,
                            SourceID: 1
                        };

                        $scope.bookingDetails.push(chargeDetail);
                        isValid = true;

                    }

                });


            }

        }

        if ($scope.bookingInfo.Header.Childrens > 0) {
            var childrensRate = $scope.getRates($scope.bookingInfo.Header.PickUpArrival,
				$scope.bookingInfo.Header.DropOffArrival, 2, numberOfChildren, $scope.submissionDate, $scope.bookingInfo.Header.Class);

            childrensRate.success(function (response) {

                if (response.Result == "ERROR") {
                    toastr.error(response.Message, "");
                    isValid = false;
                    throw response.Message;
                    return;
                } else {
                    var chargeDetail = {
                        //ID: 0,
                        //BookingID: 0,
                        Description: response.Record.PickUpLocation + " - " + response.Record.DropOffLocation + " (CHILDREN" + "-" + $filter('filter')($scope.classRate, { ID: $scope.bookingInfo.Header.Class })[0].Description + ")",
                        Amount: response.Record.Amount,
                        Boat: response.Record.Boat,
                        SourceID:1,
                        Quantity: numberOfChildren//$scope.bookingInfo.Header.Childrens
                    };

                    $scope.bookingDetails.push(chargeDetail);
                    isValid = true;

                }

            });


            if ($scope.bookingInfo.Header.Routing == 2) {
                var childrensRate2 = $scope.getRates($scope.bookingInfo.Header.PickUpDeparture,
					$scope.bookingInfo.Header.DropOffDeparture, 2, numberOfChildren, $scope.submissionDate, $scope.bookingInfo.Header.Class2);

                childrensRate2.success(function (response) {

                    if (response.Result == "ERROR") {
                        toastr.error(response.Message, "");
                        isValid = false;
                        throw response.Message;
                        return;
                    } else {
                        var chargeDetail = {
                            //ID: 0,
                            //BookingID: 0,
                            Description: response.Record.PickUpLocation + " - " + response.Record.DropOffLocation + " (CHILDREN" + "-" + $filter('filter')($scope.classRate, { ID: $scope.bookingInfo.Header.Class2 })[0].Description + ")",
                            Amount: response.Record.Amount,
                            Boat: response.Record.Boat,
                            SourceID:1,
                            Quantity: numberOfChildren//$scope.bookingInfo.Header.Childrens
                        };

                        $scope.bookingDetails.push(chargeDetail);
                        isValid = true;

                    }

                });

            }

        }






    }

    //GET RATE
    $scope.getRates = function (pickUp, dropOff, passengerType, quantity, flightdate, classRate) {

        return $http.post("/Booking/GetRate", { id: 0, pickUp: pickUp, dropOff: dropOff, type: passengerType, agentID: $scope.bookingInfo.Header.CreatedBy, date: flightdate, classRate: classRate })
							  .success(function (response) {

							      logResult("GET SUCCESS", response, response, response, response);




							      //var chargeDetail = {
							      //    //ID: 0,
							      //    //BookingID: 0,
							      //    Description: data.Record.PickUpLocation + " - " + data.Record.DropOffLocation,
							      //    Amount: data.Record.Amount,
							      //    Quantity: quantity
							      //};

							      //  $scope.bookingDetails.push(chargeDetail);
							      return response;




							  }).error(function (data, status, headers, config) {
							      logResult("GET ERROR", data, status, headers, config);


							  });

    };




    //DELETE ITEM
    $scope.deleteItem = function (index) {

        // alert(1);
        $timeout(function () {
            $scope.bookingDetails.splice(index, 1);
        });


        

    }

    $scope.clearBooking = function () {
        $scope.grandTotal = 0;
        $scope.bookingDetails = [];
    };
    //CREATE BOOKING
    $scope.createBooking = function (status, form) {

        checkSession();

        if (!form.validate()) {
            return;
        }

        if (status == 0) {
            if (!confirm("Are you sure you want to submit this booking? Once submitted, booking will be for confirmation of ISE and can't be edited anymore "))
                return false;
        }






        $scope.bookingInfo.Header.Status = status;
        $scope.bookingInfo.Details = $scope.bookingDetails;
        $scope.bookingInfo.OtherCharges = $scope.otherCharges;
        $scope.bookingInfo.OCs = $scope.OCs;

        var data = JSON.stringify($scope.bookingInfo);
        var config = {

            contentType: "application/json; charset=utf-8",
            dataType: "json",

        };

        $http.post("/Booking/Book", data, config)
							.success(function (data, status, headers, config) {

							    logResult("GET SUCCESS", data, status, headers, config);

							    if (data.Result == "ERROR") {
							        toastr.error(data.Message, "");
							        return;
							    }

							    toastr.success("Saved!", "");

							    window.location.href = "/Booking/View/" + data.Record.Header.ID;


							}).error(function (data, status, headers, config) {
							    logResult("GET ERROR", data, status, headers, config);


							});

    };


    //GENERATE QUOTATION
    $scope.generateQuotation = function () {

        checkSession();

        $scope.bookingInfo.Details = $scope.bookingDetails;
        $scope.bookingInfo.OtherCharges = $scope.otherCharges;
        var data = JSON.stringify($scope.bookingInfo);
        var config = {

            contentType: "application/json; charset=utf-8",
            dataType: "json",

        };


        $http.post("/Booking/Quotation", data, config)
						   .success(function (data, status, headers, config) {

						       logResult("GET SUCCESS", data, status, headers, config);

						       if (data.Result == "ERROR") {
						           toastr.error(data.Message, "");
						           return;
						       }

						       // window.location.href = "/Booking/QuotationPrint/" + data.File;

						       //toastr.success("Saved!", "");

						       var w = 1024;
						       var h = 768;




						       var url = '/Reports/Quotation.aspx?id=' + data.File;
						       var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
						       var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

						       width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
						       height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
						       var title = "";
						       var left = ((width / 2) - (w / 2)) + dualScreenLeft;
						       var top = ((height / 2) - (h / 2)) + dualScreenTop;
						       newWindow = window.open(url, title, 'scrollbars=no,resizable=no,fullscreen=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);


						       if (window.focus) {




						       }



						   }).error(function (data, status, headers, config) {
						       logResult("GET ERROR", data, status, headers, config);


						   });




    };

    ////////////////////////End of Create Booking///////////////////

    //CONFIRM BOOKING
    $scope.confirmBooking = function () {

        checkSession()

        $.blockUI();

        var config = {

            contentType: "application/json; charset=utf-8",
            dataType: "json",

        };

        var data = JSON.stringify({ bookID: bookingID });

        $http.post("/Booking/Confirm/" + bookingID, data, config)
						 .success(function (data, status, headers, config) {

						     logResult("GET SUCCESS", data, status, headers, config);

						     $.unblockUI();

						     if (data.Result == "ERROR") {
						         toastr.error(data.Message, "");
						         return;
						     }

						     toastr.success("Booking has been confirmed!", "");


						     setTimeout(function () {

						         window.location.reload(true);

						     }, 2000)




						 }).error(function (data, status, headers, config) {
						     logResult("GET ERROR", data, status, headers, config);


						 });

    };


    //FINALIZED BOOKING
    $scope.finalizedBooking = function (frm) {

        checkSession()

        if (!(frm.validate())) {
            return;
        }


        $.blockUI();

        var config = {

            contentType: "application/json; charset=utf-8",
            dataType: "json",

        };

        var data = JSON.stringify({ bookID: bookingID });

        $http.post("/Booking/Finalized/" + bookingID, data, config)
						 .success(function (data, status, headers, config) {

						     logResult("GET SUCCESS", data, status, headers, config);

						     $.unblockUI();

						     if (data.Result == "ERROR") {
						         toastr.error(data.Message, "");
						         return;
						     }

						     toastr.success("Booking has been finalized!", "");


						     setTimeout(function () {

						         window.location.href = "/";
						     }, 2000)




						 }).error(function (data, status, headers, config) {
						     logResult("GET ERROR", data, status, headers, config);


						 });

    }


    //CANCEL BOOKING
    $scope.cancelBooking = function () {


        checkSession()

        $.blockUI();

        var config = {

            contentType: "application/json; charset=utf-8",
            dataType: "json",

        };

        var data = JSON.stringify({ bookID: bookingID });

        $http.post("/Booking/Cancel/" + bookingID, data, config)
						 .success(function (data, status, headers, config) {

						     logResult("GET SUCCESS", data, status, headers, config);

						     $.unblockUI();

						     if (data.Result == "ERROR") {
						         toastr.error(data.Message, "");
						         return;
						     }

						     toastr.success("Booking has been cancelled!", "");


						     setTimeout(function () {

						         window.location.href = "/";
						     }, 2000)




						 }).error(function (data, status, headers, config) {
						     logResult("GET ERROR", data, status, headers, config);


						 });


    };


    //OVERRIDE BOOKING
    $scope.overrideBooking = function () {

        $scope.bookingInfo.Header.Status = 10;
        $scope.Editing = false;

    }



    $scope.loadAgencies = function (query) {
        return $filter('filter')($scope.agentsList, query);
    }


    //GET LIST OF AGENTS
    $scope.getAgents = function () {


        var config = {

            contentType: "application/json; charset=utf-8",
            dataType: "json",

        };

        var agentData = JSON.stringify({ activeOnly: false, agentType: 0, unique: true });

        console.log(agentData)

        $http.post("/Agents/Get", agentData, config)
						   .success(function (data, status, headers, config) {

						       logResult("GET SUCCESS", data, status, headers, config);

						       if (data.Result == "ERROR") {
						           toastr.error(data.Message, "");
						           return;
						       }

						       $scope.agentsList = data;

						       //$scope.agentsList.unshift({
						       //    ContactNumber: "NO CONTACT",
						       //    CreditLimit: 500000,
						       //    DateCreated: "2016-09-07 04:00:28",
						       //    DateLastUpdated: "2016-09-07 05:35:57",
						       //    Email: "",
						       //    FirstName: "---WALK IN---",
						       //    Company: "---WALK IN---",
						       //    ID: 0,
						       //    IsActive: true,
						       //    LastName: "",
						       //    MonthToDateAmount: 0,
						       //    RemainingCredit: 500000,
						       //    Type: 1,
						       //    YearToDateAmount: 0,
						       //    PaymentTypeID: 2
						       //});


						       $scope.activeAgents = $filter('filter')($scope.agentsList, { IsActive: true });
						       //debugger;

						       if (bookingID == 0 || bookingID == null) {
						           $scope.currentTravelAgency = $filter('filter')($scope.activeAgents, { ID: $scope.bookingInfo.Header.CreatedBy }, true)[0];

						           if ($scope.currentTravelAgency != null) {
						               $scope.currentTravelAgency.ticked = true;
						           }

						          
						       }


						       // console.log($scope.activeAgents)


						   }).error(function (data, status, headers, config) {
						       logResult("GET ERROR", data, status, headers, config);


						   });

    }


    $scope.changePaymentType = function (agentID) {

        var agent = $filter("filter")($scope.agentsList, { ID: agentID })[0];

        $scope.bookingInfo.Header.PaymentTypeID = agent.PaymentTypeID;


    };

    //ADD ITENERARY
    $scope.addItenerary = function () {


        var chargeDetail = {
            //ID: 0,
            //BookingID: 0,
            Description: "",
            Amount: "",
            Quantity: 0,
            IsItenerary: true,
            Boat: 0,
            SourceID:1
        };


        jQuery.validator.addClassRules('txtChargingName', {
            required: true

        });
        jQuery.validator.addClassRules('txtChargingQuantity', {
            required: true,
            digits: true,
            min: 1

        });
        jQuery.validator.addClassRules('txtChargingPrice', {
            required: true,
            number: true

        });


        console.log(chargeDetail);

        $scope.bookingDetails.push(chargeDetail);



    }


    //ADD MANIFESTO
    $scope.addManifesto = function () {

        var adults = parseFloat($scope.bookingInfo.Header.Adults || 0) + parseFloat($scope.bookingInfo.Header.Senior || 0);
        var kids = parseFloat($scope.bookingInfo.Header.ChildrensBelowSix || 0) + parseFloat($scope.bookingInfo.Header.Childrens || 0);
        var infants = parseFloat($scope.bookingInfo.Header.Infants || 0);



        var newManifesto = {
            ResortHotel: $scope.bookingInfo.Header.DropOffLocation1,
            //Date: moment($scope.bookingInfo.Header.FlightDate1, 'YYYY-MM-DD HH:mm'),
            Date: $scope.bookingInfo.Header.FlightDate1,
            Guests: $scope.bookingInfo.Header.GroupName,
            Pickup: $scope.bookingInfo.Header.PickUpArrival,
            DropOff: $scope.bookingInfo.Header.DropOffArrival,
            Adults: adults,
            Kids: kids,
            Type: null,
            FlightNo: $scope.bookingInfo.Header.FlightNo1,
            Infants: infants
        }

        $scope.bookingManifesto.push(newManifesto);

        jQuery.validator.addClassRules('txtManType', {
            required: true

        });
        jQuery.validator.addClassRules('txtManifestoFlightNo', {
            required: true

        });

        jQuery.validator.addClassRules('txtMFResortHotel', {
            required: true

        });
        jQuery.validator.addClassRules('txtMFGuests', {
            required: true

        });
        jQuery.validator.addClassRules('txtMFRemarks', {
            required: true

        });

        jQuery.validator.addClassRules('txtMFArrivalDate', {
            required: true

        });


        jQuery.validator.addClassRules('txtMFPickup', {
            required: true
        });
        jQuery.validator.addClassRules('txtMFDropOff', {
            required: true
        });

        //jQuery.validator.addClassRules('txtMFAdults', {
        //    required_group: true
        //});
        //jQuery.validator.addClassRules('txtMFKids', {
        //    required_group: true
        //});
        //jQuery.validator.addClassRules('txtMFInfants', {
        //    required_group: true
        //});

        //console.log(newManifesto);




    }

    //ADD MANIFESTO ARRIVAL
    $scope.addManifestoArrival = function () {

        var adults = parseFloat($scope.bookingInfo.Header.Adults || 0) + parseFloat($scope.bookingInfo.Header.Senior || 0);
        var kids = parseFloat($scope.bookingInfo.Header.ChildrensBelowSix || 0) + parseFloat($scope.bookingInfo.Header.Childrens || 0);
        var infants = parseFloat($scope.bookingInfo.Header.Infants || 0);






        var newManifesto = {
            ResortHotel: $scope.bookingInfo.Header.DropOffLocation1,
            //Date: moment($scope.bookingInfo.Header.FlightDate1, 'YYYY-MM-DD HH:mm'),
            Date: $scope.bookingInfo.Header.FlightDate1,
            TourGuides: $scope.bookingInfo.Header.TourGuides,
            Escorts: $scope.bookingInfo.Header.Escorts,
            FOC: $scope.bookingInfo.Header.FOC,
            Guests: $scope.bookingInfo.Header.GroupName,
            Pickup: $scope.bookingInfo.Header.PickUpArrival,
            DropOff: $scope.bookingInfo.Header.DropOffArrival,
            Adults: adults,
            Kids: kids,
            Type: 1,
            FlightNo: $scope.bookingInfo.Header.FlightNo1,
            Infants: infants,
            Remarks: $scope.bookingInfo.Header.Remarks
        }



        $scope.bookingManifesto.push(newManifesto);

        jQuery.validator.addClassRules('txtManType', {
            required: true

        });
        jQuery.validator.addClassRules('txtManifestoFlightNo', {
            required: true

        });

        jQuery.validator.addClassRules('txtMFResortHotel', {
            required: true

        });
        jQuery.validator.addClassRules('txtMFGuests', {
            required: true

        });
        jQuery.validator.addClassRules('txtMFRemarks', {
            required: true

        });

        jQuery.validator.addClassRules('txtMFArrivalDate', {
            required: true

        });


        jQuery.validator.addClassRules('txtMFPickup', {
            required: true
        });
        jQuery.validator.addClassRules('txtMFDropOff', {
            required: true
        });

        //jQuery.validator.addClassRules('txtMFAdults', {
        //    required_group: true
        //});
        //jQuery.validator.addClassRules('txtMFKids', {
        //    required_group: true
        //});
        //jQuery.validator.addClassRules('txtMFInfants', {
        //    required_group: true
        //});

        //console.log(newManifesto);




    }

    //ADD MANIFESTO ARRIVAL
    $scope.addManifestoDeparture = function () {

        var adults = parseFloat($scope.bookingInfo.Header.Adults || 0) + parseFloat($scope.bookingInfo.Header.Senior || 0);
        var kids = parseFloat($scope.bookingInfo.Header.ChildrensBelowSix || 0) + parseFloat($scope.bookingInfo.Header.Childrens || 0);
        var infants = parseFloat($scope.bookingInfo.Header.Infants || 0);



        var newManifesto = {
            ResortHotel: $scope.bookingInfo.Header.DropOffLocation2,
            //Date: moment($scope.bookingInfo.Header.FlightDate2, 'YYYY-MM-DD HH:mm'),
            Date: $scope.bookingInfo.Header.FlightDate2,
            TourGuides: $scope.bookingInfo.Header.TourGuides,
            Escorts: $scope.bookingInfo.Header.Escorts,
            FOC: $scope.bookingInfo.Header.FOC,
            Guests: $scope.bookingInfo.Header.GroupName,
            Pickup: $scope.bookingInfo.Header.PickUpDeparture,
            DropOff: $scope.bookingInfo.Header.DropOffDeparture,
            Adults: adults,
            Kids: kids,
            Type: 2,
            FlightNo: $scope.bookingInfo.Header.FlightNo1,
            Infants: infants,
            Remarks: $scope.bookingInfo.Header.Remarks
        }


        jQuery.validator.addClassRules('txtManType', {
            required: true

        });
        jQuery.validator.addClassRules('txtManifestoFlightNo', {
            required: true

        });

        jQuery.validator.addClassRules('txtMFResortHotel', {
            required: true

        });
        jQuery.validator.addClassRules('txtMFGuests', {
            required: true

        });
        jQuery.validator.addClassRules('txtMFRemarks', {
            required: true

        });

        jQuery.validator.addClassRules('txtMFArrivalDate', {
            required: true

        });


        jQuery.validator.addClassRules('txtMFPickup', {
            required: true
        });
        jQuery.validator.addClassRules('txtMFDropOff', {
            required: true
        });

        //jQuery.validator.addClassRules('txtMFAdults', {
        //    required_group: true
        //});
        //jQuery.validator.addClassRules('txtMFKids', {
        //    required_group: true
        //});
        //jQuery.validator.addClassRules('txtMFInfants', {
        //    required_group: true
        //});

        //console.log(newManifesto);

        $scope.bookingManifesto.push(newManifesto);


    }


    //SAVE MANIFESTO
    $scope.saveManifesto = function (form) {

        //FORM VALIDATION
        // NOT WORKING STRANGE BEHAVIOR
        if (!(form.validate())) {
            return;
        }


        if (!checkNumberOfPax()) return;




        $.blockUI();

        $scope.bookingInfo.Manifesto = $scope.bookingManifesto;

        var data = JSON.stringify($scope.bookingInfo);
        var config = {

            contentType: "application/json; charset=utf-8",
            dataType: "json",

        };

        $http.post("/Booking/SaveManifesto", data, config)
							.success(function (data, status, headers, config) {

							    $.unblockUI();
							    logResult("GET SUCCESS", data, status, headers, config);

							    if (data.Result == "ERROR") {
							        toastr.error(data.Message, "");
							        return;
							    }

							    toastr.success("Saved!", "");

							    window.location.reload(true);


							}).error(function (data, status, headers, config) {
							    $.unblockUI();
							    logResult("GET ERROR", data, status, headers, config);


							});


    }


    //DELETE MANIFESTO
    $scope.deleteManifesto = function (manifesto) {

        var idx = $scope.bookingInfo.Manifesto.indexOf(manifesto);

        if (idx != -1) {
            $scope.bookingInfo.Manifesto.splice(idx, 1);
        }


    }



    //CHECK PAX

    function checkNumberOfPax() {

        var arrivals = $filter('filter')($scope.bookingInfo.Manifesto, { Type: 1 });
        var departures = $filter('filter')($scope.bookingInfo.Manifesto, { Type: 2 });

        var adults = parseFloat($scope.bookingInfo.Header.Adults || 0) + parseFloat($scope.bookingInfo.Header.Senior || 0);
        var kids = parseFloat($scope.bookingInfo.Header.ChildrensBelowSix || 0) + parseFloat($scope.bookingInfo.Header.Childrens || 0);
        var infants = parseFloat($scope.bookingInfo.Header.Infants || 0);
        var escorts = parseFloat($scope.bookingInfo.Header.Escorts || 0);
        var tourguides = parseFloat($scope.bookingInfo.Header.TourGuides || 0);
        var foc = parseFloat($scope.bookingInfo.Header.FOC || 0);

        var arrivalAdults = $filter('sumByKey')(arrivals, "Adults");
        var arrivalKids = $filter('sumByKey')(arrivals, "Kids");
        var arrivalInfants = $filter('sumByKey')(arrivals, "Infants");
        var arrivalEscorts = $filter('sumByKey')(arrivals, "Escorts");
        var arrivalTourGuides = $filter('sumByKey')(arrivals, "TourGuides");
        var arrivalFOC = $filter('sumByKey')(arrivals, "FOC");


        var departureAdults = $filter('sumByKey')(departures, "Adults");
        var departureKids = $filter('sumByKey')(departures, "Kids");
        var departureInfants = $filter('sumByKey')(departures, "Infants");
        var departureEscorts = $filter('sumByKey')(departures, "Escorts");
        var departureTourGuides = $filter('sumByKey')(departures, "TourGuides");
        var departureFOC = $filter('sumByKey')(departures, "FOC");

        if (arrivals.length != 0) {
            if (arrivalAdults != adults) {
                toastr.error("Number of arriving adults does not match!");
                return false;
            }

            if (arrivalKids != kids) {
                toastr.error("Number of arriving kids does not match!");
                return false;
            }

            if (arrivalInfants != infants) {
                toastr.error("Number of arriving infants does not match!");
                return false;
            }

            if (arrivalEscorts != escorts) {
                toastr.error("Number of arriving escorts does not match!");
                return false;
            }

            if (arrivalTourGuides != tourguides) {
                toastr.error("Number of arriving tour guides does not match!");
                return false;
            }

            if (arrivalFOC != foc) {
                toastr.error("Number of arriving FOC does not match!");
                return false;
            }
        }

        console.log(departures);


        if (departures.length != 0) {
            if (departureAdults != adults) {
                toastr.error("Number of departing adults does not match!");
                return false;
            }

            if (departureKids != kids) {
                toastr.error("Number of departing kids does not match!");
                return false;
            }

            if (departureInfants != infants) {
                toastr.error("Number of departing infants does not match!");
                return false;
            }

            if (departureEscorts != escorts) {
                toastr.error("Number of departing escorts does not match!");
                return false;
            }

            if (departureTourGuides != tourguides) {
                toastr.error("Number of departing tour guides does not match!");
                return false;
            }

            if (departureFOC != foc) {
                toastr.error("Number of departing FOC does not match!");
                return false;
            }

        }



        return true;
    }


}]);


