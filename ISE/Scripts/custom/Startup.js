﻿var App = angular.module('isebookingapp', ['angularFileUpload',
    'ngAnimate',
    'ui.bootstrap',
    'ngMessages',
    'ui.bootstrap.datetimepicker',
    "ngValidate",
    "moment-picker",
    "angularUtils.directives.dirPagination",
    "angularjs-dropdown-multiselect",
    "ngTagsInput",
    "isteven-multi-select",
    "ui.mask",
    'ui.grid', 'ui.grid.grouping', 'ui.grid.selection', 'ui.grid.resizeColumns', 'ui.grid.moveColumns', 'ui.grid.pagination', 'ui.grid.edit', 'ui.grid.cellNav', 'ui.grid.pinning', 'ui.grid.autoResize', 'ui.grid.validate'
    , 'daterangepicker',
    'angular-encryption'
   ]);


App.filter('sumByKey', function () {

    return function (data, key) {
        if (typeof(data) === 'undefined' || typeof (key) === 'undefined') {
            return 0;
        }


        var sum = 0;

        angular.forEach(data, function (obj, objKey) {

            sum += parseFloat(obj[key] || 0);

        });

        return sum;
    }

});


App.filter('padNumber', function () {
    return function (input, size) {
        var zero = (size ? size : 4) - input.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + input;
    }
});

App.directive('fileuploadbooking', function () {

    var controller = ['$scope', '$http', 'jsonFilter', 'FileUploader', '$uibModal', function ($scope, $http, jsonFilter, FileUploader, $uibModalUploadInstance) {



        var uploader = $scope.uploader = new FileUploader({
            url: '/api/fileupload/uploadfile/0',
            autoUpload: true,
            // removeAfterUpload: true

        });

        // FILTERS
        uploader.filters.push({
            name: 'customFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                return this.queue.length < 10;
            }
        });


        // CALLBACKS
        uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function (fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function (addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function (item) {
            item.formData.push({ DetailID: $scope.detailID, HeaderID: $scope.headerID, isHeader: $scope.isHeader });
            console.info('onBeforeUploadItem', item);
        };
        uploader.onProgressItem = function (fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function (progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function (fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };
        uploader.onErrorItem = function (fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);



        };
        uploader.onCancelItem = function (fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function (fileItem, response, status, headers) {

            if (response.Result !== "OK") {
                toastr.error(response.Message, "");
                return;
            }

              if ($scope.bookingInfo.Attachments === null) {
                    $scope.bookingInfo.Attachments = [];
                }

           
             // console.log(response.Record);
                $scope.bookingInfo.Attachments.push(response.Record);



        };
        uploader.onCompleteAll = function () {
            console.info('onCompleteAll');
        };

        console.info('uploader', uploader);

        //var controller = $scope.controller = {
        //    isImage: function (item) {
        //        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        //        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        //    }
        //};

    }];

    return {
        scope: true,
        restrict: 'AE',
        controller: controller,
        controllerAs: 'uploader',
        transclude: true,
         bindToController: true,
        templateUrl: '/Templates/FileUpload.html'
    }
});


App.config(function ($validatorProvider) {

    $validatorProvider.setDefaults({
        errorElement: 'span',
        errorClass: 'has-error'

    });

    $validatorProvider.setDefaultMessages({
        required: "This field is required.",
        remote: "Please fix this field.",
        email: "Please enter a valid email address.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: $validatorProvider.format("Please enter no more than {0} characters."),
        minlength: $validatorProvider.format("Please enter at least {0} characters."),
        rangelength: $validatorProvider.format("Please enter a value between {0} and {1} characters long."),
        range: $validatorProvider.format("Please enter a value between {0} and {1}."),
        max: $validatorProvider.format("Please enter a value less than or equal to {0}."),
        min: $validatorProvider.format("Please enter a value greater than or equal to {0}.")
    });


});

jQuery.validator.addMethod('required_group', function (val, el) {
    var $module = $(el).parent('div').parent('div');
    return $module.find('.required_group:filled').length;
}, 'Please fill out at least one of these fields');

