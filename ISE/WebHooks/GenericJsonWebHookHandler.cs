﻿using Microsoft.AspNet.WebHooks;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ISE.WebHooks
{
    public class GenericJsonWebHookHandler : WebHookHandler
    {
        public GenericJsonWebHookHandler()
        {
           // this.Receiver = "ISEBooking";
            this.Receiver = GenericJsonWebHookReceiver.ReceiverName;

        }
        public override Task ExecuteAsync(string receiver, WebHookHandlerContext context)
        {
           // if (Common.IsBitBucketReceiver(receiver))
            //{
                var dataJObject = context.GetDataOrDefault<JObject>();
                var action = context.Actions.First();

                string ordernumber = (dataJObject.GetValue("arg") ?? "0").ToString();
                string orderstatus = (dataJObject.GetValue("action") ?? "woocommerce_order_status_completed").ToString();

                if (orderstatus == "woocommerce_order_status_completed" && ordernumber != "0")
                {
                    WooCommerce.Integration integration = new WooCommerce.Integration();
                    integration.CreateOrderFromWebHook(Convert.ToInt64(ordernumber));
                }

                //var processActivities = new ProcessActivities(dataJObject, action);
                //processActivities.Process();
           // }
            return Task.FromResult(true);
        }
    }
}