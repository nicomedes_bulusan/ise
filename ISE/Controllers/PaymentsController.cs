﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISE.DAL;
using Newtonsoft.Json;
using ISE.Models;

namespace ISE.Controllers
{
    public class PaymentsController : Controller
    {
        // GET: Payments
        public ActionResult Index()
        {
            ViewBag.ActiveTab = "AccountReceivables";
            return View();
        }

        public ActionResult Payment(string id="")
        {
            ViewBag.ActiveTab = "AccountReceivables";
            Boolean isNewEntry = true;

            isNewEntry = !String.IsNullOrEmpty(id);
            ViewBag.PaymentNo = id;
            ViewBag.isNewEntry = isNewEntry;
            return View();
        }

        public ActionResult ViewPayment(string id = "")
        {
            ViewBag.ActiveTab = "AccountReceivables";
            ViewBag.PaymentNo = id;
            return View();
        }


        [HttpPost]
        public ContentResult GetPayment(int id)
        {
            try
            {

                PaymentsModel payRepo = new PaymentsModel();

                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                var result = payRepo.GetPayment(auth, id);

                return new ContentResult
                {
                    // Content=json,
                    Content = JsonConvert.SerializeObject(new { Result = "OK", Record = result }, new JsonSerializerSettings() { DateFormatString = "MM/dd/yyyy HH:mm" }),
                    ContentType = "application/json"
                };
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
       
                return new ContentResult
                {
                    Content = JsonConvert.SerializeObject(new { Result = "ERROR", Message = ex.Message }),
                    ContentType = "application/json"
                };
            }
        }

        [HttpGet]
        public JsonResult GetAllAccountingEntries()
        {
            try {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                AccountingTermsModel acctRepo = new AccountingTermsModel();

                var result = acctRepo.GetAll();

                return Json(new { Result = "OK", Records = result }, JsonRequestBehavior.AllowGet);

            
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            
        }

        [HttpGet]
        public JsonResult GetAllModeOfPayments()
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                ModeOfPaymentsModel payRepo = new ModeOfPaymentsModel();

                var result = payRepo.GetAll();

                return Json(new { Result = "OK", Records = result }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ContentResult GetAllUnpaidBookings(string cvNumber,string groupName,int travelAgencyID,string search=null, int startIndex = 1, int pageSize = 1000)
        {
            try {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Session has expired!");
                if (auth.Type != 3) throw new Exception("ERROR! Unauthorized");

                PaymentsModel bookRepo = new PaymentsModel();
                var result = bookRepo.GetUnpaidBookings(cvNumber, groupName,travelAgencyID, search, startIndex, pageSize);

                return new ContentResult
                {
                    // Content=json,
                    Content = JsonConvert.SerializeObject(new { Result = "OK", Records = result }, new JsonSerializerSettings() { DateFormatString = "MM/dd/yyyy HH:mm" }),
                    ContentType = "application/json"
                };
            
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
                return new ContentResult
                {
                    Content = JsonConvert.SerializeObject(new { Result = "ERROR", Message = ex.Message }),
                    ContentType = "application/json"
                };
            }
        }

        [HttpPost]
        public ContentResult GetAllUnpaidBillings(int travelAgencyID, string search = null, int startIndex = 1, int pageSize = 10)
        {

            try
            {


                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Session has expired!");
                if (auth.Type != 3) throw new Exception("ERROR! Unauthorized");

                BookingsPaged bookRepo = new BookingsPaged();
                var result = bookRepo.GetUnpaidBillingStatements(travelAgencyID, search, startIndex, pageSize);

                return new ContentResult
                {
                    // Content=json,
                    Content = JsonConvert.SerializeObject(new { Result = "OK", Records = result }, new JsonSerializerSettings() { DateFormatString = "MM/dd/yyyy HH:mm" }),
                    ContentType = "application/json"
                };

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
                return new ContentResult
                {
                    Content = JsonConvert.SerializeObject(new { Result = "ERROR", Message = ex.Message }),
                    ContentType = "application/json"
                };
            }
        }


        [HttpPost]
        public ContentResult GetAllUnpaidBillingByID(int billingID, string search = null, int startIndex = 1, int pageSize = 10)
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Session has expired!");
                if (auth.Type != 3) throw new Exception("ERROR! Unauthorized");

                BookingsPaged bookRepo = new BookingsPaged();
                var result = bookRepo.GetUpaidByBillingID(billingID, search, startIndex, pageSize);

                return new ContentResult
                {
                    // Content=json,
                    Content = JsonConvert.SerializeObject(new { Result = "OK", Records = result }, new JsonSerializerSettings() { DateFormatString = "MM/dd/yyyy HH:mm" }),
                    ContentType = "application/json"
                };

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
                return new ContentResult
                {
                    Content = JsonConvert.SerializeObject(new { Result = "ERROR", Message = ex.Message }),
                    ContentType = "application/json"
                };
            }
        }


        [HttpPost]
        public JsonResult CheckReceiptIfUsed(PaymentReferenceType paymentRefType, string receiptNo, string businessLocation)
        {

            PaymentsModel paymentRepo = new PaymentsModel();

            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                return Json(new { Result = "OK", Record = paymentRepo.CheckReceiptIfUsed(paymentRefType, receiptNo, businessLocation) },JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

           


        }

        [HttpPost]
        public ContentResult GetPaymentsPaged( string search = null, int startIndex = 1, int pageSize = 10)
        {
            try
            {

                PaymentsModel payRepo = new PaymentsModel();

                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

              

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                var result = payRepo.GetAllPayments(auth, search, startIndex, pageSize);
                //return Json(new { Result = "OK", Records=result }, JsonRequestBehavior.AllowGet);



                //  var json = ServiceStack.Text.JsonSerializer.SerializeToString(result);

                return new ContentResult
                {
                    // Content=json,
                    Content = JsonConvert.SerializeObject(new { Result = "OK", Records = result }, new JsonSerializerSettings() { DateFormatString = "MM/dd/yyyy HH:mm" }),
                    ContentType = "application/json"
                };
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
                return new ContentResult
                {
                    Content = JsonConvert.SerializeObject(new { Result = "ERROR", Message = ex.Message }),
                    ContentType = "application/json"
                };
            }
        }


        [HttpPost]
        public JsonResult Submit(PaymentsModel payment)
        {
            try {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                PaymentsModel paymentRepo = new PaymentsModel();
                paymentRepo.SubmitPayment(auth, payment);

                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);
            
            
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ConfirmPayment(PaymentsModel payRef)
        {
            try {

                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                PaymentsModel paymentRepo = new PaymentsModel();
                paymentRepo.VerifyPayment(auth, payRef);

                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);
            
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CancelPayment(int paymentID)
        {
            try
            {

                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                PaymentsModel paymentRepo = new PaymentsModel();
                paymentRepo.CancelPayment(auth, paymentID);

                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateChanges(PaymentsModel payment)
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                PaymentsModel paymentRepo = new PaymentsModel();
                paymentRepo.UpdateChanges(auth, payment);

                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public JsonResult ReverseVerifiedPayments()
        {
            try {

                if (Request.UserHostAddress != "::1")
                     throw new HttpException((int)System.Net.HttpStatusCode.Forbidden, "Unauthorized!");

                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                PaymentsModel paymentRepo = new PaymentsModel();


               

                paymentRepo.ReverseAllVerifiedPayments(auth);

                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);
            
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }



    }
}