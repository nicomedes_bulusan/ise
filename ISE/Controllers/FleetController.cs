﻿using ISE.DAL;
using ISE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ISE.Controllers
{
    public class FleetController : Controller
    {
        // GET: Fleet
        public ActionResult Index()
        {
            ViewBag.ActiveTab = "Fleet";
            return View();
        }




        [HttpGet]
        public JsonResult GetFleet()
        {
            try {

                var auth = AuthorizationGateway.GetAuthorizedInfo(null);
                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                FleetModel fleetRepo = new FleetModel();
                return Json(new { Result = "OK", Records = fleetRepo.GetFleet() }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult CreateOrUpdate(FleetModel fleet)
        {
            try {

                var auth = AuthorizationGateway.GetAuthorizedInfo(null);
                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                FleetModel fleetRepo = new FleetModel();
                return Json(new { Result = "OK", Record = fleetRepo.CreateOrUpdate(fleet,auth) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}