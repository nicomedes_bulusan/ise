﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISE.DAL;
using ISE.Models;
using Newtonsoft.Json;
using ISE.ActionFilters;

namespace ISE.Controllers
{
    //[CompressFilter]
    [OutputCache(NoStore = true, Duration = 0)]
    public class BookingController : Controller
    {
        // GET: Booking
        public ActionResult Index()
        {
            ViewBag.ActiveTab = "Bookings";
            return View();
        }

        public ActionResult Create()
        {
            ViewBag.ActiveTab = "Bookings";
            ViewBag.BookID = 0;
            return View();
        }

        public ActionResult Dashboard()
        {
            ViewBag.ActiveTab = "Dashboard";
            return View();
        }

        public ActionResult View(int id)
        {
            ViewBag.ActiveTab = "Bookings";
            ViewBag.BookID = id;
            return View();
        }

        public ActionResult QuotationPrint(string id)
        {
            ViewBag.QuoteID = id;
            return View();
        }


        public ActionResult Reports()
        {
            ViewBag.ActiveTab = "Reports";
            return View();
        }


        [HttpPost]
        public JsonResult Quotation(Bookings book)
        {

            Guid g = Guid.NewGuid();
            string filename = g.ToString();
            ResortsAndHotelsModel resortRepo = new ResortsAndHotelsModel();

            var resort1 = resortRepo.Get(book.Header.DropOffLocation1 ?? 0);
            var resort2 = resortRepo.Get(book.Header.DropOffLocation2 ?? 0);
            book.Header.ResortHotel1 = resort1 == null ? "" : resort1.Name;
            book.Header.ResortHotel2 = resort2 == null ? "" : resort2.Name;

            var bookResult = JsonConvert.SerializeObject(book);

            string path = System.AppDomain.CurrentDomain.BaseDirectory;




            System.IO.File.AppendAllText(path + "\\TempData\\" + filename + ".json", bookResult);






            return Json(new { Result = "OK", File = filename }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetInitialOtherCharges()
        {
            BookingOtherChargesIndicatorModel otherCharges = new BookingOtherChargesIndicatorModel();

            var result = otherCharges.GetOtherChargesInitialIndicator();

            return Json(result, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult GetRate(int id, int pickUp, int dropOff, int type, DateTime date, int agentID = 0, ClassRate classRate = ClassRate.REGULAR)
        {


            try
            {


                RatesMatrices rates = new RatesMatrices();
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth.Type == 2 || auth.Type==3)
                {
                    if (agentID == 0) agentID = auth.ID;

                    AgentsModel agentRepo = new AgentsModel();
                    auth = agentRepo.FindAgentByID(agentID);
                }

                var result = rates.GetRate(pickUp, dropOff, type, auth, date, classRate);

                if (result == null) throw new Exception("Invalid destinations!");

                return Json(new { Result = "OK", Record = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public JsonResult Book(Bookings book)
        {
            Bookings bookRepository = new Bookings();

            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                if (auth.Type == 2)
                {
                    AgentsModel agentRepo = new AgentsModel();

                    if (book.Header.CreatedBy != 0)
                    {

                        auth = agentRepo.FindAgentByID(book.Header.CreatedBy);
                    }


                }

                var result = bookRepository.CreateBooking(book, auth);

                return Json(new { Result = "OK", Record = result }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }



        [HttpPost]
        public JsonResult GetBookings()
        {
            Bookings books = new Bookings();
            var auth = AuthorizationGateway.GetAuthorizedInfo(null);

            return Json(books.GetBookings(auth), JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ContentResult GetBookingsPaged(DateTime fromDate, DateTime toDate, List<int> statuses = null, string search = null, int startIndex = 1, int pageSize = 10)
        {
            try
            {
         
                BookingsPaged books = new BookingsPaged();

                var auth = AuthorizationGateway.GetAuthorizedInfo(null);
                var result = books.GetList(auth, fromDate, toDate, statuses, search, startIndex, pageSize);
                //return Json(new { Result = "OK", Records=result }, JsonRequestBehavior.AllowGet);
            
                //  var json = ServiceStack.Text.JsonSerializer.SerializeToString(result);

                return new ContentResult
                {
                    // Content=json,
                    Content = JsonConvert.SerializeObject(new { Result = "OK", Records = result }, new JsonSerializerSettings() { DateFormatString = "MM/dd/yyyy HH:mm" }),
                    ContentType = "application/json"
                };
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
                return new ContentResult
                {
                    Content = JsonConvert.SerializeObject(new { Result = "ERROR", Message = ex.Message }),
                    ContentType = "application/json"
                };
            }
        }


        [HttpPost]
        public ContentResult GetBooking(int id)
        {
            Bookings books = new Bookings();
            var auth = AuthorizationGateway.GetAuthorizedInfo(null);

            // return Json(books.GetBooking(id), JsonRequestBehavior.AllowGet);
            return new ContentResult
            {
                Content = JsonConvert.SerializeObject(books.GetBooking(id), new JsonSerializerSettings() { DateFormatString = "MM/dd/yyyy HH:mm" }),
                ContentType = "application/json"
            };
        }

        [HttpPost]
        public JsonResult GetBookingSummary()
        {
            Bookings book = new Bookings();
            var auth = AuthorizationGateway.GetAuthorizedInfo(null);

            return Json(book.GetBookingSummary(auth), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Confirm(int bookID)
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");


                Bookings bookingRepository = new Bookings();
                var result = bookingRepository.Confirm(bookID, auth);


                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);



            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Cancel(int bookID)
        {
            try
            {
                Bookings bookRepository = new Bookings();

                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                bookRepository.Cancel(bookID, auth);

                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Finalized(int bookID)
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                Bookings bookingRepository = new Bookings();
                var result = bookingRepository.Finalized(bookID, auth);


                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);



            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveManifesto(Bookings book)
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                Bookings bookingRepository = new Bookings();
                var result = bookingRepository.SaveManifesto(book, auth);


                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);



            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


    }
}