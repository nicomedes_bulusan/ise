﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ISE.DAL;

namespace ISE.Controllers
{
    public class FileUploadController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage UploadFile()
        {
            string path = System.AppDomain.CurrentDomain.BaseDirectory + "\\Uploads";
            long epochTicks = new DateTime(1970, 1, 1).Ticks;
            long unixTime = ((DateTime.UtcNow.Ticks - epochTicks) / TimeSpan.TicksPerSecond);
            string timestamp = unixTime.ToString();


            try
            {



                if (HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    // Get the uploaded image from the Files collection
                    //  var httpPostedFile = HttpContext.Current.Request.Files["attachment"];
                    // var httpPostedFile = HttpContext.Current.Request.Files["file"];
                    var httpPostedFile = HttpContext.Current.Request.Files["file"];
                    string newFilename = Path.GetFileNameWithoutExtension(httpPostedFile.FileName) +
                  " - " + timestamp + Path.GetExtension(httpPostedFile.FileName);

                    if (httpPostedFile != null)
                    {
                        // Validate the uploaded image(optional)

                        // Get the complete file path
                        var fileSavePath = Path.Combine(path, newFilename);



                        if (!Directory.Exists(path))
                        {
                            throw new Exception("Upload directory is missing!");
                        }

                        if (File.Exists(fileSavePath))
                        {
                            throw new Exception("File already exist!");
                        }

                        // Save the uploaded file to "UploadedFiles" folder
                        httpPostedFile.SaveAs(fileSavePath);



                    }



                    BookingAttachmentsModel attachment = new BookingAttachmentsModel();
                    attachment.BookingID = 0;
                    attachment.DateCreated = DateTime.Now;
                    attachment.Filename = newFilename;
                    attachment.OriginalFilename = httpPostedFile.FileName;
                    attachment.MimeType = MimeMapping.GetMimeMapping(httpPostedFile.FileName);





                    string resultHtml = "{\"Result\": \"OK\", \"Record\": " + Newtonsoft.Json.JsonConvert.SerializeObject(attachment).ToString() + "}";


                    var response = new HttpResponseMessage();
                    response.Content = new StringContent(resultHtml);
                    //response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("text/html");
                    response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                    return response;


                  


                }
                else
                {
                    throw new Exception("Invalid file!");
                }



            }
            catch (Exception ex)
            {

               

                string result = "{\"Result\": \"ERROR\", \"Message\": \"" + ex.Message + "\"}";



                var response = new HttpResponseMessage();
                response.Content = new StringContent(result);
                //response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("text/html");
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/html");
                return response;


            }


        }

    }
}
