﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISE.DAL;
using ISE.Models;

namespace ISE.Controllers
{
    public class MarketTypeController : Controller
    {
        // GET: MarketType
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult GetList()
        {
            MarketTypeModel marketRepo = new MarketTypeModel();

            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                var result = marketRepo.GetList();

                return Json(new { Result = "OK", Records = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult CreateOrUpdate(MarketTypeModel market)
        {
            MarketTypeModel marketRepo = new MarketTypeModel();

            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                var result = marketRepo.CreateOrUpdate(market);

                return Json(new { Result = "OK", Record = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}