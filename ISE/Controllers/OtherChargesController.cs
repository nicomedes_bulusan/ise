﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISE.DAL;
using ISE.Models;

namespace ISE.Controllers
{
    public class OtherChargesController : Controller
    {

        [HttpPost]
        public JsonResult GetOtherCharges()
        {
            OtherChargesModel otherChargesRepository = new OtherChargesModel();

            return Json(otherChargesRepository.GetOtherCharges(), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult CreateOtherCharge(OtherChargesModel charge)
        {
            try {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                OtherChargesModel otherChargesRepository = new OtherChargesModel();

                return Json(new { Result = "OK", Record = otherChargesRepository.CreateOtherCharges(charge) }, JsonRequestBehavior.AllowGet);

            }
            catch(Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateOtherCharge(OtherChargesModel charge)
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                OtherChargesModel otherChargesRepository = new OtherChargesModel();
                otherChargesRepository.UpdateCharge(charge);

                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult DeleteOtherCharge(OtherChargesModel charge)
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                OtherChargesModel otherChargesRepository = new OtherChargesModel();
                otherChargesRepository.DeleteCharge(charge);

                return Json(new { Result = "OK"}, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


    }
}