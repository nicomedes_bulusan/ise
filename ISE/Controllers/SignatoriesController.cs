﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISE.DAL;
using ISE.Models;

namespace ISE.Controllers
{
    public class SignatoriesController : Controller
    {
        // GET: Signatories
        public ActionResult Index()
        {
            ViewBag.ActiveTab = "Signatories";
            return View();
        }


        [HttpPost]
        public JsonResult GetSignatories()
        {

            try {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                SystemSignatoriesModel signatoryRepo = new SystemSignatoriesModel();
                var result = signatoryRepo.AllSignatories();

                return Json(new { Result = "OK", Records = result }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }


        }


        [HttpPost]
        public JsonResult CreateOrUpdate(SystemSignatoriesModel signatory)
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                SystemSignatoriesModel signatoryRepo = new SystemSignatoriesModel();

                var result = signatoryRepo.CreateOrUpdate(signatory, auth);
                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);
              

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}