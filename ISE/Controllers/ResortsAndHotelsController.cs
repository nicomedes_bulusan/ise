﻿using ISE.ActionFilters;
using ISE.DAL;
using ISE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ISE.Controllers
{
    [CompressFilter]      
    public class ResortsAndHotelsController : Controller
    {
        // GET: ResortsAndHotels
        public ActionResult Index()
        {
            ViewBag.ActiveTab = "Resorts";
            return View();
        }


        [HttpPost]
        public JsonResult GetResortsAndHotels(Boolean activeOnly=false)
        {
            var auth = AuthorizationGateway.GetAuthorizedInfo(null);

            if (auth == null) throw new Exception("Your session has expired! Please log on again.");
            ResortsAndHotelsModel rhRepo = new ResortsAndHotelsModel();

            return Json(rhRepo.GetResortsAndHotels(activeOnly), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult CreateResortsAndHotels(ResortsAndHotelsModel rh)
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                ResortsAndHotelsModel rhRepo = new ResortsAndHotelsModel();

                return Json(new { Result = "OK", Record = rhRepo.Create(rh) }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateResortsAndHotels(ResortsAndHotelsModel rh)
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                ResortsAndHotelsModel rhRepo = new ResortsAndHotelsModel();

                rhRepo.Update(rh);

                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


    }
}