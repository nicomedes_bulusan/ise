﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISE.ActionFilters;
using ISE.DAL;
using Newtonsoft.Json;
using ISE.Models;

namespace ISE.Controllers
{
    [CompressFilter]
    [OutputCache(NoStore = true, Duration = 0)]
    public class ReceivingController : Controller
    {
        // GET: Receiving
        public ActionResult Index()
        {
            ViewBag.ActiveTab = "AccountReceivables";
            return View();
        }


        [HttpPost]
        public ContentResult GetUnconfirmedCollectibles()
        {


            try
            {

                AccountReceivablesModel arRepo = new AccountReceivablesModel();

                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                var result = arRepo.GetUnconfirmedCollectibles(auth);

                return new ContentResult
                {
                    Content = JsonConvert.SerializeObject(new { Result = "OK", Records = result }, new JsonSerializerSettings() { DateFormatString = "MM/dd/yyyy HH:mm" }),
                    ContentType = "application/json"
                };


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
                return new ContentResult
                {
                    Content = JsonConvert.SerializeObject(new { Result = "ERROR", Message = ex.Message }),
                    ContentType = "application/json"
                };
            }


        }


    }
}