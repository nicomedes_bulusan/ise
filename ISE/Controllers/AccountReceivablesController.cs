﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISE.DAL;
using ISE.Models;
using Ionic.Zip;
using System.IO;
using ISE.ActionFilters;
using System.Net;

namespace ISE.Controllers
{

    // [OutputCache(NoStore = true, Duration = 0)]
    public class AccountReceivablesController : Controller
    {
        // GET: AccountReceivables
        public ActionResult Index()
        {
            ViewBag.ActiveTab = "AccountReceivables";
            return View();
        }


        public ActionResult Batches()
        {
            ViewBag.ActiveTab = "AccountReceivables";
            return View();
        }



        public JsonResult GetAccountReceivables()
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                AccountReceivablesModel arRepo = new AccountReceivablesModel();
                var result = arRepo.GetAccountReceivables();

                return Json(new { Result = "OK", Records = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }


        public JsonResult GetAccountReceivablesPaged(DateTime fromDate,DateTime toDate,string search="",List<int> travelAgencies=null,int startIndex=0,int pageSize=1000)
        {
            try
            {
                AccountReceivablesModel arRepo = new AccountReceivablesModel();
                var result = arRepo.GetAccountsReceivablePaged(fromDate,toDate,search,travelAgencies,startIndex,pageSize);

                return Json(new { Result = "OK", Records = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }


        public JsonResult GenerateMissingBillingStatements()
        {
            try
            {

                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                AccountReceivablesModel arRepo = new AccountReceivablesModel();

                arRepo.GenerateMissingStatements(auth);

                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ReGenerateBillingStatements(int billingPeriodID,int batchID)
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                AccountReceivablesModel arRepo = new AccountReceivablesModel();
                arRepo.RegeneratePDF(auth, billingPeriodID, batchID);


                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GenerateBillingStatements(List<int> bookingIDs)
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                AccountReceivablesModel arRepo = new AccountReceivablesModel();
                arRepo.GenerateStatement(bookingIDs, auth);


                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        public void GetFilesByBatch(int id)
        {
            AccountReceivablesModel acctARRepo = new AccountReceivablesModel();

            var auth = AuthorizationGateway.GetAuthorizedInfo(null);

            if (auth == null)
            {
                Response.StatusCode = (int)System.Net.HttpStatusCode.Forbidden;
                Response.End();
            }



            Response.Clear();
            Response.BufferOutput = false;
            string archiveName = String.Format("Billing-Statements_Batch-{0}.zip", id);
            Response.ContentType = "application/zip";
            // see http://support.microsoft.com/kb/260519
            Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode(archiveName));
            using (ZipFile zip = new ZipFile())
            {
                // filesToInclude is a IEnumerable<String> (String[] or List<String> etc)
                zip.AddFiles(acctARRepo.GetFilesByBatch(id), "");
                zip.Save(Response.OutputStream);
            }
            // Response.End(); // will throw an exception internally.
            Response.Close();
        }

        public JsonResult GetBatches()
        {
            try
            {
                AccountReceivablesModel acctARRepo = new AccountReceivablesModel();
                var result = acctARRepo.GetBatches();

                return Json(new { Result = "OK", Records = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult TaggedAsReceived(int billingPeriodID, DateTime dateReceived)
        {
            var auth = AuthorizationGateway.GetAuthorizedInfo(null);
            AccountReceivablesModel acctARRepo = new AccountReceivablesModel();

            try
            {
              

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                acctARRepo.TagAsReceived(auth, billingPeriodID, dateReceived);

                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult PostPayments(int billingPeriodID, double amount, string remarks = "")
        {
            var auth = AuthorizationGateway.GetAuthorizedInfo(null);
            AccountReceivablesModel acctARRepo = new AccountReceivablesModel();

            try
            {

              

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                acctARRepo.PostPayment(auth, billingPeriodID, amount, remarks);

                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CancelBillingStatement(int billingPeriodID)
        {
            var auth = AuthorizationGateway.GetAuthorizedInfo(null);
            AccountReceivablesModel acctARRepo = new AccountReceivablesModel();

            try
            {

               

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");
                acctARRepo.CancelBillingStatement(auth, billingPeriodID);

                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }



        public void ViewBillingStatement(int billingPeriodID, int agentID)
        {
            var auth = AuthorizationGateway.GetAuthorizedInfo(null);
            AccountReceivablesModel acctARRepo = new AccountReceivablesModel();
            string path = System.AppDomain.CurrentDomain.BaseDirectory + "\\Reports\\Statements\\";

            try
            {
                if (auth == null)
                {
                    Response.StatusCode = (int)System.Net.HttpStatusCode.Forbidden;
                    Response.End();
                }

                string filename = acctARRepo.GetBillingStatement(billingPeriodID, agentID, auth);
                string fullPath = path + filename;

                if (System.IO.File.Exists(fullPath))
                {


                    FileStream fs = System.IO.File.OpenRead(fullPath);

                    byte[] bytes = new byte[fs.Length];
                    fs.Read(bytes, 0, Convert.ToInt32(fs.Length));
                    fs.Close();
                    fs.Dispose();


                    //AVOID MEMORY LEAK
                    using (MemoryStream ms = new MemoryStream(bytes))
                    {

                        ms.WriteTo(Response.OutputStream);
                    }



                    Response.AddHeader("Cache-Control", "no-cache");
                    Response.AddHeader("Pragma", "");
                    Response.AddHeader("Content-type", MimeMapping.GetMimeMapping(filename));
                    Response.AddHeader("Content-Length", bytes.Length.ToString());
                    Response.AddHeader("content-disposition", "inline; filename=" + HttpUtility.UrlEncode(filename));
                    Response.AddHeader("Content-Transfer-Encoding", "binary");


                    Response.End();




                }
                else
                {
                    throw new Exception("Billing Statement is deleted or missing!");
                }


            }
            catch (Exception ex)
            {
                HttpContext.Response.Write(ex.Message);

                //Response.Clear();
                //Response.ContentType = "text/html";
                //Response.Write(ex.Message);
                //Response.Close();
                //Response.End();

            }


        }


        public void DownloadBillingStatement(int billingPeriodID, int agentID)
        {
            var auth = AuthorizationGateway.GetAuthorizedInfo(null);
            AccountReceivablesModel acctARRepo = new AccountReceivablesModel();
            string path = System.AppDomain.CurrentDomain.BaseDirectory + "\\Reports\\Statements\\";

            try
            {

                if (auth == null)
                {
                    Response.StatusCode = (int)System.Net.HttpStatusCode.Forbidden;
                    Response.End();
                }


                string filename = acctARRepo.GetBillingStatement(billingPeriodID, agentID, auth).Replace(".pdf", ".xls");
                string fullPath = path + filename;

                if (System.IO.File.Exists(fullPath))
                {


                    FileStream fs = System.IO.File.OpenRead(fullPath);

                    byte[] bytes = new byte[fs.Length];
                    fs.Read(bytes, 0, Convert.ToInt32(fs.Length));
                    fs.Close();
                    fs.Dispose();

                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        ms.WriteTo(Response.OutputStream);
                    }



                    Response.AddHeader("Cache-Control", "no-cache");
                    Response.AddHeader("Pragma", "");
                    Response.AddHeader("Content-type", MimeMapping.GetMimeMapping(filename));
                    Response.AddHeader("Content-Length", bytes.Length.ToString());
                    Response.AddHeader("content-disposition", "inline; filename=" + HttpUtility.UrlEncode(filename));
                    Response.AddHeader("Content-Transfer-Encoding", "binary");


                    Response.End();




                }
                else
                {
                    throw new Exception("Billing Statement is deleted or missing!");
                }


            }
            catch (Exception ex)
            {
                HttpContext.Response.Write(ex.Message);

                //Response.Clear();
                //Response.ContentType = "text/html";
                //Response.Write(ex.Message);
                //Response.Close();
                //Response.End();

            }


        }



    }
}