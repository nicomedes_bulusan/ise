﻿using ISE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISE.DAL;

namespace ISE.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
        
            return View();
        }

        public ActionResult Login(string returnUrl = "/")
        {
            Session["UserInfo"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult Authenticate(LoginViewModel model, string returnUrl = "/")
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userInfo = AuthorizationGateway.GetAuthorizedInfo(model);
                    if (userInfo != null)
                    {
                        //await SignInAsync(user, model.RememberMe);
                        Session.Timeout = 30;
                        Session["UserInfo"] = userInfo;
                        return Redirect(returnUrl);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }





            }

            // If we got this far, something failed, redisplay form
            return View("Login", model);
        }


        [HttpPost]
        public JsonResult IsSessionActive()
        {

            var session = HttpContext.Session["UserInfo"];           

            try
            {

                Boolean stat = false;            

                if (session == null) stat = false;

                if (session != null)
                {                  
                        stat = true;                    
                }

              

                return Json(new { Result = "OK", Record = stat}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }


        }

       
     
    }
}