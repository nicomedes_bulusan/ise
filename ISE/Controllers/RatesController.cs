﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISE.DAL;
using ISE.Models;
using ISE.ActionFilters;

namespace ISE.Controllers
{
    [CompressFilter]
    [OutputCache(NoStore = true, Duration = 0)]
    public class RatesController : Controller
    {
        // GET: Rates
        public ActionResult Index()
        {
            ViewBag.ActiveTab = "Rates";
            return View();
        }


        public ActionResult GetRateMatrices(int id=0)
        {
            AgentsModel agent = new AgentsModel();
            RatesMatrices rates = new RatesMatrices();
            AgentsModel auth = new AgentsModel();

            if (id == 0)
            {
                auth = AuthorizationGateway.GetAuthorizedInfo(null);
            }
            else
            {
                auth = agent.FindAgentByID(id);
            }

            return Json(rates.GetMatrices(auth), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CreateMatrix(RatesMatrices rate,int agentID)
        {
            try {

                AgentsModel agent = new AgentsModel();
                
                AgentsModel auth = new AgentsModel();

                if (agentID == 0)
                {
                    auth = AuthorizationGateway.GetAuthorizedInfo(null);
                }
                else
                {
                    auth = agent.FindAgentByID(agentID);
                }
                RatesMatrices ratesRepository = new RatesMatrices();

                return Json(new { Result = "OK", Record = ratesRepository.CreateMatrix(rate,auth) }, JsonRequestBehavior.AllowGet);

            }
            catch(Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
          
        }

        [HttpPost]
        public ActionResult UpdateMatrix(RatesMatrices rate,int agentID)
        {
            try
            {
                AgentsModel agent = new AgentsModel();
               
                AgentsModel auth = new AgentsModel();

                if (agentID == 0)
                {
                    auth = AuthorizationGateway.GetAuthorizedInfo(null);
                }
                else
                {
                    auth = agent.FindAgentByID(agentID);
                }
                RatesMatrices ratesRepository = new RatesMatrices();
                ratesRepository.Updatematrix(rate,auth);

                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult DeleteMatrix(RatesMatrices rate,int agentID)
        {
            try
            {
                AgentsModel agent = new AgentsModel();
                
                AgentsModel auth = new AgentsModel();

                if (agentID == 0)
                {
                    auth = AuthorizationGateway.GetAuthorizedInfo(null);
                }
                else
                {
                    auth = agent.FindAgentByID(agentID);
                }
                RatesMatrices ratesRepository = new RatesMatrices();
                ratesRepository.DeleteMatrix(rate,auth);

                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

    }
}