﻿using ISE.DAL;
using ISE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ISE.Controllers
{
    public class ManifestoController : Controller
    {
        // GET: Manifesto
        public ActionResult Index()
        {
            ViewBag.ActiveTab = "Manifesto";
            return View();
        }

        public ActionResult Create()
        {
            ViewBag.ActiveTab = "Manifesto";
            return View();
        }

        public ActionResult Preview(int id)
        {
            ViewBag.ActiveTab = "Manifesto";
            ViewBag.ID = id;
            return View();
        }


        [HttpGet]
        public JsonResult GetScheduledManifestos(int status = 0)
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);
                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                ManifestoSchedules sched = new ManifestoSchedules();
                return Json(new { Result = "OK", Records = sched.GetSchedules((ManifestoStatus)status) }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public JsonResult GetManifestos(DateTime date, int type,string tpLoc)
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);
                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                ManifestoSchedules sched = new ManifestoSchedules();
                int tpMarker = 0;

                if (type == 1)
                {
                    if (tpLoc=="CAGBAN")
                    {
                        tpMarker = 0;
                    }
                    return Json(new { Result = "OK", Records = sched.GetArivingManifesto(date,tpLoc,tpMarker) }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (tpLoc != "CAGBAN")
                    {
                        tpMarker = 0;
                    }

                    return Json(new { Result = "OK", Records = sched.GetDepartingManifesto(date,tpLoc,tpMarker) }, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetManifestoSchedule(int id)
        {
            try
            {

                var auth = AuthorizationGateway.GetAuthorizedInfo(null);
                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                ManifestoSchedules sched = new ManifestoSchedules();
                return Json(new { Result = "OK", Record = sched.GetSchedule(id) }, JsonRequestBehavior.AllowGet);

                
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetSchedulesByCoordinator(string username, string password, DateTime fromDate)
        {
            try
            {

                LoginViewModel login = new LoginViewModel();
                login.UserName = username;
                login.Password = password;
                login.RememberMe = false;
                var auth = AuthorizationGateway.GetAuthorizedInfo(login, true);
                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                ManifestoSchedules sched = new ManifestoSchedules();

                return Json(new { Result = "OK", Records = sched.GetSchedulesByCoordinator(auth, fromDate) }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<JsonResult> CreateOrUpdate(ManifestoSchedules schedule)
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);
                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                ManifestoSchedules sched = new ManifestoSchedules();



                return Json(new { Result = "OK", Records = await sched.CreateOrUpdateManifestoSchedule(schedule, auth) }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public async Task<JsonResult> SyncManifesto(List<ManifestoDataset> schedules, string username, string password)
        {
            try
            {
                LoginViewModel login = new LoginViewModel();
                login.UserName = username;
                login.Password = password;
                login.RememberMe = false;
                var auth = AuthorizationGateway.GetAuthorizedInfo(login, true);
                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                ManifestoSchedules sched = new ManifestoSchedules();



                return Json(new { Result = "OK", Records = await sched.SyncManifesto(schedules, auth) }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult VerifyAuthCode(string code, int travelAgencyID)
        {
            try
            {
                ManifestoSchedules sched = new ManifestoSchedules();
                sched.VerifyAuthCode(code, travelAgencyID);

                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}