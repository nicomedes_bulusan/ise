﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISE.DAL;
using ISE.ActionFilters;
using ISE.Models;
using Newtonsoft.Json;

namespace ISE.Controllers
{
    [CompressFilter]  
    public class AgentsController : Controller
    {
        // GET: Agents
        public ActionResult Index()
        {
            ViewBag.ActiveTab = "Agents";
            return View();
        }

        [HttpPost]
        public JsonResult CheckBalance(int travelAgencyID)
        {
            try
            {

                var auth = AuthorizationGateway.GetAuthorizedInfo(null);
                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                AgentsModel agentRepo = new AgentsModel();
                return Json(new { Result = "OK", Record = agentRepo.CheckBalance(travelAgencyID) }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetTravelAgencies()
        {
            var auth = AuthorizationGateway.GetAuthorizedInfo(null);

            if (auth == null) throw new Exception("Your session has expired! Please log on again.");
            AgentsModel agents = new AgentsModel();

            return Json(agents.GetTravelAgencies(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetAccountingPersons()
        {
            var auth = AuthorizationGateway.GetAuthorizedInfo(null);

            if (auth == null) throw new Exception("Your session has expired! Please log on again.");
            AgentsModel agents = new AgentsModel();

            return Json(agents.GetAccountingPersons(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get(bool activeOnly=true,int agentType=0,bool unique=false)
        {
            var auth = AuthorizationGateway.GetAuthorizedInfo(null);

            if (auth == null) throw new Exception("Your session has expired! Please log on again.");
            AgentsModel agents = new AgentsModel();

            return Json(agents.GetAgents(activeOnly,agentType,unique), JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult Create(AgentsModel agent,string newPassword,string retypePassword)
        {
            AgentsModel agentRepository = new AgentsModel();

            try
            {

                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                if (newPassword != retypePassword) throw new Exception("Password does not match!");

                var result = agentRepository.Create(agent, newPassword);

                return Json(new { Result = "OK", Record = result }, JsonRequestBehavior.AllowGet);

            }catch(Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult Update(AgentsModel agent,string oldPassword,string newPassword)
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                AgentsModel agentRepository = new AgentsModel();

                var result = agentRepository.Update(agent, oldPassword, newPassword);

                return Json(new { Result = "OK", Record = result }, JsonRequestBehavior.AllowGet);


            }
            catch(Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Deactivate(AgentsModel agent)
        {
            try
            {

                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                AgentsModel agentRepository = new AgentsModel();

                var result = agentRepository.Deactivate(agent);

                return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);

            }
            catch(Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetCoordinators()
        {
            try
            {
                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                if (!(auth.Type == 2 || auth.Type == 3 || auth.Type == 4)) throw new Exception("Access is denied!");

                AgentsModel agentRepository = new AgentsModel();

                var today = new  DateTime(2016,1,1);

                var data = agentRepository.GetCoordinators(today);

            

                return Json(new { Result = "OK", Records = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult GenerateAuthCodes()
        {
            try
            {


                var auth = AuthorizationGateway.GetAuthorizedInfo(null);

                if (auth == null) throw new Exception("Your session has expired! Please log on again.");

                if (!(auth.Type == 2 || auth.Type == 3 || auth.Type == 4)) throw new Exception("Access is denied!");

                ManifestoSchedules manifesto = new ManifestoSchedules();
                manifesto.GenerateAuthCodes();

                return Json(new { Result = "OK"}, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult AuthCodes()
        {
            ViewBag.ActiveTab = "Auth Codes";
            return View();
        }

        [HttpPost]
        public ContentResult GetAuthCodes()
        {
            try
            {

                AgentsModel agent = new AgentsModel();

                var auth = AuthorizationGateway.GetAuthorizedInfo(null);
                var result = agent.GetAuthCodes(auth.TravelAgencyID);


                return new ContentResult
                {
                    // Content=json,
                    Content = JsonConvert.SerializeObject(new { Result = "OK", Records = result }, new JsonSerializerSettings() { DateFormatString = "MM/dd/yyyy HH:mm" }),
                    ContentType = "application/json"
                };
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
                return new ContentResult
                {
                    Content = JsonConvert.SerializeObject(new { Result = "ERROR", Message = ex.Message }),
                    ContentType = "application/json"
                };
            }
        }


        public JsonResult SyncCoordinators(DateTime lastSyncDate)
        {
            try {
            
             AgentsModel agentRepository = new AgentsModel();


             var data =  agentRepository.GetCoordinators(lastSyncDate,true);

             var transformed = (from a in data
                                  select new
                                  {
                                      username = a.Email,
                                      password = a.Password,
                                      complete_name = a.Fullname,
                                      status = a.IsActive
                                  }).ToList();

             return Json(new { Result = "OK", Records = transformed }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                 Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            
        }

        public JsonResult SyncAuthCodes()
        {
            try {


                using (var db = new isebookingEntities())
                {
                    var result = (from a in db.authorizationcodes
                                  select new
                                  {
                                      TravelAgencyID = a.TravelAgencyID,
                                      AuthCode = a.AuthCode,
                                      Status = a.Status
                                  }).ToList();
                    return Json(new { Result = "OK", Records = result }, JsonRequestBehavior.AllowGet);

                }

            
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Result = "ERROR", Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}