﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISE.DAL;
using ISE.ActionFilters;
using ISE.Models;

namespace ISE.Controllers
{
    [CompressFilter]      
    public class DestinationsController : Controller
    {
        // GET: Destinations
        public ActionResult Index()
        {
           
            return View();
        }

       
        public JsonResult GetDestinations()
        {
            var auth = AuthorizationGateway.GetAuthorizedInfo(null);

            if (auth == null) throw new Exception("Your session has expired! Please log on again.");

            DestinationsModel destinatonRepository = new DestinationsModel();

            var result = destinatonRepository.GetDestinations();

            return Json(result, JsonRequestBehavior.AllowGet);


        }
    }
}