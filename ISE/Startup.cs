﻿using Hangfire;
using Hangfire.Common;
using Hangfire.Console;
using Hangfire.Redis;
using Microsoft.Owin;
using Owin;
using System;
using ISE.DAL;

[assembly: OwinStartupAttribute(typeof(ISE.Startup))]
namespace ISE
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);


            GlobalConfiguration.Configuration.UseRedisStorage("localhost:6379", new RedisStorageOptions()
            {
                Db = 2,
                Prefix = "ise:"
            }).UseConsole(new ConsoleOptions
            {

                BackgroundColor = "#0F3462",
                TimestampColor = "#3B9CCD"
            });


            // ISE.DAL.AccountReceivablesModel acctARRepo = new ISE.DAL.AccountReceivablesModel();
            //RecurringJob.AddOrUpdate("Batch Job", () => acctARRepo.GetBatchJobs(), "0/10 * * * *", TimeZoneInfo.Local);

            Tools tool = new Tools();
            AgentsModel agent = new AgentsModel();

            MobileIntegration mobile = new MobileIntegration();

            WooCommerce.Integration integration = new WooCommerce.Integration();
         //   RecurringJob.AddOrUpdate("Batch Job", () => integration.SyncCompletedOrders(), "0/5 * * * *", TimeZoneInfo.Local);

            RecurringJob.AddOrUpdate("Collect Garbage", () => tool.CollectGarbage(), "0/30 * * * *", TimeZoneInfo.Local);

            RecurringJob.AddOrUpdate("Auto Book Walk-in", () => mobile.GetWalkIns(), "0/5 * * * *", TimeZoneInfo.Local);

            RecurringJob.AddOrUpdate("Auto Manifest", () => mobile.CreateManifestForAddtionalBooking(), "0/5 * * * *", TimeZoneInfo.Local);

            //     RecurringJob.AddOrUpdate("Auto Book Excess", () => mobile.GetExcessBooking(), "0/5 * * * *", TimeZoneInfo.Local);


            RecurringJob.AddOrUpdate("Sync Users To Mobile", () => agent.SyncUsersToMobile(), "0/15 * * * *", TimeZoneInfo.Local);

            app.UseHangfireDashboard();
            app.UseHangfireServer(new BackgroundJobServerOptions
            {
                WorkerCount = 5,
                

            });


        }
    }
}
