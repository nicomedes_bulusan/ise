﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ISE.Reports
{
    public partial class ManifestoDepartures : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            var startDate = Request.QueryString["startDate"];
            var format = Request.QueryString["format"];

            var auth = Models.AuthorizationGateway.GetAuthorizedInfo(null);

            if (auth == null)
            {
                Response.Clear();
                Response.Write("Access Denied");
                Response.StatusCode = 401;
                Response.End();
            }

            if (auth.Type == 1)
            {
                Response.Clear();
                Response.Write("Access Denied");
                Response.StatusCode = 401;
                Response.End();
            }



            if (!String.IsNullOrEmpty(startDate))
            {
                DateTime oDate = Convert.ToDateTime(startDate);

                ISE.DAL.Reporting rpt = new ISE.DAL.Reporting();
                List<ISE.DAL.ManifestoDataset> reports = rpt.GetManifestoDepartures(oDate);
                ISE.DAL.DestinationsModel destinationRepo = new DAL.DestinationsModel();

                var lr = new LocalReport
                {
                    ReportPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\\Reports\\ManifestoDepartures.rdlc",
                    EnableExternalImages = true
                };
                ReportParameter p1 = new ReportParameter("StartDate", startDate);


                 lr.DataSources.Add(new ReportDataSource("DestinationsModel", destinationRepo.GetDestinations()));
                lr.DataSources.Add(new ReportDataSource("ManifestoDS", reports));
                lr.SetParameters(new ReportParameter[] { p1 });
                Warning[] warnings;



                string mimeType, encoding, extension;
                string[] streams;
                string filename = "Manisfesto Arrivals " + startDate;

                if (string.IsNullOrEmpty(format)) format = "PDF";


                var renderedBytes = lr.Render
                    (
                        format,
                        @"<DeviceInfo><OutputFormat>PDF</OutputFormat><HumanReadablePDF>False</HumanReadablePDF></DeviceInfo>",
                        out mimeType,
                        out encoding,
                        out extension,
                        out streams,
                        out warnings
                    );

                Response.Buffer = true;
                Response.Clear();
                Response.ContentType = mimeType;
                Response.AddHeader(
                    "content-disposition",
                    "inline; filename= " +  filename + "." + extension);
                Response.OutputStream.Write(renderedBytes, 0, renderedBytes.Length);
                Response.Flush();
                Response.End();
            }



        }
    }
}