﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ISE.Reports
{
    public partial class Service_Voucher : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ISE.DAL.AgentsModel agentRepo = new DAL.AgentsModel();
            ISE.DAL.DestinationsModel destinationRepo = new DAL.DestinationsModel();
            var lr = new LocalReport
            {
                ReportPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\\Reports\\Service Voucher.rdlc",
                EnableExternalImages = true,
                EnableHyperlinks=true
            };
        
            string refereceID = "";
            if (Request.QueryString["id"] != "")
            {
                refereceID = Request.QueryString["id"];
            }

            ISE.DAL.Bookings bookeRepo = new DAL.Bookings();
            ISE.DAL.Bookings book = bookeRepo.GetBooking(Convert.ToInt32(refereceID));

            var bookingAgent = agentRepo.FindAgentByID(book.Header.CreatedBy);
         
            lr.DataSources.Add(new ReportDataSource("agents", new List<ISE.DAL.AgentsModel>() { bookingAgent }));
            lr.DataSources.Add(new ReportDataSource("Bookings", new List<ISE.DAL.BookingHeaderModel>() { book.Header }));
            lr.DataSources.Add(new ReportDataSource("destinations", destinationRepo.GetDestinations()));
            Warning[] warnings;
            
            string mimeType, encoding, extension;
            string[] streams;
            var renderedBytes = lr.Render
                (
                    "PDF",
                    @"<DeviceInfo><OutputFormat>PDF</OutputFormat><HumanReadablePDF>False</HumanReadablePDF></DeviceInfo>",
                    out mimeType,
                    out encoding,
                    out extension,
                    out streams,
                    out warnings
                );

            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader(
                "content-disposition",
                "inline; filename= filename" + "." + extension);
            Response.OutputStream.Write(renderedBytes, 0, renderedBytes.Length);
            Response.Flush();
            Response.End();
        }
    }
}