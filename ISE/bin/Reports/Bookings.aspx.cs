﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ISE.Reports
{
    public partial class Bookings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ISE.DAL.Reporting rpt = new ISE.DAL.Reporting();
            string rdlcFile = "Bookings.rdlc";
            ReportViewer1.ShowParameterPrompts = true;
            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;

            ReportViewer1.LocalReport.ReportPath = Server.MapPath(rdlcFile);
            List<ISE.DAL.BookingsDS> reports = rpt.GetBookingsByAgentDateStatus();

            Microsoft.Reporting.WebForms.ReportDataSource headerInfo = new Microsoft.Reporting.WebForms.ReportDataSource("BookingDS", reports);
            
            ReportViewer1.LocalReport.DataSources.Add(headerInfo);



            ReportViewer1.LocalReport.Refresh();
        }
    }
}