﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ISE.Reports
{
    public partial class Departures : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ISE.DAL.Reporting rpt = new ISE.DAL.Reporting();
            List<ISE.DAL.BookingsDS> reports = rpt.GetBookingsByAgentDateStatus();
            ISE.DAL.DestinationsModel destinationRepo = new DAL.DestinationsModel();

            var lr = new LocalReport
            {
                ReportPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\\Reports\\Departures.rdlc",
                EnableExternalImages = true
            };



            lr.DataSources.Add(new ReportDataSource("destinations", destinationRepo.GetDestinations()));
            lr.DataSources.Add(new ReportDataSource("Bookings", reports));

            Warning[] warnings;



            string mimeType, encoding, extension;
            string[] streams;
            var renderedBytes = lr.Render
                (
                    "PDF",
                    @"<DeviceInfo><OutputFormat>PDF</OutputFormat><HumanReadablePDF>False</HumanReadablePDF></DeviceInfo>",
                    out mimeType,
                    out encoding,
                    out extension,
                    out streams,
                    out warnings
                );

            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader(
                "content-disposition",
                "inline; filename= filename" + "." + extension);
            Response.OutputStream.Write(renderedBytes, 0, renderedBytes.Length);
            Response.Flush();
            Response.End();
        }
    }
}