﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManifestoDepartures.aspx.cs" Inherits="ISE.Reports.ManifestoDepartures" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Daily Manifesto Departures</title>
    <link rel="stylesheet" type="text/css" href="../Content/zebra_datepicker/default.css" />
    <link rel="stylesheet" type="text/css" href="../Content/bootstrap.min.css" />

    <script type="text/javascript" src="../Scripts/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="../Scripts/zebra_datepicker.js"></script>


</head>
<body>
    <div class="container-fluid" style="padding: 0;">

        <div class="col-md-12" style="height: 20px; background-color: #588658; border: 1px solid #8FC393;">
            <label class="control-label" style="color: #fff;"><a href="/Booking/Reports">Reports</a> > Daily Manifesto (Departures)</label>
        </div>
        <div class="col-md-12" style="height: 60px; background-color: #8EC292; border: 1px solid #8FC393; padding-top: 15px;">
            <div class="col-md-10">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-2">Select Date:</label>
                        <div class="col-md-4">
                            <input type="text" class="datepicker form-control" id="txtStartDate" />
                        </div>
                    </div>
                </form>

            </div>
            <div class="col-md-2">
                <a href="#" class="btn btn-default" onclick="getReport()">View Report</a>


                <a href="#" class="btn btn-default" onclick="exportToExcel()">Export to Excel</a>
            </div>
        </div>
        <div class="col-md-12" style="height: 88vh; padding: 0;">
             <iframe src="" height="0" width="0" id="frameExport" hidden></iframe>
            <iframe src="" height="100%" width="100%" id="frameReport"></iframe>



        </div>








    </div>
    <script type="text/javascript">
        $(document).ready(function () {

            $('.datepicker').Zebra_DatePicker();

        });

        function getReport() {

            var startDate = $('#txtStartDate').val();

            $('#frameReport').attr("src", "/Reports/ManifestoDepartures.aspx?startDate=" + startDate);


        }

        function exportToExcel() {
            var startDate = $('#txtStartDate').val();

            $('#frameReport').attr("src", "/Reports/ManifestoDepartures.aspx?startDate=" + startDate + "&format=EXCELOPENXML");
        }

    </script>
</body>
</html>
