﻿using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ISE.Reports
{
    public partial class Quotation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            var lr = new LocalReport
            {
                ReportPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\\Reports\\Quotation.rdlc",
                EnableExternalImages = true
            };
            ISE.DAL.AgentsModel agentRepo = new DAL.AgentsModel();

            string refereceID = "";
            if (Request.QueryString["id"] != "")
            {
                refereceID = Request.QueryString["id"];
            }

            string bookingInfo = File.ReadAllText(System.AppDomain.CurrentDomain.BaseDirectory + @"\\TempData\\"+ refereceID + ".json");
         
             ISE.DAL.Bookings book = JsonConvert.DeserializeObject<ISE.DAL.Bookings>(bookingInfo);



            var bookingAgent = agentRepo.FindAgentByID(book.Header.CreatedBy);


           
            lr.DataSources.Add(new ReportDataSource("bookingdetails", book.Details));
            lr.DataSources.Add(new ReportDataSource("agents", new List<ISE.DAL.AgentsModel>() { bookingAgent }));
            lr.DataSources.Add(new ReportDataSource("Bookings", new List<ISE.DAL.BookingHeaderModel>() { book.Header }));
            Warning[] warnings;

           // string mimeType = string.Empty;
            //string encoding = string.Empty;
           // string extension = string.Empty;
           // string HIJRA_TODAY = "01/10/1435";
            // ReportParameter[] param = new ReportParameter[3];
            // param[0] = new ReportParameter("CUSTOMER_NUM", CUSTOMER_NUMTBX.Text);
            // param[1] = new ReportParameter("REF_CD", REF_CDTB.Text);
            // param[2] = new ReportParameter("HIJRA_TODAY", HIJRA_TODAY);

            // Microsoft.Reporting.WebForms.ReportViewer ReportViewer1;


            string mimeType, encoding, extension;
            string[] streams;
            var renderedBytes = lr.Render
                (
                    "PDF",
                    @"<DeviceInfo><OutputFormat>PDF</OutputFormat><HumanReadablePDF>False</HumanReadablePDF></DeviceInfo>",
                    out mimeType,
                    out encoding,
                    out extension,
                    out streams,
                    out warnings
                );

            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader(
                "content-disposition",
                "inline; filename= filename" + "." + extension);
            Response.OutputStream.Write(renderedBytes, 0, renderedBytes.Length); 
            Response.Flush(); 
            Response.End();

        }
    }
}