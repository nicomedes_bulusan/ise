﻿using ISE.Models;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ISE.DAL;

namespace ISE.Reports
{
    public partial class Quotation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            SystemSignatoriesModel signatoryRepo = new SystemSignatoriesModel();

            var lr = new LocalReport
            {
                //ReportPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\\Reports\\Quotation.rdlc",
                ReportEmbeddedResource = "ISE.Reports.Quotation.rdlc",
                EnableExternalImages = true
            };
            ISE.DAL.AgentsModel agentRepo = new DAL.AgentsModel();

            string refereceID = "";
            if (Request.QueryString["id"] != "")
            {
                refereceID = Request.QueryString["id"];
            }

            string bookingInfo = File.ReadAllText(System.AppDomain.CurrentDomain.BaseDirectory + @"\\TempData\\" + refereceID + ".json");

            ISE.DAL.Bookings book = JsonConvert.DeserializeObject<ISE.DAL.Bookings>(bookingInfo);

            var auth = AuthorizationGateway.GetAuthorizedInfo(null);

            var bookingAgent = agentRepo.FindAgentByID(book.Header.CreatedBy);


            var preparer = signatoryRepo.Get(auth.ID);
            var verifier = signatoryRepo.GetHighestSignatory(auth.Location,"SOA");


            lr.DataSources.Add(new ReportDataSource("bookingdetails", book.Details));
            lr.DataSources.Add(new ReportDataSource("agents", new List<ISE.DAL.AgentsModel>() { bookingAgent }));
            lr.DataSources.Add(new ReportDataSource("Bookings", new List<ISE.DAL.BookingHeaderModel>() { book.Header }));
            Warning[] warnings;


            ReportParameter p1 = new ReportParameter();
            ReportParameter p2 = new ReportParameter();
            ReportParameter p3 = new ReportParameter();
            ReportParameter p4 = new ReportParameter();

            p1 = new ReportParameter("PreparedBy", auth.Fullname);
            p2 = new ReportParameter("PreparedByDesignation", preparer==null?" ":preparer.Designation);
            p3 = new ReportParameter("VerifiedBy", verifier==null?" ":verifier.ImmediateHeadName);
            p4 = new ReportParameter("VerifiedByDesignation", verifier == null ? " " : verifier.ImmediateHeadDesignation);

            lr.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });

            string mimeType, encoding, extension;
            string[] streams;
            var renderedBytes = lr.Render
                (
                    "PDF",
                    @"<DeviceInfo><OutputFormat>PDF</OutputFormat><HumanReadablePDF>False</HumanReadablePDF></DeviceInfo>",
                    out mimeType,
                    out encoding,
                    out extension,
                    out streams,
                    out warnings
                );

            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader(
                "content-disposition",
                "inline; filename= filename" + "." + extension);
            Response.OutputStream.Write(renderedBytes, 0, renderedBytes.Length);
            Response.Flush();
            Response.End();

        }
    }
}