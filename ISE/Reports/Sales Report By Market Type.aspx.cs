﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ISE.Reports
{
    public partial class Sales_Report_By_Market_Type : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            var auth = Models.AuthorizationGateway.GetAuthorizedInfo(null);

            if (auth == null)
            {
                Response.Clear();
                Response.Write("Access Denied");
                Response.StatusCode = 401;
                Response.End();
            }

            if (auth.Type == 1)
            {
                Response.Clear();
                Response.Write("Access Denied");
                Response.StatusCode = 401;
                Response.End();
            }

            var startDate = Request.QueryString["startDate"];
            var endDate = Request.QueryString["endDate"];
            var format = Request.QueryString["format"];

            if (!String.IsNullOrEmpty(startDate))
            {
                DateTime sDate = Convert.ToDateTime(startDate);
                DateTime eDate = Convert.ToDateTime(endDate);

                ISE.DAL.Reporting rpt = new ISE.DAL.Reporting();
                List<ISE.DAL.SalesByMarketType> reports = rpt.SalesByMarketTypeSummary(sDate,eDate);
            
                var lr = new LocalReport
                {
                    //ReportPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\\Reports\\Sales Report.rdlc",
                    ReportEmbeddedResource = "ISE.Reports.SalesReportByMarketType.rdlc",
                    EnableExternalImages = true
                };
               


               // lr.DataSources.Add(new ReportDataSource("DestinationsModel", destinationRepo.GetDestinations()));
                lr.DataSources.Add(new ReportDataSource("SalesDS", reports));
               
                Warning[] warnings;
                ReportParameter p1 = new ReportParameter("StartDate", startDate);
                ReportParameter p2 = new ReportParameter("EndDate", endDate);

                lr.SetParameters(new ReportParameter[] { p1, p2 });


                string mimeType, encoding, extension;
                string[] streams;
                string filename = "Sales Report " + startDate;

                if (string.IsNullOrEmpty(format)) format = "PDF";


                var renderedBytes = lr.Render
                    (
                        format,
                        @"<DeviceInfo><OutputFormat>PDF</OutputFormat><HumanReadablePDF>False</HumanReadablePDF></DeviceInfo>",
                        out mimeType,
                        out encoding,
                        out extension,
                        out streams,
                        out warnings
                    );

                Response.Buffer = true;
                Response.Clear();
                Response.ContentType = mimeType;
                Response.AddHeader(
                    "content-disposition",
                    "inline; filename= " + filename + "." + extension);
                Response.OutputStream.Write(renderedBytes, 0, renderedBytes.Length);
                Response.Flush();
                Response.End();
            }


        }
    }
}