﻿using ISE.DAL;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ISE.Reports
{
    public partial class Service_Voucher : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ISE.DAL.AgentsModel agentRepo = new DAL.AgentsModel();
            ISE.DAL.DestinationsModel destinationRepo = new DAL.DestinationsModel();
            SystemSignatoriesModel bookingSignatories = new SystemSignatoriesModel();
            SystemSignatoriesModel signatoryRepo = new SystemSignatoriesModel();
            var lr = new LocalReport
            {
                
#if !DEBUG
 ReportEmbeddedResource = "ISE.Reports.Service Voucher.rdlc",
#else
  ReportPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\\Reports\\Service Voucher.rdlc",
#endif

             
                EnableExternalImages = true,
                EnableHyperlinks = true
            };

            string refereceID = "";
            if (Request.QueryString["id"] != "")
            {
                refereceID = Request.QueryString["id"];
            }

            ISE.DAL.Bookings bookeRepo = new DAL.Bookings();
            ISE.DAL.Bookings book = new ISE.DAL.Bookings();


            try
            {
                book = bookeRepo.GetBookingByCVNumber(refereceID);

                if (book.Header == null) throw new Exception("Booking not found!");

                bookingSignatories = signatoryRepo.GetSVSignatories(book.Header.ID);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                Response.End();
            }



            var bookingAgent = agentRepo.FindAgentByID(book.Header.CreatedBy);

            lr.DataSources.Add(new ReportDataSource("agents", new List<ISE.DAL.AgentsModel>() { bookingAgent }));
            lr.DataSources.Add(new ReportDataSource("Bookings", new List<ISE.DAL.BookingHeaderModel>() { book.Header }));
            lr.DataSources.Add(new ReportDataSource("destinations", destinationRepo.GetDestinations()));
            Warning[] warnings;




            SystemSignatoriesModel preparer = new SystemSignatoriesModel();
            SystemSignatoriesModel verifier = new SystemSignatoriesModel();
            ReportParameter p1 = new ReportParameter();
            ReportParameter p2 = new ReportParameter();
            ReportParameter p3 = new ReportParameter();
            ReportParameter p4 = new ReportParameter();
            ReportParameter p5 = new ReportParameter();
            if (bookingSignatories == null)
            {
                var preparedBy = book.Header.Logs.Where(x => x.Status == BookingStatus.FINALIZED).Select(x => x.Agent).FirstOrDefault();




                verifier = signatoryRepo.GetHighestSignatory(bookingAgent.Location, "SV");

                if (verifier == null) throw new Exception("Signatories are missing!");


                verifier.AgentName = preparedBy.Fullname;
                verifier.Designation = " ";



                preparer.AddSVSignatories(verifier, book.Header.ID);


                p1 = new ReportParameter("PreparedBy", preparedBy.Fullname);
                p2 = new ReportParameter("PreparedByDesignation",  verifier.Designation);
                p3 = new ReportParameter("VerifiedBy", verifier == null ? " " : verifier.ImmediateHeadName);
                p4 = new ReportParameter("VerifiedByDesignation", verifier == null ? " " : verifier.ImmediateHeadDesignation);
                p5 = new ReportParameter("DateOfIssue", DateTime.Now.ToShortDateString());
            }
            else
            {

                var preparedBy = book.Header.Logs.Where(x => x.Status == BookingStatus.FINALIZED).Select(x => x.Agent).FirstOrDefault();
                p1 = new ReportParameter("PreparedBy", preparedBy.Fullname);
                p2 = new ReportParameter("PreparedByDesignation", " ");

                p3 = new ReportParameter("VerifiedBy", bookingSignatories.ImmediateHeadName);
                p4 = new ReportParameter("VerifiedByDesignation", bookingSignatories.ImmediateHeadDesignation);
                p5 = new ReportParameter("DateOfIssue", bookingSignatories.DateCreated.ToShortDateString());

            }

            



            lr.SetParameters(new ReportParameter[] { p1, p2, p3, p4, p5 });



            string mimeType, encoding, extension;
            string[] streams;
            var renderedBytes = lr.Render
                (
                    "PDF",
                    @"<DeviceInfo><OutputFormat>PDF</OutputFormat><HumanReadablePDF>False</HumanReadablePDF></DeviceInfo>",
                    out mimeType,
                    out encoding,
                    out extension,
                    out streams,
                    out warnings
                );

            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader(
                "content-disposition",
                "inline; filename= filename" + "." + extension);
            Response.OutputStream.Write(renderedBytes, 0, renderedBytes.Length);
            Response.Flush();
            Response.End();
        }
    }
}