﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ISE.Reports
{
    public partial class Sales_Report : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            var auth = Models.AuthorizationGateway.GetAuthorizedInfo(null);

            if (auth == null)
            {
                Response.Clear();
                Response.Write("Access Denied");
                Response.StatusCode = 401;
                Response.End();
            }

            if (auth.Type == 1)
            {
                Response.Clear();
                Response.Write("Access Denied");
                Response.StatusCode = 401;
                Response.End();
            }

            var startDate = Request.QueryString["startDate"];
            var endDate = Request.QueryString["endDate"];
            var format = Request.QueryString["format"];

            //AppDomainSetup setup = new AppDomainSetup { ApplicationBase = Environment.CurrentDirectory, LoaderOptimization = LoaderOptimization.MultiDomainHost };
            //setup.SetCompatibilitySwitches(new[] { "NetFx40_LegacySecurityPolicy" });
            //AppDomain _casPolicyEnabledDomain = AppDomain.CreateDomain("Full Trust", null, setup);


            
                PermissionSet permissions = new PermissionSet(PermissionState.Unrestricted);

                if (!String.IsNullOrEmpty(startDate))
                {
                    DateTime sDate = Convert.ToDateTime(startDate);
                    DateTime eDate = Convert.ToDateTime(endDate);

                    ISE.DAL.Reporting rpt = new ISE.DAL.Reporting();
                    List<ISE.DAL.SalesModel> reports = rpt.GetSales(sDate, eDate, auth);
                    ISE.DAL.DestinationsModel destinationRepo = new DAL.DestinationsModel();

                    var lr = new LocalReport
                    {
                        //ReportPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\\Reports\\Sales Report.rdlc",
                        ReportEmbeddedResource = "ISE.Reports.Sales Report.rdlc",
                        EnableExternalImages = true
                       
                    };

                    lr.SetBasePermissionsForSandboxAppDomain(permissions);
                   // lr.ExecuteReportInSandboxAppDomain();
                  

                    // lr.DataSources.Add(new ReportDataSource("DestinationsModel", destinationRepo.GetDestinations()));
                    lr.DataSources.Add(new ReportDataSource("SalesDS", reports));

                    Warning[] warnings;



                    string mimeType, encoding, extension;
                    string[] streams;
                    string filename = "Sales Report " + startDate;

                    if (string.IsNullOrEmpty(format)) format = "PDF";


                    var renderedBytes = lr.Render
                        (
                            format,
                            @"<DeviceInfo><OutputFormat>PDF</OutputFormat><HumanReadablePDF>False</HumanReadablePDF></DeviceInfo>",
                            out mimeType,
                            out encoding,
                            out extension,
                            out streams,
                            out warnings
                        );

                    Response.Buffer = true;
                    Response.Clear();
                    Response.ContentType = mimeType;
                    Response.AddHeader(
                        "content-disposition",
                        "inline; filename= " + filename + "." + extension);
                    Response.OutputStream.Write(renderedBytes, 0, renderedBytes.Length);
                    Response.Flush();
                    Response.End();
                }

            
          





        }
    }
}