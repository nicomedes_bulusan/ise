﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ISE.Reports
{
    public partial class Aging_Summary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            var auth = Models.AuthorizationGateway.GetAuthorizedInfo(null);

            if (auth == null)
            {
                Response.Clear();
                Response.Write("Access Denied");
                Response.StatusCode = 401;
                Response.End();
            }

            if (auth.Type == 1)
            {
                Response.Clear();
                Response.Write("Access Denied");
                Response.StatusCode = 401;
                Response.End();
            }

          
            var format = Request.QueryString["format"];

              

                ISE.DAL.Reporting rpt = new ISE.DAL.Reporting();
                List<ISE.DAL.AccountsReceivableAgingSummary> reports = rpt.GetAgingSummary();
            
                var lr = new LocalReport
                {
                    //ReportPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\\Reports\\Aging Report Summary.rdlc",
                    ReportEmbeddedResource = "ISE.Reports.Aging Report Summary.rdlc",
                    EnableExternalImages = true
                };
               


               // lr.DataSources.Add(new ReportDataSource("DestinationsModel", destinationRepo.GetDestinations()));
                lr.DataSources.Add(new ReportDataSource("AccountsReceivableDS", reports));
               
                Warning[] warnings;

           

                string mimeType, encoding, extension;
                string[] streams;
                string filename = "Aging Summary ";

                if (string.IsNullOrEmpty(format)) format = "PDF";


                var renderedBytes = lr.Render
                    (
                        format,
                        @"<DeviceInfo><OutputFormat>PDF</OutputFormat><HumanReadablePDF>False</HumanReadablePDF></DeviceInfo>",
                        out mimeType,
                        out encoding,
                        out extension,
                        out streams,
                        out warnings
                    );

                Response.Buffer = true;
                Response.Clear();
                Response.ContentType = mimeType;
                Response.AddHeader(
                    "content-disposition",
                    "inline; filename= " + filename + "." + extension);
                Response.OutputStream.Write(renderedBytes, 0, renderedBytes.Length);
                Response.Flush();
                Response.End();
            


        }
    }
}