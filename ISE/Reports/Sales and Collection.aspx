﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Sales and Collection.aspx.cs" Inherits="ISE.Reports.Sales_and_Commission" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sales and Collection</title>
    <link rel="stylesheet" type="text/css" href="../Content/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="../Content/bootstrap.min.css" />

    <script type="text/javascript" src="../Scripts/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="../Scripts/moment.min.js"></script>
    <script type="text/javascript" src="../Scripts/daterangepicker.js"></script>


</head>
<body>
    <div class="container-fluid" style="padding: 0;">

        <div class="col-md-12" style="height: 20px; background-color: #588658; border: 1px solid #8FC393;">
            <label class="control-label" style="color: #fff;"><a href="/Booking/Reports">Reports</a> > Sales and Collection</label>
        </div>
        <div class="col-md-12" style="height: 60px; background-color: #8EC292; border: 1px solid #8FC393; padding-top: 15px;">
            <div class="col-md-9">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-2">Select Date:</label>
                        <div class="col-md-6">

                            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;    <span></span><b class="caret"></b>
                            </div>
                        </div>
                        <div class="checkbox col-md-2">
                            <input id="checkbox1" type="checkbox" name="allowWithoutPayment" />
                            <label for="checkbox1">
                                Without payments
                       
                            </label>
                        </div>
                    </div>
                </form>

            </div>
            <div class="col-md-3">
                <a href="#" class="btn btn-default" onclick="getReport()">View Report</a>


                <a href="#" class="btn btn-default" onclick="exportToExcel()">Export to Excel</a>
            </div>
        </div>
        <div class="col-md-12" style="height: 88vh; padding: 0;">
            <iframe src="" height="0" width="0" id="frameExport" hidden></iframe>
            <iframe src="" height="100%" width="100%" id="frameReport"></iframe>



        </div>








    </div>
    <script type="text/javascript">

        var startDate;
        var endDate;
        $(document).ready(function () {

            // $('.datepicker').Zebra_DatePicker();

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

                startDate = start.format('YYYY-MM-DD');
                endDate = end.format('YYYY-MM-DD');

            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                "alwaysShowCalendars": true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

            $('#reportrange').on('apply.daterangepicker', function (ev, picker) {

                startDate = picker.startDate.format('YYYY-MM-DD');
                endDate = picker.endDate.format('YYYY-MM-DD');
            });


        });

        function getReport() {



            console.log(startDate)
            console.log(endDate)

            $('#frameReport').attr("src", "/Reports/Sales and Collection.aspx?startDate=" + startDate + "&endDate=" + endDate +"&allowWithoutPayment="+$('#checkbox1').is(':checked'));


        }

        function exportToExcel() {
            console.log(startDate)
            console.log(endDate)

            $('#frameReport').attr("src", "/Reports/Sales and Collection.aspx?startDate=" + startDate + "&endDate=" + endDate + "&format=EXCEL" + "&allowWithoutPayment=" + $('#checkbox1').is(':checked'));
        }

    </script>
</body>
</html>
