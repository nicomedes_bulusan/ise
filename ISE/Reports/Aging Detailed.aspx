﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Aging Detailed.aspx.cs" Inherits="ISE.Reports.Aging_Detailed" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Aging Detailed</title>
    <link rel="stylesheet" type="text/css" href="../Content/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="../Content/bootstrap.min.css" />

    <script type="text/javascript" src="../Scripts/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="../Scripts/moment.min.js"></script>
    <script type="text/javascript" src="../Scripts/daterangepicker.js"></script>


</head>
<body>
    <div class="container-fluid" style="padding: 0;">

        <div class="col-md-12" style="height: 20px; background-color: #588658; border: 1px solid #8FC393;">
            <label class="control-label" style="color: #fff;"><a href="/Booking/Reports">Reports</a> > Aging Detailed</label>
        </div>
        <div class="col-md-12" style="height: 60px; background-color: #8EC292; border: 1px solid #8FC393; padding-top: 15px;">
          
        </div>
        <div class="col-md-12" style="height: 88vh; padding: 0;">
            <iframe src="" height="0" width="0" id="frameExport" hidden></iframe>
            <iframe src="" height="100%" width="100%" id="frameReport"></iframe>



        </div>








    </div>
    <script type="text/javascript">

        var startDate;
        var endDate;
        $(document).ready(function () {

            // $('.datepicker').Zebra_DatePicker();

            var start = moment().subtract(29, 'days');
            var end = moment();

          

        });

        function getReport() {
          
            

            console.log(startDate)
            console.log(endDate)

            $('#frameReport').attr("src", "/Reports/Aging Detailed.aspx");


        }

        function exportToExcel() {
            console.log(startDate)
            console.log(endDate)

            ('#frameReport').attr("src", "/Reports/Aging Detailed.aspx?format=EXCEL");
        }

    </script>
</body>
</html>
