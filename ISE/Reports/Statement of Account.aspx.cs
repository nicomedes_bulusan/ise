﻿using ISE.DAL;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ISE.Reports
{
    public partial class Statement_of_Account : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SystemSignatoriesModel signatoryRepo = new SystemSignatoriesModel();
            AgentsModel agentRepo = new AgentsModel();
            DestinationsModel destinationRepo = new DestinationsModel();

            SystemSignatoriesModel bookingSignatories = new SystemSignatoriesModel();

            var lr = new LocalReport
            {
                // ReportPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\\Reports\\Statement of Account.rdlc",
                ReportEmbeddedResource = "ISE.Reports.Statement of Account.rdlc",
                EnableExternalImages = true,
                EnableHyperlinks = true

            };

            string refereceID = "";
            if (Request.QueryString["id"] != "")
            {
                refereceID = Request.QueryString["id"];
            }

            if (refereceID == "")
            {
                Response.Write("Booking ID is is invalid!");
                Response.End();
            }

            ISE.DAL.Bookings bookeRepo = new DAL.Bookings();
            ISE.DAL.Bookings book = new DAL.Bookings();

            try
            {
                book = bookeRepo.GetBookingByCVNumber(refereceID);

                if (book.Header == null) throw new Exception("Booking not found!");

                bookingSignatories = signatoryRepo.GetSOASignatories(book.Header.ID);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
                Response.End();
            }

            var bookingAgent = agentRepo.FindAgentByID(book.Header.CreatedBy);

            lr.DataSources.Add(new ReportDataSource("agents", new List<ISE.DAL.AgentsModel>() { bookingAgent }));
            lr.DataSources.Add(new ReportDataSource("Bookings", new List<ISE.DAL.BookingHeaderModel>() { book.Header }));
            lr.DataSources.Add(new ReportDataSource("destinations", destinationRepo.GetDestinations()));
            lr.DataSources.Add(new ReportDataSource("bookingdetails", book.Details));
            Warning[] warnings;


            SystemSignatoriesModel preparer = new SystemSignatoriesModel();
            SystemSignatoriesModel verifier = new SystemSignatoriesModel();
            ReportParameter p1 = new ReportParameter();
            ReportParameter p2 = new ReportParameter();
            ReportParameter p3 = new ReportParameter();
            ReportParameter p4 = new ReportParameter();

            if (bookingSignatories == null)
            {
             
                verifier = signatoryRepo.GetHighestSignatory(bookingAgent.Location, "SOA");
                var preparedBy = book.Header.Logs.Where(x => x.Status == BookingStatus.FINALIZED).Select(x => x.Agent).FirstOrDefault();
                verifier = signatoryRepo.GetHighestSignatory(bookingAgent.Location, "SV");

                if (verifier == null) throw new Exception("Signatories are missing!");


                verifier.AgentName = preparedBy.Fullname;
                verifier.Designation = " ";

           

                preparer.AddSOASignatories(verifier, book.Header.ID);


                p1 = new ReportParameter("PreparedBy", preparedBy.Fullname);
                p2 = new ReportParameter("PreparedByDesignation",  verifier.Designation);
                p3 = new ReportParameter("VerifiedBy", verifier == null ? " " : verifier.ImmediateHeadName);
                p4 = new ReportParameter("VerifiedByDesignation", verifier == null ? " " : verifier.ImmediateHeadDesignation);

            }
            else
            {

                var preparedBy = book.Header.Logs.Where(x => x.Status == BookingStatus.FINALIZED).Select(x => x.Agent).FirstOrDefault();
                p1 = new ReportParameter("PreparedBy", preparedBy.Fullname);
                p2 = new ReportParameter("PreparedByDesignation", " ");

                p3 = new ReportParameter("VerifiedBy", bookingSignatories.ImmediateHeadName);
                p4 = new ReportParameter("VerifiedByDesignation", bookingSignatories.ImmediateHeadDesignation);

            }


       

            lr.SetParameters(new ReportParameter[] { p1, p2, p3, p4 });

            string mimeType, encoding, extension;
            string[] streams;
            var renderedBytes = lr.Render
                (
                    "PDF",
                    @"<DeviceInfo><OutputFormat>PDF</OutputFormat><HumanReadablePDF>False</HumanReadablePDF></DeviceInfo>",
                    out mimeType,
                    out encoding,
                    out extension,
                    out streams,
                    out warnings
                );

            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader(
                "content-disposition",
                "inline; filename= filename" + "." + extension);
            Response.OutputStream.Write(renderedBytes, 0, renderedBytes.Length);
            Response.Flush();
            Response.End();
        }
    }
}