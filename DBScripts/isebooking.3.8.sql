ALTER TABLE destinations
ADD COLUMN `IsActive`  bit NOT NULL DEFAULT 1 AFTER `Code`;



ALTER TABLE `billingperiods`
ADD COLUMN `EWT`  double NOT NULL DEFAULT 0 AFTER `BatchID`,
ADD COLUMN `GrossTotal`  double NOT NULL DEFAULT 0 AFTER `EWT`,
ADD COLUMN `NetTotal`  double NOT NULL DEFAULT 0 AFTER `GrossTotal`;

CREATE TABLE `paymentlogs` (
  `ID` int(11) NOT NULL,
  `BillingPeriodID` int(11) NOT NULL,
  `PaymentDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `PostedBy` int(11) NOT NULL,
  `Remarks` varchar(255) NOT NULL,
  `Amount` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BillingPeriodID` (`BillingPeriodID`),
  KEY `PostedBy` (`PostedBy`),
  CONSTRAINT `paymentlogs_ibfk_1` FOREIGN KEY (`BillingPeriodID`) REFERENCES `billingperiods` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `paymentlogs_ibfk_2` FOREIGN KEY (`PostedBy`) REFERENCES `agents` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


update destinations SET IsActive=0 where Code='KLO';