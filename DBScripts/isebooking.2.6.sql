ALTER TABLE `agents`
MODIFY COLUMN `IsActive`  bit(11) NOT NULL DEFAULT 1 AFTER `MonthToDateAmount`;

ALTER TABLE `ratematrix`
MODIFY COLUMN `Status`  bit(1) NOT NULL DEFAULT 1 AFTER `DateLastUpdated`;

ALTER TABLE `ratematrixagents`
MODIFY COLUMN `Status`  bit(1) NOT NULL DEFAULT 1 AFTER `DateLastUpdated`;
