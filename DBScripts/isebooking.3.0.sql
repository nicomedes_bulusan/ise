ALTER TABLE `bookings`
MODIFY COLUMN `PickUpArrival`  int(11) NULL AFTER `Routing`,
MODIFY COLUMN `DropOffArrival`  int(11) NULL AFTER `PickUpArrival`,
MODIFY COLUMN `FlightDate2`  datetime NULL DEFAULT NULL AFTER `FlightDate1`,
MODIFY COLUMN `FlightDate1`  datetime NULL AFTER `FlightNo2`;

update bookings set FlightDate2=null where Routing=1;
update bookings set DropOffLocation1=(select r.ID from resortsandhotels r where r.`Name` LIKE DropOffLocation1 LIMIT 1);
update bookings set DropOffLocation2=(select r.ID from resortsandhotels r where r.`Name` LIKE DropOffLocation2 LIMIT 1);

ALTER TABLE `bookings`
MODIFY COLUMN `DropOffLocation1`  int(11) NULL DEFAULT NULL AFTER `ChildrensBelowSix`,
MODIFY COLUMN `DropOffLocation2`  int(11) NULL DEFAULT NULL AFTER `DropOffLocation1`;

ALTER TABLE `bookings` ADD FOREIGN KEY (`DropOffLocation2`) REFERENCES `resortsandhotels` (`ID`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `bookings` ADD FOREIGN KEY (`DropOffLocation1`) REFERENCES `resortsandhotels` (`ID`) ON DELETE RESTRICT ON UPDATE CASCADE;

DROP TABLE ManifestoDetails;
DROP TABLE Manifesto;

CREATE TABLE `manifesto` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BookingID` int(11) NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Pax` int(11) NOT NULL,
  `ResortHotel` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `Pickup` int(11) NOT NULL,
  `DropOff` int(11) NOT NULL,
  `Adults` int(11) NOT NULL DEFAULT '0',
  `Kids` int(11) NOT NULL DEFAULT '0',
  `Infants` int(11) NOT NULL DEFAULT '0',
  `Type` int(11) NOT NULL,
  `FlightNo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `BookingID` (`BookingID`),
  KEY `Pickup` (`Pickup`),
  KEY `DropOff` (`DropOff`),
  KEY `ResortHotel` (`ResortHotel`),
  CONSTRAINT `manifesto_ibfk_1` FOREIGN KEY (`BookingID`) REFERENCES `bookings` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `manifesto_ibfk_2` FOREIGN KEY (`Pickup`) REFERENCES `destinations` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `manifesto_ibfk_3` FOREIGN KEY (`DropOff`) REFERENCES `destinations` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `manifesto_ibfk_4` FOREIGN KEY (`ResortHotel`) REFERENCES `resortsandhotels` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

CREATE TABLE `manifestodetails` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ManifestoID` int(11) NOT NULL,
  `GuestName` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ManifestoID` (`ManifestoID`),
  CONSTRAINT `manifestodetails_ibfk_1` FOREIGN KEY (`ManifestoID`) REFERENCES `manifesto` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;



