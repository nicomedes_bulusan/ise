ALTER TABLE `manifestodetails` DROP FOREIGN KEY `manifestodetails_ibfk_1`;

ALTER TABLE `manifestodetails` ADD CONSTRAINT `manifestodetails_ibfk_1` FOREIGN KEY (`ManifestoID`) REFERENCES `manifesto` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

