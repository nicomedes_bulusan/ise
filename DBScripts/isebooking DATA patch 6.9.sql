/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100108
Source Host           : localhost:3306
Source Database       : isebooking1116

Target Server Type    : MYSQL
Target Server Version : 100108
File Encoding         : 65001

Date: 2017-12-18 17:47:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Procedure structure for getAllUnpaidBookings
-- ----------------------------
DROP PROCEDURE IF EXISTS `getAllUnpaidBookings`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAllUnpaidBookings`()
    READS SQL DATA
SELECT book.ID FROM bookings book INNER JOIN
 (
select 
je.BookingID,SUM(je.Payments) Payments
from payments pay 
INNER JOIN journalentries je on pay.ID = je.PaymentID
WHERE pay.`Status`<>2
and je.Payments > 0 and je.BookingID is not null
GROUP BY je.BookingID
) AS CTE ON CTE.BookingID = book.ID 
WHERE
 CTE.Payments <> book.Total
;;
DELIMITER ;
