UPDATE bookings book
INNER JOIN journalentries je on book.ID = je.BookingID
INNER JOIN payments pay on je.PaymentID = pay.ID
SET book.IsPaid=0
where BookingID is not null and pay.`Status`=2 and book.IsPaid=1;