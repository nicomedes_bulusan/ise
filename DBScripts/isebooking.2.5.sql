CREATE TABLE `resortsandhotels` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IsActive` bit(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `businesslocations` (
  `Code` varchar(255) NOT NULL,
  `Name` varchar(255) NOT NULL,
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `isebooking`.`BusinessLocations` (`Code`, `Name`) VALUES ('BOR', 'BORACAY');
INSERT INTO `isebooking`.`BusinessLocations` (`Code`, `Name`) VALUES ('MNL', 'MANILA');



ALTER TABLE `agents` ADD COLUMN `Location` VARCHAR(255) NULL DEFAULT 'BOR' AFTER `MarketTypeID`;

UPDATE `agents` SET `Location`='BOR';

ALTER TABLE `agents` ADD FOREIGN KEY (`Location`) REFERENCES `BusinessLocations` (`Code`) ON DELETE RESTRICT ON UPDATE CASCADE;
