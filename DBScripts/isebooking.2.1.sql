
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for manifesto
-- ----------------------------
DROP TABLE IF EXISTS `manifesto`;
CREATE TABLE `manifesto` (
`ID`  int(11) NOT NULL AUTO_INCREMENT ,
`BookingID`  int(11) NOT NULL ,
`DateCreated`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ,
`DateLastUpdated`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
`Pax`  int(11) NOT NULL ,
`ResortHotel`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`ArrivalDate`  datetime NOT NULL ,
`DepartureDate`  datetime NOT NULL ,
`Remarks`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
PRIMARY KEY (`ID`),
FOREIGN KEY (`BookingID`) REFERENCES `bookings` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
INDEX `BookingID` (`BookingID`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=2

;

-- ----------------------------
-- Table structure for manifestodetails
-- ----------------------------
DROP TABLE IF EXISTS `manifestodetails`;
CREATE TABLE `manifestodetails` (
`ID`  int(11) NOT NULL AUTO_INCREMENT ,
`ManifestoID`  int(11) NOT NULL ,
`GuestName`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
PRIMARY KEY (`ID`),
FOREIGN KEY (`ManifestoID`) REFERENCES `manifesto` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
INDEX `ManifestoID` (`ManifestoID`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=14
;

ALTER TABLE `bookings`
MODIFY COLUMN `Senior`  int(11) NOT NULL DEFAULT 0 AFTER `CVDate`;