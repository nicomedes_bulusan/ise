CREATE TABLE `travelagencies` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IsActive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `travelagencies`
MODIFY COLUMN `ID`  int(11) NOT NULL AUTO_INCREMENT FIRST ;

ALTER TABLE `agents`
MODIFY COLUMN `LastName`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL AFTER `MiddleName`;

ALTER TABLE `agents`
MODIFY COLUMN `IsActive`  bit(1) NOT NULL DEFAULT b'1' AFTER `MonthToDateAmount`;

update `agents` set `LastName`='' where TRIM(`FirstName`)=TRIM(`LastName`);
update `agents` set `Company`='NA' where `Company` is NULL;

INSERT INTO travelagencies(Name,DateCreated,DateLastUpdated,IsActive)
select DISTINCT TRIM(UCASE(Company)),NOW(),NOW(),1 from agents ORDER BY Company;

ALTER TABLE `agents`
ADD COLUMN `TravelAgencyID`  int NULL AFTER `Location`;

ALTER TABLE `agents` ADD FOREIGN KEY (`TravelAgencyID`) REFERENCES `travelagencies` (`ID`) ON DELETE RESTRICT ON UPDATE CASCADE;

UPDATE agents SET TravelAgencyID = (select tra.ID from travelagencies tra where tra.Name=TRIM(UCASE(agents.Company)) LIMIT 1);


ALTER TABLE `ratematrixagents` DROP FOREIGN KEY `ratematrixagents_ibfk_3`;

UPDATE ratematrixagents set AgentID = (select TravelAgencyID from agents agent where agent.ID = AgentID LIMIT 1);

ALTER TABLE `ratematrixagents`
CHANGE COLUMN `AgentID` `TravelAgencyID`  int(11) NOT NULL AFTER `ToDate`;


ALTER TABLE `ratematrixagents` ADD FOREIGN KEY (`TravelAgencyID`) REFERENCES `travelagencies` (`ID`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `agents` ADD FOREIGN KEY (`TravelAgencyID`) REFERENCES `travelagencies` (`ID`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `bookings`
ADD COLUMN `TravelAgencyID`  int NOT NULL DEFAULT 0 AFTER `Class`;

update bookings set TravelAgencyID = (select agent.TravelAgencyID from agents agent where agent.ID=bookings.CreatedBy limit 1);

ALTER TABLE `agents`
MODIFY COLUMN `LastName`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL AFTER `MiddleName`;


