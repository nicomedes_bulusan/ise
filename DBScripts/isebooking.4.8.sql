ALTER TABLE `payments`
ADD COLUMN `Status`  int(255) NOT NULL DEFAULT 0 AFTER `Remarks`;

ALTER TABLE `journalentries`
ADD UNIQUE INDEX (`AccountingCode`, `BookingID`, `Type`, `PaymentID`) USING BTREE ;

CREATE TABLE `paymentcounter` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DateCreated` datetime NOT NULL,
  `Series` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `paymentCounter` (`ID`, `DateCreated`, `Series`) VALUES ('1', '2017-08-30 10:27:31', '1000');

