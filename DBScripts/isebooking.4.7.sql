ALTER TABLE `journalentriesreferences`
ADD COLUMN `LocationCode`  varchar(255) NOT NULL AFTER `DateCreated`;

ALTER TABLE `journalentriesreferences` ADD FOREIGN KEY (`LocationCode`) REFERENCES `businesslocations` (`Code`) ON UPDATE CASCADE;

ALTER TABLE `journalentriesreferences`
DROP INDEX `ReferenceType` ,
ADD UNIQUE INDEX `ReferenceType` (`ReferenceType`, `ReferenceNo`, `LocationCode`) USING BTREE ;

ALTER TABLE `journalentriesreferences`
ADD COLUMN `JournalID`  int NOT NULL AFTER `LocationCode`;

