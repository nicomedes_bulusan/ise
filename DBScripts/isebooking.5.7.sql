ALTER TABLE `payments`
MODIFY COLUMN `PaymentDate`  datetime NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `PaymentNo`;

ALTER TABLE `journalentries`
MODIFY COLUMN `Amount`  double NOT NULL AFTER `BookingID`;

ALTER TABLE `sales`
MODIFY COLUMN `TransactionDate`  datetime NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `BookingID`;
