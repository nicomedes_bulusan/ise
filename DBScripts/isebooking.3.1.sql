CREATE FUNCTION `TruncateTime`(dateValue DateTime) RETURNS date
return Date(dateValue);

DROP TABLE `manifestodetails`;

CREATE TABLE `manifestodetails` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ManifestoID` int(11) NOT NULL,
  `GuestName` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ManifestoID` (`ManifestoID`),
  CONSTRAINT `manifestodetails_ibfk_1` FOREIGN KEY (`ManifestoID`) REFERENCES `manifesto` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;


DROPTABLE `manifesto`;

CREATE TABLE `manifesto` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BookingID` int(11) NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Pax` int(11) NOT NULL,
  `ResortHotel` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `Pickup` int(11) NOT NULL,
  `DropOff` int(11) NOT NULL,
  `Adults` int(11) NOT NULL DEFAULT '0',
  `Kids` int(11) NOT NULL DEFAULT '0',
  `Infants` int(11) NOT NULL DEFAULT '0',
  `Type` int(11) NOT NULL,
  `FlightNo` varchar(255) DEFAULT NULL,
  `TourGuides` int(11) NOT NULL DEFAULT '0',
  `Escorts` int(11) NOT NULL DEFAULT '0',
  `FOC` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `BookingID` (`BookingID`),
  KEY `Pickup` (`Pickup`),
  KEY `DropOff` (`DropOff`),
  KEY `ResortHotel` (`ResortHotel`),
  CONSTRAINT `manifesto_ibfk_1` FOREIGN KEY (`BookingID`) REFERENCES `bookings` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `manifesto_ibfk_2` FOREIGN KEY (`Pickup`) REFERENCES `destinations` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `manifesto_ibfk_3` FOREIGN KEY (`DropOff`) REFERENCES `destinations` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `manifesto_ibfk_4` FOREIGN KEY (`ResortHotel`) REFERENCES `resortsandhotels` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;