/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100108
Source Host           : localhost:3306
Source Database       : isebooking0911

Target Server Type    : MYSQL
Target Server Version : 100108
File Encoding         : 65001

Date: 2017-09-18 14:54:42
*/

SET FOREIGN_KEY_CHECKS=0;

ALTER TABLE `journalentriesreferences`
DROP INDEX `ReferenceType`,
ADD INDEX (`ReferenceNo`) USING BTREE ,
ADD INDEX (`ReferenceType`) USING BTREE ;



-- ----------------------------
-- Procedure structure for rptCashReceiptsORAR
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptCashReceiptsORAR`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptCashReceiptsORAR`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	

select 
tra.ID as TravelAgencyID,
tra.`Name` as TravelAgency,
pay.PaymentDate,
journal.ReceiptNo,
journal.ReferenceType,
acct.`Code` as AccountCode,
acct.Description as AccountName,
journal.Amount,
pay.Remarks,
pay.BusinessLocationCode as DivisionCode
 from payments pay
INNER JOIN journalentries journal on pay.ID = journal.PaymentID
INNER JOIN accountingterms acct on journal.AccountingCode = acct.`Code`
INNER JOIN travelagencies tra on pay.TravelAgencyID = tra.ID
WHERE (journal.ReferenceType=1 || journal.ReferenceType=3)
AND 
	fromDate <=  TruncateTime(pay.PaymentDate)
AND
toDate >=  TruncateTime(pay.PaymentDate);


END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptCashReceiptsPR
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptCashReceiptsPR`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptCashReceiptsPR`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	

select 
tra.ID as TravelAgencyID,
tra.`Name` as TravelAgency,
pay.PaymentDate,
journal.ReceiptNo,
journal.ReferenceType,
acct.`Code` as AccountCode,
acct.Description as AccountName,
journal.Amount,
pay.Remarks,
pay.BusinessLocationCode as DivisionCode
 from payments pay
INNER JOIN journalentries journal on pay.ID = journal.PaymentID
INNER JOIN accountingterms acct on journal.AccountingCode = acct.`Code`
INNER JOIN travelagencies tra on pay.TravelAgencyID = tra.ID
WHERE journal.ReferenceType=2
AND 
	fromDate <=  TruncateTime(pay.PaymentDate)
AND
toDate >=  TruncateTime(pay.PaymentDate);



END
;;
DELIMITER ;
