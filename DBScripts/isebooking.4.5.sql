CREATE TABLE `journalentriesreferences` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ReferenceType` int(11) NOT NULL,
  `ReferenceNo` varchar(255) NOT NULL,
  `DateCreated` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ReferenceType` (`ReferenceType`,`ReferenceNo`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `bookingdetails`
ADD COLUMN `SourceID`  int NULL AFTER `Boat`;

UPDATE bookingdetails SET SourceID=4 where Description IN ('TF IN','TF OUT');
UPDATE bookingdetails SET SourceID=2 where Description LIKE '%ENVIRONMENTAL%';

UPDATE bookingdetails SET SourceID=1 WHERE
Description LIKE 'Caticlan Jetty Port - Kalibo International Airport%' OR
Description LIKE 'Kalibo International Airport - Caticlan Jetty Port%' OR
Description LIKE 'Caticlan Jetty Port - Resort%' OR
Description LIKE 'Resort - Caticlan Jetty Port%' OR
Description LIKE 'Caticlan Jetty Port - Cagban Port - Boracay%' OR
Description LIKE 'Cagban Port - Boracay - Caticlan Jetty Port%' OR
Description LIKE 'Caticlan Jetty Port - Caticlan Airport%' OR
Description LIKE 'Caticlan Airport - Caticlan Jetty Port%' OR
Description LIKE 'Caticlan Jetty Port - OTHERS%' OR
Description LIKE 'OTHERS - Caticlan Jetty Port%' OR
Description LIKE 'Kalibo International Airport - Resort%' OR
Description LIKE 'Resort - Kalibo International Airport%' OR
Description LIKE 'Kalibo International Airport - Cagban Port - Boracay%' OR
Description LIKE 'Cagban Port - Boracay - Kalibo International Airport%' OR
Description LIKE 'Kalibo International Airport - Caticlan Airport%' OR
Description LIKE 'Caticlan Airport - Kalibo International Airport%' OR
Description LIKE 'Kalibo International Airport - OTHERS%' OR
Description LIKE 'OTHERS - Kalibo International Airport%' OR
Description LIKE 'Resort - Cagban Port - Boracay%' OR
Description LIKE 'Cagban Port - Boracay - Resort%' OR
Description LIKE 'Resort - Caticlan Airport%' OR
Description LIKE 'Caticlan Airport - Resort%' OR
Description LIKE 'Resort - OTHERS%' OR
Description LIKE 'OTHERS - Resort%' OR
Description LIKE 'Cagban Port - Boracay - Caticlan Airport%' OR
Description LIKE 'Caticlan Airport - Cagban Port - Boracay%' OR
Description LIKE 'Cagban Port - Boracay - OTHERS%' OR
Description LIKE 'OTHERS - Cagban Port - Boracay%' OR
Description LIKE 'Caticlan Airport - OTHERS%' OR
Description LIKE 'OTHERS - Caticlan Airport%';

UPDATE bookingdetails SET SourceID=1 WHERE
Description LIKE 'Free-of-charge%';

UPDATE bookingdetails SET SourceID=5 WHERE
Description LIKE 'NIGHT NAVIGATION FEE%';

UPDATE bookingdetails SET SourceID=1 WHERE
Description LIKE 'KALIBO AIRPORT - RESORT%' OR
Description LIKE 'CATICLAN AIRPORT  - RESORT%' OR
Description LIKE 'CATICLAN JETTYPORT - KALIBO AIRPORT%' OR
Description LIKE 'CATICLAN JETTYPORT - RESORT%' OR
Description LIKE 'KALIBO AIRPORT - RESORT%' OR
Description LIKE 'KALIBO INTERNATIONAL AIRPORT%';

UPDATE bookingdetails SET SourceID=3 WHERE
Description LIKE 'BOAT%';

UPDATE bookingdetails SET SourceID=5 WHERE
Description LIKE 'NIGHT NAVIGATION%';

UPDATE bookingdetails SET SourceID=1 WHERE
Description LIKE 'RESORT - KALIBO AIRPORT%';

UPDATE bookingdetails SET SourceID=4 WHERE
Description LIKE 'TERMINAL FEE%'  OR
Description LIKE 'TERMINAL OUT%' OR
Description LIKE 'TF%';

UPDATE bookingdetails set SourceID=5 where Description like 'NIGHT%';

UPDATE bookingdetails set SourceID=1 where 
Description like 'RESORT-CATICLAN AIRPORT%' OR
Description like 'RESORT-KALIBO INTERNATIONAL AIRPORT%';


UPDATE bookingdetails set SourceID=6 where SourceID is null;

