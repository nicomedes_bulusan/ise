ALTER TABLE `agents`
MODIFY COLUMN `TravelAgencyID`  int(11) NOT NULL AFTER `Location`;

ALTER TABLE `ratematrixagents`
MODIFY COLUMN `Status`  bit(1) NOT NULL DEFAULT 1 AFTER `DateLastUpdated`;

ALTER TABLE `ratematrix`
MODIFY COLUMN `Status`  bit(1) NOT NULL DEFAULT 1 AFTER `DateLastUpdated`;

ALTER TABLE `agents` DROP FOREIGN KEY `agents_ibfk_3`;

ALTER TABLE `agents` DROP FOREIGN KEY `agents_ibfk_4`;

ALTER TABLE `agents`
MODIFY COLUMN `TravelAgencyID`  int(11) NOT NULL AFTER `Location`;

ALTER TABLE `agents` ADD FOREIGN KEY (`TravelAgencyID`) REFERENCES `travelagencies` (`ID`) ON UPDATE CASCADE;

CREATE TABLE `reset_login` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AgentID` int(11) NOT NULL,
  `Token` varchar(255) NOT NULL,
  `IsExpired` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`ID`),
  KEY `reset_login_ibfk_1` (`AgentID`),
  CONSTRAINT `reset_login_ibfk_1` FOREIGN KEY (`AgentID`) REFERENCES `agents` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;


