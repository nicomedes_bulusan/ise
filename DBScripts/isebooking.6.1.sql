/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100108
Source Host           : localhost:3306
Source Database       : isebooking1002

Target Server Type    : MYSQL
Target Server Version : 100108
File Encoding         : 65001

Date: 2017-10-06 11:30:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Procedure structure for rptAccountsReceivableAging
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptAccountsReceivableAging`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptAccountsReceivableAging`()
    READS SQL DATA
BEGIN

SELECT 
ag.TravelAgencyID,
book.ID as BookingID,
book.CVNumber,
book.GroupName as AccountName,
tra.`Name`,
	TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	) AS TransactionDate,
SUM(ar.Balance) as Balance
 from bookings book
INNER  JOIN accountreceivables ar on book.ID = ar.BookingID
INNER JOIN agents ag on book.CreatedBy = ag.ID
INNER JOIN travelagencies tra ON ag.TravelAgencyID = tra.ID
WHERE book.IsPaid =false AND book.PaymentTypeID=1

GROUP BY book.ID;


END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptCashReceiptsORAR
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptCashReceiptsORAR`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptCashReceiptsORAR`(IN `fromDate` date,IN `toDate` date,IN `businessLoc` VARCHAR(255))
    READS SQL DATA
BEGIN


select 
tra.ID as TravelAgencyID,
tra.`Name` as TravelAgency,
pay.PaymentDate,
journal.ReceiptNo,
journal.ReferenceType,
acct.`Code` as AccountCode,
acct.Description as AccountName,
journal.Amount,
pay.Remarks,
pay.BusinessLocationCode as DivisionCode
 from payments pay
INNER JOIN journalentries journal on pay.ID = journal.PaymentID
INNER JOIN accountingterms acct on journal.AccountingCode = acct.`Code`
INNER JOIN travelagencies tra on pay.TravelAgencyID = tra.ID
WHERE (journal.ReferenceType=1 || journal.ReferenceType=3)
AND pay.BusinessLocationCode =`businessLoc` 
AND 
	fromDate <=  TruncateTime(pay.PaymentDate)
AND
toDate >=  TruncateTime(pay.PaymentDate);


END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptCashReceiptsPR
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptCashReceiptsPR`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptCashReceiptsPR`(IN `fromDate` date,IN `toDate` date,IN `businessLoc` VARCHAR(255))
    READS SQL DATA
BEGIN

select 
tra.ID as TravelAgencyID,
tra.`Name` as TravelAgency,
pay.PaymentDate,
journal.ReceiptNo,
journal.ReferenceType,
acct.`Code` as AccountCode,
acct.Description as AccountName,
journal.Amount,
pay.Remarks,
pay.BusinessLocationCode as DivisionCode
 from payments pay
INNER JOIN journalentries journal on pay.ID = journal.PaymentID
INNER JOIN accountingterms acct on journal.AccountingCode = acct.`Code`
INNER JOIN travelagencies tra on pay.TravelAgencyID = tra.ID
WHERE journal.ReferenceType=2
AND pay.BusinessLocationCode =`businessLoc`
AND 
	fromDate <=  TruncateTime(pay.PaymentDate)
AND
toDate >=  TruncateTime(pay.PaymentDate);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptSalesCommission
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptSalesCommission`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptSalesCommission`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	

SELECT
DISTINCTROW
book.GroupName as AccountName,
	book.CVNumber,
	book.Total as Sales,
	TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	) AS TransactionDate,
	CONCAT(agent2.FirstName," ",agent2.LastName) as Agent,
	tra.`Name` AS TravelAgency,
pay.PaymentDate as CollectionDate,
pay.PaymentNo as PaymentReferenceNo,
je.Payments as Collected,
(select  group_concat(DISTINCT (CONCAT((CASE j1.ReferenceType WHEN 1 THEN 'OR' WHEN 2 then 'PR' WHEN 3 THEN 'AR' END),j1.ReceiptNo))SEPARATOR ' ' ) from journalentries j1 where j1.PaymentID=pay.ID AND j1.BookingID IS NULL) AS ReceiptNo
FROM
	bookings book
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
INNER JOIN journalentries je on book.ID = je.BookingID
INNER JOIN payments pay on je.PaymentID = pay.ID
INNER JOIN bookinglogs bkl on book.ID = bkl.BookingID
INNER JOIN agents agent2 on bkl.AgentID = agent2.ID
WHERE
	fromDate <= TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	)
AND toDate >= TruncateTime (
	LEAST(
		IFNULL(
			book.FlightDate1,
			book.FlightDate2
		),
		IFNULL(
			book.FlightDate2,
			book.FlightDate1
		)
	)
)
AND book.`Status` = 2
AND bkl.`Status` =2
AND pay.`Status` =1
ORDER BY book.ID;


END
;;
DELIMITER ;
