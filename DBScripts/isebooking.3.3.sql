ALTER TABLE `bookings`
MODIFY COLUMN `PickUpArrival`  int(11) NULL AFTER `Routing`,
MODIFY COLUMN `DropOffArrival`  int(11) NULL AFTER `PickUpArrival`,
MODIFY COLUMN `FlightDate1`  datetime NULL AFTER `FlightNo2`;