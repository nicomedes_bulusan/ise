DROP PROCEDURE IF EXISTS `rptSalesCommission`;

CREATE DEFINER = `root`@`localhost` PROCEDURE `rptSalesCommission`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	

SELECT
DISTINCTROW
book.GroupName as AccountName,
	book.CVNumber,
	book.Total as Sales,
	TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	) AS TransactionDate,
	CONCAT(agent2.FirstName," ",agent2.LastName) as Agent,
	tra.`Name` AS TravelAgency,
pay.PaymentDate as CollectionDate,
pay.PaymentNo as PaymentReferenceNo,
je.Payments as Collected,
(select  group_concat(DISTINCT (CONCAT((CASE j1.ReferenceType WHEN 1 THEN 'OR' WHEN 2 then 'PR' WHEN 3 THEN 'AR' END),j1.ReceiptNo))SEPARATOR ' ' ) from journalentries j1 where j1.PaymentID=pay.ID AND j1.BookingID IS NULL) AS ReceiptNo
FROM
	bookings book
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
INNER JOIN journalentries je on book.ID = je.BookingID
INNER JOIN payments pay on je.PaymentID = pay.ID
INNER JOIN bookinglogs bkl on book.ID = bkl.BookingID
INNER JOIN agents agent2 on bkl.AgentID = agent2.ID
WHERE
	fromDate <= TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	)
AND toDate >= TruncateTime (
	LEAST(
		IFNULL(
			book.FlightDate1,
			book.FlightDate2
		),
		IFNULL(
			book.FlightDate2,
			book.FlightDate1
		)
	)
)
AND book.`Status` = 2
AND bkl.`Status` =2
#AND pay.`Status` =1
ORDER BY book.ID;


END;