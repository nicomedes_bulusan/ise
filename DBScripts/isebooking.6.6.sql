/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100108
Source Host           : localhost:3306
Source Database       : isebooking1002

Target Server Type    : MYSQL
Target Server Version : 100108
File Encoding         : 65001

Date: 2017-10-19 13:19:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Procedure structure for rptSalesVsPayments
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptSalesVsPayments`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptSalesVsPayments`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	

SELECT

book.GroupName as AccountName,
	book.CVNumber,
	book.Total as Sales,
	TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	) AS TransactionDate,
	CONCAT(agent2.FirstName," ",agent2.LastName) as Agent,
	tra.`Name` AS TravelAgency,
pay.PaymentDate as CollectionDate,
pay.PaymentNo as PaymentReferenceNo,
je.Amount as Collected,
(select  group_concat(DISTINCT (CONCAT((CASE j1.ReferenceType WHEN 1 THEN 'OR' WHEN 2 then 'PR' WHEN 3 THEN 'AR' END),j1.ReceiptNo))SEPARATOR ' ' ) from journalentries j1 where j1.PaymentID=pay.ID AND j1.BookingID IS NULL) AS ReceiptNo
FROM
	bookings book
INNER JOIN agents agent ON book.CreatedBy = agent.ID
LEFT JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
LEFT JOIN journalentries je on book.ID = je.BookingID
LEFT JOIN payments pay on je.PaymentID = pay.ID
LEFT JOIN bookinglogs bkl on book.ID = bkl.BookingID
LEFT JOIN agents agent2 on bkl.AgentID = agent2.ID
WHERE
	fromDate <= TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	)
AND toDate >= TruncateTime (
	LEAST(
		IFNULL(
			book.FlightDate1,
			book.FlightDate2
		),
		IFNULL(
			book.FlightDate2,
			book.FlightDate1
		)
	)
)
AND book.`Status` = 2
AND bkl.`Status` =2
AND pay.`Status` =1
ORDER BY book.ID;


END
;;
DELIMITER ;
