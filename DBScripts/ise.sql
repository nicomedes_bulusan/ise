/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100108
Source Host           : localhost:3306
Source Database       : ise

Target Server Type    : MYSQL
Target Server Version : 100108
File Encoding         : 65001

Date: 2018-07-13 15:11:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth_code
-- ----------------------------
DROP TABLE IF EXISTS `auth_code`;
CREATE TABLE `auth_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(10) NOT NULL,
  `book_id` varchar(10) NOT NULL,
  `auth_code` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of auth_code
-- ----------------------------
INSERT INTO `auth_code` VALUES ('1', '1', '2902', '4L6GP8');
INSERT INTO `auth_code` VALUES ('2', '1', '2930', '4L6GP8');
INSERT INTO `auth_code` VALUES ('3', '1', '2936', '4L6GP8');
INSERT INTO `auth_code` VALUES ('4', '1', '2937', '4L6GP8');
INSERT INTO `auth_code` VALUES ('5', '1', '2877', '4L6GP8');
INSERT INTO `auth_code` VALUES ('6', '1', '2900', '4L6GP8');
INSERT INTO `auth_code` VALUES ('7', '1', '2917', '4L6GP8');
INSERT INTO `auth_code` VALUES ('8', '1', '2925', '4L6GP8');
INSERT INTO `auth_code` VALUES ('9', '1', '2899', '4L6GP8');
INSERT INTO `auth_code` VALUES ('10', '1', '2901', '4L6GP8');
INSERT INTO `auth_code` VALUES ('16', '1', '2902', 'GW3VL9');
INSERT INTO `auth_code` VALUES ('17', '1', '2930', 'GW3VL9');
INSERT INTO `auth_code` VALUES ('18', '1', '2936', 'GW3VL9');
INSERT INTO `auth_code` VALUES ('19', '1', '2937', 'GW3VL9');
INSERT INTO `auth_code` VALUES ('20', '1', '2877', 'GW3VL9');
INSERT INTO `auth_code` VALUES ('21', '1', '2900', 'GW3VL9');
INSERT INTO `auth_code` VALUES ('22', '1', '2917', 'GW3VL9');
INSERT INTO `auth_code` VALUES ('23', '1', '2925', 'GW3VL9');
INSERT INTO `auth_code` VALUES ('24', '1', '2899', 'GW3VL9');
INSERT INTO `auth_code` VALUES ('25', '1', '2901', 'GW3VL9');

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(100) DEFAULT NULL,
  `cv_number` varchar(100) DEFAULT NULL,
  `account_name` varchar(100) DEFAULT NULL,
  `book_a` varchar(100) DEFAULT NULL,
  `book_k` varchar(100) DEFAULT NULL,
  `book_foc` varchar(100) DEFAULT NULL,
  `book_inf` varchar(100) DEFAULT NULL,
  `book_tg` varchar(100) DEFAULT NULL,
  `book_e` varchar(100) DEFAULT NULL,
  `book_arrival` varchar(100) DEFAULT NULL,
  `book_time` varchar(100) DEFAULT NULL,
  `actual_a` varchar(100) DEFAULT NULL,
  `actual_k` varchar(100) DEFAULT NULL,
  `actual_foc` varchar(100) DEFAULT NULL,
  `actual_inf` varchar(100) DEFAULT NULL,
  `actual_tg` varchar(100) DEFAULT NULL,
  `actual_e` varchar(100) DEFAULT NULL,
  `actual_arrival` varchar(100) DEFAULT NULL,
  `actual_time` varchar(100) DEFAULT NULL,
  `resort_hotel` varchar(100) DEFAULT NULL,
  `unit` varchar(100) DEFAULT NULL,
  `unit_atd` varchar(100) DEFAULT NULL,
  `driver` varchar(100) DEFAULT NULL,
  `coordinator` varchar(100) DEFAULT NULL,
  `sales_handle` varchar(100) DEFAULT NULL,
  `agency` varchar(100) DEFAULT NULL,
  `remarks` varchar(100) DEFAULT NULL,
  `user` int(10) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `LastUpdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `cv_number` (`cv_number`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2938 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of book
-- ----------------------------
INSERT INTO `book` VALUES ('2877', 'Kalibo International Airport', 'MNL - 20170001188', 'LUNINGNING AZARCON & CO', '2', '0', '0', '0', '0', '0', 'Z2711', '10:05', '', '', '', '', '', '', '', '', 'LA CARMELA', 'BUS', '8081', 'ty', '', 'KLARITA IZABELLE HERMANO', 'TRAVEL ONLINE PHILIPPINES TRAVEL AGENCY INC.', 'ASSTD TRANSFER OIPA NOTE: DEPARTURE PICK UP TIME WILL BE INFORMED BY BORACAY RESERVATION THRU HOTEL ', '390', '0', '2018-07-13 14:58:18');
INSERT INTO `book` VALUES ('2899', 'Caticlan Airport', 'BOR - 20170001240', 'CABUANG ANA ROSE', '8', '0', '0', '0', '0', '0', 'Z2221', '11:05', '', '', '', '', '', '', '', '', 'LA CARMELA', 'BUS', '8081', 'ty', '', 'RONALYN SALVADOR', 'SHADOW TRAVEL AND TOURS', 'ASSIST W/ MCAB OIPA RTT BOOKED BY GLAICY PAID 03/24/17 BDO', '390', '0', '2018-07-13 14:58:18');
INSERT INTO `book` VALUES ('2900', 'Caticlan Airport', 'BOR - 20170001241', 'PICAZO GIAN CARLO', '3', '0', '0', '0', '0', '0', '5J899', '11:05', '', '', '', '', '', '', '', '', 'HENANN REGENCY STATION 2', 'BUS', '8081', 'ty', '', 'RONALYN SALVADOR', 'SHADOW TRAVEL AND TOURS', 'ASSIST W/ MCAB OIPA RTT BOOKED BY GLAICY PAID 3/24/17 BDO', '390', '0', '2018-07-13 14:58:18');
INSERT INTO `book` VALUES ('2901', 'Kalibo International Airport', 'BOR - 20170001242', 'HUNG IRISH', '6', '0', '0', '0', '0', '0', '5J339', '12:05', '', '', '', '', '', '', '', '', 'LA CARMELA', 'BUS', '8081', 'ty', '', 'RONALYN SALVADOR', 'SHADOW TRAVEL AND TOURS', 'ASSIST W/ MCAB OIPA RTT BOOKED BY GLAICY PAID 03/13/17 BDO', '390', '0', '2018-07-13 14:58:18');
INSERT INTO `book` VALUES ('2902', 'Kalibo International Airport', 'BOR - 20170001244', 'ORONCE REDEN', '2', '0', '0', '0', '0', '0', 'PR2969', '09:05', '', '', '', '', '', '', '', '', 'LA CARMELA', 'BUS', '8081', 'ty', '', 'RONALYN SALVADOR', 'SHADOW TRAVEL AND TOURS', 'ASSIST W/ MCAB OIPA RTT BOOKED BY JOANNE PAID 01/25/17 BDO', '390', '0', '2018-07-13 14:58:18');
INSERT INTO `book` VALUES ('2917', 'Caticlan Airport', 'MNL - 20170001268', 'COLTRIAN MANICEL & CO', '4', '2', '0', '1', '0', '0', 'Z2221', '11:05', '', '', '', '', '', '', '', '', 'HENANN GARDEN', 'BUS', '8081', 'ty', '', 'NEIL KEVIN CERDA', 'EXPRESS SERVE TRAVEL', 'ASSTD TRANSFER W/ T&E NOTE: DEPARTURE PICK UP TIME WILL BE INFORMED BY BORACAY RESERVATION THRU HOTE', '390', '0', '2018-07-13 14:58:18');
INSERT INTO `book` VALUES ('2925', 'Caticlan Airport', 'MNL - 20170001281', 'KHATE SANTOS & CO', '2', '0', '0', '0', '0', '0', 'Z2221', '11:05', '', '', '', '', '', '', '', '', 'LA CARMELA', 'BUS', '8081', 'ty', '', 'KLARITA IZABELLE HERMANO', 'TRAVEL ONLINE PHILIPPINES TRAVEL AGENCY INC.', 'ASSTD TRANSFER OIPA NOTE: DEPARTURE PICK UP TIME WILL BE INFORMED BY BORACAY RESERVATION THRU HOTEL ', '390', '0', '2018-07-13 14:58:18');
INSERT INTO `book` VALUES ('2930', 'Caticlan Airport', 'MNL - 20170001289', 'RENANTE ZULUETA AND PTY', '4', '2', '0', '1', '0', '0', '5J895', '10:05', '', '', '', '', '', '', '', '', 'ISLAND INN', 'BUS', '8081', 'ty', '', 'Jerlyn jane wong', 'AKZ TRAVEL AND TOURS', 'ASSTD W/ UPGRADED MCAB OIPA NOTE: DEPARTURE PICK UP TIME WILL BE INFORMED BY BORACAY RESERVATION THR', '390', '0', '2018-07-13 14:58:18');
INSERT INTO `book` VALUES ('2936', 'Caticlan Airport', 'BOR - 20170001298', 'DE GUZMAN AR JANE', '2', '0', '0', '0', '0', '0', '5J895', '10:05', '', '', '', '', '', '', '', '', 'HENANN REGENCY STATION 2', 'BUS', '8081', 'ty', '', 'RONALYN SALVADOR', 'SHADOW TRAVEL AND TOURS', 'ASSIST W/ MCAB OIPA RTT BOOKED BY SHEILA PAID 03/24/17 BDO', '390', '0', '2018-07-13 14:58:18');
INSERT INTO `book` VALUES ('2937', 'Caticlan Airport', 'BOR - 20170001300', 'MENDOZA ARNOLD', '4', '0', '0', '0', '0', '0', '5J895', '10:05', '', '', '', '', '', '', '', '', 'LA CARMELA', 'BUS', '8081', 'ty', '', 'RONALYN SALVADOR', 'SHADOW TRAVEL AND TOURS', 'ASSIST W/ MCAB OIPA RTT BOOKED BY CLARA PAID 03/31/17 BDO', '390', '0', '2018-07-13 14:58:18');

-- ----------------------------
-- Table structure for syncstate
-- ----------------------------
DROP TABLE IF EXISTS `syncstate`;
CREATE TABLE `syncstate` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LastSync` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsSuccess` bit(1) NOT NULL DEFAULT b'0',
  `Type` int(11) NOT NULL DEFAULT '1' COMMENT '1=WEB2MOBILE; 2=MOBILE2WEB',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of syncstate
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `ID` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `complete_name` varchar(300) NOT NULL,
  `status` smallint(1) NOT NULL,
  `last_sync` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '123456', 'Admin Admin', '1', '2018-06-27 02:07:42');
INSERT INTO `user` VALUES ('2', 'coordinator', '123456', 'Coordinator', '1', '2018-06-27 02:07:42');
INSERT INTO `user` VALUES ('390', 'nico.coordinator@gmail.com', '12345', 'Nico The Coordinator', '1', '2018-07-11 00:00:20');
