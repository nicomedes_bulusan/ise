ALTER TABLE `bookings`
ADD COLUMN `Creator`  int NULL AFTER `OnlineBookingOrderID`,
ADD COLUMN `CreationDate`  datetime NULL AFTER  `Creator`,
ADD COLUMN `ConfirmedBy`  int NULL AFTER `CreationDate`,
ADD COLUMN `ConfirmationDate`  datetime NULL AFTER `ConfirmedBy`,
ADD COLUMN `FinalizedBy`  int NULL AFTER `ConfirmationDate`,
ADD COLUMN `FinalizedDate`  datetime NULL  AFTER `FinalizedBy`;


UPDATE  bookings book
INNER JOIN bookinglogs bl on book.ID = bl.BookingID
SET book.Creator = bl.AgentID, book.CreationDate=bl.DateCreated
WHERE book.Creator is null
AND bl.`Status` = 9;


UPDATE  bookings book
INNER JOIN bookinglogs bl on book.ID = bl.BookingID
SET book.Creator = bl.AgentID, book.CreationDate=bl.DateCreated
WHERE book.Creator is null
AND bl.`Status` = 0;


UPDATE  bookings book
SET book.Creator = book.CreatedBy, book.CreationDate=book.DateCreated
WHERE book.Creator is null;


UPDATE  bookings book
INNER JOIN bookinglogs bl on book.ID = bl.BookingID
SET book.ConfirmedBy = bl.AgentID, book.ConfirmationDate=bl.DateCreated
WHERE book.ConfirmedBy is null
AND bl.`Status` = 1;


INNER JOIN bookinglogs bl on book.ID = bl.BookingID
SET book.FinalizedBy = bl.AgentID, book.FinalizedDate=bl.DateCreated
WHERE book.FinalizedBy is null
AND bl.`Status` = 2;

CREATE TABLE `fleet` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VIDNumber` varchar(30) NOT NULL,
  `PlateNo` varchar(15) DEFAULT NULL,
  `Type` varchar(50) DEFAULT NULL,
  `Capacity` int(11) DEFAULT NULL,
  `DateCreated` datetime NOT NULL,
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CreatedBy` int(11) NOT NULL,
  `LastUpdateBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `VIDNumber` (`VIDNumber`) USING BTREE,
  UNIQUE KEY `PlateNo` (`PlateNo`) USING BTREE
) ENGINE=InnoDB;

CREATE TABLE `authorizationcodes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TravelAgencyID` int(11) NOT NULL,
  `AuthCode` varchar(255) NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Status` int(11) NOT NULL DEFAULT '1',
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `TravelAgencyID` (`TravelAgencyID`,`AuthCode`) USING BTREE,
  CONSTRAINT `authorizationcodes_ibfk_1` FOREIGN KEY (`TravelAgencyID`) REFERENCES `travelagencies` (`ID`)
) ENGINE=InnoDB;

CREATE TABLE `manifestosched` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `DateLastUpadated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CoordinatorID` int(255) NOT NULL,
  `ManisfestoType` int(1) NOT NULL,
  `ManifestoDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `VehicleID` int(11) NOT NULL,
  `Driver` varchar(255) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  `CreatedBy` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `CoordinatorID` (`CoordinatorID`),
  KEY `manifestosched_ibfk_2` (`VehicleID`),
  CONSTRAINT `manifestosched_ibfk_1` FOREIGN KEY (`CoordinatorID`) REFERENCES `agents` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `manifestosched_ibfk_2` FOREIGN KEY (`VehicleID`) REFERENCES `fleet` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB;


CREATE TABLE `manifestoscheddetails` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ManifestoSchedID` int(11) NOT NULL,
  `Location` varchar(255) DEFAULT NULL,
  `CVNumber` varchar(255) NOT NULL,
  `AccountName` varchar(255) NOT NULL,
  `Adult` int(11) NOT NULL DEFAULT '0',
  `Kid` int(11) NOT NULL DEFAULT '0',
  `FOC` int(11) NOT NULL DEFAULT '0',
  `Infant` int(11) NOT NULL DEFAULT '0',
  `TourGuide` int(11) NOT NULL DEFAULT '0',
  `Escort` int(11) NOT NULL DEFAULT '0',
  `ActualAdult` int(11) NOT NULL DEFAULT '0',
  `ActualKid` int(11) NOT NULL DEFAULT '0',
  `ActualFOC` int(11) NOT NULL DEFAULT '0',
  `ActualInfant` int(11) NOT NULL DEFAULT '0',
  `ActualTourGuide` int(11) NOT NULL DEFAULT '0',
  `ActualEscort` int(11) NOT NULL DEFAULT '0',
  `ActualDate` date DEFAULT NULL,
  `ActualTime` time DEFAULT NULL,
  `BookingID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ManifestoSchedID` (`ManifestoSchedID`,`CVNumber`) USING BTREE,
  KEY `BookingID` (`BookingID`),
  CONSTRAINT `manifestoscheddetails_ibfk_1` FOREIGN KEY (`ManifestoSchedID`) REFERENCES `manifestosched` (`ID`),
  CONSTRAINT `manifestoscheddetails_ibfk_2` FOREIGN KEY (`BookingID`) REFERENCES `bookings` (`ID`)
) ENGINE=InnoDB;