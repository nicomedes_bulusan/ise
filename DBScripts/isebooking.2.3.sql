/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100108
Source Host           : localhost:3306
Source Database       : isebooking

Target Server Type    : MYSQL
Target Server Version : 100108
File Encoding         : 65001

Date: 2017-02-21 10:35:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for manifesto
-- ----------------------------
DROP TABLE IF EXISTS `manifesto`;
CREATE TABLE `manifesto` (
`ID`  int(11) NOT NULL AUTO_INCREMENT ,
`BookingID`  int(11) NOT NULL ,
`DateCreated`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ,
`DateLastUpdated`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
`Pax`  int(11) NOT NULL ,
`ResortHotel`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`ArrivalDate`  datetime NOT NULL ,
`DepartureDate`  datetime NOT NULL ,
`Remarks`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`Pickup`  int(11) NOT NULL ,
`DropOff`  int(11) NOT NULL ,
`Adults`  int(11) NOT NULL DEFAULT 0 ,
`Kids`  int(11) NOT NULL DEFAULT 0 ,
`Infants`  int(11) NOT NULL DEFAULT 0 ,
PRIMARY KEY (`ID`),
FOREIGN KEY (`BookingID`) REFERENCES `bookings` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
FOREIGN KEY (`Pickup`) REFERENCES `destinations` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
FOREIGN KEY (`DropOff`) REFERENCES `destinations` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
INDEX `BookingID` (`BookingID`) USING BTREE ,
INDEX `Pickup` (`Pickup`) USING BTREE ,
INDEX `DropOff` (`DropOff`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=6

;

-- ----------------------------
-- Table structure for manifestodetails
-- ----------------------------
DROP TABLE IF EXISTS `manifestodetails`;
CREATE TABLE `manifestodetails` (
`ID`  int(11) NOT NULL AUTO_INCREMENT ,
`ManifestoID`  int(11) NOT NULL ,
`GuestName`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
PRIMARY KEY (`ID`),
FOREIGN KEY (`ManifestoID`) REFERENCES `manifesto` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
INDEX `ManifestoID` (`ManifestoID`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=19

;

-- ----------------------------
-- Auto increment value for manifesto
-- ----------------------------
ALTER TABLE `manifesto` AUTO_INCREMENT=6;

-- ----------------------------
-- Auto increment value for manifestodetails
-- ----------------------------
ALTER TABLE `manifestodetails` AUTO_INCREMENT=19;
