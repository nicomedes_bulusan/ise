update journalentries
set Payments = Amount
 WHERE 
ReferenceType=0 AND AccountingCode='1011000' AND Payments <> Amount
and BookingID is NOT NULL AND `Type`=2;

update billingperiods SET `Payment`=0;

update accountreceivables SET `Balance`=`OriginalAmount` where BillingPeriodID is not null;