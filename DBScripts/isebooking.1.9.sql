/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100108
Source Host           : localhost:3306
Source Database       : isebooking

Target Server Type    : MYSQL
Target Server Version : 100108
File Encoding         : 65001

Date: 2017-01-05 07:48:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for agents
-- ----------------------------
DROP TABLE IF EXISTS `agents`;
CREATE TABLE `agents` (
`ID`  int(11) NOT NULL AUTO_INCREMENT ,
`FirstName`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`MiddleName`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`LastName`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`ContactNumber`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`Email`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`DateCreated`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ,
`DateLastUpdated`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
`CreditLimit`  double NOT NULL DEFAULT 0 ,
`RemainingCredit`  double NOT NULL DEFAULT 0 ,
`YearToDateAmount`  double NOT NULL DEFAULT 0 ,
`MonthToDateAmount`  double NOT NULL DEFAULT 0 ,
`IsActive`  int(11) NOT NULL DEFAULT 1 ,
`Password`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`Type`  int(11) NOT NULL DEFAULT 1 ,
`PaymentTypeID`  int(11) NOT NULL ,
`Company`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`AccountBalance`  double NOT NULL DEFAULT 0 ,
`GracePeriod`  int(11) NOT NULL DEFAULT 30 ,
`MonthlyInterestRate`  double(11,0) NOT NULL DEFAULT 1 ,
`Address`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`MarketTypeID`  int(11) NOT NULL ,
PRIMARY KEY (`ID`),
FOREIGN KEY (`PaymentTypeID`) REFERENCES `paymenttypes` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`MarketTypeID`) REFERENCES `markettypes` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
INDEX `PaymentTypeID` (`PaymentTypeID`) USING BTREE ,
INDEX `MarketTypeID` (`MarketTypeID`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=6

;

-- ----------------------------
-- Records of agents
-- ----------------------------
BEGIN;
INSERT INTO `agents` VALUES ('1', 'John', '', 'Doe', 'NO CONTACT', 'johndoe@gmail.com', '2016-10-10 08:26:46', '2017-01-05 07:46:23', '500000', '478780', '0', '0', '1', '$2a$10$dpDzH3Ym0HUKwjoZJ.WqCuOIQtP0LayYFn0tJ/9HkMEXZXDQb7eOW', '2', '2', 'WALK-IN', '5460', '30', '1', '4th/F One E-com Center Building\r\nPalm Coast Drive, Mall of Asia Complex,\r\nPasay City\r\n', '1'), ('2', 'Juan', '', 'Cruz', 'NO CONTACT', 'juandelacruz@gmail.com', '2016-09-07 04:00:28', '2017-01-05 07:46:23', '50000', '34800', '0', '0', '1', '$2a$10$dpDzH3Ym0HUKwjoZJ.WqCuOIQtP0LayYFn0tJ/9HkMEXZXDQb7eOW', '1', '1', 'ABAKDA COMPANY', '15200', '30', '1', 'METRO MANILA', '1'), ('3', 'Maria', 'P@ssw0rd', 'makiling', 'mar', 'mariamakiling@yahoo.com', '2016-10-11 07:47:51', '2017-01-05 07:46:23', '5000', '5000', '0', '0', '0', '$2a$10$dpDzH3Ym0HUKwjoZJ.WqCuOIQtP0LayYFn0tJ/9HkMEXZXDQb7eOW', '1', '1', null, '0', '30', '1', null, '1'), ('4', 'nic', 'P@ssw0rd', 'nic', 'nic nac', 'nbulusanjr@gmail.com', '2016-10-11 08:57:25', '2017-01-05 07:46:23', '5000', '5000', '0', '0', '0', '$2a$10$yfaz0EayQP3CC9gE0XcPAObxM8p1QJj23mzhk7vWhlfIegVkBoiAG', '2', '2', null, '0', '30', '1', null, '1'), ('5', 'nic', '', 'nic', 'nic nac', 'nicz_bz@yahoo.com', '2016-10-11 08:59:29', '2017-01-05 07:46:23', '50000', '50000', '0', '0', '1', '$2a$10$vveoNTUkthzu7V2Q/LaIse1lZQtpUGmf/dUUqo//eWZaoEJb8iscm', '1', '1', 'ABC', '0', '30', '1', null, '1');
COMMIT;

-- ----------------------------
-- Table structure for markettypes
-- ----------------------------
DROP TABLE IF EXISTS `markettypes`;
CREATE TABLE `markettypes` (
`ID`  int(11) NOT NULL AUTO_INCREMENT ,
`Description`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`DateCreated`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ,
`DateLastUpdated`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
PRIMARY KEY (`ID`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=2

;

-- ----------------------------
-- Records of markettypes
-- ----------------------------
BEGIN;
INSERT INTO `markettypes` VALUES ('1', 'TRAVEL AGENT LOCAL', '2017-01-05 07:46:12', '2017-01-05 07:46:12');
COMMIT;

-- ----------------------------
-- Auto increment value for agents
-- ----------------------------
ALTER TABLE `agents` AUTO_INCREMENT=6;

-- ----------------------------
-- Auto increment value for markettypes
-- ----------------------------
ALTER TABLE `markettypes` AUTO_INCREMENT=2;
