CREATE  PROCEDURE `SyncToIseMobile`()
BEGIN
insert into isemobile.book(
`location` ,  `cv_number` ,  `account_name` ,
  `book_a` ,  `book_k` ,  `book_foc` ,  `book_inf` ,  `book_tg` ,  `book_e` ,  `book_arrival` ,  `book_time` ,
  `actual_a` ,  `actual_k` ,  `actual_foc` ,  `actual_inf` ,  `actual_tg` ,  `actual_e` ,  `actual_arrival` ,  `actual_time` , 
 `resort_hotel` ,
  `unit` ,  `unit_atd` ,  `driver` ,  `coordinator` ,
  `sales_handle` , 
 `agency` ,  `remarks` ,  `user` ,  `status`, `LastUpdate`,`Type`,agency_id,Manifesto_ID,`flight_date`
)
(
select

sched.Location,sched.CVNumber,sched.AccountName,
sched.Adult,sched.Kid,sched.FOC,sched.Infant,sched.TourGuide,sched.Escort,man.FlightNo,DATE_FORMAT(schedH.ManifestoDate,'%H:%m'),
'','','','','','','','',
resort.`Name`,
fleet.Type,fleet.VIDNumber,schedH.Driver,'',
(CONCAT(sales.FirstName,' ',sales.LastName)),
tra.`Name`,man.Remarks,schedH.CoordinatorID,'0',schedH.DateLastUpadated,schedH.ManisfestoType,agent.TravelAgencyID,schedH.ID,DATE_FORMAT(schedH.ManifestoDate,"%Y-%m-%d")
from manifestoscheddetails sched
INNER JOIN manifesto man ON sched.BookingID = man.BookingID
INNER JOIN manifestosched schedH ON sched.ManifestoSchedID = schedH.ID
INNER JOIN fleet ON schedH.VehicleID = fleet.ID
INNER JOIN bookings book ON sched.BookingID = book.ID
INNER JOIN resortsandhotels  resort ON man.ResortHotel=resort.ID
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
LEFT JOIN agents sales ON book.FinalizedBy = sales.ID
WHERE schedH.`Status` = 1 
and man.Type=schedH.ManisfestoType
and schedH.DateLastUpadated>=(SELECT IFNULL(MAX(ss.LastSync),'0000-00-00 00:00') from isemobile.syncstate ss where ss.Type=1)
ORDER BY schedH.DateLastUpadated
) ON DUPLICATE KEY UPDATE isemobile.book.Remarks = isemobile.book.Remarks;

END

CREATE PROCEDURE `SyncToWeb`()
BEGIN
UPDATE manifestoscheddetails sd
INNER JOIN manifestosched ms ON sd.ManifestoSchedID = ms.ID
INNER JOIN isemobile.book book ON sd.CVNumber = book.cv_number and  ms.ManisfestoType = book.`Type` AND ms.ID = book.Manifesto_ID
SET sd.ActualAdult = CAST(book.actual_a AS UNSIGNED),
sd.ActualEscort = CAST(book.actual_e  AS UNSIGNED),
sd.ActualFOC = CAST(book.actual_foc  AS UNSIGNED),
sd.ActualInfant = CAST(book.actual_inf  AS UNSIGNED),
sd.ActualKid = CAST(book.actual_k  AS UNSIGNED),
sd.ActualTourGuide = CAST(book.actual_tg  AS UNSIGNED)
WHERE 
 book.`status`=1;
UPDATE manifestosched sc
INNER JOIN manifestoscheddetails sd ON sd.ManifestoSchedID = sc.ID
INNER JOIN manifestosched ms ON sd.ManifestoSchedID = ms.ID
INNER JOIN isemobile.book book ON sd.CVNumber = book.cv_number and  ms.ManisfestoType = book.`Type` AND ms.ID = book.Manifesto_ID
SET sc.`Status` =2
WHERE
 book.`status`=1;
END
