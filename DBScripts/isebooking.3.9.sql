INSERT INTO `isebookinglive`.`destinations` (`ID`, `Description`, `DateCeated`, `DateLastUpdated`, `Code`, `IsActive`) VALUES ('7', 'OTHERS', '2017-06-21 11:32:04', '2017-06-21 11:32:04', 'OTH', b'1');

ALTER TABLE `billingstatements`
ADD COLUMN `DateReceived`  datetime NULL AFTER `AgentID`,
ADD COLUMN `DateLastUpdated`  timestamp NOT NULL DEFAULT NOW() ON UPDATE CURRENT_TIMESTAMP AFTER `DateReceived`,
ADD COLUMN `LastUpdatedBy`  int NULL AFTER `DateLastUpdated`;


