/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100108
Source Host           : localhost:3306
Source Database       : isebooking0906

Target Server Type    : MYSQL
Target Server Version : 100108
File Encoding         : 65001

Date: 2017-09-11 13:24:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Procedure structure for rptGetSales
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptGetSales`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptGetSales`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	#Routine body goes here...

SELECT
		(book.Adults + book.Senior) AS Adults,
	book.CreatedBy AS AgentID,
	book.FlightNo1 AS ArrivalFlight,
	book.FlightNo2 AS DepartureFlight,
	book.ID AS BookingID,
	book.CVNumber,
	book.FlightDate1 AS DateIn,
	book.FlightDate2 AS DateOut,
	book.Escorts,
	book.FOC,
	(
		book.Childrens + book.ChildrensBelowSix + book.Infants
	) AS Kids,
	book.Infants,
	book.GroupName AS PaxName,
	book.Total,
	book.TourGuides,
	TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	) AS TransactionDate,
	agent.TravelAgencyID,
	tra.`Name` AS TravelAgency
FROM
	bookings book
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
WHERE
	fromDate <= TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	)
AND toDate >= TruncateTime (
	LEAST(
		IFNULL(
			book.FlightDate1,
			book.FlightDate2
		),
		IFNULL(
			book.FlightDate2,
			book.FlightDate1
		)
	)
)
AND book.`Status` = 2
ORDER BY book.ID;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptGetSalesByAgentByMarketType
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptGetSalesByAgentByMarketType`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptGetSalesByAgentByMarketType`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	

SELECT
book.ID as BookingID,
book.CVNumber,
book.GroupName as PaxName,
tra.ID as TravelAgencyID,
tra.`Name` as TravelAgency,
agent.ID as AgentID,
CONCAT(agent.FirstName," ",IFNULL(agent.LastName,"")) as AgentName,
mkt.ID as MarketTypeID,
mkt.Description as MarketType,
0 as Transfer,
0 as BoatFee,
0 as EnvironmentFee,
0 as Others,
0 as NightNavigation,
bd.SourceID,
bd.Boat,
bd.Amount,
bd.Quantity	

FROM
	bookings book
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
INNER JOIN markettypes mkt on agent.MarketTypeID = mkt.ID
INNER JOIN bookingdetails bd on book.ID = bd.BookingID
WHERE
	fromDate <= TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	)
AND toDate >= TruncateTime (
	LEAST(
		IFNULL(
			book.FlightDate1,
			book.FlightDate2
		),
		IFNULL(
			book.FlightDate2,
			book.FlightDate1
		)
	)
)
AND book.`Status` = 2
ORDER BY book.ID;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptGetSalesByMarketTypeBreakdown
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptGetSalesByMarketTypeBreakdown`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptGetSalesByMarketTypeBreakdown`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	

SELECT
book.ID as BookingID,
book.CVNumber,
book.GroupName as PaxName,
tra.ID as TravelAgencyID,
tra.`Name` as TravelAgency,
mkt.ID as MarketTypeID,
mkt.Description as MarketType,
0 as Transfer,
0 as BoatFee,
0 as EnvironmentFee,
0 as Others,
0 as NightNavigation,
bd.SourceID,
bd.Boat,
bd.Amount,
bd.Quantity	

FROM
	bookings book
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
INNER JOIN markettypes mkt on agent.MarketTypeID = mkt.ID
INNER JOIN bookingdetails bd on book.ID = bd.BookingID
WHERE
	fromDate <= TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	)
AND toDate >= TruncateTime (
	LEAST(
		IFNULL(
			book.FlightDate1,
			book.FlightDate2
		),
		IFNULL(
			book.FlightDate2,
			book.FlightDate1
		)
	)
)
AND book.`Status` = 2
ORDER BY book.ID;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptGetSalesByMarketTypeSummary
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptGetSalesByMarketTypeSummary`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptGetSalesByMarketTypeSummary`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	

SELECT
mkt.ID as MarketTypeID,
mkt.Description as MarketType,
0 as Transfer,
0 as BoatFee,
0 as EnvironmentFee,
0 as Others,
0 as NightNavigation,
bd.SourceID,
bd.Boat,
bd.Amount,
bd.Quantity	

FROM
	bookings book
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
INNER JOIN markettypes mkt on agent.MarketTypeID = mkt.ID
INNER JOIN bookingdetails bd on book.ID = bd.BookingID
WHERE
	fromDate <= TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	)
AND toDate >= TruncateTime (
	LEAST(
		IFNULL(
			book.FlightDate1,
			book.FlightDate2
		),
		IFNULL(
			book.FlightDate2,
			book.FlightDate1
		)
	)
)
AND book.`Status` = 2
ORDER BY book.ID;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptGetSalesBySeller
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptGetSalesBySeller`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptGetSalesBySeller`(IN `fromDate` date,IN `toDate` date)
BEGIN
	

SELECT

agent.ID as AgentID,
CONCAT(agent.FirstName," ",IFNULL(agent.LastName,"")) as AgentName,
0 as Transfer,
bd.Boat,
bd.Amount,
0 as BoatFee,
0 as EnvironmentFee,
0 as TerminalFee,
0 as Others,
0 as NightNavigation,
bd.Quantity,
bd.SourceID

FROM
	bookings book
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
INNER JOIN markettypes mkt on agent.MarketTypeID = mkt.ID
INNER JOIN bookingdetails bd on book.ID = bd.BookingID
WHERE
	fromDate <= TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	)
AND toDate >= TruncateTime (
	LEAST(
		IFNULL(
			book.FlightDate1,
			book.FlightDate2
		),
		IFNULL(
			book.FlightDate2,
			book.FlightDate1
		)
	)
)
AND book.`Status` = 2
ORDER BY book.ID;


END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptGetSalesByTravelAgency
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptGetSalesByTravelAgency`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptGetSalesByTravelAgency`(IN `fromDate` date,IN `toDate` date)
BEGIN
	

SELECT

tra.ID as AgentID,
tra.`Name` as AgentName,
0 as Transfer,
bd.Boat,
bd.Amount,
0 as BoatFee,
0 as EnvironmentFee,
0 as TerminalFee,
0 as Others,
0 as NightNavigation,
bd.Quantity,
bd.SourceID

FROM
	bookings book
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
INNER JOIN markettypes mkt on agent.MarketTypeID = mkt.ID
INNER JOIN bookingdetails bd on book.ID = bd.BookingID
WHERE
	fromDate <= TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	)
AND toDate >= TruncateTime (
	LEAST(
		IFNULL(
			book.FlightDate1,
			book.FlightDate2
		),
		IFNULL(
			book.FlightDate2,
			book.FlightDate1
		)
	)
)
AND book.`Status` = 2
ORDER BY book.ID;


END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptTrialBalance
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptTrialBalance`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptTrialBalance`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	

SELECT 
PaymentDate,
PaymentNo,
tra.`Name` as TravelAgency,
payment.CheckDate,
payment.CheckNo,
payment.Remarks,
journal.CVNumber,
journal.ReceiptNo,
journal.AccountName,
journal.Amount,
journal.Type
 from payments payment
INNER JOIN journalentries journal on payment.ID = journal.PaymentID
INNER JOIN travelagencies tra on payment.TravelAgencyID = tra.ID
WHERE
	fromDate <= TruncateTime (
		PaymentDate
	)
AND toDate >= TruncateTime (
	PaymentDate
);

END
;;
DELIMITER ;


UPDATE bookingdetails SET SourceID=4 where Description IN ('TF IN','TF OUT') AND SourceID is null;
UPDATE bookingdetails SET SourceID=2 where Description LIKE '%ENVIRONMENTAL%' AND SourceID is null;

UPDATE bookingdetails SET SourceID=1 WHERE
(Description LIKE 'Caticlan Jetty Port - Kalibo International Airport%' OR
Description LIKE 'Kalibo International Airport - Caticlan Jetty Port%' OR
Description LIKE 'Caticlan Jetty Port - Resort%' OR
Description LIKE 'Resort - Caticlan Jetty Port%' OR
Description LIKE 'Caticlan Jetty Port - Cagban Port - Boracay%' OR
Description LIKE 'Cagban Port - Boracay - Caticlan Jetty Port%' OR
Description LIKE 'Caticlan Jetty Port - Caticlan Airport%' OR
Description LIKE 'Caticlan Airport - Caticlan Jetty Port%' OR
Description LIKE 'Caticlan Jetty Port - OTHERS%' OR
Description LIKE 'OTHERS - Caticlan Jetty Port%' OR
Description LIKE 'Kalibo International Airport - Resort%' OR
Description LIKE 'Resort - Kalibo International Airport%' OR
Description LIKE 'Kalibo International Airport - Cagban Port - Boracay%' OR
Description LIKE 'Cagban Port - Boracay - Kalibo International Airport%' OR
Description LIKE 'Kalibo International Airport - Caticlan Airport%' OR
Description LIKE 'Caticlan Airport - Kalibo International Airport%' OR
Description LIKE 'Kalibo International Airport - OTHERS%' OR
Description LIKE 'OTHERS - Kalibo International Airport%' OR
Description LIKE 'Resort - Cagban Port - Boracay%' OR
Description LIKE 'Cagban Port - Boracay - Resort%' OR
Description LIKE 'Resort - Caticlan Airport%' OR
Description LIKE 'Caticlan Airport - Resort%' OR
Description LIKE 'Resort - OTHERS%' OR
Description LIKE 'OTHERS - Resort%' OR
Description LIKE 'Cagban Port - Boracay - Caticlan Airport%' OR
Description LIKE 'Caticlan Airport - Cagban Port - Boracay%' OR
Description LIKE 'Cagban Port - Boracay - OTHERS%' OR
Description LIKE 'OTHERS - Cagban Port - Boracay%' OR
Description LIKE 'Caticlan Airport - OTHERS%' OR
Description LIKE 'OTHERS - Caticlan Airport%') AND  SourceID is null;

UPDATE bookingdetails SET SourceID=1 WHERE
Description LIKE 'Free-of-charge%' AND SourceID is null;

UPDATE bookingdetails SET SourceID=5 WHERE
Description LIKE 'NIGHT NAVIGATION FEE%' AND SourceID is null;

UPDATE bookingdetails SET SourceID=1 WHERE
(Description LIKE 'KALIBO AIRPORT - RESORT%' OR
Description LIKE 'CATICLAN AIRPORT  - RESORT%' OR
Description LIKE 'CATICLAN JETTYPORT - KALIBO AIRPORT%' OR
Description LIKE 'CATICLAN JETTYPORT - RESORT%' OR
Description LIKE 'KALIBO AIRPORT - RESORT%' OR
Description LIKE 'KALIBO INTERNATIONAL AIRPORT%') AND  SourceID is null;

UPDATE bookingdetails SET SourceID=3 WHERE
Description LIKE 'BOAT%' AND  SourceID is null;

UPDATE bookingdetails SET SourceID=5 WHERE
Description LIKE 'NIGHT NAVIGATION%' AND SourceID is null;

UPDATE bookingdetails SET SourceID=1 WHERE
Description LIKE 'RESORT - KALIBO AIRPORT%';

UPDATE bookingdetails SET SourceID=4 WHERE
(Description LIKE 'TERMINAL FEE%'  OR
Description LIKE 'TERMINAL OUT%' OR
Description LIKE 'TF%') AND SourceID is null;

UPDATE bookingdetails set SourceID=5 where Description like 'NIGHT%' AND SourceID is null;

UPDATE bookingdetails set SourceID=1 where 
(Description like 'RESORT-CATICLAN AIRPORT%' OR
Description like 'RESORT-KALIBO INTERNATIONAL AIRPORT%') AND  SourceID is null;


UPDATE bookingdetails set SourceID=6 where SourceID is null;

UPDATE `agents` SET MarketTypeID=1 WHERE MarketTypeID=0;

ALTER TABLE `agents` ADD FOREIGN KEY (`MarketTypeID`) REFERENCES `markettypes` (`ID`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `bookingdetails`
MODIFY COLUMN `SourceID`  int(11) NOT NULL AFTER `Boat`;



