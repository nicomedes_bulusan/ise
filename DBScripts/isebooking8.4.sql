CREATE TABLE `fleetcategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(255) NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB;

ALTER TABLE `fleet`
ADD COLUMN `CategoryID`  int(255) NOT NULL DEFAULT 1 AFTER `LastUpdateBy`;

ALTER TABLE `fleet` ADD FOREIGN KEY (`CategoryID`) REFERENCES `FleetCategory` (`ID`);

INSERT INTO `isebookinglive`.`FleetCategory` (`ID`, `Description`, `DateCreated`) VALUES ('1', 'ISE OWNED', '2019-02-05 11:54:28');
INSERT INTO `isebookinglive`.`FleetCategory` (`ID`, `Description`, `DateCreated`) VALUES ('2', 'PRIVATE', '2019-02-05 11:54:45');
INSERT INTO `isebookinglive`.`FleetCategory` (`ID`, `Description`, `DateCreated`) VALUES ('3', 'INLAND', '2019-02-05 11:55:11');
