UPDATE `accountingterms` SET `Code`='1010200', `Description`='CASH IN BANK - BPI MLA 1731-0093-13', `DateCreated`='2017-08-03 04:34:10', `DateLastUpdated`='2017-09-12 08:37:41', `IsActive`=b'1' WHERE (`Code`='1010200');
UPDATE `bookings` SET IsPaid=0 WHERE IsPaid=1;
UPDATE bookings SET IsPaid=1 
WHERE ID IN (
select j.BookingID from journalentries j where j.Payments>0 and j.Amount=Payments);

UPDATE payments SET `Status`=0,VerifiedBy=null
WHERE  ID IN (
select j.PaymentID from journalentries j where j.Payments>0 and j.Amount!=Payments)

update payments SET `Status`=1 WHERE VerifiedBy is not null and Payments>0 and Amount=Payments;
UPDATE bookingdetails SET SourceID=1 WHERE SourceID=3;

update bookings set TravelAgencyID = (select agent.TravelAgencyID from agents agent where agent.ID=bookings.CreatedBy limit 1) WHERE TravelAgencyID=0;