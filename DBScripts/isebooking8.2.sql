/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100108
Source Host           : localhost:3306
Source Database       : isebookinglive

Target Server Type    : MYSQL
Target Server Version : 100108
File Encoding         : 65001

Date: 2018-10-10 11:43:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Event structure for ToMobile
-- ----------------------------
DROP EVENT IF EXISTS `ToMobile`;
DELIMITER ;;
CREATE  EVENT `ToMobile` ON SCHEDULE EVERY 3 MINUTE STARTS '2018-07-10 23:35:33' ON COMPLETION PRESERVE ENABLE DO BEGIN
insert into isemobile.book(
`location` ,  `cv_number` ,  `account_name` ,
  `book_a` ,  `book_k` ,  `book_foc` ,  `book_inf` ,  `book_tg` ,  `book_e` ,  `book_arrival` ,  `book_time` ,
  `actual_a` ,  `actual_k` ,  `actual_foc` ,  `actual_inf` ,  `actual_tg` ,  `actual_e` ,  `actual_arrival` ,  `actual_time` , 
 `resort_hotel` ,
  `unit` ,  `unit_atd` ,  `driver` ,  `coordinator` ,
  `sales_handle` , 
 `agency` ,  `remarks` ,  `user` ,  `status`, `LastUpdate`,`type`,`flight_date`
)
(
select

sched.Location,sched.CVNumber,sched.AccountName,
sched.Adult,sched.Kid,sched.FOC,sched.Infant,sched.TourGuide,sched.Escort,man.FlightNo,DATE_FORMAT(man.Date,'%H:%m'),
'','','','','','','','',
resort.`Name`,
fleet.Type,fleet.VIDNumber,schedH.Driver,'',
(CONCAT(sales.FirstName,' ',sales.LastName)),
tra.`Name`,man.Remarks,schedH.CoordinatorID,'0',schedH.DateLastUpadated,schedH.ManisfestoType,
(CASE man.Type WHEN 1 then book.FlightDate1 else book.FlightDate2 END)
from manifestoscheddetails sched
INNER JOIN manifesto man ON sched.BookingID = man.BookingID
INNER JOIN manifestosched schedH ON sched.ManifestoSchedID = schedH.ID
INNER JOIN fleet ON schedH.VehicleID = fleet.ID
INNER JOIN bookings book ON sched.BookingID = book.ID
INNER JOIN resortsandhotels  resort ON man.ResortHotel=resort.ID
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
LEFT JOIN agents sales ON book.FinalizedBy = sales.ID
WHERE schedH.`Status` = 1 and schedH.ManisfestoType = man.Type and schedH.DateLastUpadated>=(SELECT IFNULL(MAX(ss.LastSync),'0000-00-00 00:00') from isemobile.syncstate ss where ss.Type=1)
ORDER BY schedH.DateLastUpadated
) ON DUPLICATE KEY UPDATE isemobile.book.Remarks = isemobile.book.Remarks;

#DECLARE @auth VARCHAR(10);


INSERT INTO isemobile.syncstate(LastSync,IsSuccess,Type)
SELECT `LastUpdate`,1,1 from ise.book WHERE ID =  (SELECT LAST_INSERT_ID());
SELECT @auth:=authcode FROM authorizationcodes WHERE `status`=1 LIMIT 1;


insert into isemobile.auth_code(user_id,book_id,auth_code) 
(
select
1,
book.ID,
@auth
from manifestoscheddetails sched
INNER JOIN manifesto man ON sched.BookingID = man.BookingID
INNER JOIN manifestosched schedH ON sched.ManifestoSchedID = schedH.ID
INNER JOIN fleet ON schedH.VehicleID = fleet.ID
INNER JOIN bookings book ON sched.BookingID = book.ID
INNER JOIN resortsandhotels  resort ON man.ResortHotel=resort.ID
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
LEFT JOIN agents sales ON book.FinalizedBy = sales.ID
WHERE schedH.`Status` = 1 and schedH.ManisfestoType=1 and man.Type=1 and schedH.DateLastUpadated>=(SELECT IFNULL(MAX(ss.LastSync),'0000-00-00 00:00') from isemobile.syncstate ss where ss.Type=1)
ORDER BY schedH.DateLastUpadated
);

UPDATE authorizationcodes SET `Status`=0 WHERE AuthCode = @auth;

END
;;
DELIMITER ;

-- ----------------------------
-- Event structure for ToWeb
-- ----------------------------
DROP EVENT IF EXISTS `ToWeb`;
DELIMITER ;;
CREATE  EVENT `ToWeb` ON SCHEDULE EVERY 5 MINUTE STARTS '2018-07-11 13:14:23' ON COMPLETION PRESERVE ENABLE DO BEGIN

UPDATE manifestoscheddetails sd
INNER JOIN manifestosched ms ON sd.ManifestoSchedID = ms.ID
INNER JOIN isemobile.book book ON sd.CVNumber = book.cv_number
SET sd.ActualAdult = CAST(book.actual_a AS UNSIGNED),
sd.ActualEscort = CAST(book.actual_e  AS UNSIGNED),
sd.ActualFOC = CAST(book.actual_foc  AS UNSIGNED),
sd.ActualInfant = CAST(book.actual_inf  AS UNSIGNED),
sd.ActualKid = CAST(book.actual_k  AS UNSIGNED),
sd.ActualTourGuide = CAST(book.actual_tg  AS UNSIGNED)
WHERE ms.ManisfestoType = book.type and book.`status`=1
and (book.cv_number like 'BOR%' OR book.cv_number like 'MNL%');

UPDATE manifestosched sc
INNER JOIN manifestoscheddetails sd ON sd.ManifestoSchedID = sc.ID
INNER JOIN manifestosched ms ON sd.ManifestoSchedID = ms.ID
INNER JOIN isemobile.book book ON sd.CVNumber = book.cv_number
SET sc.`Status` =2
WHERE ms.ManisfestoType = book.type and book.`status`=1 
and (book.cv_number like 'BOR%' OR book.cv_number like 'MNL%');


END
;;
DELIMITER ;

-- ----------------------------
-- Event structure for UsersToMobile
-- ----------------------------
DROP EVENT IF EXISTS `UsersToMobile`;
DELIMITER ;;
CREATE  EVENT `UsersToMobile` ON SCHEDULE EVERY 10 MINUTE STARTS '2018-07-10 23:40:20' ON COMPLETION PRESERVE ENABLE DO insert into isemobile.`user`(ID,`username`,`password`,`complete_name`,`status`)
(SELECT a.ID,a.Email,'12345',CONCAT(a.FirstName,' ',a.LastName),a.IsActive FROM agents a WHERE Type=5) ON DUPLICATE KEY UPDATE isemobile.`user`.`status` = a.IsActive
;;
DELIMITER ;
