ALTER TABLE `manifestosched`
ADD COLUMN `Location`  varchar(255) NOT NULL DEFAULT '' AFTER `CreatedBy`,
ADD COLUMN `Touchpoint`  int NOT NULL DEFAULT 0 AFTER `Location`;

CREATE TABLE `AgentAuthCodes` (
`ID`  int NOT NULL AUTO_INCREMENT ,
`AgentID`  int NOT NULL ,
`Code`  varchar(8) NOT NULL ,
`DateCreated`  datetime NOT NULL DEFAULT NOW() ,
`DateUpdated`  timestamp NOT NULL DEFAULT NOW() ON UPDATE CURRENT_TIMESTAMP ,
`Status`  int NOT NULL DEFAULT 0 COMMENT '0=Active,1=Used' ,
PRIMARY KEY (`ID`),
FOREIGN KEY (`AgentID`) REFERENCES `agents` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
)
;
