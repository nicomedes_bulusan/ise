/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100108
Source Host           : localhost:3306
Source Database       : isebooking0731

Target Server Type    : MYSQL
Target Server Version : 100108
File Encoding         : 65001

Date: 2017-08-03 07:38:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for accountingterms
-- ----------------------------
DROP TABLE IF EXISTS `accountingterms`;
CREATE TABLE `accountingterms` (
`Code`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`Description`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`DateCreated`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ,
`DateLastUpdated`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
`IsActive`  bit(1) NOT NULL DEFAULT b'1' ,
PRIMARY KEY (`Code`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci

;

-- ----------------------------
-- Records of accountingterms
-- ----------------------------
BEGIN;
INSERT INTO `accountingterms` VALUES ('1010200', 'CASH IN BANK - BPI MLA 1731-0093-11', '2017-08-03 04:34:10', '2017-08-03 04:34:10', ''), ('1010220', 'CASH IN BANK - BPI KALIBO 1113-3050-84', '2017-08-03 07:09:24', '2017-08-03 07:09:24', ''), ('1010250', 'CASH IN BANK - BDO MLA 007100019092', '2017-08-03 04:33:04', '2017-08-03 04:33:04', ''), ('1010280', 'CASH IN BANK  - PAYPAL', '2017-08-03 04:34:24', '2017-08-03 04:34:43', ''), ('1011000', 'ACCOUNTS RECEIVABLE - TRADE', '2017-08-03 04:33:32', '2017-08-03 04:33:32', ''), ('1011272', 'ACCOUNTS RECEIVABLE - BEGINNING', '2017-08-03 04:36:33', '2017-08-03 04:36:33', ''), ('6031590', 'BANK CHARGES - PAYPAL', '2017-08-03 04:35:15', '2017-08-03 04:35:15', '');
COMMIT;

-- ----------------------------
-- Table structure for journalentries
-- ----------------------------
DROP TABLE IF EXISTS `journalentries`;
CREATE TABLE `journalentries` (
`ID`  int(11) NOT NULL AUTO_INCREMENT ,
`AccountingCode`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`AccountName`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`CVNumber`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`BookingID`  int(11) NULL DEFAULT NULL,
`Amount`  decimal(10,0) NOT NULL ,
`Remarks`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`Type`  int(11) NOT NULL ,
`PaymentID`  int(11) NOT NULL ,
PRIMARY KEY (`ID`),
FOREIGN KEY (`AccountingCode`) REFERENCES `accountingterms` (`Code`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`BookingID`) REFERENCES `bookings` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`PaymentID`) REFERENCES `payments` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
INDEX `AccountingCode` (`AccountingCode`) USING BTREE ,
INDEX `BookingID` (`BookingID`) USING BTREE ,
INDEX `PaymentID` (`PaymentID`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of journalentries
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for modeofpayments
-- ----------------------------
DROP TABLE IF EXISTS `modeofpayments`;
CREATE TABLE `modeofpayments` (
`ID`  int(11) NOT NULL AUTO_INCREMENT ,
`Description`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
PRIMARY KEY (`ID`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=8

;

-- ----------------------------
-- Records of modeofpayments
-- ----------------------------
BEGIN;
INSERT INTO `modeofpayments` VALUES ('1', 'BANK To BANK'), ('2', 'CANCELLED'), ('3', 'DATED CHECK'), ('4', 'OTHERS'), ('5', 'PHILIPPINE PESO'), ('6', 'POST DATED CHECK'), ('7', 'US DOLLAR');
COMMIT;

-- ----------------------------
-- Table structure for payments
-- ----------------------------
DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
`ID`  int(11) NOT NULL AUTO_INCREMENT,
`PaymentNo`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`PaymentDate`  datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP ,
`BusinessLocationCode`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`TravelAgencyID`  int(11) NOT NULL ,
`ModeOfPaymentID`  int(255) NOT NULL ,
`ReceiptNo`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`CheckDate`  datetime NULL DEFAULT NULL ,
`CheckNo`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`PreparedBy`  int(11) NOT NULL ,
`VerifiedBy`  int(11) NOT NULL ,
`Remarks`  varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
PRIMARY KEY (`ID`),
FOREIGN KEY (`BusinessLocationCode`) REFERENCES `businesslocations` (`Code`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`TravelAgencyID`) REFERENCES `travelagencies` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`ModeOfPaymentID`) REFERENCES `modeofpayments` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`PreparedBy`) REFERENCES `agents` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
FOREIGN KEY (`VerifiedBy`) REFERENCES `agents` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
INDEX `BusinessLocationCode` (`BusinessLocationCode`) USING BTREE ,
INDEX `TravelAgencyID` (`TravelAgencyID`) USING BTREE ,
INDEX `ModeOfPaymentID` (`ModeOfPaymentID`) USING BTREE ,
INDEX `PreparedBy` (`PreparedBy`) USING BTREE ,
INDEX `VerifiedBy` (`VerifiedBy`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci

;

ALTER TABLE `bookings`
ADD COLUMN `IsPaid`  bit NOT NULL DEFAULT 0 AFTER `TravelAgencyID`;
