/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100108
Source Host           : localhost:3306
Source Database       : isebooking0731

Target Server Type    : MYSQL
Target Server Version : 100108
File Encoding         : 65001

Date: 2017-09-08 15:50:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Procedure structure for rptGetSales
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptGetSales`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptGetSales`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	#Routine body goes here...

SELECT
		(book.Adults + book.Senior) AS Adults,
	book.CreatedBy AS AgentID,
	book.FlightNo1 AS ArrivalFlight,
	book.FlightNo2 AS DepartureFlight,
	book.ID AS BookingID,
	book.CVNumber,
	book.FlightDate1 AS DateIn,
	book.FlightDate2 AS DateOut,
	book.Escorts,
	book.FOC,
	(
		book.Childrens + book.ChildrensBelowSix + book.Infants
	) AS Kids,
	book.Infants,
	book.GroupName AS PaxName,
	book.Total,
	book.TourGuides,
	TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	) AS TransactionDate,
	agent.TravelAgencyID,
	tra.`Name` AS TravelAgency
FROM
	bookings book
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
WHERE
	fromDate <= TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	)
AND toDate >= TruncateTime (
	LEAST(
		IFNULL(
			book.FlightDate1,
			book.FlightDate2
		),
		IFNULL(
			book.FlightDate2,
			book.FlightDate1
		)
	)
)
AND book.`Status` = 2
ORDER BY book.ID;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptGetSalesByMarketTypeSummary
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptGetSalesByMarketTypeSummary`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptGetSalesByMarketTypeSummary`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	

SELECT
mkt.ID as MarketTypeID,
mkt.Description as MarketType,
0 as Transfer,
0 as BoatFee,
0 as EnvironmentFee,
0 as Others,
0 as NightNavigation,
bd.SourceID,
bd.Boat,
bd.Amount,
bd.Quantity	

FROM
	bookings book
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
INNER JOIN markettypes mkt on agent.MarketTypeID = mkt.ID
INNER JOIN bookingdetails bd on book.ID = bd.BookingID
WHERE
	fromDate <= TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	)
AND toDate >= TruncateTime (
	LEAST(
		IFNULL(
			book.FlightDate1,
			book.FlightDate2
		),
		IFNULL(
			book.FlightDate2,
			book.FlightDate1
		)
	)
)
AND book.`Status` = 2
ORDER BY book.ID;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptGetSalesBySeller
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptGetSalesBySeller`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptGetSalesBySeller`(IN `fromDate` date,IN `toDate` date)
BEGIN
	

SELECT

agent.ID as AgentID,
agent.FirstName + " " + agent.LastName as AgentName,
0 as Transfer,
bd.Boat,
bd.Amount,
0 as BoatFee,
0 as EnvironmentFee,
0 as TerminalFee,
0 as Others,
0 as NightNavigation,
bd.Quantity,
bd.SourceID

FROM
	bookings book
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
INNER JOIN markettypes mkt on agent.MarketTypeID = mkt.ID
INNER JOIN bookingdetails bd on book.ID = bd.BookingID
WHERE
	fromDate <= TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	)
AND toDate >= TruncateTime (
	LEAST(
		IFNULL(
			book.FlightDate1,
			book.FlightDate2
		),
		IFNULL(
			book.FlightDate2,
			book.FlightDate1
		)
	)
)
AND book.`Status` = 2
ORDER BY book.ID;


END
;;
DELIMITER ;
