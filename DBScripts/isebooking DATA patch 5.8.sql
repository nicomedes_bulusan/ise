TRUNCATE TABLE `forupdates`;

INSERT INTO `forupdates` (`CVNumber`, `DateCreated`, `DateLastUpdated`) VALUES ('MNL - 20170000195', '2017-10-04 09:32:48', '2017-10-04 09:32:48');
INSERT INTO `forupdates` (`CVNumber`, `DateCreated`, `DateLastUpdated`) VALUES ('MNL - 20170000461', '2017-10-04 09:32:48', '2017-10-04 09:32:48');
INSERT INTO `forupdates` (`CVNumber`, `DateCreated`, `DateLastUpdated`) VALUES ('MNL - 20170001070', '2017-10-04 09:32:49', '2017-10-04 09:32:49');
INSERT INTO `forupdates` (`CVNumber`, `DateCreated`, `DateLastUpdated`) VALUES ('MNL - 20170001071', '2017-10-04 09:32:49', '2017-10-04 09:32:49');
INSERT INTO `forupdates` (`CVNumber`, `DateCreated`, `DateLastUpdated`) VALUES ('MNL - 20170001088', '2017-10-04 09:32:49', '2017-10-04 09:32:49');
INSERT INTO `forupdates` (`CVNumber`, `DateCreated`, `DateLastUpdated`) VALUES ('MNL - 20170001067', '2017-10-04 09:32:49', '2017-10-04 09:32:49');
INSERT INTO `forupdates` (`CVNumber`, `DateCreated`, `DateLastUpdated`) VALUES ('MNL - 20170001066', '2017-10-04 09:32:49', '2017-10-04 09:32:49');
INSERT INTO `forupdates` (`CVNumber`, `DateCreated`, `DateLastUpdated`) VALUES ('MNL - 20170001069', '2017-10-04 09:32:49', '2017-10-04 09:32:49');
INSERT INTO `forupdates` (`CVNumber`, `DateCreated`, `DateLastUpdated`) VALUES ('MNL - 20170001065', '2017-10-04 09:32:49', '2017-10-04 09:32:49');
INSERT INTO `forupdates` (`CVNumber`, `DateCreated`, `DateLastUpdated`) VALUES ('MNL - 20170001068', '2017-10-04 09:32:49', '2017-10-04 09:32:49');


Update bookings book
INNER JOIN forupdates f on book.CVNumber = f.CVNumber
SET book.PaymentTypeID =1
WHERE book.ID not IN (SELECT BookingID from accountreceivables)
AND book.PaymentTypeID !=1;


INSERT INTO accountreceivables(AgentID,BookingID,TransactionDate,DueDate,OriginalAmount,Balance,BillingPeriodID)
SELECT book.CreatedBy,book.ID,NOW(),NOW(),book.Total,book.Total,null FROM bookings book
INNER JOIN forupdates f on book.CVNumber = f.CVNumber
WHERE book.ID not IN (SELECT BookingID from accountreceivables)
AND book.PaymentTypeID =1;
