ALTER TABLE `ratematrix`
ADD COLUMN `FromDate`  date NULL DEFAULT NULL AFTER `Type`,
ADD COLUMN `ToDate`  date NULL DEFAULT NULL AFTER `FromDate`;

update ratematrix set FromDate='2017-01-01',ToDate='2017-12-31';

ALTER TABLE `ratematrixagents`
ADD COLUMN `FromDate`  date NULL DEFAULT NULL AFTER `Type`,
ADD COLUMN `ToDate`  date NULL DEFAULT NULL AFTER `FromDate`;

update ratematrixagents set FromDate='2017-01-01',ToDate='2017-12-31';

ALTER TABLE `ratematrix`
MODIFY COLUMN `FromDate`  date NOT NULL AFTER `Type`,
MODIFY COLUMN `ToDate`  date NOT NULL AFTER `FromDate`;

ALTER TABLE `ratematrixagents`
MODIFY COLUMN `FromDate`  date NOT NULL AFTER `Type`,
MODIFY COLUMN `ToDate`  date NOT NULL AFTER `FromDate`;

update bookingotherchargesselected set OCID='withTFInBelowSix' where OCID='withTerminalEnvBelowSix';
update bookingotherchargesselected set OCID='withTFInTourGuide' where OCID='withTerminalEnvFeeTourGuide';
update bookingotherchargesselected set OCID='withTFInEscort' where OCID='withTerminalEnvFeeEscort';
update bookingotherchargesselected set OCID='withTFInFOC' where OCID='withTerminalEnvFeeFOC';

