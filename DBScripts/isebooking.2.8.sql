CREATE TABLE `bookinglogs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BookingID` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  `DateCreated` datetime DEFAULT CURRENT_TIMESTAMP,
  `AgentID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `bookinglogs_ibfk_1` (`BookingID`),
  KEY `AgentID` (`AgentID`),
  CONSTRAINT `bookinglogs_ibfk_1` FOREIGN KEY (`BookingID`) REFERENCES `bookings` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `bookinglogs_ibfk_2` FOREIGN KEY (`AgentID`) REFERENCES `agents` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
