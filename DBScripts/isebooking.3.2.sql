CREATE TABLE `cvcounter` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DateCreated` datetime DEFAULT NULL,
  `Year` year(4) NOT NULL,
  `Series` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Year` (`Year`,`Series`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;