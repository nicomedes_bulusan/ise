/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100108
Source Host           : localhost:3306
Source Database       : isebooking

Target Server Type    : MYSQL
Target Server Version : 100108
File Encoding         : 65001

Date: 2016-10-21 12:37:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for agents
-- ----------------------------
DROP TABLE IF EXISTS `agents`;
CREATE TABLE `agents` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(255) NOT NULL,
  `MiddleName` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `ContactNumber` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CreditLimit` double NOT NULL DEFAULT '0',
  `RemainingCredit` double NOT NULL DEFAULT '0',
  `YearToDateAmount` double NOT NULL DEFAULT '0',
  `MonthToDateAmount` double NOT NULL DEFAULT '0',
  `IsActive` int(11) NOT NULL DEFAULT '1',
  `Password` varchar(255) DEFAULT NULL,
  `Type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agents
-- ----------------------------
INSERT INTO `agents` VALUES ('1', 'John', ' ', 'Doe', 'NO CONTACT', 'johndoe@gmail.com', '2016-10-10 08:26:46', '2016-10-11 08:34:16', '500000', '484240', '0', '0', '1', '$2a$10$dpDzH3Ym0HUKwjoZJ.WqCuOIQtP0LayYFn0tJ/9HkMEXZXDQb7eOW', '2');
INSERT INTO `agents` VALUES ('2', 'Juan', '', 'Cruz', 'NO CONTACT', 'juandelacruz@gmail.com', '2016-09-07 04:00:28', '2016-10-11 08:46:28', '50000', '50000', '0', '0', '1', '$2a$10$dpDzH3Ym0HUKwjoZJ.WqCuOIQtP0LayYFn0tJ/9HkMEXZXDQb7eOW', '1');
INSERT INTO `agents` VALUES ('3', 'Maria', 'P@ssw0rd', 'makiling', 'mar', 'mariamakiling@yahoo.com', '2016-10-11 07:47:51', '2016-10-11 08:46:30', '5000', '5000', '0', '0', '0', '$2a$10$dpDzH3Ym0HUKwjoZJ.WqCuOIQtP0LayYFn0tJ/9HkMEXZXDQb7eOW', '1');
INSERT INTO `agents` VALUES ('4', 'nic', 'P@ssw0rd', 'nic', 'nic nac', 'nbulusanjr@gmail.com', '2016-10-11 08:57:25', '2016-10-11 08:59:06', '5000', '5000', '0', '0', '0', '$2a$10$yfaz0EayQP3CC9gE0XcPAObxM8p1QJj23mzhk7vWhlfIegVkBoiAG', '2');
INSERT INTO `agents` VALUES ('5', 'nic', 'P@ssw0rd', 'nic', 'nic nac', 'nicz_bz@yahoo.com', '2016-10-11 08:59:29', '2016-10-11 08:59:29', '50000', '50000', '0', '0', '1', '$2a$10$vveoNTUkthzu7V2Q/LaIse1lZQtpUGmf/dUUqo//eWZaoEJb8iscm', '1');

-- ----------------------------
-- Table structure for bookingattachments
-- ----------------------------
DROP TABLE IF EXISTS `bookingattachments`;
CREATE TABLE `bookingattachments` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Filename` varchar(255) NOT NULL,
  `MimeType` varchar(255) NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `BookingID` int(11) NOT NULL,
  `OriginalFilename` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `bookingattachments_ibfk_1` (`BookingID`),
  CONSTRAINT `bookingattachments_ibfk_1` FOREIGN KEY (`BookingID`) REFERENCES `bookings` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bookingattachments
-- ----------------------------
INSERT INTO `bookingattachments` VALUES ('1', 'TEST - 1474406380.pdf', 'application/pdf', '2016-09-21 05:20:22', '9', 'TEST.pdf');

-- ----------------------------
-- Table structure for bookingdetails
-- ----------------------------
DROP TABLE IF EXISTS `bookingdetails`;
CREATE TABLE `bookingdetails` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BookingID` int(11) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `Amount` double(255,2) NOT NULL,
  `Quantity` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `bookingdetails_ibfk_1` (`BookingID`),
  CONSTRAINT `bookingdetails_ibfk_1` FOREIGN KEY (`BookingID`) REFERENCES `bookings` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bookingdetails
-- ----------------------------
INSERT INTO `bookingdetails` VALUES ('5', '4', 'TF IN', '20.00', '5');
INSERT INTO `bookingdetails` VALUES ('6', '4', 'TF OUT', '20.00', '5');
INSERT INTO `bookingdetails` VALUES ('7', '4', 'ENVIRONMENTAL FEE', '10.00', '5');
INSERT INTO `bookingdetails` VALUES ('8', '4', 'NIGHT NAVIGATION FEE', '10.00', '5');
INSERT INTO `bookingdetails` VALUES ('9', '4', 'Kalibo - Caticlan', '500.00', '5');
INSERT INTO `bookingdetails` VALUES ('10', '4', 'TF IN', '20.00', '5');
INSERT INTO `bookingdetails` VALUES ('11', '4', 'TF OUT', '20.00', '5');
INSERT INTO `bookingdetails` VALUES ('12', '4', 'ENVIRONMENTAL FEE', '10.00', '5');
INSERT INTO `bookingdetails` VALUES ('13', '4', 'NIGHT NAVIGATION FEE', '10.00', '5');
INSERT INTO `bookingdetails` VALUES ('14', '4', 'Kalibo - Caticlan', '500.00', '5');
INSERT INTO `bookingdetails` VALUES ('15', '5', 'TF IN', '20.00', '5');
INSERT INTO `bookingdetails` VALUES ('16', '5', 'TF OUT', '20.00', '5');
INSERT INTO `bookingdetails` VALUES ('17', '5', 'ENVIRONMENTAL FEE', '10.00', '5');
INSERT INTO `bookingdetails` VALUES ('18', '5', 'NIGHT NAVIGATION FEE', '10.00', '5');
INSERT INTO `bookingdetails` VALUES ('19', '5', 'Kalibo - Caticlan', '500.00', '5');
INSERT INTO `bookingdetails` VALUES ('20', '6', 'TF IN', '20.00', '10');
INSERT INTO `bookingdetails` VALUES ('21', '6', 'TF OUT', '20.00', '10');
INSERT INTO `bookingdetails` VALUES ('22', '6', 'ENVIRONMENTAL FEE', '10.00', '10');
INSERT INTO `bookingdetails` VALUES ('23', '6', 'NIGHT NAVIGATION FEE', '10.00', '10');
INSERT INTO `bookingdetails` VALUES ('24', '6', 'Kalibo - Caticlan', '600.00', '5');
INSERT INTO `bookingdetails` VALUES ('25', '7', 'TF IN', '20.00', '5');
INSERT INTO `bookingdetails` VALUES ('26', '7', 'TF OUT', '20.00', '5');
INSERT INTO `bookingdetails` VALUES ('27', '7', 'ENVIRONMENTAL FEE', '10.00', '5');
INSERT INTO `bookingdetails` VALUES ('28', '7', 'NIGHT NAVIGATION FEE', '10.00', '5');
INSERT INTO `bookingdetails` VALUES ('29', '7', 'Kalibo - Caticlan', '600.00', '5');
INSERT INTO `bookingdetails` VALUES ('30', '8', 'TF IN', '20.00', '10');
INSERT INTO `bookingdetails` VALUES ('31', '8', 'TF OUT', '20.00', '10');
INSERT INTO `bookingdetails` VALUES ('32', '8', 'ENVIRONMENTAL FEE', '10.00', '10');
INSERT INTO `bookingdetails` VALUES ('33', '8', 'NIGHT NAVIGATION FEE', '10.00', '10');
INSERT INTO `bookingdetails` VALUES ('34', '8', 'Kalibo - Caticlan', '600.00', '5');
INSERT INTO `bookingdetails` VALUES ('35', '9', 'TF IN', '20.00', '10');
INSERT INTO `bookingdetails` VALUES ('36', '9', 'TF OUT', '20.00', '10');
INSERT INTO `bookingdetails` VALUES ('37', '9', 'ENVIRONMENTAL FEE', '10.00', '10');
INSERT INTO `bookingdetails` VALUES ('38', '9', 'NIGHT NAVIGATION FEE', '10.00', '10');
INSERT INTO `bookingdetails` VALUES ('39', '9', 'Kalibo - Caticlan', '600.00', '5');
INSERT INTO `bookingdetails` VALUES ('40', '10', 'TF IN', '20.00', '10');
INSERT INTO `bookingdetails` VALUES ('41', '10', 'TF OUT', '20.00', '10');
INSERT INTO `bookingdetails` VALUES ('42', '10', 'ENVIRONMENTAL FEE', '10.00', '10');
INSERT INTO `bookingdetails` VALUES ('43', '10', 'NIGHT NAVIGATION FEE', '10.00', '10');
INSERT INTO `bookingdetails` VALUES ('44', '10', 'test', '12.00', '10');
INSERT INTO `bookingdetails` VALUES ('45', '10', 'Kalibo - Caticlan', '600.00', '5');
INSERT INTO `bookingdetails` VALUES ('46', '11', 'TF IN', '20.00', '20');
INSERT INTO `bookingdetails` VALUES ('47', '11', 'TF OUT', '20.00', '20');
INSERT INTO `bookingdetails` VALUES ('48', '11', 'ENVIRONMENTAL FEE', '10.00', '20');
INSERT INTO `bookingdetails` VALUES ('49', '11', 'NIGHT NAVIGATION FEE', '10.00', '20');
INSERT INTO `bookingdetails` VALUES ('50', '11', 'test', '12.00', '20');
INSERT INTO `bookingdetails` VALUES ('51', '11', 'Free-of-charge', '0.00', '5');
INSERT INTO `bookingdetails` VALUES ('52', '11', 'Kalibo - Caticlan', '400.00', '5');
INSERT INTO `bookingdetails` VALUES ('53', '11', 'Kalibo - Caticlan', '600.00', '5');
INSERT INTO `bookingdetails` VALUES ('54', '12', 'TF IN', '20.00', '5');
INSERT INTO `bookingdetails` VALUES ('55', '12', 'ENVIRONMENTAL FEE', '10.00', '5');
INSERT INTO `bookingdetails` VALUES ('56', '12', 'Kalibo - Caticlan', '750.00', '5');
INSERT INTO `bookingdetails` VALUES ('57', '13', 'TF IN', '20.00', '10');
INSERT INTO `bookingdetails` VALUES ('58', '13', 'TF OUT', '20.00', '10');
INSERT INTO `bookingdetails` VALUES ('59', '13', 'ENVIRONMENTAL FEE', '10.00', '10');
INSERT INTO `bookingdetails` VALUES ('60', '13', 'NIGHT NAVIGATION FEE', '10.00', '0');
INSERT INTO `bookingdetails` VALUES ('61', '13', 'test', '12.00', '0');
INSERT INTO `bookingdetails` VALUES ('62', '13', 'Free-of-charge', '0.00', '5');
INSERT INTO `bookingdetails` VALUES ('63', '13', 'Kalibo - Caticlan', '750.00', '10');

-- ----------------------------
-- Table structure for bookingotherchargesindicator
-- ----------------------------
DROP TABLE IF EXISTS `bookingotherchargesindicator`;
CREATE TABLE `bookingotherchargesindicator` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BookingID` int(11) NOT NULL,
  `ChargeID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `BookingID` (`BookingID`),
  KEY `ChargeID` (`ChargeID`),
  CONSTRAINT `bookingotherchargesindicator_ibfk_1` FOREIGN KEY (`BookingID`) REFERENCES `bookings` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `bookingotherchargesindicator_ibfk_2` FOREIGN KEY (`ChargeID`) REFERENCES `othercharges` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bookingotherchargesindicator
-- ----------------------------
INSERT INTO `bookingotherchargesindicator` VALUES ('1', '8', '1');
INSERT INTO `bookingotherchargesindicator` VALUES ('2', '8', '2');
INSERT INTO `bookingotherchargesindicator` VALUES ('3', '8', '3');
INSERT INTO `bookingotherchargesindicator` VALUES ('4', '8', '4');
INSERT INTO `bookingotherchargesindicator` VALUES ('5', '9', '1');
INSERT INTO `bookingotherchargesindicator` VALUES ('6', '9', '2');
INSERT INTO `bookingotherchargesindicator` VALUES ('7', '9', '3');
INSERT INTO `bookingotherchargesindicator` VALUES ('8', '9', '4');
INSERT INTO `bookingotherchargesindicator` VALUES ('9', '10', '1');
INSERT INTO `bookingotherchargesindicator` VALUES ('10', '10', '2');
INSERT INTO `bookingotherchargesindicator` VALUES ('11', '10', '3');
INSERT INTO `bookingotherchargesindicator` VALUES ('12', '10', '4');
INSERT INTO `bookingotherchargesindicator` VALUES ('13', '10', '5');
INSERT INTO `bookingotherchargesindicator` VALUES ('14', '11', '1');
INSERT INTO `bookingotherchargesindicator` VALUES ('15', '11', '2');
INSERT INTO `bookingotherchargesindicator` VALUES ('16', '11', '3');
INSERT INTO `bookingotherchargesindicator` VALUES ('17', '11', '4');
INSERT INTO `bookingotherchargesindicator` VALUES ('18', '11', '5');
INSERT INTO `bookingotherchargesindicator` VALUES ('19', '12', '1');
INSERT INTO `bookingotherchargesindicator` VALUES ('20', '12', '3');
INSERT INTO `bookingotherchargesindicator` VALUES ('21', '12', '4');
INSERT INTO `bookingotherchargesindicator` VALUES ('22', '13', '1');
INSERT INTO `bookingotherchargesindicator` VALUES ('23', '13', '2');
INSERT INTO `bookingotherchargesindicator` VALUES ('24', '13', '3');
INSERT INTO `bookingotherchargesindicator` VALUES ('25', '13', '4');
INSERT INTO `bookingotherchargesindicator` VALUES ('26', '13', '5');

-- ----------------------------
-- Table structure for bookings
-- ----------------------------
DROP TABLE IF EXISTS `bookings`;
CREATE TABLE `bookings` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupName` varchar(255) NOT NULL,
  `ContactPerson` varchar(255) NOT NULL,
  `ContactNumber` varchar(255) NOT NULL,
  `Remarks` varchar(255) NOT NULL,
  `CVNumber` varchar(255) DEFAULT NULL,
  `Adults` int(11) NOT NULL DEFAULT '0',
  `Childrens` int(11) NOT NULL DEFAULT '0',
  `Infants` int(11) NOT NULL DEFAULT '0',
  `TourGuides` int(11) NOT NULL DEFAULT '0',
  `Escorts` int(11) NOT NULL DEFAULT '0',
  `FOC` int(11) NOT NULL DEFAULT '0',
  `Routing` int(11) NOT NULL DEFAULT '1',
  `PickUpArrival` int(11) NOT NULL,
  `DropOffArrival` int(11) NOT NULL,
  `DropOffLocation` int(255) DEFAULT NULL,
  `PickUpDeparture` int(11) DEFAULT NULL,
  `DrofOffDeparture` int(11) DEFAULT NULL,
  `FlightNo1` varchar(255) DEFAULT NULL,
  `FlightNo2` varchar(255) DEFAULT NULL,
  `FlightDate1` datetime NOT NULL,
  `FlightDate2` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `Total` float DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `Status` int(11) NOT NULL DEFAULT '0',
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ChildrensBelowSix` int(11) NOT NULL DEFAULT '0',
  `DropOffLocation1` varchar(255) DEFAULT NULL,
  `DropOffLocation2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PickUpArrival` (`PickUpArrival`),
  KEY `DropOffArrival` (`DropOffArrival`),
  KEY `PickUpDeparture` (`PickUpDeparture`),
  KEY `DrofOffDeparture` (`DrofOffDeparture`),
  KEY `CreatedBy` (`CreatedBy`),
  CONSTRAINT `bookings_ibfk_2` FOREIGN KEY (`PickUpArrival`) REFERENCES `destinations` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `bookings_ibfk_3` FOREIGN KEY (`DropOffArrival`) REFERENCES `destinations` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `bookings_ibfk_4` FOREIGN KEY (`PickUpDeparture`) REFERENCES `destinations` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `bookings_ibfk_5` FOREIGN KEY (`DrofOffDeparture`) REFERENCES `destinations` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `bookings_ibfk_6` FOREIGN KEY (`CreatedBy`) REFERENCES `agents` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bookings
-- ----------------------------
INSERT INTO `bookings` VALUES ('4', 'Nicomedes Bulusan', 'Nicomedes Bulusan', 'Nicomedes Bulusan', 'Nicomedes Bulusan', '', '5', '0', '0', '0', '0', '0', '1', '1', '2', '0', null, null, null, null, '2016-09-21 04:05:27', '2016-10-11 08:14:55', '5600', '1', '1', '2016-09-07 04:59:12', '2016-10-11 08:14:55', '0', null, null);
INSERT INTO `bookings` VALUES ('5', '1', '1', '1', '1', '', '5', '0', '0', '0', '0', '0', '1', '1', '2', '0', null, null, '1', null, '2016-09-04 00:00:00', '2016-10-11 08:08:40', '2800', '1', '0', '2016-09-07 04:59:12', '2016-10-11 08:08:40', '0', null, null);
INSERT INTO `bookings` VALUES ('6', 'Julius Caesar', 'Julius Caesar', 'Julius Caesar', 'Julius Caesar', '', '5', '0', '0', '5', '0', '0', '1', '1', '2', null, null, null, null, null, '2016-09-21 10:00:00', '2016-10-11 08:08:40', '3600', '1', '0', '0001-01-01 00:00:00', '2016-10-11 08:08:40', '0', null, null);
INSERT INTO `bookings` VALUES ('7', 'Julius Caesar', 'Julius Caesar', 'Julius Caesar', 'Julius Caesar', '', '5', '0', '0', '0', '0', '0', '1', '1', '2', null, null, null, null, null, '2016-09-30 10:00:00', '2016-10-11 08:08:40', '3300', '1', '0', '2016-09-21 04:34:05', '2016-10-11 08:08:40', '0', null, null);
INSERT INTO `bookings` VALUES ('8', 'Julius Caesar', 'Julius Caesar', 'Julius Caesar', 'Julius Caesar', '', '5', '0', '0', '5', '0', '0', '1', '1', '2', null, null, null, null, null, '2016-09-30 12:00:00', '2016-10-11 08:08:40', '3600', '1', '0', '2016-09-21 04:49:55', '2016-10-11 08:08:40', '0', null, null);
INSERT INTO `bookings` VALUES ('9', 'Napoleon Bonnarotti', 'Napoleon Bonnarotti', 'Napoleon Bonnarotti', 'Napoleon Bonnarotti', '', '5', '0', '0', '5', '0', '0', '1', '1', '2', null, null, null, null, null, '2016-09-30 10:00:00', '2016-10-11 08:08:40', '3600', '1', '0', '2016-09-21 05:20:22', '2016-10-11 08:08:40', '0', null, null);
INSERT INTO `bookings` VALUES ('10', 'emilio aguinaldo', 'emilio aguinaldo', 'emilio aguinaldo', 'emilio aguinaldo', '', '5', '0', '0', '5', '0', '0', '1', '1', '2', null, null, null, null, null, '2016-10-04 23:50:00', '2016-10-11 08:14:15', '3720', '1', '1', '2016-10-04 23:44:27', '2016-10-11 08:14:15', '0', null, null);
INSERT INTO `bookings` VALUES ('11', 'Michael angelo', 'Michael angelo', 'Michael angelo', 'Michael angelo', '', '5', '5', '5', '5', '5', '5', '1', '1', '2', null, null, null, null, null, '2016-10-06 15:55:00', '2016-10-11 08:12:46', '6440', '1', '1', '2016-10-05 00:27:16', '2016-10-11 08:12:46', '5', null, null);
INSERT INTO `bookings` VALUES ('12', 'Napoleon', 'Napoleon', 'Napoleon', 'Napoleon', '', '5', '0', '0', '5', '0', '0', '1', '1', '1', null, null, null, null, null, '2016-10-22 11:10:00', null, '3900', '2', '0', '2016-10-21 10:40:15', '2016-10-21 10:40:15', '0', null, null);
INSERT INTO `bookings` VALUES ('13', 'John Doe', 'John Doe', 'John Doe', 'John Doe', '', '10', '0', '0', '10', '5', '5', '1', '1', '2', null, '2', '1', 'bcj46', 'bxg67', '2016-10-19 09:45:00', '2016-09-27 22:50:00', '8000', '2', '0', '2016-10-21 11:41:07', '2016-10-21 11:41:07', '0', 'OTHERS', null);

-- ----------------------------
-- Table structure for bookingstatus
-- ----------------------------
DROP TABLE IF EXISTS `bookingstatus`;
CREATE TABLE `bookingstatus` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(255) NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bookingstatus
-- ----------------------------

-- ----------------------------
-- Table structure for destinations
-- ----------------------------
DROP TABLE IF EXISTS `destinations`;
CREATE TABLE `destinations` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(255) NOT NULL,
  `DateCeated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of destinations
-- ----------------------------
INSERT INTO `destinations` VALUES ('1', 'Kalibo', '2016-08-30 00:13:45', '2016-08-30 00:13:47');
INSERT INTO `destinations` VALUES ('2', 'Caticlan', '2016-08-30 00:17:15', '2016-08-30 00:17:15');
INSERT INTO `destinations` VALUES ('3', 'Kalibo International Airport', '2016-08-30 00:17:36', '2016-08-30 00:17:36');
INSERT INTO `destinations` VALUES ('4', 'Resort', '2016-08-30 00:17:39', '2016-08-30 00:17:39');
INSERT INTO `destinations` VALUES ('5', 'Cagban Port - Boracay', '2016-08-30 00:18:00', '2016-08-30 00:18:07');
INSERT INTO `destinations` VALUES ('6', 'Caticlan Airport', '2016-08-30 00:18:22', '2016-08-30 00:18:22');

-- ----------------------------
-- Table structure for othercharges
-- ----------------------------
DROP TABLE IF EXISTS `othercharges`;
CREATE TABLE `othercharges` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(255) NOT NULL,
  `Amount` double(255,2) NOT NULL DEFAULT '0.00',
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IsActive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of othercharges
-- ----------------------------
INSERT INTO `othercharges` VALUES ('1', 'TF IN', '20.00', '0000-00-00 00:00:00', '2016-08-31 04:13:15', '1');
INSERT INTO `othercharges` VALUES ('2', 'TF OUT', '20.00', '0000-00-00 00:00:00', '2016-08-31 04:14:23', '1');
INSERT INTO `othercharges` VALUES ('3', 'ENVIRONMENTAL FEE', '10.00', '0000-00-00 00:00:00', '2016-08-31 04:14:39', '1');
INSERT INTO `othercharges` VALUES ('4', 'NIGHT NAVIGATION FEE', '10.00', '0000-00-00 00:00:00', '2016-08-31 04:14:51', '1');
INSERT INTO `othercharges` VALUES ('5', 'test', '12.00', '2016-09-21 05:46:15', '2016-09-21 05:46:15', '0');

-- ----------------------------
-- Table structure for ratematrix
-- ----------------------------
DROP TABLE IF EXISTS `ratematrix`;
CREATE TABLE `ratematrix` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PickUp` int(11) NOT NULL,
  `DropOff` int(11) NOT NULL,
  `Amount` double NOT NULL DEFAULT '0',
  `Boat` double NOT NULL DEFAULT '0',
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Status` int(11) NOT NULL,
  `Type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  KEY `PickUp` (`PickUp`),
  KEY `DropOff` (`DropOff`),
  CONSTRAINT `ratematrix_ibfk_1` FOREIGN KEY (`PickUp`) REFERENCES `destinations` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `ratematrix_ibfk_2` FOREIGN KEY (`DropOff`) REFERENCES `destinations` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ratematrix
-- ----------------------------
INSERT INTO `ratematrix` VALUES ('1', '1', '2', '600', '25', '2016-09-04 20:03:05', '2016-09-19 05:58:32', '1', '1');
INSERT INTO `ratematrix` VALUES ('2', '3', '4', '400', '0', '2016-09-04 20:03:19', '2016-09-21 04:20:17', '1', '1');
INSERT INTO `ratematrix` VALUES ('3', '4', '3', '400', '0', '2016-09-04 20:04:06', '2016-09-21 04:20:17', '1', '1');
INSERT INTO `ratematrix` VALUES ('4', '3', '5', '375', '0', '2016-09-04 20:05:59', '2016-09-21 04:20:17', '1', '1');
INSERT INTO `ratematrix` VALUES ('5', '3', '4', '260', '0', '2016-09-04 20:06:22', '2016-09-21 04:20:17', '1', '2');
INSERT INTO `ratematrix` VALUES ('6', '4', '3', '260', '0', '2016-09-04 20:06:48', '2016-09-21 04:20:18', '1', '2');
INSERT INTO `ratematrix` VALUES ('7', '3', '5', '235', '0', '2016-09-04 20:07:11', '2016-09-21 04:20:18', '1', '2');
INSERT INTO `ratematrix` VALUES ('8', '5', '3', '375', '0', '2016-09-04 20:07:33', '2016-09-21 04:20:18', '1', '1');
INSERT INTO `ratematrix` VALUES ('9', '5', '3', '235', '0', '2016-09-04 20:07:53', '2016-09-21 04:20:18', '1', '2');
INSERT INTO `ratematrix` VALUES ('10', '6', '5', '325', '0', '2016-09-04 20:08:22', '2016-09-21 04:20:18', '1', '1');
INSERT INTO `ratematrix` VALUES ('11', '6', '5', '205', '0', '2016-09-04 20:08:43', '2016-09-21 04:20:18', '1', '2');
INSERT INTO `ratematrix` VALUES ('12', '1', '2', '400', '0', '2016-09-07 05:46:53', '2016-09-21 04:20:18', '1', '2');
INSERT INTO `ratematrix` VALUES ('18', '1', '2', '1000', '100', '2016-09-19 06:06:33', '2016-09-19 06:06:33', '1', '2');

-- ----------------------------
-- Table structure for ratematrixagents
-- ----------------------------
DROP TABLE IF EXISTS `ratematrixagents`;
CREATE TABLE `ratematrixagents` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PickUp` int(11) NOT NULL,
  `DropOff` int(11) NOT NULL,
  `Amount` double NOT NULL DEFAULT '0',
  `Boat` double NOT NULL DEFAULT '0',
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Status` int(11) NOT NULL,
  `Type` int(11) NOT NULL DEFAULT '1',
  `AgentID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `PickUp` (`PickUp`),
  KEY `DropOff` (`DropOff`),
  KEY `AgentID` (`AgentID`),
  CONSTRAINT `ratematrixagents_ibfk_1` FOREIGN KEY (`PickUp`) REFERENCES `destinations` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `ratematrixagents_ibfk_2` FOREIGN KEY (`DropOff`) REFERENCES `destinations` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `ratematrixagents_ibfk_3` FOREIGN KEY (`AgentID`) REFERENCES `agents` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ratematrixagents
-- ----------------------------
INSERT INTO `ratematrixagents` VALUES ('1', '1', '2', '750', '25', '2016-09-04 20:03:05', '2016-10-21 10:37:18', '1', '1', '2');
INSERT INTO `ratematrixagents` VALUES ('2', '3', '4', '420', '0', '2016-09-04 20:03:19', '2016-10-21 10:37:18', '1', '1', '2');
INSERT INTO `ratematrixagents` VALUES ('3', '4', '3', '410', '0', '2016-09-04 20:04:06', '2016-10-21 10:37:18', '1', '1', '2');
INSERT INTO `ratematrixagents` VALUES ('4', '3', '5', '400', '0', '2016-09-04 20:05:59', '2016-10-21 10:37:18', '1', '1', '2');
INSERT INTO `ratematrixagents` VALUES ('5', '3', '4', '280', '0', '2016-09-04 20:06:22', '2016-10-21 10:37:18', '1', '2', '2');
INSERT INTO `ratematrixagents` VALUES ('6', '4', '3', '280', '0', '2016-09-04 20:06:48', '2016-10-21 10:37:18', '1', '2', '2');
INSERT INTO `ratematrixagents` VALUES ('7', '3', '5', '250', '0', '2016-09-04 20:07:11', '2016-10-21 10:37:18', '1', '2', '2');
INSERT INTO `ratematrixagents` VALUES ('8', '5', '3', '400', '0', '2016-09-04 20:07:33', '2016-10-21 10:37:19', '1', '1', '2');
INSERT INTO `ratematrixagents` VALUES ('9', '5', '3', '250', '0', '2016-09-04 20:07:53', '2016-10-21 10:37:19', '1', '2', '2');
INSERT INTO `ratematrixagents` VALUES ('10', '6', '5', '350', '0', '2016-09-04 20:08:22', '2016-10-21 10:37:19', '1', '1', '2');
INSERT INTO `ratematrixagents` VALUES ('11', '6', '5', '200', '0', '2016-09-04 20:08:43', '2016-10-21 10:37:19', '1', '2', '2');
INSERT INTO `ratematrixagents` VALUES ('12', '1', '2', '400', '0', '2016-09-07 05:46:53', '2016-10-21 10:37:19', '1', '2', '2');
INSERT INTO `ratematrixagents` VALUES ('18', '1', '2', '1000', '100', '2016-09-19 06:06:33', '2016-10-21 10:37:19', '1', '2', '2');
INSERT INTO `ratematrixagents` VALUES ('20', '1', '2', '100', '10', '2016-10-11 18:22:12', '2016-10-11 18:22:12', '1', '1', '5');
