/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100108
Source Host           : localhost:3306
Source Database       : isebooking

Target Server Type    : MYSQL
Target Server Version : 100108
File Encoding         : 65001

Date: 2017-05-04 09:27:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for accountreceivables
-- ----------------------------
DROP TABLE IF EXISTS `accountreceivables`;
CREATE TABLE `accountreceivables` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AgentID` int(11) NOT NULL,
  `BookingID` int(11) NOT NULL,
  `TransactionDate` datetime NOT NULL,
  `DueDate` datetime NOT NULL,
  `OriginalAmount` double NOT NULL,
  `Balance` double DEFAULT NULL,
  `BillingPeriodID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `AgentID` (`AgentID`),
  KEY `BookingID` (`BookingID`),
  KEY `BillingPeriodID` (`BillingPeriodID`),
  CONSTRAINT `accountreceivables_ibfk_1` FOREIGN KEY (`AgentID`) REFERENCES `agents` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `accountreceivables_ibfk_2` FOREIGN KEY (`BookingID`) REFERENCES `bookings` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `accountreceivables_ibfk_3` FOREIGN KEY (`BillingPeriodID`) REFERENCES `billingperiods` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for batch
-- ----------------------------
DROP TABLE IF EXISTS `batch`;
CREATE TABLE `batch` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DateCreated` datetime NOT NULL,
  `DateLastUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `Processed` int(11) NOT NULL DEFAULT '0',
  `Total` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for billingperiods
-- ----------------------------
DROP TABLE IF EXISTS `billingperiods`;
CREATE TABLE `billingperiods` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `StartingDate` datetime NOT NULL,
  `EndingDate` datetime NOT NULL,
  `IsOpen` bit(1) NOT NULL DEFAULT b'0',
  `AgentID` int(11) NOT NULL,
  `LastUpdate` datetime DEFAULT NULL,
  `LastUpdateTest` varchar(255) DEFAULT NULL,
  `BatchID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `AgentID` (`AgentID`),
  KEY `billingperiods_ibfk_2` (`BatchID`),
  CONSTRAINT `billingperiods_ibfk_1` FOREIGN KEY (`AgentID`) REFERENCES `agents` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `billingperiods_ibfk_2` FOREIGN KEY (`BatchID`) REFERENCES `batch` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for billingstatements
-- ----------------------------
DROP TABLE IF EXISTS `billingstatements`;
CREATE TABLE `billingstatements` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Filename` varchar(255) NOT NULL,
  `BacthID` int(11) NOT NULL,
  `BillingPeriodID` int(11) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `DateSent` datetime DEFAULT NULL,
  `IsSent` bit(1) NOT NULL DEFAULT b'0',
  `AgentID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE PROCEDURE `UpdateBatch`(IN `BatchID` int)
BEGIN
	
Update batch set Processed = Processed + 1 where ID = BatchID;

END
