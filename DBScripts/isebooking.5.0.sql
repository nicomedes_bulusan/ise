INSERT INTO `isebooking0731`.`accountingterms` (`Code`, `Description`, `DateCreated`, `DateLastUpdated`, `IsActive`) VALUES ('100001', 'CASH ADVANCE', '2017-09-04 09:19:40', '2017-09-04 09:19:40', b'1');
ALTER TABLE `travelagencies`
ADD COLUMN `CashDeposits`  double NOT NULL DEFAULT 0 AFTER `IsActive`;

ALTER TABLE `journalentries` DROP FOREIGN KEY `journalentries_ibfk_1`;

ALTER TABLE `journalentries` ADD CONSTRAINT `journalentries_ibfk_1` FOREIGN KEY (`AccountingCode`) REFERENCES `accountingterms` (`Code`) ON DELETE RESTRICT ON UPDATE CASCADE;
