/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100108
Source Host           : localhost:3306
Source Database       : isebookinglive

Target Server Type    : MYSQL
Target Server Version : 100108
File Encoding         : 65001

Date: 2018-03-07 10:21:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for onlinebookings
-- ----------------------------
DROP TABLE IF EXISTS `onlinebookings`;
CREATE TABLE `onlinebookings` (
`ID`  int(11) NOT NULL AUTO_INCREMENT ,
`OnlineBookingID`  int(11) NOT NULL ,
`DateCreated`  datetime NOT NULL ,
PRIMARY KEY (`ID`),
UNIQUE INDEX `OnlineBookingID` (`OnlineBookingID`) USING BTREE 
)
ENGINE=InnoDB;


-- ----------------------------
-- Table structure for syncerrors
-- ----------------------------
DROP TABLE IF EXISTS `syncerrors`;
CREATE TABLE `syncerrors` (
`ID`  int(11) NOT NULL AUTO_INCREMENT ,
`OrderID`  int(11) NOT NULL ,
`Remarks`  text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`DateCreated`  datetime NOT NULL ,
PRIMARY KEY (`ID`)
)
ENGINE=InnoDB;


-- ----------------------------
-- Table structure for synclogs
-- ----------------------------
DROP TABLE IF EXISTS `synclogs`;
CREATE TABLE `synclogs` (
`ID`  int(11) NOT NULL AUTO_INCREMENT ,
`DateCreated`  datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ,
`DateLastUpdated`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
`CompletedDate`  datetime NOT NULL ,
PRIMARY KEY (`ID`)
)
ENGINE=InnoDB;

ALTER TABLE `bookings`
MODIFY COLUMN `Remarks`  text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL AFTER `ContactNumber`;

ALTER TABLE `manifesto`
MODIFY COLUMN `Remarks`  text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL AFTER `Date`;
