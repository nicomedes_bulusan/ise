ALTER TABLE `billingperiods`
ADD COLUMN `Payment`  double NOT NULL DEFAULT 0 AFTER `NetTotal`,
ADD COLUMN `Status`  int NOT NULL DEFAULT 0 AFTER `Payment`;
