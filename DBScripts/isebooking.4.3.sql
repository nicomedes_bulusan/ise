ALTER TABLE `bookings`
ADD COLUMN `Class2`  int NULL AFTER `IsPaid`;

ALTER TABLE `payments`
MODIFY COLUMN `VerifiedBy`  int(11) NULL AFTER `PreparedBy`;

