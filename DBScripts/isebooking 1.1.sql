/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100108
Source Host           : localhost:3306
Source Database       : isebooking

Target Server Type    : MYSQL
Target Server Version : 100108
File Encoding         : 65001

Date: 2016-09-04 22:41:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for agents
-- ----------------------------
DROP TABLE IF EXISTS `agents`;
CREATE TABLE `agents` (
  `ID` int(11) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `MiddleName` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `ContactNumber` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CreditLimit` double NOT NULL DEFAULT '0',
  `RemainingCredit` double NOT NULL DEFAULT '0',
  `YearToDateAmount` double NOT NULL DEFAULT '0',
  `MonthToDateAmount` double NOT NULL DEFAULT '0',
  `IsActive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of agents
-- ----------------------------

-- ----------------------------
-- Table structure for bookingattachments
-- ----------------------------
DROP TABLE IF EXISTS `bookingattachments`;
CREATE TABLE `bookingattachments` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Filename` varchar(255) NOT NULL,
  `MimeType` varchar(255) NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `BookingID` int(11) NOT NULL,
  `OriginalFilename` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `bookingattachments_ibfk_1` (`BookingID`),
  CONSTRAINT `bookingattachments_ibfk_1` FOREIGN KEY (`BookingID`) REFERENCES `bookings` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bookingattachments
-- ----------------------------

-- ----------------------------
-- Table structure for bookingdetails
-- ----------------------------
DROP TABLE IF EXISTS `bookingdetails`;
CREATE TABLE `bookingdetails` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BookingID` int(11) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `Amount` double(255,2) NOT NULL,
  `Quantity` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `BookingID` (`BookingID`),
  CONSTRAINT `bookingdetails_ibfk_1` FOREIGN KEY (`BookingID`) REFERENCES `bookings` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bookingdetails
-- ----------------------------

-- ----------------------------
-- Table structure for bookingotherchargesindicator
-- ----------------------------
DROP TABLE IF EXISTS `bookingotherchargesindicator`;
CREATE TABLE `bookingotherchargesindicator` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BookingID` int(11) NOT NULL,
  `ChargeID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `BookingID` (`BookingID`),
  KEY `ChargeID` (`ChargeID`),
  CONSTRAINT `bookingotherchargesindicator_ibfk_1` FOREIGN KEY (`BookingID`) REFERENCES `bookings` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `bookingotherchargesindicator_ibfk_2` FOREIGN KEY (`ChargeID`) REFERENCES `othercharges` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bookingotherchargesindicator
-- ----------------------------

-- ----------------------------
-- Table structure for bookings
-- ----------------------------
DROP TABLE IF EXISTS `bookings`;
CREATE TABLE `bookings` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupName` varchar(255) NOT NULL,
  `ContanctPerson` varchar(255) NOT NULL,
  `ContactNumber` varchar(255) NOT NULL,
  `Remarks` varchar(255) NOT NULL,
  `CVNumber` varchar(255) DEFAULT NULL,
  `Adults` int(11) NOT NULL DEFAULT '0',
  `Childrens` int(11) NOT NULL DEFAULT '0',
  `Infants` int(11) NOT NULL DEFAULT '0',
  `TourGuides` int(11) NOT NULL DEFAULT '0',
  `Escorts` int(11) NOT NULL DEFAULT '0',
  `FOC` int(11) NOT NULL DEFAULT '0',
  `Routing` int(11) NOT NULL DEFAULT '1',
  `PickUpArrival` int(11) NOT NULL,
  `DropOffArrival` int(11) NOT NULL,
  `DropOffLocation` int(255) DEFAULT NULL,
  `PickUpDeparture` int(11) DEFAULT NULL,
  `DrofOffDeparture` int(11) DEFAULT NULL,
  `FlightNo1` varchar(255) DEFAULT NULL,
  `FlightNo2` varchar(255) DEFAULT NULL,
  `FlightDate1` datetime DEFAULT NULL,
  `FlightDate2` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `Total` float DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `Status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CreatedBy` (`CreatedBy`),
  KEY `PickUpArrival` (`PickUpArrival`),
  KEY `DropOffArrival` (`DropOffArrival`),
  KEY `PickUpDeparture` (`PickUpDeparture`),
  KEY `DrofOffDeparture` (`DrofOffDeparture`),
  CONSTRAINT `bookings_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `agents` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `bookings_ibfk_2` FOREIGN KEY (`PickUpArrival`) REFERENCES `destinations` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `bookings_ibfk_3` FOREIGN KEY (`DropOffArrival`) REFERENCES `destinations` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `bookings_ibfk_4` FOREIGN KEY (`PickUpDeparture`) REFERENCES `destinations` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `bookings_ibfk_5` FOREIGN KEY (`DrofOffDeparture`) REFERENCES `destinations` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bookings
-- ----------------------------

-- ----------------------------
-- Table structure for bookingstatus
-- ----------------------------
DROP TABLE IF EXISTS `bookingstatus`;
CREATE TABLE `bookingstatus` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(255) NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bookingstatus
-- ----------------------------

-- ----------------------------
-- Table structure for destinations
-- ----------------------------
DROP TABLE IF EXISTS `destinations`;
CREATE TABLE `destinations` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(255) NOT NULL,
  `DateCeated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of destinations
-- ----------------------------
INSERT INTO `destinations` VALUES ('1', 'Kalibo', '2016-08-30 00:13:45', '2016-08-30 00:13:47');
INSERT INTO `destinations` VALUES ('2', 'Caticlan', '2016-08-30 00:17:15', '2016-08-30 00:17:15');
INSERT INTO `destinations` VALUES ('3', 'Kalibo International Airport', '2016-08-30 00:17:36', '2016-08-30 00:17:36');
INSERT INTO `destinations` VALUES ('4', 'Resort', '2016-08-30 00:17:39', '2016-08-30 00:17:39');
INSERT INTO `destinations` VALUES ('5', 'Cagban Port - Boracay', '2016-08-30 00:18:00', '2016-08-30 00:18:07');
INSERT INTO `destinations` VALUES ('6', 'Caticlan Airport', '2016-08-30 00:18:22', '2016-08-30 00:18:22');

-- ----------------------------
-- Table structure for othercharges
-- ----------------------------
DROP TABLE IF EXISTS `othercharges`;
CREATE TABLE `othercharges` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(255) NOT NULL,
  `Amount` double(255,2) NOT NULL DEFAULT '0.00',
  `DateCreated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IsActive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of othercharges
-- ----------------------------
INSERT INTO `othercharges` VALUES ('1', 'TF IN', '20.00', '0000-00-00 00:00:00', '2016-08-31 04:13:15', '1');
INSERT INTO `othercharges` VALUES ('2', 'TF OUT', '20.00', '0000-00-00 00:00:00', '2016-08-31 04:14:23', '1');
INSERT INTO `othercharges` VALUES ('3', 'ENVIRONMENTAL FEE', '10.00', '0000-00-00 00:00:00', '2016-08-31 04:14:39', '1');
INSERT INTO `othercharges` VALUES ('4', 'NIGHT NAVIGATION FEE', '10.00', '0000-00-00 00:00:00', '2016-08-31 04:14:51', '1');

-- ----------------------------
-- Table structure for ratematrix
-- ----------------------------
DROP TABLE IF EXISTS `ratematrix`;
CREATE TABLE `ratematrix` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PickUp` int(11) NOT NULL,
  `DropOff` int(11) NOT NULL,
  `Amount` double NOT NULL DEFAULT '0',
  `Boat` double NOT NULL DEFAULT '0',
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Status` int(11) NOT NULL,
  `Type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  KEY `PickUp` (`PickUp`),
  KEY `DropOff` (`DropOff`),
  CONSTRAINT `ratematrix_ibfk_1` FOREIGN KEY (`PickUp`) REFERENCES `destinations` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `ratematrix_ibfk_2` FOREIGN KEY (`DropOff`) REFERENCES `destinations` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ratematrix
-- ----------------------------
INSERT INTO `ratematrix` VALUES ('1', '1', '2', '500', '25', '2016-09-04 20:03:05', '2016-09-04 20:03:05', '0', '1');
INSERT INTO `ratematrix` VALUES ('2', '3', '4', '400', '0', '2016-09-04 20:03:19', '2016-09-04 20:03:19', '0', '1');
INSERT INTO `ratematrix` VALUES ('3', '4', '3', '400', '0', '2016-09-04 20:04:06', '2016-09-04 20:04:06', '0', '1');
INSERT INTO `ratematrix` VALUES ('4', '3', '5', '375', '0', '2016-09-04 20:05:59', '2016-09-04 20:05:59', '0', '1');
INSERT INTO `ratematrix` VALUES ('5', '3', '4', '260', '0', '2016-09-04 20:06:22', '2016-09-04 20:06:22', '0', '2');
INSERT INTO `ratematrix` VALUES ('6', '4', '3', '260', '0', '2016-09-04 20:06:48', '2016-09-04 20:06:48', '0', '2');
INSERT INTO `ratematrix` VALUES ('7', '3', '5', '235', '0', '2016-09-04 20:07:11', '2016-09-04 20:07:11', '0', '2');
INSERT INTO `ratematrix` VALUES ('8', '5', '3', '375', '0', '2016-09-04 20:07:33', '2016-09-04 20:07:33', '0', '1');
INSERT INTO `ratematrix` VALUES ('9', '5', '3', '235', '0', '2016-09-04 20:07:53', '2016-09-04 20:07:53', '0', '2');
INSERT INTO `ratematrix` VALUES ('10', '6', '5', '325', '0', '2016-09-04 20:08:22', '2016-09-04 20:08:22', '0', '1');
INSERT INTO `ratematrix` VALUES ('11', '6', '5', '205', '0', '2016-09-04 20:08:43', '2016-09-04 20:08:43', '0', '2');
