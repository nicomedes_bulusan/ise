ALTER TABLE `payments`
DROP COLUMN `ReceiptNo`;

ALTER TABLE `journalentries`
ADD COLUMN `ReceiptNo`  varchar(255) NULL AFTER `PaymentID`,
ADD COLUMN `ReferenceType`  int(1) NOT NULL DEFAULT 0 AFTER `ReceiptNo`;

