/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100108
Source Host           : localhost:3306
Source Database       : isebooking0911

Target Server Type    : MYSQL
Target Server Version : 100108
File Encoding         : 65001

Date: 2017-09-18 14:54:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- View structure for viewbookingsaleshandle
-- ----------------------------
DROP VIEW IF EXISTS `viewbookingsaleshandle`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `viewbookingsaleshandle` AS SELECT DISTINCT BookingID,AgentID from bookinglogs WHERE `Status`=2 GROUP BY BookingID ;

-- ----------------------------
-- Procedure structure for rptCashReceiptsORAR
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptCashReceiptsORAR`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptCashReceiptsORAR`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	

select 
tra.ID as TravelAgencyID,
tra.`Name` as TravelAgency,
pay.PaymentDate,
journal.ReceiptNo,
journal.ReferenceType,
acct.`Code` as AccountCode,
acct.Description as AccountName,
journal.Amount,
pay.Remarks,
pay.BusinessLocationCode as DivisionCode
 from payments pay
INNER JOIN journalentries journal on pay.ID = journal.PaymentID
INNER JOIN accountingterms acct on journal.AccountingCode = acct.`Code`
INNER JOIN travelagencies tra on pay.TravelAgencyID = tra.ID
WHERE (journal.ReferenceType=1 || journal.ReferenceType=3)
AND 
	fromDate <=  TruncateTime(pay.PaymentDate)
AND
toDate <=  TruncateTime(pay.PaymentDate);


END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptCashReceiptsPR
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptCashReceiptsPR`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptCashReceiptsPR`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	

select 
tra.ID as TravelAgencyID,
tra.`Name` as TravelAgency,
pay.PaymentDate,
journal.ReceiptNo,
journal.ReferenceType,
acct.`Code` as AccountCode,
acct.Description as AccountName,
journal.Amount,
pay.Remarks,
pay.BusinessLocationCode as DivisionCode
 from payments pay
INNER JOIN journalentries journal on pay.ID = journal.PaymentID
INNER JOIN accountingterms acct on journal.AccountingCode = acct.`Code`
INNER JOIN travelagencies tra on pay.TravelAgencyID = tra.ID
WHERE journal.ReferenceType=2
AND 
	fromDate <=  TruncateTime(pay.PaymentDate)
AND
toDate <=  TruncateTime(pay.PaymentDate);



END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptGetSales
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptGetSales`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptGetSales`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	#Routine body goes here...

SELECT
		(book.Adults + book.Senior) AS Adults,
	book.CreatedBy AS AgentID,
	book.FlightNo1 AS ArrivalFlight,
	book.FlightNo2 AS DepartureFlight,
	book.ID AS BookingID,
	book.CVNumber,
	book.FlightDate1 AS DateIn,
	book.FlightDate2 AS DateOut,
	book.Escorts,
	book.FOC,
	(
		book.Childrens + book.ChildrensBelowSix + book.Infants
	) AS Kids,
	book.Infants,
	book.GroupName AS PaxName,
	book.Total,
	book.TourGuides,
	TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	) AS TransactionDate,
	agent.TravelAgencyID,
	tra.`Name` AS TravelAgency
FROM
	bookings book
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
WHERE
	fromDate <= TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	)
AND toDate >= TruncateTime (
	LEAST(
		IFNULL(
			book.FlightDate1,
			book.FlightDate2
		),
		IFNULL(
			book.FlightDate2,
			book.FlightDate1
		)
	)
)
AND book.`Status` = 2
ORDER BY book.ID;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptGetSalesByAgentByMarketType
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptGetSalesByAgentByMarketType`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptGetSalesByAgentByMarketType`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	

SELECT
book.ID as BookingID,
book.CVNumber,
book.GroupName as PaxName,
tra.ID as TravelAgencyID,
tra.`Name` as TravelAgency,
agent.ID as AgentID,
CONCAT(agent.FirstName," ",IFNULL(agent.LastName,"")) as AgentName,
mkt.ID as MarketTypeID,
mkt.Description as MarketType,
0 as Transfer,
0 as BoatFee,
0 as EnvironmentFee,
0 as Others,
0 as NightNavigation,
bd.SourceID,
bd.Boat,
bd.Amount,
bd.Quantity	

FROM
	bookings book
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
INNER JOIN markettypes mkt on agent.MarketTypeID = mkt.ID
INNER JOIN bookingdetails bd on book.ID = bd.BookingID
WHERE
	fromDate <= TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	)
AND toDate >= TruncateTime (
	LEAST(
		IFNULL(
			book.FlightDate1,
			book.FlightDate2
		),
		IFNULL(
			book.FlightDate2,
			book.FlightDate1
		)
	)
)
AND book.`Status` = 2
ORDER BY book.ID;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptGetSalesByMarketTypeBreakdown
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptGetSalesByMarketTypeBreakdown`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptGetSalesByMarketTypeBreakdown`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	

SELECT
book.ID as BookingID,
book.CVNumber,
book.GroupName as PaxName,
tra.ID as TravelAgencyID,
tra.`Name` as TravelAgency,
mkt.ID as MarketTypeID,
mkt.Description as MarketType,
0 as Transfer,
0 as BoatFee,
0 as EnvironmentFee,
0 as Others,
0 as NightNavigation,
bd.SourceID,
bd.Boat,
bd.Amount,
bd.Quantity	

FROM
	bookings book
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
INNER JOIN markettypes mkt on agent.MarketTypeID = mkt.ID
INNER JOIN bookingdetails bd on book.ID = bd.BookingID
WHERE
	fromDate <= TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	)
AND toDate >= TruncateTime (
	LEAST(
		IFNULL(
			book.FlightDate1,
			book.FlightDate2
		),
		IFNULL(
			book.FlightDate2,
			book.FlightDate1
		)
	)
)
AND book.`Status` = 2
ORDER BY book.ID;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptGetSalesByMarketTypeSummary
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptGetSalesByMarketTypeSummary`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptGetSalesByMarketTypeSummary`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	

SELECT
mkt.ID as MarketTypeID,
mkt.Description as MarketType,
0 as Transfer,
0 as BoatFee,
0 as EnvironmentFee,
0 as Others,
0 as NightNavigation,
bd.SourceID,
bd.Boat,
bd.Amount,
bd.Quantity	

FROM
	bookings book
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
INNER JOIN markettypes mkt on agent.MarketTypeID = mkt.ID
INNER JOIN bookingdetails bd on book.ID = bd.BookingID
WHERE
	fromDate <= TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	)
AND toDate >= TruncateTime (
	LEAST(
		IFNULL(
			book.FlightDate1,
			book.FlightDate2
		),
		IFNULL(
			book.FlightDate2,
			book.FlightDate1
		)
	)
)
AND book.`Status` = 2
ORDER BY book.ID;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptGetSalesBySeller
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptGetSalesBySeller`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptGetSalesBySeller`(IN `fromDate` date,IN `toDate` date)
BEGIN
	

SELECT


agent.ID as AgentID,
CONCAT(agent.FirstName," ",IFNULL(agent.LastName,"")) as AgentName,
0 as Transfer,
bd.Boat,
bd.Amount,
0 as BoatFee,
0 as EnvironmentFee,
0 as TerminalFee,
0 as Others,
0 as NightNavigation,
bd.Quantity,
bd.SourceID

FROM
	bookings book
INNER JOIN viewbookingsaleshandle bsh on book.ID = bsh.BookingID
INNER JOIN agents agent ON bsh.AgentID = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
INNER JOIN markettypes mkt on agent.MarketTypeID = mkt.ID
INNER JOIN bookingdetails bd on book.ID = bd.BookingID
WHERE
	fromDate <= TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	)
AND toDate >= TruncateTime (
	LEAST(
		IFNULL(
			book.FlightDate1,
			book.FlightDate2
		),
		IFNULL(
			book.FlightDate2,
			book.FlightDate1
		)
	)
)
AND book.`Status` = 2;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptGetSalesByTravelAgency
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptGetSalesByTravelAgency`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptGetSalesByTravelAgency`(IN `fromDate` date,IN `toDate` date)
BEGIN
	

SELECT

tra.ID as AgentID,
tra.`Name` as AgentName,
0 as Transfer,
bd.Boat,
bd.Amount,
0 as BoatFee,
0 as EnvironmentFee,
0 as TerminalFee,
0 as Others,
0 as NightNavigation,
bd.Quantity,
bd.SourceID

FROM
	bookings book
INNER JOIN agents agent ON book.CreatedBy = agent.ID
INNER JOIN travelagencies tra ON agent.TravelAgencyID = tra.ID
INNER JOIN markettypes mkt on agent.MarketTypeID = mkt.ID
INNER JOIN bookingdetails bd on book.ID = bd.BookingID
WHERE
	fromDate <= TruncateTime (
		LEAST(
			IFNULL(
				book.FlightDate1,
				book.FlightDate2
			),
			IFNULL(
				book.FlightDate2,
				book.FlightDate1
			)
		)
	)
AND toDate >= TruncateTime (
	LEAST(
		IFNULL(
			book.FlightDate1,
			book.FlightDate2
		),
		IFNULL(
			book.FlightDate2,
			book.FlightDate1
		)
	)
)
AND book.`Status` = 2
ORDER BY book.ID;


END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for rptTrialBalance
-- ----------------------------
DROP PROCEDURE IF EXISTS `rptTrialBalance`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `rptTrialBalance`(IN `fromDate` date,IN `toDate` date)
    READS SQL DATA
BEGIN
	

SELECT 
PaymentDate,
PaymentNo,
tra.`Name` as TravelAgency,
payment.CheckDate,
payment.CheckNo,
payment.Remarks,
journal.CVNumber,
journal.ReceiptNo,
journal.AccountName,
journal.Amount,
journal.Type
 from payments payment
INNER JOIN journalentries journal on payment.ID = journal.PaymentID
INNER JOIN travelagencies tra on payment.TravelAgencyID = tra.ID
WHERE
	fromDate <= TruncateTime (
		PaymentDate
	)
AND toDate >= TruncateTime (
	PaymentDate
);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for UpdateBatch
-- ----------------------------
DROP PROCEDURE IF EXISTS `UpdateBatch`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateBatch`(IN `BatchID` INT)
    NO SQL
Update batch set Processed = Processed + 1 where ID = BatchID ;
;;
DELIMITER ;
