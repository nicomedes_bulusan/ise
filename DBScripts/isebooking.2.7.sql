ALTER TABLE `ratematrix`
ADD COLUMN `Class`  int NOT NULL DEFAULT 1 AFTER `ToDate`;

ALTER TABLE `ratematrixagents`
ADD COLUMN `Class`  int NOT NULL DEFAULT 1 AFTER `AgentID`;

ALTER TABLE `bookings`
ADD COLUMN `Class`  int NOT NULL DEFAULT 1 AFTER `Senior`;
