CREATE TABLE `statementofaccounts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BookingID` int(11) NOT NULL,
  `DateOfIssue` datetime NOT NULL,
  `IssuedBy` varchar(255) NOT NULL,
  `AuthorizedBy` varchar(255) NOT NULL,
  `IssuerDesignation` varchar(255) DEFAULT NULL,
  `AuthorizerDesignation` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `BookingID` (`BookingID`),
  CONSTRAINT `statementofaccounts_ibfk_1` FOREIGN KEY (`BookingID`) REFERENCES `bookings` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `servicevouchers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BookingID` int(11) NOT NULL,
  `DateOfIssue` datetime NOT NULL,
  `IssuedBy` varchar(255) NOT NULL,
  `AuthorizedBy` varchar(255) NOT NULL,
  `IssuerDesignation` varchar(255) DEFAULT NULL,
  `AuthorizerDesignation` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `BookingID` (`BookingID`),
  CONSTRAINT `servicevouchers_ibfk_1` FOREIGN KEY (`BookingID`) REFERENCES `bookings` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;



SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for systemsignatories
-- ----------------------------
DROP TABLE IF EXISTS `systemsignatories`;
CREATE TABLE `systemsignatories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BusinessLocationCode` varchar(255) NOT NULL,
  `AgentID` int(11) NOT NULL,
  `Designation` varchar(255) NOT NULL,
  `ImmediateHeadID` int(11) NOT NULL,
  `FormType` varchar(255) NOT NULL,
  `DateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateLastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ImmediateHeadDesignation` varchar(255) NOT NULL,
  `IsActive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`ID`),
  KEY `BusinessLocationCode` (`BusinessLocationCode`),
  KEY `AgentID` (`AgentID`),
  KEY `ImmediateHeadID` (`ImmediateHeadID`),
  CONSTRAINT `systemsignatories_ibfk_1` FOREIGN KEY (`BusinessLocationCode`) REFERENCES `businesslocations` (`Code`) ON UPDATE CASCADE,
  CONSTRAINT `systemsignatories_ibfk_2` FOREIGN KEY (`AgentID`) REFERENCES `agents` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `systemsignatories_ibfk_3` FOREIGN KEY (`ImmediateHeadID`) REFERENCES `agents` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of systemsignatories
-- ----------------------------
INSERT INTO `systemsignatories` VALUES ('1', 'MNL', '9', 'SALES MANAGER', '9', 'SV', '2017-07-11 08:24:44', '2017-07-11 08:25:11', 'SALES MANAGER', '');
INSERT INTO `systemsignatories` VALUES ('2', 'MNL', '10', 'SENIOR ACCOUNT OFFICER', '9', 'SV', '2017-07-11 08:24:44', '2017-07-11 08:27:08', 'SALES MANAGER', '');
INSERT INTO `systemsignatories` VALUES ('3', 'MNL', '11', 'SALES ASSOCIATE', '9', 'SV', '2017-07-11 08:24:44', '2017-07-11 08:27:23', 'SALES MANAGER', '');
INSERT INTO `systemsignatories` VALUES ('4', 'MNL', '13', 'SALES ASSOCIATE', '9', 'SV', '2017-07-11 08:24:44', '2017-07-11 08:26:49', 'SALES MANAGER', '');
INSERT INTO `systemsignatories` VALUES ('5', 'MNL', '14', 'ASST. SALES', '9', 'SV', '2017-07-11 08:24:44', '2017-07-11 08:26:28', 'SALES MANAGER', '');
INSERT INTO `systemsignatories` VALUES ('6', 'MNL', '9', 'SALES MANAGER', '9', 'SOA', '2017-07-11 08:24:44', '2017-07-11 08:25:11', 'SALES MANAGER', '');
INSERT INTO `systemsignatories` VALUES ('7', 'MNL', '10', 'SENIOR ACCOUNT OFFICER', '9', 'SOA', '2017-07-11 08:24:44', '2017-07-11 08:27:08', 'SALES MANAGER', '');
INSERT INTO `systemsignatories` VALUES ('8', 'MNL', '11', 'SALES ASSOCIATE', '9', 'SOA', '2017-07-11 08:24:44', '2017-07-11 08:27:23', 'SALES MANAGER', '');
INSERT INTO `systemsignatories` VALUES ('9', 'MNL', '13', 'SALES ASSOCIATE', '9', 'SOA', '2017-07-11 08:24:44', '2017-07-11 08:26:49', 'SALES MANAGER', '');
INSERT INTO `systemsignatories` VALUES ('10', 'MNL', '14', 'ASST. SALES', '9', 'SOA', '2017-07-11 08:24:44', '2017-07-11 08:26:28', 'SALES MANAGER', '');
INSERT INTO `systemsignatories` VALUES ('11', 'BOR', '15', 'ISE SUPERVISOR', '15', 'SOA', '2017-07-11 10:58:00', '2017-07-11 10:58:00', 'ISE SUPERVISOR', '');
INSERT INTO `systemsignatories` VALUES ('12', 'BOR', '17', 'ISE SUPERVISOR', '15', 'SOA', '2017-07-11 10:58:25', '2017-07-11 10:58:25', 'ISE SUPERVISOR', '');
INSERT INTO `systemsignatories` VALUES ('13', 'BOR', '15', 'ISE SUPERVISOR', '15', 'SV', '2017-07-11 10:58:00', '2017-07-11 10:58:00', 'ISE SUPERVISOR', '');
INSERT INTO `systemsignatories` VALUES ('14', 'BOR', '17', 'ISE SUPERVISOR', '15', 'SV', '2017-07-11 10:58:25', '2017-07-11 10:58:25', 'ISE SUPERVISOR', '');