/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 100108
Source Host           : localhost:3306
Source Database       : isemobile

Target Server Type    : MYSQL
Target Server Version : 100108
File Encoding         : 65001

Date: 2018-11-06 09:25:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth_code
-- ----------------------------
DROP TABLE IF EXISTS `auth_code`;
CREATE TABLE `auth_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(10) NOT NULL,
  `book_id` varchar(10) NOT NULL,
  `auth_code` varchar(10) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17984 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(100) DEFAULT NULL,
  `cv_number` varchar(100) DEFAULT NULL,
  `account_name` varchar(100) DEFAULT NULL,
  `vehicle` int(10) NOT NULL,
  `book_a` varchar(100) DEFAULT NULL,
  `book_k` varchar(100) DEFAULT NULL,
  `book_foc` varchar(100) DEFAULT NULL,
  `book_inf` varchar(100) DEFAULT NULL,
  `book_tg` varchar(100) DEFAULT NULL,
  `book_e` varchar(100) DEFAULT NULL,
  `book_arrival` varchar(100) DEFAULT NULL,
  `book_time` varchar(100) DEFAULT NULL,
  `actual_a` varchar(100) DEFAULT NULL,
  `actual_k` varchar(100) DEFAULT NULL,
  `actual_foc` varchar(100) DEFAULT NULL,
  `actual_inf` varchar(100) DEFAULT NULL,
  `actual_tg` varchar(100) DEFAULT NULL,
  `actual_e` varchar(100) DEFAULT NULL,
  `actual_arrival` varchar(100) DEFAULT NULL,
  `actual_time` varchar(100) DEFAULT NULL,
  `no_adults` int(11) NOT NULL,
  `resort_hotel` varchar(100) DEFAULT NULL,
  `unit` varchar(100) DEFAULT NULL,
  `unit_atd` varchar(100) DEFAULT NULL,
  `driver` varchar(100) DEFAULT NULL,
  `coordinator` varchar(100) DEFAULT NULL,
  `sales_handle` varchar(100) DEFAULT NULL,
  `agency` varchar(100) DEFAULT NULL,
  `remarks` varchar(100) DEFAULT NULL,
  `user` int(10) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `LastUpdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `flight_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `cv_number` (`cv_number`,`type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3772 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for syncstate
-- ----------------------------
DROP TABLE IF EXISTS `syncstate`;
CREATE TABLE `syncstate` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LastSync` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `IsSuccess` bit(1) NOT NULL DEFAULT b'0',
  `Type` int(11) NOT NULL DEFAULT '1' COMMENT '1=WEB2MOBILE; 2=MOBILE2WEB',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `ID` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `complete_name` varchar(300) NOT NULL,
  `status` smallint(1) NOT NULL,
  `last_sync` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for vehicle
-- ----------------------------
DROP TABLE IF EXISTS `vehicle`;
CREATE TABLE `vehicle` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
